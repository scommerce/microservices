﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Upload.MicroServices
{
    public partial class UpdateData2020816 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseLanguage",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    Lang = table.Column<string>(nullable: true),
                    OrigianlText = table.Column<string>(nullable: true),
                    ToText = table.Column<string>(nullable: true),
                    Error = table.Column<bool>(nullable: false),
                    BaseApiReturnId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseLanguage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseApiReturns_BaseApiReturnId",
                        column: x => x.BaseApiReturnId,
                        principalTable: "BaseApiReturns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BaseProperties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Property = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    PropetyType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_CreateUserId",
                table: "BaseLanguage",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_UpdateUserId",
                table: "BaseLanguage",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CreateUserId",
                table: "BaseProperties",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CustomerId",
                table: "BaseProperties",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_UpdateUserId",
                table: "BaseProperties",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaseLanguage");

            migrationBuilder.DropTable(
                name: "BaseProperties");
        }
    }
}
