﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Upload.Entity
{
    public class MediaFile:BaseEntityCusCode
    {
        public string FileName { get; set; }
        public string NewFileName { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }

    }
}
