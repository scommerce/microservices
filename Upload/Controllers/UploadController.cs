﻿using Framework.BaseAttrib;
using Framework.Startup;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Upload.DbContext;
using Upload.Entity;


namespace Upload.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UploadController : BaseController<DataContext>
    {
        public UploadController(IHostingEnvironment hostingEnvironment)
        {
            env = hostingEnvironment;
        }
        public readonly IHostingEnvironment env;

        [HttpGet]
        public IActionResult GetImage(string CusCode,long id)
        {
            var m =GetRepo().GetData<MediaFile>(n => n.Customer.CustomerCode == CusCode && n.Id == id && n.DeleteFlag == false);
            if (m == null)
                return Problem("Image Not Found");

            var filePath = env.WebRootFileProvider.GetFileInfo(m.Path)?.PhysicalPath;

            byte[] b = System.IO.File.ReadAllBytes(filePath);   // You can use your own method over here.         
            return File(b, "image/"+m.Type);
        }


        [HttpGet]
        public IActionResult GetFile(string CusCode, long id)
        {
            var m = GetRepo().GetData<MediaFile>(n => n.Customer.CustomerCode == CusCode && n.Id == id && n.DeleteFlag == false);
            if (m == null)
                return Problem("Image Not Found");

            var filePath = env.WebRootFileProvider.GetFileInfo(m.Path)?.PhysicalPath;

            byte[] b = System.IO.File.ReadAllBytes(filePath);   // You can use your own method over here.         
            return File(b, "application/octet-stream",m.FileName);
        }
        [HttpGet]
        public IActionResult GetVideo(string CusCode, long id)
        {
            var m = GetRepo().GetData<MediaFile>(n => n.Customer.CustomerCode == CusCode && n.Id == id && n.DeleteFlag == false);
            if (m == null)
                return Problem("Image Not Found");

            var filePath = env.WebRootFileProvider.GetFileInfo(m.Path)?.PhysicalPath;

            //byte[] b = System.IO.File.ReadAllBytes(filePath);   // You can use your own method over here.         
                                                                // return File(b, "video/mp4", m.FileName);
         
            return PhysicalFile(filePath, "application/octet-stream", enableRangeProcessing: true);

        }


        [HttpGet]
        public IActionResult GetImageThumbnail(string CusCode, long id,int width=100,int height=100)
        {
            var m = GetRepo().GetData<MediaFile>(n => n.Customer.CustomerCode == CusCode && n.Id == id && n.DeleteFlag == false);
            if (m == null)
                return Problem("Image Not Found");

         
            var path = "Upload/" + CusCode + "_Thumbnail/" + m.NewFileName.Replace("."+m.Type,"")+"_W_"+width.ToString()+"_H_"+height;
            var dir = env.WebRootFileProvider.GetFileInfo("Upload/" + CusCode + "_Thumbnail/")?.PhysicalPath;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            var filePath = env.WebRootFileProvider.GetFileInfo(path + "_thumbnail." + m.Type)?.PhysicalPath;
            if (!Directory.Exists(filePath))
            {
                var stream1 = env.WebRootFileProvider.GetFileInfo(m.Path)?.CreateReadStream();
                //var s = System.Drawing.Image.FromFile(env.WebRootFileProvider.GetFileInfo(m.Path).PhysicalPath);
                SixLabors.ImageSharp.Image newImage = GetReducedImage(width, height, stream1);
                newImage.Save(filePath);
            }

            byte[] b = System.IO.File.ReadAllBytes(filePath);   // You can use your own method over here.         
            return File(b, "image/" + m.Type);

        }

        [HttpPut]
        [NotDataMap]
        public async Task<IActionResult> Image(IFormFile file,[FromQuery] string CusCode)
        {
            if (file.Length > 0)
            {

                var baseCusCode = await GetRepo().RegisterCustomer(CusCode);

                var name = file.FileName.Split(".");
                var newName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                var path = "Upload/" + CusCode + "/" + newName;
                var filePath = env.WebRootFileProvider.GetFileInfo(path)?.PhysicalPath;
                var dir = env.WebRootFileProvider.GetFileInfo("Upload/" + CusCode )?.PhysicalPath;
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                using (var stream = System.IO.File.Create(filePath + "." + name[1]))
                {
                    await file.CopyToAsync(stream);
                }
                Stream stream1 = file.OpenReadStream();

              
                var ImagePath = "/" + path + "." + name[1];

                MediaFile mediaFile = new MediaFile();
                mediaFile.Customer = baseCusCode;
                mediaFile.FileName = file.FileName;
                mediaFile.NewFileName = newName +"."+ name[1];
                mediaFile.Path = ImagePath;
                mediaFile.Type = name[1];

                GetRepo().Add(mediaFile);
                GetRepo().SaveChange();
                return new ObjectResult(new
                {
                    imageId = mediaFile.Id
                }) ;

            }

            return Problem("Upload Error");
        }

        [HttpPost]
        [NotDataMap]
        public async Task<string> ImageWebBuilder(IFormFile file, [FromQuery] string CusCode)
        {
            if (file.Length > 0)
            {

                var baseCusCode = await GetRepo().RegisterCustomer(CusCode);

                var name = file.FileName.Split(".");
                var newName = DateTime.Now.ToString("ddMMyyyyHHmmssffff");
                var path = "Upload/" + CusCode + "/" + newName;
                var filePath = env.WebRootFileProvider.GetFileInfo(path)?.PhysicalPath;
                var dir = env.WebRootFileProvider.GetFileInfo("Upload/" + CusCode)?.PhysicalPath;
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                using (var stream = System.IO.File.Create(filePath + "." + name[1]))
                {
                    await file.CopyToAsync(stream);
                }
                Stream stream1 = file.OpenReadStream();


                var ImagePath = "/" + path + "." + name[1];

                MediaFile mediaFile = new MediaFile();
                mediaFile.Customer = baseCusCode;
                mediaFile.FileName = file.FileName;
                mediaFile.NewFileName = newName + "." + name[1];
                mediaFile.Path = ImagePath;
                mediaFile.Type = name[1];

                GetRepo().Add(mediaFile);
                GetRepo().SaveChange();
                return @"http://203.150.107.134:9003/api/Upload/GetImage?Cuscode="+ CusCode + "&id="+ mediaFile.Id;

            }

            return "Upload Error";
        }


        public SixLabors.ImageSharp.Image GetReducedImage(int width, int height, Stream image1)
        {

            //Image image = System.Drawing.Image.FromStream(image1);

            //Image thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero);
            var image = SixLabors.ImageSharp.Image.Load(image1);
              image.Mutate(x => x.Resize(width, height));
            return image;


            //  return destImage;

        }
    }
}