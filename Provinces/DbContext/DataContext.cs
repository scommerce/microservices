﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Provinces.Entity;

namespace Provinces.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.Provinces> Provinces { get; set; }
        public DbSet<CPRProvinces> CPRProvinces { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Provinces;User Id=sa;Password=P@ssword1234;";
        }
    }
}
