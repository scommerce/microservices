﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Provinces.DbContext;
using Provinces.Models;

namespace Provinces.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProvincesController : BaseController<DataContext>
    {
        [HttpPost]
        public IActionResult AddProvinces(ProvincesModel provincesModel)
        {
            if (provincesModel == null)
            {
                return Problem("Data is null");
            }
            Entity.Provinces provinces = new Entity.Provinces();
            provinces.Name = provincesModel.Name;

            GetRepo().Add(provinces);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public  IActionResult GetProvinces(string pv)
        {
            if (pv == null)
            {
                var p = GetAll<Entity.Provinces>().ToList();
                return new ObjectResult(p);
            }
            else
            {
                var p = GetAll<Entity.Provinces>(n=>n.Name == pv && n.DeleteFlag == false).ToList();
                return new ObjectResult(p);
            }
        }
    }   
}