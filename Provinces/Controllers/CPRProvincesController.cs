﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Provinces.DbContext;
using Provinces.Entity;

namespace Provinces.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CPRProvincesController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetCPRProvinces(string pv)
        {
            if (pv == null)
            {
                var p = GetAll<CPRProvinces>().ToList();
                return new ObjectResult(p);
            }
            else
            {
                var p = GetAll<CPRProvinces>(n => n.Name == pv && n.DeleteFlag == false).ToList();
                return new ObjectResult(p);
            }
        }
    }
}