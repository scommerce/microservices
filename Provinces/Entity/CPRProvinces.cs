﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Provinces.Entity
{
    public class CPRProvinces : BaseEntityCusCode
    {
        public string Name { get; set; }
    }
}
