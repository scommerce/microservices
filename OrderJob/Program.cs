﻿using Framework.BaseFramework;
using Order.DbContext;
using Order.Entity;
using Order.Enum;
using Order.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace OrderJob
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            Console.WriteLine("StartJob");
 
            while (true)
            {
                await Shop();
                Thread.Sleep(5000);
            }
        }

        private static async Task Shop()
        {

            BaseRepository repo = new BaseRepository(new DataContext());
            var order = repo.GetDataAll<LinglomOrder>(n => n.DeleteFlag == false && n.RiderId == null && (n.OrderStatus == OrderStatus.FoodComplate)).ToList();
            Console.WriteLine("OrderList::" + order.Count());
            foreach (var item in order)
            {
                Console.WriteLine("OrderStart::"+item.OrderNo);
                try
                {
                    //Console.WriteLine(item.ShopId);
                    var microServices = repo.GetMicroServices("LinglomShop");
                    var data = await repo.sendRequest<ShopModel>(microServices.ServiceIp + "/api/LinglomShop/GetShopById?Shop=" + item.ShopId, HttpMethod.Get, null, false);

                    //var d = (data == null) ? DateTime.Now.AddMinutes(30).TimeOfDay : data.CookingTime;
                    //var cooking = item.UpdateDate.Value.Add(d);
                    //var time = cooking.TimeOfDay - DateTime.Now.TimeOfDay;

                    var riderMicroService = repo.GetMicroServices("Rider");
                    if (item.OrderStatus == OrderStatus.FoodComplate)
                    {
                        try
                        {
                            var rider = await repo.sendRequest<RiderModel>(riderMicroService.ServiceIp + "/api/Rider/GetFirstRiderFromLocation", HttpMethod.Post, new
                            {
                                Lat = data.Lat,
                                Lng = data.Lng
                            }, true);

                            item.RiderId = rider.Rider.Id;
                            item.RiderName = rider.Rider.FirstName + " " + rider.Rider.LastName;
                            item.RiderLPR = rider.Rider.CPRName;
                            item.RiderProvince = rider.Rider.CPRProvinceId;

                            repo.Update(item);
                            repo.SaveChange();

                            var riderMessageMicroService = repo.GetMicroServices("Login");
                            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode;
                            var firebase = await repo.sendRequest<Firebase>(riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode, HttpMethod.Get, null, true);
                            repo.sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
                            {

                                registration_ids = new string[] { firebase.Token },
                                notification = new
                                {
                                    title = "ลิงลม รับ Order",
                                    body = "มีรายการสั่งซื้อรหัส " + item.OrderNo + " กรุณาตรวจสอบภายรายการ",
                                    mutable_content = true,
                                    sound = "Tri-tone"
                                }
                            }, true);
                           
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    //else
                    //{
                    //    if (time.TotalMinutes < 5)
                    //    {
                    //        try
                    //        {
                    //            var rider = await repo.sendRequest<RiderModel>(riderMicroService.ServiceIp + "/api/Rider/GetFirstRiderFromLocation", HttpMethod.Post, new
                    //            {
                    //                Lat = data.Lat,
                    //                Lng = data.Lng
                    //            }, true);

                    //            item.RiderId = rider.Rider.Id;
                    //            item.RiderName = rider.Rider.FirstName + " " + rider.Rider.LastName;
                    //            item.RiderLPR = rider.Rider.CPRName;
                    //            item.RiderProvince = rider.Rider.CPRProvinceId;

                    //            repo.Update(item);
                    //            repo.SaveChange();

                    //            var riderMessageMicroService = repo.GetMicroServices("Login");
                    //            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode;
                    //            var firebase = await repo.sendRequest<Firebase>(riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode, HttpMethod.Get, null, true);
                    //            repo.sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
                    //            {

                    //                registration_ids = new string[] { firebase.Token },
                    //                notification = new
                    //                {
                    //                    title = "ลิงลม รับ Order",
                    //                    body = "มีรายการสั่งซื้อรหัส " + item.OrderNo + " กรุณาตรวจสอบภายรายการ",
                    //                    mutable_content = true,
                    //                    sound = "Tri-tone"
                    //                }
                    //            }, true);

                    //        }
                    //        catch (Exception ex)
                    //        {

                    //        }
                    //    }
                    //}
                    Console.WriteLine("OrderFinish::" + item.OrderNo);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
       
            }

        }
    }
}
