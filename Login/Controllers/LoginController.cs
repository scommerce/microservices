﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.BaseEntity;
using Framework.BaseExtjs;
using Framework.Helper;
using Framework.Startup;
using Login.DbContext;
using Login.Entity;
using Login.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Login.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LoginController : BaseController<DataContext>
    {
        [HttpGet]
        [NotDataMap]
        public IActionResult GetLoginUI(string Cuscode="",string theme= "default")
        {
            LoginModel login = new LoginModel();
            login.Image = "https://customer.wframwork.com/api/CustomerApi/GetLogoPath?Cuscode="+ Cuscode;
            login.CusCode = Cuscode;
            var a = GetUI(login, theme);


            string outputJson = JsonConvert.SerializeObject(a);
            
            return CreateExtjs("Ext.Login",outputJson); 
        }

        [HttpGet]
        [NotDataMap]
        public IActionResult GetLoginUIJs(string Cuscode = "", string theme = "default")
        {
            LoginModel login = new LoginModel();
            login.Image = "https://customer.wframwork.com/api/CustomerApi/GetLogoPath?Cuscode=" + Cuscode;
            login.CusCode = Cuscode;
            var a = GetUI(login, theme);


            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }

        [HttpPost]
        public IActionResult Login(LoginModel loginModel)
        {
            var password = Encrypt.EncryptString(GetRepo().GetSetting(loginModel.CusCode, "Encode", "User").Value, loginModel.Password);
            var data = GetRepo().GetData<User>(n => n.UserName == loginModel.UserName && n.Customer.CustomerCode == loginModel.CusCode && n.Password == password && n.DeleteFlag==false);
            if(data==null)
                return Problem("Login Fail");
            string key = Guid.NewGuid().ToString().Replace("-","");
            data.LastLogin = DateTime.Now;
            if (loginModel.CusCode == "SWANE-001")
            {
                if (data.Profile.Count() > 0)
                {
                   

                    if (data.Profile.Where(n => n.Property == "Approve" && n.Value == "true").Count() == 0)
                        return Problem("Login Fail");
                }
            }
           

            var old = GetRepo().GetDataAll<BaseApiToken>(n =>n.DeleteFlag==false && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin && n.CreateUser.UserName == loginModel.UserName && n.Customer.CustomerCode == loginModel.CusCode);
            foreach(var o in old)
            {
                o.DeleteFlag = true;
                GetRepo().Update(o);
            }
          
            GetRepo().SaveChange();

            

            BaseApiToken baseApiToken = new BaseApiToken();
            baseApiToken.Token = key;
            baseApiToken.TokenType = Framework.BaseEnum.TokenType.BaseLogin;
            baseApiToken.CreateUser = GetRepo().GetData<BaseUser>(n=>n.BaseUserId == data.Id);
            baseApiToken.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == loginModel.CusCode);
            GetRepo().Add(baseApiToken);
            GetRepo().SaveChange();
            LoginSession(key);

            return new ObjectResult(new { data=key});
          
        }


        [HttpGet]
        public IActionResult RegisterFireBase(string ApiKey,string Firebase)
        {

            var data = GetRepo().GetData<BaseApiToken>(n => n.Token == ApiKey && n.DeleteFlag == false);
            var old = GetRepo().GetDataAll<BaseApiToken>(n => n.DeleteFlag == false && n.TokenType == Framework.BaseEnum.TokenType.Firebase && n.CreateUser.UserName == data.CreateUser.UserName && n.Customer.CustomerCode == data.CreateUser.Customer.CustomerCode);
            if (old != null) {
                foreach (var o in old)
                {
                    o.DeleteFlag = true;
                    GetRepo().Update(o);
                }
                GetRepo().SaveChange();
            }
            BaseApiToken baseApiToken = new BaseApiToken();
            baseApiToken.Token = Firebase;
            baseApiToken.TokenType = Framework.BaseEnum.TokenType.Firebase;
            baseApiToken.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == data.CreateUser.UserName && n.Customer.CustomerCode == data.CreateUser.Customer.CustomerCode);
            baseApiToken.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == data.CreateUser.Customer.CustomerCode);
            GetRepo().Add(baseApiToken);
            GetRepo().SaveChange();

            return Ok();

        }

        [HttpGet]
        public IActionResult CheckTokenByUserName(string UserName,string CusCode)
        {
            var data = GetRepo().GetData<BaseApiToken>(n => n.CreateUser.UserName == UserName && n.Customer.CustomerCode == CusCode&& n.TokenType == Framework.BaseEnum.TokenType.BaseLogin && n.DeleteFlag == false);
            if (data == null)
            {
                return Problem("Token not found");
            }
            return new ObjectResult(new
            {
                Token = data.Token,
            });

        }

        [HttpPost]
        public IActionResult CheckToken(ApiModel apiModel)
        {
           var data= GetRepo().GetData<BaseApiToken>(n => n.Token == apiModel.ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin && n.DeleteFlag == false);
            if (data == null)
            {
                return Problem("Token not found");
            }
            return new ObjectResult(new { 
                UserId = data.CreateUser.BaseUserId,
                UserName = data.CreateUser.UserName,
                Cuscode = data.Customer.CustomerCode,
                CuscodeId = data.Customer.BaseCustomerId
            });

        }

        [HttpPost]
        public IActionResult GetTokenFireBase(ApiModel apiModel)
        {
            var u = GetRepo().GetData<BaseApiToken>(n => n.Token == apiModel.ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin && n.DeleteFlag == false);
            var data = GetRepo().GetData<BaseApiToken>(n => n.CreateUser == u.CreateUser && n.TokenType == Framework.BaseEnum.TokenType.Firebase && n.DeleteFlag == false);

            return new ObjectResult(new
            {
                Token = data.Token
            }) ;

        }

        [HttpGet]
        public IActionResult GetTokenFireBaseFromUser(string UserName,string Cuscode)
        {

           
            var data = GetRepo().GetData<BaseApiToken>(n => n.CreateUser.UserName == UserName && n.CreateUser.Customer.CustomerCode == Cuscode && n.TokenType == Framework.BaseEnum.TokenType.Firebase && n.DeleteFlag == false);

            return new ObjectResult(new
            {
                Token = data.Token
            });

        }
        [HttpGet]
        public IActionResult GetTokenFireBaseFromUserId(long userId)
        {


            var data = GetRepo().GetData<BaseApiToken>(n => n.CreateUser.BaseUserId == userId  && n.TokenType == Framework.BaseEnum.TokenType.Firebase && n.DeleteFlag == false);

            return new ObjectResult(new
            {
                Token = data.Token
            });

        }

        [HttpGet]
        [NotDataMap]
        public IActionResult ViewPassword(long UserId,string Cuscode)
        {
            var data = GetRepo().GetData<User>(n => n.Id == UserId);

            return new ObjectResult(new
            {
                Token = Decrypt.DecryptString(GetRepo().GetSetting(Cuscode, "Encode", "User").Value, data.Password)
            });

        }
        [HttpPost]
        public async Task<IActionResult> RegisterWithFireBase(RegisterFbTokenModel registerFbTokenModel)
        {
            HttpResponseMessage account;
            try
            {
                account = await GetRepo().sendRequest("https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyBXfEJ9ktrN5rXPsUF3G0cLDy2kKmXtFug", HttpMethod.Post, new { idToken = registerFbTokenModel.idToken }, true);

            }catch(Exception ex)
            {

                return Problem("Can't get account data");
            }

            var response = JsonConvert.DeserializeObject<ReturnAccountModel>(await account.Content.ReadAsStringAsync());
            
            var token = Guid.NewGuid().ToString().Replace("-", "");

            var userdata = GetRepo().GetData<User>(n=>n.UserName == response.users.FirstOrDefault().localId && n.DeleteFlag == false);

            BaseApiToken baseApiToken = new BaseApiToken();
           
            if (userdata == null)
            {
                User user = new User();
                user.Email = response.users.FirstOrDefault().email;
                user.UserName = response.users.FirstOrDefault().localId;
                user.Password = (response.users.FirstOrDefault().passwordHash == null)? response.users.FirstOrDefault().providerUserInfo.FirstOrDefault().rawId : response.users.FirstOrDefault().passwordHash ;
                user.Customer = await GetRepo().GetCustomer(registerFbTokenModel.cuscode);
                //user.UserStatus = UserStatus.Active;
                GetRepo().Add(user);
                GetRepo().SaveChange();

                BaseUser baseUser = new BaseUser();
                baseUser.BaseUserId = user.Id;
                baseUser.UserName = user.UserName;
                baseUser.Customer = await GetRepo().GetCustomer(registerFbTokenModel.cuscode);

                GetRepo().Add(baseUser);
                GetRepo().SaveChange();

            }
            var baseuser1 = GetRepo().GetData<BaseUser>(n => n.UserName == response.users.FirstOrDefault().localId && n.DeleteFlag == false);

            var bu = GetAll<BaseApiToken>(n => n.CreateUser.Id == baseuser1.Id && n.DeleteFlag == false).ToList();
            foreach(var loop in bu)
            {
                loop.DeleteFlag = true;
                GetRepo().SaveChange();
            }
                baseApiToken.Token = token;
                baseApiToken.TokenType = Framework.BaseEnum.TokenType.BaseLogin;
                baseApiToken.CreateUser = baseuser1;
                baseApiToken.Customer = baseuser1.Customer;
                baseApiToken.DeleteFlag = false;
                GetRepo().Add(baseApiToken);
                GetRepo().SaveChange();

            return new ObjectResult(new
            {
                Token = token
            });
        }
    }
}