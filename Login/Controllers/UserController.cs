﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Helper;
using Framework.Startup;
using Login.DbContext;
using Login.Entity;
using Login.Model;
using Login.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Login.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : BaseController<DataContext>
    {
        [HttpGet]
        [NotDataMap]
        public IActionResult GetUserUI(string theme = "default")
        {
            User user = new User();

            var a = GetUI(user, theme);

            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }

        [HttpGet]
    
        public IActionResult GetUserAll(string ApiKey, int page=0,int start=0,int limit=0,string sort=null,string filter=null, string theme = "default")
        {
            if (ApiKey == null)
                return Problem("Please Login");

            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey);
            if (a == null)
                return Problem("Please Login");

            
        
            BaseDataTable baseTable = new BaseDataTable();
            baseTable.Start = start;
            baseTable.Length = limit;
            if (sort != null)
            {
                var res = JsonConvert.DeserializeObject<List<BaseSortDatatable>>(sort);
                baseTable.Sort = res;
            }
            if (filter != null)
            {
                var res = JsonConvert.DeserializeObject<List<BaseFilterDatatable>>(filter);
                baseTable.Filter = res;
            }

            var v1 = GetRepo().GetDataAll<User>(n => n.Customer == a.Customer && n.DeleteFlag == false).ToDataTable(baseTable);
            
            return new ObjectResult(v1);
        }


        [HttpPost]
        public  async Task<IActionResult> AddUser(UserModel userModel)
        {
            
            if (userModel.Password != userModel.ConfirmPassword)
            {
                return BadRequest("Password Not Match");
            }

            if(GetRepo().GetData<User>(n=>n.UserName==userModel.UserName && n.Customer.CustomerCode == userModel.CusCode && n.DeleteFlag==false) != null)
            {
                return Problem("Data is Duplicate");
            }
            if(GetRepo().GetSetting(userModel.CusCode, "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }

            User user = new User();
            user.UserName = userModel.UserName;
            user.Password = Encrypt.EncryptString(GetRepo().GetSetting(userModel.CusCode, "Encode", "User").Value, userModel.Password);
            user.Customer = await GetRepo().GetCustomer(userModel.CusCode);

      

            GetRepo().Add(user);

          

            GetRepo().SaveChange();

            BaseUser baseUser = new BaseUser();
            baseUser.BaseUserId = user.Id;
            baseUser.UserName = userModel.UserName;
            baseUser.Customer = await GetRepo().GetCustomer(userModel.CusCode);

            GetRepo().Add(baseUser);
            GetRepo().SaveChange();

            string key = Guid.NewGuid().ToString().Replace("-", "");

            BaseApiToken baseApiToken = new BaseApiToken();
            baseApiToken.Token = key;
            baseApiToken.TokenType = Framework.BaseEnum.TokenType.BaseLogin;
            baseApiToken.CreateUser = baseUser;
            baseApiToken.Customer = await GetRepo().GetCustomer(userModel.CusCode); 
            GetRepo().Add(baseApiToken);
            GetRepo().SaveChange();


            return new ObjectResult(new { data = key });
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangPasswordModel userModel)
        {

            if (userModel.Password != userModel.ConfirmPassword)
            {
                return BadRequest("Password Not Match");
            }


            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == userModel.ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.UserName == a.CreateUser.UserName && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);

            if (GetRepo().GetSetting(a.CreateUser.Customer.CustomerCode, "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }



            u.Password = Encrypt.EncryptString(GetRepo().GetSetting(a.CreateUser.Customer.CustomerCode, "Encode", "User").Value, userModel.Password);




            GetRepo().Update(u);



            GetRepo().SaveChange();



            return Ok();
        }

        [HttpGet]
        public IActionResult ChangePasswordBySwane(long userId,string NewPassword)
        {

          

            

            var u = GetRepo().GetData<User>(n => n.Id == userId && n.CustomerId==7);
            if (u == null)
            {
                return Problem("User Not Found");
            }

            if (GetRepo().GetSetting(u.Customer.CustomerCode, "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }



            u.Password = Encrypt.EncryptString(GetRepo().GetSetting(u.Customer.CustomerCode, "Encode", "User").Value,NewPassword);




            GetRepo().Update(u);



            GetRepo().SaveChange();



            return Ok();
        }

        [HttpPost]
        public IActionResult ChangePasswordByUserName(ChangePasswordShopModel userModel)
        {

            if (userModel.Password != userModel.ConfirmPassword)
            {
                return BadRequest("Password Not Match");
            }
            var user = GetRepo().GetData<User>(n => n.DeleteFlag == false && n.UserName == userModel.UserName);


            var u = GetRepo().GetData<User>(n => n.UserName == user.CreateUser.UserName && n.Customer.CustomerCode == user.CreateUser.Customer.CustomerCode);

            if (GetRepo().GetSetting(user.CreateUser.Customer.CustomerCode, "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }

            u.Password = Encrypt.EncryptString(GetRepo().GetSetting(user.CreateUser.Customer.CustomerCode, "Encode", "User").Value, userModel.Password);

            GetRepo().Update(u);
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public IActionResult ChangePasswordByIclass(ChangePasswordShopModel userModel)
        {

            if (userModel.Password != userModel.ConfirmPassword)
            {
                return BadRequest("Password Not Match");
            }
            var user = GetRepo().GetData<User>(n => n.DeleteFlag == false && n.UserName == userModel.UserName);


            var u = GetRepo().GetData<User>(n => n.UserName == user.UserName && n.Customer.CustomerCode == "ICS-001");

            if (GetRepo().GetSetting("ICS-001", "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }

            u.Password = Encrypt.EncryptString(GetRepo().GetSetting("ICS-001", "Encode", "User").Value, userModel.Password);

            GetRepo().Update(u);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public IActionResult GetUserForCus(string ApiKey, string Cuscode)
        {
            //var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            //if (a == null)
            //    return Problem("User is not Login");

            var data = GetRepo().GetDataAll<User>(n => n.Customer.CustomerCode == Cuscode).ToList();
            return new ObjectResult(data);
        }
        [HttpGet]
        public IActionResult UpdateProfile(string ApiKey,string Property,string Value,PropertyType PropertyType)
        {

            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.UserName == a.CreateUser.UserName && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);
            var p = u.Profile.Where(n => n.Property == Property && n.DeleteFlag == false).FirstOrDefault();
            if (p == null)
            {
                p = new ProfileUser();
                u.Profile.Add(p);
                p.CreateDate = DateTime.Now;
            }
            p.Property = Property;
            p.Value = Value;
            p.PropetyType = PropertyType;
          
            GetRepo().Update(u);
            GetRepo().SaveChange();
            return Ok();

        }

        [HttpGet]
        public IActionResult UpdateProfileSub(string ApiKey,long UserId, string Property, string Value, PropertyType PropertyType)
        {
            Console.WriteLine("Data::"+ ApiKey);
            Console.WriteLine("UserId::" + UserId);
            Console.WriteLine("Property::" + Property);
            Console.WriteLine("Value::" + Value);
            Console.WriteLine("PropertyType::" + PropertyType);

            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.UserName == "S"+UserId );
            if(u.Profile == null)
            {
                u.Profile = new List<ProfileUser>();
            }
            var p = u.Profile.Where(n => n.Property == Property && n.DeleteFlag == false).FirstOrDefault();
            if (p == null)
            {
                p = new ProfileUser();
                u.Profile.Add(p);
                p.CreateDate = DateTime.Now;
            }
            p.Property = Property;
            p.Value = Value;
            p.PropetyType = PropertyType;

            GetRepo().Update(u);
            GetRepo().SaveChange();
            return Ok();

        }

        [HttpPost]
        public IActionResult UpdateProfiles(InsertProfile insertProfile)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == insertProfile.ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.Id == a.CreateUser.BaseUserId && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);
            if (u.Profile == null)
                u.Profile = new List<ProfileUser>();
            foreach (var ip in u.Profile)
            {
                ip.DeleteFlag = true;
                GetRepo().Update(u);
            }
            foreach (var ip in insertProfile.ProfileProperty)
            {

                var p = u.Profile.Where(n => n.Property == ip.Property).FirstOrDefault();
                if (p == null)
                {
                    p = new ProfileUser();
                    u.Profile.Add(p);
                    p.CreateDate = DateTime.Now;
                }
                p.DeleteFlag = false;
                p.Property = ip.Property;
                p.Value = ip.Value;
                p.PropetyType = ip.PropertyType;

                GetRepo().Update(u);
            }
            GetRepo().SaveChange();
            return Ok();

        }

        [HttpPost]
        public IActionResult UpdateProfilesForSwane(InsertProfile insertProfile)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.CreateUser.BaseUserId == insertProfile.UserId && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.Id == a.CreateUser.BaseUserId && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);
            if (u.Profile == null)
                u.Profile = new List<ProfileUser>();
            foreach (var ip in u.Profile)
            {
                ip.DeleteFlag = true;
                GetRepo().Update(u);
            }
            foreach (var ip in insertProfile.ProfileProperty)
            {

                var p = u.Profile.Where(n => n.Property == ip.Property).FirstOrDefault();
                if (p == null)
                {
                    p = new ProfileUser();
                    u.Profile.Add(p);
                    p.CreateDate = DateTime.Now;
                }
                p.DeleteFlag = false;
                p.Property = ip.Property;
                p.Value = ip.Value;
                p.PropetyType = ip.PropertyType;

                GetRepo().Update(u);
            }
            GetRepo().SaveChange();
            return Ok();

        }

        [HttpGet]
        [NotDataMap]
        public IActionResult GetProfile(String ApiKey)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            var u = GetRepo().GetData<User>(n => n.UserName == a.CreateUser.UserName && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);
            ExpandoObject v = new ExpandoObject();
            if (u.Customer.CustomerCode == "SWANE-001")
            {
                v.TryAdd("UserId", u.UserName.Replace("S", ""));
            }
            else
            {
                v.TryAdd("UserId", u.Id);
            }
            foreach (var p in u.Profile.Where(n=>n.DeleteFlag==false))
            {
                if (p.Value == null)
                    v.TryAdd(p.Property, null);
                else
                {
                    switch (p.PropetyType)
                    {
                        case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse(p.Value)); break;
                        case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                        case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                        case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                        case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                        case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                        case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                        default: v.TryAdd(p.Property, p.Value); break;
                    }
                }
            
            }
            return new ObjectResult(v);
        }

        [HttpGet]
        public IActionResult GetFromUserName(String Username,String Cuscode)
        {
            

            var u = GetRepo().GetData<User>(n => n.DeleteFlag==false && n.UserName == Username && n.Customer.CustomerCode == Cuscode);
            if (u == null)
                return Problem("NoUser");
            return new ObjectResult(u);
        }

        [HttpGet]
        [NotDataMap]
        public IActionResult AddParentUser(String ApiKey, int UserId)
        {


            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");

            //UserLavel l = GetRepo().GetData<UserLavel>(n => n.User.Id == UserId && n.DeleteFlag == false);

            //if (l == null)
            //{ 
            //    l = new UserLavel();

            //    GetRepo().Add(l);

            //}
            UserLavel l = l = new UserLavel();

            GetRepo().Add(l);
            l.ParentId = GetRepo().GetData<BaseUser>(n => n.BaseUserId == UserId);
            l.User = a.CreateUser;

           
           
            GetRepo().Update(l.User);
            GetRepo().Update(l);
            GetRepo().SaveChange();

            if (l.ParentId.UserName == "S1")
            {
      
                    l.User.UserName = "S" + ((GetRepo().GetDataAll<UserLavel>(n => n.ParentId.UserName == "S1" && n.DeleteFlag ==false).Count()+10)+1).ToString();
            }
            else
                l.User.UserName = l.ParentId.UserName.Substring(0, 3) + l.Id.ToString("000#");

            var u = GetRepo().GetData<User>(n => n.Id == l.User.BaseUserId);
            u.UserName = l.User.UserName;
            GetRepo().Update(u);
            GetRepo().Update(l.User);
            GetRepo().Update(l);
            GetRepo().SaveChange();

            return new ObjectResult(new { data = l.User.UserName });
        }
        [HttpGet]
        [NotDataMap]
        public IActionResult GetSubeUser(String ApiKey)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");
            var l = GetRepo().GetDataAll<UserLavel>(n => n.ParentId == a.CreateUser && n.DeleteFlag == false).ToList();
            List<ExpandoObject> user = new List<ExpandoObject>();
            foreach(var ll in l)
            {
                try { 
                var u = GetRepo().GetData<User>(n => n.Id == ll.User.BaseUserId);
                ExpandoObject v = new ExpandoObject();
                if (u.Customer.CustomerCode == "SWANE-001")
                {
                    v.TryAdd("UserId", u.UserName.Replace("S", ""));
                }
                else
                {
                    v.TryAdd("UserId", u.Id);
                }
                foreach (var p in u.Profile.Where(n => n.DeleteFlag == false))
                {
                    if (p.Value == null)
                        v.TryAdd(p.Property, null);
                    else
                    {
                        switch (p.PropetyType)
                        {
                            case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value=="0")?"false":(p.Value=="1")?"true":p.Value)); break;
                            case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                            case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                            case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                            case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                            case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                            case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                            default: v.TryAdd(p.Property, p.Value); break;
                        }
                    }


                }
                ((dynamic)v).Approve = ((dynamic)v).Approve;
                    ((dynamic)v).IdCard = ((dynamic)v).IdCard + "(" + ll.User.UserName + ")";
                user.Add(v);
                }
                catch (Exception ex)
                {

                }
            }
            return new ObjectResult(user.Where(n=>((dynamic)n).Approve == false && ((dynamic)n).ApproveDate == null));

        }

        [HttpGet]
        [NotDataMap]
        public IActionResult GetSubAprrove(String ApiKey)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");
            var l = GetRepo().GetDataAll<UserLavel>(n => n.ParentId == a.CreateUser && n.DeleteFlag == false).ToList();
            List<ExpandoObject> user = new List<ExpandoObject>();
            foreach (var ll in l)
            {
                try
                {
                    var u = GetRepo().GetData<User>(n => n.Id == ll.User.BaseUserId);
                    ExpandoObject v = new ExpandoObject();
                    if (u.Customer.CustomerCode == "SWANE-001")
                    {
                        v.TryAdd("UserId", u.UserName.Replace("S", ""));
                    }
                    else
                    {
                        v.TryAdd("UserId", u.Id);
                    }
                    foreach (var p in u.Profile.Where(n => n.DeleteFlag == false))
                    {
                        if (p.Value == null)
                            v.TryAdd(p.Property, null);
                        else
                        {
                            switch (p.PropetyType)
                            {
                                case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value == "0") ? "false" : (p.Value == "1") ? "true" : p.Value)); break;
                                case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                                case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                                case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                                case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                                case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                                case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                                default: v.TryAdd(p.Property, p.Value); break;
                            }
                        }


                    }
                ((dynamic)v).Approve = ((dynamic)v).Approve;
                    user.Add(v);
                }catch(Exception ex)
                {

                }
            }
            return new ObjectResult(user.Where(n => ((dynamic)n).Approve == true));

        }

        [HttpGet]
        [NotDataMap]
        public IActionResult GetUserInSub(String ApiKey)
        {
            var a = GetRepo().GetData<BaseApiToken>(n => n.DeleteFlag == false && n.Token == ApiKey && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin);

            if (a == null)
                return Problem("User is not Login");
            List<UserLavel> l = new List<UserLavel>();
            List<string> SwaneLavleUser = new List<string>();
            var userLavels = GetRepo().GetDataAll<UserLavel>(n => n.DeleteFlag == false).ToList();
            try
            {
                SwaneLavleUser.Add(a.CreateUser.UserName);
                foreach (var ll in userLavels.Where(n=>n.ParentId == a.CreateUser))
                {

                    l.Add(ll) ;

                    SwaneServices.addUserLevel(l, userLavels, ll.User.UserName, SwaneLavleUser);
                }
            }catch( Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            List<ExpandoObject> user = new List<ExpandoObject>();
            foreach (var ll in l)
            {
                try
                {
                    var u = GetRepo().GetData<User>(n => n.Id == ll.User.BaseUserId);
                    ExpandoObject v = new ExpandoObject();

                    if (u.Customer.CustomerCode == "SWANE-001")
                    {
                        v.TryAdd("UserId", u.UserName.Replace("S", ""));
                    }
                    else
                    {
                        v.TryAdd("UserId", u.Id);
                    }


                    foreach (var p in u.Profile.Where(n => n.DeleteFlag == false))
                    {
                        if (p.Value == null)
                            v.TryAdd(p.Property, null);
                        else
                        {
                            switch (p.PropetyType)
                            {
                                case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value == "0") ? "false" : (p.Value == "1") ? "true" : p.Value)); break;
                                case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                                case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                                case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                                case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                                case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                                case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                                default: v.TryAdd(p.Property, p.Value); break;
                            }
                        }


                    }
                      ((dynamic)v).Approve = ((dynamic)v).Approve;
                 ((dynamic)v).IdCard = ((dynamic)v).IdCard + "(" + ll.User.UserName + ")";
                    user.Add(v);
                }catch(Exception ex)
                {

                }
            }
            return new ObjectResult(user.Where(n => ((dynamic)n).Approve == true).ToList());

        }
       

        

        [HttpPost]
        public async Task<IActionResult> AddSwane(UserModel userModel)
        {
            User user = new User();
            user.UserName = Guid.NewGuid().ToString().Replace("-",""); 
            if (userModel.Password != userModel.ConfirmPassword)
            {
                return BadRequest("Password Not Match");
            }

            if (GetRepo().GetData<User>(n => n.UserName == userModel.UserName && n.Customer.CustomerCode == userModel.CusCode && n.DeleteFlag == false) != null)
            {
                return Problem("Data is Duplicate");
            }
            if (GetRepo().GetSetting(userModel.CusCode, "Encode", "User") == null)
            {
                return Problem("No Data Setting");
            }

         
            
            user.Password = Encrypt.EncryptString(GetRepo().GetSetting(userModel.CusCode, "Encode", "User").Value, userModel.Password);
            user.Customer = await GetRepo().GetCustomer(userModel.CusCode);

             

            GetRepo().Add(user);



            GetRepo().SaveChange();

            BaseUser baseUser = new BaseUser();
            baseUser.BaseUserId = user.Id;
            baseUser.UserName = user.UserName;
            baseUser.Customer = await GetRepo().GetCustomer(userModel.CusCode);

            GetRepo().Add(baseUser);
            GetRepo().SaveChange();

        


            return new ObjectResult(new { data = user.UserName });
        }


        [HttpGet]
        [NotDataMap]
        public  IActionResult GetUserSwane()
        {
            var l = GetRepo().GetDataAll<UserLavel>(n =>n.DeleteFlag == false ).ToList();
            List<ExpandoObject> user = new List<ExpandoObject>();
            foreach (var ll in l)
            {
                var u = GetRepo().GetData<User>(n => n.Id == ll.User.BaseUserId);
                ExpandoObject v = new ExpandoObject();

                if (u.Customer.CustomerCode == "SWANE-001")
                {
                    v.TryAdd("UserId", u.UserName.Replace("S", ""));
                    v.TryAdd("Id", u.Id);
                }
                else
                {
                    v.TryAdd("UserId", u.Id);
                }


                foreach (var p in u.Profile.Where(n => n.DeleteFlag == false))
                {
                    if (p.Value == null)
                        v.TryAdd(p.Property, null);
                    else
                    {
                        switch (p.PropetyType)
                        {
                            case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value == "0") ? "false" : (p.Value == "1") ? "true" : p.Value)); break;
                            case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                            case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                            case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                            case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                            case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                            case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                            default: v.TryAdd(p.Property, p.Value); break;
                        }
                    }


                }

                ((dynamic)v).ParentName = ll.ParentId.UserName;
                ((dynamic)v).ParentId = ll.ParentId.Id;
                user.Add(v);
            }
            return new ObjectResult(user);
        }


        [HttpGet]
        public IActionResult GetUserAdminSwane()
        {
            var v1 = GetRepo().GetDataAll<User>(n => n.Customer.CustomerCode == "SWANE-001" && n.DeleteFlag==false && n.Profile.Count()==0).ToList();

           
          
            return new ObjectResult(v1);
        }

        

        [HttpGet]
        public IActionResult DeleteUserSwane(long userId)
        {
            var v1 = GetRepo().GetData<User>(n => n.Customer.CustomerCode == "SWANE-001" && n.Id == userId);

            v1.DeleteFlag = true;

            var v2 = GetRepo().GetData<UserLavel>(n =>n.User.BaseUserId == v1.Id);
            if (v2 != null)
            {
                v2.DeleteFlag = true;
                GetRepo().Update(v2);
            }
            v1.DeleteFlag = true;
            GetRepo().Update(v1);
          
            GetRepo().SaveChange();

            return Ok();
        }

    }


}