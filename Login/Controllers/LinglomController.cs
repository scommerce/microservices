﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.Startup;
using Login.DbContext;
using Login.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Login.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LinglomController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetTokenFireBaseFromShopCode(string ShopCode,string Cuscode)
        {
            var user = GetRepo().GetData<User>(n => n.Profile.Any(p => p.Property == "ShopCode" && p.Value == ShopCode) && n.Customer.CustomerCode == Cuscode);

           var data = GetRepo().GetData<BaseApiToken>(n =>  n.CreateUser.UserName == user.UserName && n.TokenType == Framework.BaseEnum.TokenType.Firebase && n.DeleteFlag == false);

            return new ObjectResult(new
            {
                Token = data.Token
            });

        }
    }
}