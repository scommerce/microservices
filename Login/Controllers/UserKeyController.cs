﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.Startup;
using Login.DbContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Login.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserKeyController : BaseController<DataContext>
    {
        [HttpGet]
        public async Task<IActionResult> GenKey(string Cuscode)
        {

            Random rand = new Random();
            byte[] b = new byte[16];
            rand.NextBytes(b);
            string bitString = BitConverter.ToString(b).Replace("-", "").ToLower();

            await GetRepo().RegisterCustomer(Cuscode);
            GetRepo().AddSetting(Cuscode,"Encode","User",bitString);

        
            return Ok();
    
        }
    }
}