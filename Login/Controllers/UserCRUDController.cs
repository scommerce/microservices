﻿using System;
using Framework.Startup;
using Login.DbContext;
using Login.Entity;

namespace Login.Controllers
{
    public class UserCRUDController:BaseCRUD<User, User, DataContext>
    {
        public override void BeforeUpdate(User entity)
        {
            entity.Password = "99999999";
        }

        public UserCRUDController()
        {
            FieldMap.Add("UserStatusLabel", "UserStatus");
        }
    }
}
