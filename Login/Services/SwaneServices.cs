﻿using Login.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Services
{
    public static class SwaneServices
    {
 
        public static void addUserLevel(List<UserLavel> l, List<UserLavel> ll, string User, List<string> SwaneLavleUser)
        {

            if (SwaneLavleUser.Where(n => n == User).FirstOrDefault() == null)
            {
                SwaneLavleUser.Add(User);

                var u = ll.Where(n => n.ParentId.UserName == User && n.DeleteFlag == false).ToList();
                if (u != null)
                {
                    l.AddRange(u);
                    foreach (var uu in u.GroupBy(n => n.User))
                    {
                        addUserLevel(l, ll, uu.Key.UserName, SwaneLavleUser);
                    }
                }
            }

        }
    }
}
