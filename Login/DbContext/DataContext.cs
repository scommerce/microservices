﻿using Login.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Login.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserLavel> UserLavels { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Login;User Id=sa;Password=P@ssword1234;";
        }
    }
}
