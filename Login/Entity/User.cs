﻿using Framework.BaseEntity;
using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Entity
{
    [ExtGrid(title = "ข้อมูลผู้ใช้งาน", controller="https://localhost:5001/api/UserCRUD", cud = ExtGridAttribute.CUD.CrearteUpdateDelete,addWidth = "Ext.getBody().getViewSize().width-220",addHeight = "Ext.getBody().getViewSize().height-200", width = "Ext.getBody().getViewSize().width-220", height = "Ext.getBody().getViewSize().height-50",pageSize =20, child = "columns",paging =true,filter =true)]
    [ExtPanel(theme = "default-add",itemsFixTheme =true, title = "ข้อมูลผู้ใช้งาน", name = "fromLogin", frame = true, child = "items", jsonSubmit = true)]

    public class User: BaseEntityCusCode
    {
        [Required]
        [ExtGridColumn(text = "ชื่อผู้ใช้งาน",fillter = ExtGridColumn.Fillter.String)]
        [ExtInputField(theme = "default-add", emptyText = "กรุณาใส่ชื่อผู้ใช้งาน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, regex = @"/[a-zA-Z0-9@.!#$%^&]/", regexText = "กรุณาใส่ตัวอักษรภาษาอังกฤษ ตัวเลข หรืออักขระพิเศษ เท่านั้น ", fieldLabel = "ชื่อผู้ใช้งาน", order = 1)]
        public string UserName { get; set; }
        [Required]
        [ExtInputField(theme = "default-add", emptyText = "กรุณาใส่รหัสผ่าน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, regex = @"/[a-zA-Z0-9@.!#$%^&]/", regexText = "กรุณาใส่ตัวอักษรภาษาอังกฤษ ตัวเลข หรืออักขระพิเศษ เท่านั้น ", fieldLabel = "รหัสผ่าน",inputType = ExtInputFieldAttribute.InputType.password, order = 1)]
        public string Password { get; set; }
        [ExtGridColumn(text = "เบอร์โทร")]
        public string Phone { get; set; }
        [ExtGridColumn(text = "อีเมลล์")]
        public string Email { get; set; }
        [ExtGridColumn(text = "วันที่เข้าใช้งานล่าสุด")]
        public DateTime? LastLogin { get; set; }
        
        public virtual UserStatus UserStatus { get; set; }
        [ExtGridColumn(text = "สถานะ")]
        public string UserStatusLabel { get
            {
                return UserStatus.ToString();
            }
        }

        public virtual List<ProfileUser> Profile { get; set; }
    }
}
