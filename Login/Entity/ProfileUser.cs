﻿using System;
using Framework.BaseEntity;

namespace Login.Entity
{
    public class ProfileUser: BaseEntityCusCode
    {
        
        public string Property { get; set; }
        public string Value { get; set; }
        public PropertyType PropetyType { get; set; }
    }
}
