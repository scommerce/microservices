﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Model
{
    public class RegisterFbTokenModel
    {
        [Required]
        public string cuscode { get; set; }
        [Required]
        public string idToken { get; set; }
    }
}
