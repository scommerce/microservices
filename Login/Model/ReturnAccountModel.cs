﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Model
{
    public class ReturnAccountModel
    {
        public string Kind { get; set; }
        public List<user> users { get; set; }

    }
    public  class user
    {
        public string localId { get; set; }
        public string email { get; set; }
        public string passwordHash { get; set; }
        public bool emailVerified { get; set; }
        public decimal passwordUpdatedAt { get; set; }
        public List<providerUserInfo> providerUserInfo { get; set; }
        public string validSince { get; set; }
        public bool disabled { get; set; }
        public string lastLoginAt { get; set; }
        public string createdAt { get; set; }
        public string lastRefreshAt { get; set; }
    }
    public class providerUserInfo
    {
        public string providerId { get; set; }
        public string federatedId { get; set; }
        public string email { get; set; }
        public string rawId { get; set; }

    }
}
