﻿using System;
using System.Collections.Generic;
using Framework.BaseModel;

namespace Login.Model
{
    public class InsertProfile
    {
        public string ApiKey { get; set; }
        public long UserId { get; set; }
        public List<BasePropertyModel> ProfileProperty { get; set; }
    }
}
