﻿using Framework.BaseAttrib;
using Framework.BaseExtjs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Login.Model
{
    [ExtLoginForm(title = "ยินดีตอนรับเข้าสู่ระบบ",name ="fromLogin", frame = true, child = "items", LoginText = "เข้าสู่ระบบ", jsonSubmit = true, url = "https://login.wframwork.com/api/Login/Login", successUrl = "/Page?PageName=Main")]
    public class LoginModel
    {

        [Required(ErrorMessage = "กรุณาใส่ชื่อผู้ใช้งาน")]
        [ExtInputField(emptyText = "กรุณาใส่ชื่อผู้ใช้งาน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, regex = @"/[a-zA-Z0-9@.!#$%^&]/", regexText = "กรุณาใส่ตัวอักษรภาษาอังกฤษ ตัวเลข หรืออักขระพิเศษ เท่านั้น ", fieldLabel = "ชื่อผู้ใช้งาน", order = 1)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "กรุณาใส่รหัสผ่าน")]
        [ExtInputField(emptyText = "กรุณาใส่รหัสผ่าน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "รหัสผ่าน", inputType = ExtInputFieldAttribute.InputType.password, order = 2)]
        public string Password { get; set; }

        [Required(ErrorMessage = "กรุณาใส่รหัสลูกค้า")]
        [ExtInputField( allowBlank = false,hasValue =true, fieldLabel = "รหัสลูกค้า",inputType = ExtInputFieldAttribute.InputType.hidden, order =0)]
        public string CusCode { get; set; }
        [ExtImage(order =0,width = 300)]
        public string Image { get; set; }
    }
}
