﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Login.MicroServices
{
    public partial class addLavel4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropColumn(
                name: "UserLavelId",
                table: "BaseUsers");

            migrationBuilder.AddColumn<long>(
                name: "ParentIdId",
                table: "UserLavels",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserLavels_ParentIdId",
                table: "UserLavels",
                column: "ParentIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLavels_BaseUsers_ParentIdId",
                table: "UserLavels",
                column: "ParentIdId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLavels_BaseUsers_ParentIdId",
                table: "UserLavels");

            migrationBuilder.DropIndex(
                name: "IX_UserLavels_ParentIdId",
                table: "UserLavels");

            migrationBuilder.DropColumn(
                name: "ParentIdId",
                table: "UserLavels");

            migrationBuilder.AddColumn<long>(
                name: "UserLavelId",
                table: "BaseUsers",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId",
                principalTable: "UserLavels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
