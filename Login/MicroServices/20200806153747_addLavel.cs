﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Login.MicroServices
{
    public partial class addLavel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserLavelId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserLavels",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLavels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserLavels_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserLavels_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserLavels_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserLavels_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserLavelId",
                table: "Users",
                column: "UserLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLavels_CreateUserId",
                table: "UserLavels",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLavels_CustomerId",
                table: "UserLavels",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLavels_UpdateUserId",
                table: "UserLavels",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLavels_UserId",
                table: "UserLavels",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserLavels_UserLavelId",
                table: "Users",
                column: "UserLavelId",
                principalTable: "UserLavels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserLavels_UserLavelId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "UserLavels");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserLavelId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserLavelId",
                table: "Users");
        }
    }
}
