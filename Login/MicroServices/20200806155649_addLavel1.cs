﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Login.MicroServices
{
    public partial class addLavel1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLavels_Users_UserId",
                table: "UserLavels");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_UserLavels_UserLavelId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_UserLavelId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserLavelId",
                table: "Users");

            migrationBuilder.AddColumn<long>(
                name: "UserLavelId",
                table: "BaseUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId",
                principalTable: "UserLavels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserLavels_BaseUsers_UserId",
                table: "UserLavels",
                column: "UserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_UserLavels_BaseUsers_UserId",
                table: "UserLavels");

            migrationBuilder.DropIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropColumn(
                name: "UserLavelId",
                table: "BaseUsers");

            migrationBuilder.AddColumn<long>(
                name: "UserLavelId",
                table: "Users",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserLavelId",
                table: "Users",
                column: "UserLavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLavels_Users_UserId",
                table: "UserLavels",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_UserLavels_UserLavelId",
                table: "Users",
                column: "UserLavelId",
                principalTable: "UserLavels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
