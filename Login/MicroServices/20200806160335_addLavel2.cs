﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Login.MicroServices
{
    public partial class addLavel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers");

            migrationBuilder.DropColumn(
                name: "UserLavelId",
                table: "BaseUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserLavelId",
                table: "BaseUsers",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_UserLavels_UserLavelId",
                table: "BaseUsers",
                column: "UserLavelId",
                principalTable: "UserLavels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
