﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Models
{
    public class UpdateLocationModel
    {
        public string ApiKey { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
