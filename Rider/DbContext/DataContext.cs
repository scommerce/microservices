﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Rider.Entity;

namespace Rider.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.Rider> Rider { get; set; }
        public DbSet<Rider_Audit> Rider_Audit { get; set; }
        public DbSet<Entity.RiderType> RiderType { get; set; }
        public DbSet<Entity.Status> Status { get; set; }
        public DbSet<Zone> Zone { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Rider;User Id=sa;Password=P@ssword1234;";
        }
    }
}
