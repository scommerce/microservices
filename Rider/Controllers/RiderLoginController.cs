﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Helper;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rider.DbContext;
using Rider.Entity;
using Rider.Models;

namespace Rider.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RiderLoginController : BaseController<DataContext>
    {
        public IActionResult RiderLogin(LoginModel loginModel)
        {
            var password = Encrypt.EncryptString(GetRepo().GetSetting(loginModel.CusCode, "Encode", "User").Value, loginModel.Password);
            ////var data = GetRepo().GetData<Entity.Rider>(n => n.UserName == loginModel.UserName && n.Customer.CustomerCode == loginModel.CusCode && n.Password == password);
            //if (data == null)
            //    return Problem("Login Fail");
            //string key = Guid.NewGuid().ToString().Replace("-", "");



            return Ok();
        }
    }
}
