﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Rider.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rider.DbContext;
using Rider.Models;
using Framework.BaseEntity;
using NetTopologySuite.Geometries;
using System.Net.Http;
using Framework.BaseModel;
using System.Security.Cryptography;

namespace Rider.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RiderController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddRider(RiderModel riderModel)
        {
            var key = await GetRepo().GetUserFromApiKey(riderModel.ApiKey, GetHeaderLang());

            if (riderModel == null)
            {
                return Problem("Data is Null");
            }
            if (GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && (n.FirstName == riderModel.FirstName && n.LastName == riderModel.LastName && n.IDCard == riderModel.IDCard) || n.IDCard == riderModel.IDCard) != null)
            {
                return Problem("Data is Duplicate");
            }
            var age = DateTime.Now.Year - riderModel.BirthDay.Value.Year;
            if (age < 18)
            {
                return Problem("Age lessthan 18!");
            }
            if (key == null)
            {
                return Problem();
            }
            // TODO: แก้ไม่ต้องส่งทุกค่าจาก App
            Entity.Rider rider = new Entity.Rider();
            rider.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == key.Cuscode && n.DeleteFlag == false);
            rider.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == key.UserName && n.DeleteFlag == false);
            rider.FirstName = riderModel.FirstName;
            rider.LastName = riderModel.LastName;
            rider.IDCard = riderModel.IDCard;
            rider.BirthDay = riderModel.BirthDay;
            rider.BookBank = riderModel.BookBank;
            rider.BookBankNumber = riderModel.BookBankNumber;
            rider.BookBankName = riderModel.BookBankName;
            rider.Address = riderModel.Address;
            rider.Tel = riderModel.Tel;
            rider.Email = riderModel.Email;
            rider.ProvinceId = riderModel.ProvinceId;
            rider.CPRName = riderModel.CPRName;
            rider.CPRProvinceId = riderModel.CPRProvinceId;
            rider.Type = GetRepo().GetData<RiderType>(n => n.Name == riderModel.Type);
            rider.Status = GetRepo().GetData<Status>(n => n.Name == "Wait Confirm" && n.DeleteFlag == false);
            rider.StartWork = null;
            rider.ImageId = riderModel.ImageId;
            rider.ImageSlip = riderModel.ImageSlip;
            rider.ImageCPR = riderModel.ImageCPR;
            rider.ImageLicense = riderModel.ImageLicense;
            rider.CurrentLocationLat = riderModel.CurrentLocationLat;
            rider.CurrentLocationLong = riderModel.CurrentLocationLong;
            rider.Deformed = riderModel.Deformed;
            rider.Criminal = riderModel.Criminal;
            rider.CarTaxInvoice = riderModel.CarTaxInvoice;
            rider.Nationality = riderModel.Nationality;
            rider.Race = riderModel.Race;
            rider.ImageIdCard = riderModel.ImageIdCard;

            var Count = GetAll<Entity.Rider>(n => n.Customer.CustomerCode == key.Cuscode && n.DeleteFlag == false);
            rider.RiderCode = "R-" + (Count.Count() + 1).ToString("00000#");

            // TODO: Wait For Copy Command in one line Add
            Rider_Audit rider_Audit = new Rider_Audit();
            rider_Audit.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == rider.Customer.CustomerCode && n.DeleteFlag == false);
            rider_Audit.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == rider.CreateUser.UserName && n.Customer.CustomerCode == rider.Customer.CustomerCode && n.DeleteFlag == false);
            rider_Audit.FirstName = rider.FirstName;
            rider_Audit.LastName = rider.LastName;
            rider_Audit.IDCard = rider.IDCard;
            rider_Audit.BirthDay = rider.BirthDay;
            rider_Audit.BookBank = rider.BookBank;
            rider_Audit.BookBankNumber = rider.BookBankNumber;
            rider_Audit.BookBankName = rider.BookBankName;
            rider_Audit.Address = rider.Address;
            rider_Audit.Tel = rider.Tel;
            rider_Audit.Email = rider.Email;
            rider_Audit.ProvinceId = rider.ProvinceId;
            rider_Audit.CPRName = rider.CPRName;
            rider_Audit.CPRProvinceId = rider.CPRProvinceId;
            rider_Audit.Type = GetRepo().GetData<RiderType>(n => n.Name == riderModel.Type);
            rider_Audit.Status = GetRepo().GetData<Status>(n => n.Name == "Wait Confirm" && n.DeleteFlag == false);
            rider_Audit.StartWork = rider.StartWork;
            rider_Audit.ImageId = rider.ImageId;
            rider_Audit.ImageSlip = rider.ImageSlip;
            rider_Audit.ImageCPR = rider.ImageCPR;
            rider_Audit.ImageLicense = rider.ImageLicense;
            rider_Audit.CurrentLocationLat = rider.CurrentLocationLat;
            rider_Audit.CurrentLocationLong = rider.CurrentLocationLong;
            rider_Audit.Deformed = rider.Deformed;
            rider_Audit.Criminal = rider.Criminal;
            rider_Audit.CarTaxInvoice = rider.CarTaxInvoice;
            rider_Audit.Nationality = rider.Nationality;
            rider_Audit.Race = rider.Race;
            rider_Audit.Description = AuditStatus.Add.ToString();
            rider_Audit.ImageIdCard = rider.ImageIdCard;
            rider_Audit.RiderCode = rider.RiderCode;
            GetRepo().Add(rider);
            GetRepo().Add(rider_Audit);
            GetRepo().SaveChange();


            return Ok("Add Success");
        }

        [HttpPost]
        public IActionResult EditRider(RiderModel riderModel)
        {
            if (riderModel.id == null)
            {
                return Problem("Data is null");
            }

            var rider = GetRepo().GetData<Entity.Rider>(n => n.Id == riderModel.id && n.DeleteFlag == false);
            rider.FirstName = riderModel.FirstName;
            rider.LastName = riderModel.LastName;
            rider.IDCard = riderModel.IDCard;
            rider.BirthDay = riderModel.BirthDay;
            rider.BookBank = riderModel.BookBank;
            rider.BookBankNumber = riderModel.BookBankNumber;
            rider.BookBankName = riderModel.BookBankName;
            rider.Address = riderModel.Address;
            rider.Tel = riderModel.Tel;
            rider.Email = riderModel.Email;
            rider.ProvinceId = riderModel.ProvinceId;
            rider.CPRName = riderModel.CPRName;
            rider.CPRProvinceId = riderModel.CPRProvinceId;
            rider.StartWork = DateTime.Now;
            rider.Type = GetRepo().GetData<RiderType>(n => n.Name == riderModel.Type);
            rider.Status = GetRepo().GetData<Status>(n => n.Name == "Wait Confirm" && n.DeleteFlag == false);
            rider.StartWork = null;
            rider.ImageId = riderModel.ImageId;
            rider.ImageSlip = riderModel.ImageSlip;
            rider.ImageCPR = riderModel.ImageCPR;
            rider.ImageLicense = riderModel.ImageLicense;
            rider.CurrentLocationLat = riderModel.CurrentLocationLat;
            rider.CurrentLocationLong = riderModel.CurrentLocationLong;
            rider.Deformed = riderModel.Deformed;
            rider.Criminal = riderModel.Criminal;
            rider.CarTaxInvoice = riderModel.CarTaxInvoice;
            rider.Nationality = riderModel.Nationality;
            rider.Race = riderModel.Race;
            rider.ImageIdCard = riderModel.ImageIdCard;
            rider.RiderCode = riderModel.RiderCode;
            // TODO: Wait For Copy Command in one line Edit
            Rider_Audit rider_Audit = new Rider_Audit();
            rider_Audit.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == rider.Customer.CustomerCode && n.DeleteFlag == false);
            rider_Audit.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == rider.CreateUser.UserName && n.DeleteFlag == false);
            rider_Audit.FirstName = rider.FirstName;
            rider_Audit.LastName = rider.LastName;
            rider_Audit.IDCard = rider.IDCard;
            rider_Audit.BirthDay = rider.BirthDay;
            rider_Audit.BookBank = rider.BookBank;
            rider_Audit.BookBankNumber = rider.BookBankNumber;
            rider_Audit.BookBankName = rider.BookBankName;
            rider_Audit.Address = rider.Address;
            rider_Audit.Tel = rider.Tel;
            rider_Audit.Email = rider.Email;
            rider_Audit.ProvinceId = rider.ProvinceId;
            rider_Audit.CPRName = rider.CPRName;
            rider_Audit.CPRProvinceId = rider.CPRProvinceId;
            rider_Audit.Type = GetRepo().GetData<RiderType>(n => n.Name == riderModel.Type);
            rider_Audit.Status = GetRepo().GetData<Status>(n => n.Name == "Wait Confirm" && n.DeleteFlag == false);
            rider_Audit.StartWork = rider.StartWork;
            rider_Audit.ImageId = rider.ImageId;
            rider_Audit.ImageSlip = rider.ImageSlip;
            rider_Audit.ImageCPR = rider.ImageCPR;
            rider_Audit.ImageLicense = rider.ImageLicense;
            rider_Audit.CurrentLocationLat = rider.CurrentLocationLat;
            rider_Audit.CurrentLocationLong = rider.CurrentLocationLong;
            rider_Audit.Deformed = rider.Deformed;
            rider_Audit.Criminal = rider.Criminal;
            rider_Audit.CarTaxInvoice = rider.CarTaxInvoice;
            rider_Audit.Nationality = rider.Nationality;
            rider_Audit.Description = AuditStatus.Edit.ToString();
            rider_Audit.Race = rider.Race;
            rider_Audit.ImageIdCard = rider.ImageIdCard;
            rider_Audit.RiderCode = rider.RiderCode;
            GetRepo().Update(rider);
            GetRepo().Add(rider_Audit);
            GetRepo().SaveChange();
            return Ok("Edit Success");
        }

        [HttpPost]
        public IActionResult DeleteRider(long? Id)
        {
            if (Id == null)
            {
                return Problem("Data is Null");
            }
            // TODO: Wait For Copy Command in one line Delete
            var rider = GetRepo().GetData<Entity.Rider>(n => n.Id == Id && n.DeleteFlag == false);
            rider.DeleteFlag = true;

            Rider_Audit rider_Audit = new Rider_Audit();
            rider_Audit.Customer = GetRepo().GetData<BaseCustomer>(n => n.CustomerCode == rider.Customer.CustomerCode && n.DeleteFlag == false);
            rider_Audit.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == rider.CreateUser.UserName && n.DeleteFlag == false);
            rider_Audit.FirstName = rider.FirstName;
            rider_Audit.LastName = rider.LastName;
            rider_Audit.IDCard = rider.IDCard;
            rider_Audit.BirthDay = rider.BirthDay;
            rider_Audit.BookBank = rider.BookBank;
            rider_Audit.BookBankNumber = rider.BookBankNumber;
            rider_Audit.BookBankName = rider.BookBankName;
            rider_Audit.Address = rider.Address;
            rider_Audit.Tel = rider.Tel;
            rider_Audit.Email = rider.Email;
            rider_Audit.ProvinceId = rider.ProvinceId;
            rider_Audit.CPRName = rider.CPRName;
            rider_Audit.CPRProvinceId = rider.CPRProvinceId;
            rider_Audit.Type = GetRepo().GetData<RiderType>(n => n.Name == rider.Type.Name);
            rider_Audit.Status = GetRepo().GetData<Status>(n => n.Name == "Wait Confirm" && n.DeleteFlag == false);
            rider_Audit.StartWork = rider.StartWork;
            rider_Audit.ImageId = rider.ImageId;
            rider_Audit.ImageSlip = rider.ImageSlip;
            rider_Audit.ImageCPR = rider.ImageCPR;
            rider_Audit.ImageLicense = rider.ImageLicense;
            rider_Audit.CurrentLocationLat = rider.CurrentLocationLat;
            rider_Audit.CurrentLocationLong = rider.CurrentLocationLong;
            rider_Audit.Deformed = rider.Deformed;
            rider_Audit.Criminal = rider.Criminal;
            rider_Audit.CarTaxInvoice = rider.CarTaxInvoice;
            rider_Audit.Nationality = rider.Nationality;
            rider_Audit.Description = AuditStatus.Delete.ToString();
            rider_Audit.Race = rider.Race;
            rider_Audit.ImageIdCard = rider.ImageIdCard;
            rider_Audit.RiderCode = rider.RiderCode;

            GetRepo().Update(rider);
            GetRepo().Add(rider_Audit);
            GetRepo().SaveChange();

            return Ok("Delete Success");
        }

        [HttpGet]
        public IActionResult GetRider(long? id)
        {
            //var key = await GetRepo().GetUserFromApiKey(apiKey);

            if (id != null)
            {
                var p = GetAll<Entity.Rider>(n => n.Id == id && n.DeleteFlag == false).ToList();
                return new ObjectResult(p);
            }
            else
            {
                var p = GetAll<Entity.Rider>(n => n.DeleteFlag == false).ToList();
                return new ObjectResult(p);
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdateLocation(UpdateLocationModel update)
        {
            var key = await GetRepo().GetUserFromApiKey(update.ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d.CurrentLocation == null)
                d.CurrentLocation = new Entity.Location();
            d.CurrentLocationLat = update.Lat;
            d.CurrentLocationLong = update.Lng;
            d.CurrentLocation.LocationPoint = new Point(update.Lng, update.Lat) { SRID = 4326 };
            d.CurrentLocation.UpdateDate = DateTime.Now;

            GetRepo().Update(d);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpPost]
        public IActionResult GetFirstRiderFromLocation(LocationModel location)
        {
            var l = new Point(location.Lng, location.Lat) { SRID = 4326 };
            var d = GetRepo().GetDataAll<Entity.Location>(n => n.Rider.DeleteFlag == false && n.Rider.Status.Name == "Woking" && n.LocationPoint.Distance(l) * 100000 <= 1000000)
                .Select(n => new {
                    Rider = n.Rider,
                    Ditinct = Math.Round(n.LocationPoint.Distance(l) * 100000, 2, MidpointRounding.AwayFromZero)
                }).ToList();

            var rider = d.OrderBy(n => n.Ditinct).FirstOrDefault();
            if (rider == null)
            {
                return Problem("No Rider");
            }
            rider.Rider.Status = GetRepo().GetData<Status>(n => n.Name == "Running" && n.DeleteFlag == false);
            GetRepo().Update(rider.Rider);
            GetRepo().SaveChange();
            return new ObjectResult(rider);
        }

        [HttpGet]
        public async Task<IActionResult> GetRiderFromApiKey(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            return new ObjectResult(d);
        }


        [HttpGet]
        public async Task<IActionResult> RiderRuning(String ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d == null)
                return Problem("ไม่พบ Rider");
            d.Status = GetRepo().GetData<Status>(n => n.Name == "Running" && n.DeleteFlag == false);
            GetRepo().Update(d);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> RiderNormal(String ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d == null)
                return Problem("ไม่พบ Rider");
            d.Status = GetRepo().GetData<Status>(n => n.Name == "Normal" && n.DeleteFlag == false);
            GetRepo().Update(d);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> RiderRest(String ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d == null)
                return Problem("ไม่พบ Rider");
            d.Status = GetRepo().GetData<Status>(n => n.Name == "Rest" && n.DeleteFlag == false);
            GetRepo().Update(d);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> RiderWorking(String ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d == null)
                return Problem("ไม่พบ Rider");
            d.Status = GetRepo().GetData<Status>(n => n.Name == "Woking" && n.DeleteFlag == false);
            GetRepo().Update(d);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel login)
        {

            var data = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/Login/Login", HttpMethod.Post, login, true, GetHeaderLang());
            var key = await GetRepo().GetUserFromApiKey(data.data, GetHeaderLang());
            var d = GetRepo().GetData<Entity.Rider>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName && n.CreateUser.Customer.CustomerCode == key.Cuscode);
            if (d.Status.Name == "Wait Confirm")
            {
                return Problem("ไม่สามารถเข้าระบบได้ กรุณารอการอนุมัติ");
            }
            return new ObjectResult(data);
        }
        [HttpPost]
        public IActionResult ValidateRegister(ValidateRegisModel validateRegisModel)
        {
            if (validateRegisModel == null)
            {
                return Problem("Data is Null");
            }
            if (GetRepo().GetData<Entity.Rider>(n => (n.FirstName == validateRegisModel.FirstName && n.LastName == validateRegisModel.LastName && n.IDCard == validateRegisModel.IDCard) || n.IDCard == validateRegisModel.IDCard) != null)
            {
                return Problem("Data is Duplicate");
            }
            var age = DateTime.Now.Year - validateRegisModel.BirthDay.Value.Year;
            if (age < 18)
            {
                return Problem("Age lessthan 18!");
            }

            return Ok();
        }

        [HttpGet]
        public IActionResult CheckSystem()
        {
            var res = "ระบบจะเปิดให้บริการเร็วๆนี้";
            return new ObjectResult(new { 
            Result = res
            });
        }
    }
}