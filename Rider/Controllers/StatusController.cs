﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rider.DbContext;
using Rider.Entity;
using Rider.Models;

namespace Rider.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StatusController : BaseController<DataContext>
    {
        [HttpPost]
        public IActionResult AddStatus(StatusModel statusModel)
        {
            if(statusModel == null)
            {
                return Problem("Data is null");
            }
            Status status = new Status();
            status.Name = statusModel.Name;

            GetRepo().Add(status);
            GetRepo().SaveChange();
            return Ok();
        }
    }
}