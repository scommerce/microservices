﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rider.DbContext;
using Rider.Entity;

namespace Rider.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ZoneController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetZone(long? id)
        {

            if (id == null)
            {
                var zone = GetAll<Zone>().ToList();
                return new ObjectResult(zone);
            }
            else
            {
                var zone = GetAll<Zone>(n => n.Id == id && n.DeleteFlag == false).ToList();
                return new ObjectResult(zone);

            }
        }
    }
}