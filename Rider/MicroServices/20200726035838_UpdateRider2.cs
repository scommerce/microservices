﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class UpdateRider2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BookBankNumber",
                table: "Rider",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookBankNumber",
                table: "Rider");
        }
    }
}
