﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class addLang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "BaseLanguageId",
                table: "BaseApiReturns",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BaseLanguage",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    Lang = table.Column<string>(nullable: true),
                    OrigianlText = table.Column<string>(nullable: true),
                    ToText = table.Column<string>(nullable: true),
                    Error = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseLanguage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiReturns_BaseLanguageId",
                table: "BaseApiReturns",
                column: "BaseLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_CreateUserId",
                table: "BaseLanguage",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_UpdateUserId",
                table: "BaseLanguage",
                column: "UpdateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiReturns_BaseLanguage_BaseLanguageId",
                table: "BaseApiReturns",
                column: "BaseLanguageId",
                principalTable: "BaseLanguage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseApiReturns_BaseLanguage_BaseLanguageId",
                table: "BaseApiReturns");

            migrationBuilder.DropTable(
                name: "BaseLanguage");

            migrationBuilder.DropIndex(
                name: "IX_BaseApiReturns_BaseLanguageId",
                table: "BaseApiReturns");

            migrationBuilder.DropColumn(
                name: "BaseLanguageId",
                table: "BaseApiReturns");
        }
    }
}
