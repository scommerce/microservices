﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class AddRiderCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ImageIdCard",
                table: "Rider_Audit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "RiderCode",
                table: "Rider_Audit",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ImageIdCard",
                table: "Rider",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "RiderCode",
                table: "Rider",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageIdCard",
                table: "Rider_Audit");

            migrationBuilder.DropColumn(
                name: "RiderCode",
                table: "Rider_Audit");

            migrationBuilder.DropColumn(
                name: "ImageIdCard",
                table: "Rider");

            migrationBuilder.DropColumn(
                name: "RiderCode",
                table: "Rider");
        }
    }
}
