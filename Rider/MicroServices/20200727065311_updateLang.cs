﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class updateLang : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseApiReturns_BaseLanguage_BaseLanguageId",
                table: "BaseApiReturns");

            migrationBuilder.DropIndex(
                name: "IX_BaseApiReturns_BaseLanguageId",
                table: "BaseApiReturns");

            migrationBuilder.DropColumn(
                name: "BaseLanguageId",
                table: "BaseApiReturns");

            migrationBuilder.AddColumn<long>(
                name: "BaseApiReturnId",
                table: "BaseLanguage",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseLanguage_BaseApiReturns_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId",
                principalTable: "BaseApiReturns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseLanguage_BaseApiReturns_BaseApiReturnId",
                table: "BaseLanguage");

            migrationBuilder.DropIndex(
                name: "IX_BaseLanguage_BaseApiReturnId",
                table: "BaseLanguage");

            migrationBuilder.DropColumn(
                name: "BaseApiReturnId",
                table: "BaseLanguage");

            migrationBuilder.AddColumn<long>(
                name: "BaseLanguageId",
                table: "BaseApiReturns",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiReturns_BaseLanguageId",
                table: "BaseApiReturns",
                column: "BaseLanguageId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiReturns_BaseLanguage_BaseLanguageId",
                table: "BaseApiReturns",
                column: "BaseLanguageId",
                principalTable: "BaseLanguage",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
