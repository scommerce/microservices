﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class UpdateRider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "BookBank",
                table: "Rider_Audit",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AddColumn<string>(
                name: "BookBankNumber",
                table: "Rider_Audit",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<long>(
                name: "BookBank",
                table: "Rider",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookBankNumber",
                table: "Rider_Audit");

            migrationBuilder.AlterColumn<string>(
                name: "BookBank",
                table: "Rider_Audit",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<string>(
                name: "BookBank",
                table: "Rider",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
