﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Rider.MicroServices
{
    public partial class AddRider_Audit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Rider_Audit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    IDCard = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Tel = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    ProvinceId = table.Column<long>(nullable: false),
                    BookBank = table.Column<string>(nullable: false),
                    BookBankName = table.Column<string>(nullable: false),
                    CPRName = table.Column<string>(nullable: false),
                    CPRProvinceId = table.Column<long>(nullable: false),
                    StartWork = table.Column<DateTime>(nullable: false),
                    TypeId = table.Column<long>(nullable: false),
                    StatusId = table.Column<long>(nullable: false),
                    ImageId = table.Column<long>(nullable: false),
                    ImageSlip = table.Column<long>(nullable: true),
                    ImageCPR = table.Column<long>(nullable: true),
                    ImageLicense = table.Column<long>(nullable: true),
                    CarTaxInvoice = table.Column<long>(nullable: true),
                    CurrentLocationLat = table.Column<double>(nullable: false),
                    CurrentLocationLong = table.Column<double>(nullable: false),
                    Nationality = table.Column<string>(nullable: false),
                    Deformed = table.Column<string>(nullable: true),
                    Criminal = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rider_Audit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rider_Audit_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rider_Audit_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rider_Audit_Status_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rider_Audit_RiderType_TypeId",
                        column: x => x.TypeId,
                        principalTable: "RiderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rider_Audit_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Rider_Audit_CreateUserId",
                table: "Rider_Audit",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Rider_Audit_CustomerId",
                table: "Rider_Audit",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Rider_Audit_StatusId",
                table: "Rider_Audit",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Rider_Audit_TypeId",
                table: "Rider_Audit",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Rider_Audit_UpdateUserId",
                table: "Rider_Audit",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Rider_Audit");
        }
    }
}
