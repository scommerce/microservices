﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public class Zone: BaseEntityCusCode
    {
        public string Name { get; set; }
        public long?  ProvincesId { get; set; }
    }
}
