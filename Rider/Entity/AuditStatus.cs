﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public enum AuditStatus
    {
        Add,
        Edit,
        Delete
    }
}
