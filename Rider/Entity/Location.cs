﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public class Location
    {
        [Key]
        public long Id { get; set; }
        [Column(TypeName = "geometry")]
        public Point  LocationPoint { get; set; }
        public DateTime UpdateDate { get; set; }
        public long RiderId { get; set; }
        [ForeignKey("RiderId")]
        public virtual Rider Rider { get; set; }
    }
}
