﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public class Rider:BaseEntityCusCode
    {
        [Required]
        public string IDCard { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime? BirthDay { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Tel { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long? ProvinceId { get; set; }
        [Required]
        public long? BookBank { get; set; }
        [Required]
        public string BookBankNumber { get; set; }
        [Required]
        public string BookBankName { get; set; }
        [Required]
        public string CPRName { get; set; }
        [Required]
        public long? CPRProvinceId { get; set; }
        public DateTime? StartWork { get; set; }
        [Required]
        public virtual RiderType Type { get; set; }
        [Required]
        public virtual Status Status { get; set; }

        //[Required]
        //public virtual List<long> Suspend { get; set; }
        [Required]
        public long? ImageId { get; set; }
        public long? ImageSlip { get; set; }
        [Required]
        public long? ImageCPR { get; set; }
        [Required]
        public long? ImageLicense { get; set; }
        [Required]
        public long? CarTaxInvoice { get; set; }
        [Required]
        public double CurrentLocationLat { get; set; }
        [Required]
        public double CurrentLocationLong { get; set; }
        [Required]
        public string Nationality { get; set; }
        [Required]
        public string Deformed { get; set; }
        [Required]
        public bool Criminal { get; set; }
        [Required]
        public string Race { get; set; }
        [Required]
        public long? ImageIdCard { get; set; }
        [Required]
        public string RiderCode { get; set; }
        public virtual Location CurrentLocation { get; set; }
    }
}
