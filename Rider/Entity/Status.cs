﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public class Status:BaseEntityCusCode
    {
        [Required]
        public string Name { get; set; }
    }
}
