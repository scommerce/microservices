﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Rider.Entity
{
    public class Rider_Audit : BaseEntityCusCode
    {
        [Required]
        public string IDCard { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime? BirthDay { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Tel { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public long? ProvinceId { get; set; }
        [Required]
        public long? BookBank { get; set; }
        [Required]
        public string BookBankNumber { get; set; }
        [Required]
        public string BookBankName { get; set; }
        [Required]
        public string CPRName { get; set; }
        [Required]
        public long? CPRProvinceId { get; set; }
        public DateTime? StartWork { get; set; }
        [Required]
        public virtual RiderType Type { get; set; }
        [Required]
        public virtual Status Status { get; set; }
        //[Required]
        //public virtual List<long> Suspend { get; set; }
        [Required]
        public long? ImageId { get; set; }
        public long? ImageSlip { get; set; }
        public long? ImageCPR { get; set; }
        public long? ImageLicense { get; set; }
        public long? CarTaxInvoice { get; set; }
        public double CurrentLocationLat { get; set; }
        public double CurrentLocationLong { get; set; }
        [Required]
        public string Nationality { get; set; }
        public string Deformed { get; set; }
        [Required]
        public bool Criminal { get; set; }
        [Required]
        public string Race { get; set; }
        [Required]
        public long? ImageIdCard { get; set; }
        [Required]
        public string Description { get; set; }
        public string RiderCode { get; set; }
    }
}
