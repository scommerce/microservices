﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AspNetCore.Report;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ReportServices.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ReportController : ControllerBase
    {

        [HttpGet]
        public IActionResult PrintQr(long id)
        {
            ReportSettings reportSettings = new ReportSettings
            {
                ReportServer = "http://203.154.83.161/ReportServer/",

                Credential = new NetworkCredential("Administrator", "Opat2532")

                
            };

          
            ReportViewer viewer = new ReportViewer(reportSettings);
          
            ReportRequest reportRequest = new ReportRequest
            {
                Path = @"/QrCodeSystem/Report/Barcode"

            };
          


            reportRequest.SetFileName(DateTime.Now.ToString("ddMMyyyyHHmmss"));

            Dictionary<string, string> reportParams = new Dictionary<string, string>();
            reportParams.Add("id", id.ToString());
            reportRequest.Parameters = reportParams;

            reportRequest.RenderType = ReportRenderType.Pdf;
            reportRequest.ExecuteType = ReportExecuteType.Export;
    


            try
            {
                
                 ReportResponse data = viewer.Execute(reportRequest);
                Console.WriteLine("PageCount::" + data.Data.PageCount);
                return File(data.Data.Stream, "application/octet-stream", DateTime.Now.ToString("ddMMyyyyHHmmss") + ".pdf");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.InnerException.ToString());
                return Problem();
            }
        }

        [HttpGet]
        public IActionResult GetReport()
        {
            ReportSettings reportSettings = new ReportSettings
            {
                ReportServer = "http://203.154.83.161/ReportServer/",

                Credential = new NetworkCredential("Administrator", "Opat2532")


            };
            var key = Request.Query.Keys;

            ReportViewer viewer = new ReportViewer(reportSettings);

            ReportRequest reportRequest = new ReportRequest
            {
                Path = Request.Query.Where(n => n.Key == "reportName").FirstOrDefault().Value

            };



            reportRequest.SetFileName(DateTime.Now.ToString("ddMMyyyyHHmmss"));

            Dictionary<string, string> reportParams = new Dictionary<string, string>();
            foreach (var k in key.Where(n => n != "reportName" && n != "type"))
            {
                reportParams.Add(k, Request.Query.Where(n => n.Key == k).FirstOrDefault().Value);
            }
            reportRequest.Parameters = reportParams;
            var type = Request.Query.Where(n => n.Key == "type").FirstOrDefault().Value;
            if (type == "xlsx")
            {
                reportRequest.RenderType = ReportRenderType.Excel;
            }
            else
            {
                reportRequest.RenderType = ReportRenderType.Pdf;
            }
            reportRequest.ExecuteType = ReportExecuteType.Export;
            


            try
            {

                ReportResponse data = viewer.Execute(reportRequest);
                Console.WriteLine("PageCount::" + data.Data.PageCount);
                
                return File(data.Data.Stream, "application/octet-stream", DateTime.Now.ToString("ddMMyyyyHHmmss") + "."+data.Data.Extension);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.InnerException.ToString());
                return Problem();
            }
        }
    }
}