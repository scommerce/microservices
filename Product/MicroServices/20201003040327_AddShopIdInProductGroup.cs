﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class AddShopIdInProductGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ShopId",
                table: "Product_Group",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Product_Group");
        }
    }
}
