﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class UpdateDraftProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Status",
                table: "DraftProduct",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Approve",
                table: "DraftProduct",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Approve",
                table: "DraftProduct");

            migrationBuilder.AlterColumn<long>(
                name: "Status",
                table: "DraftProduct",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));
        }
    }
}
