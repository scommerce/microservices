﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class AddShopInProduct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Status",
                table: "Product_Audit",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ShopId",
                table: "Product_Audit",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ShopId",
                table: "Product",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ShopId",
                table: "DraftProduct",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Product_Audit");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "DraftProduct");

            migrationBuilder.AlterColumn<long>(
                name: "Status",
                table: "Product_Audit",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));
        }
    }
}
