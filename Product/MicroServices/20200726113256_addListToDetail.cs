﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class addListToDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MultiSelecte",
                table: "Product_Group");

            migrationBuilder.AddColumn<bool>(
                name: "MultiSelect",
                table: "Product_Group",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Product_GroupId",
                table: "Detail",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Detail_Product_GroupId",
                table: "Detail",
                column: "Product_GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_Detail_Product_Group_Product_GroupId",
                table: "Detail",
                column: "Product_GroupId",
                principalTable: "Product_Group",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Detail_Product_Group_Product_GroupId",
                table: "Detail");

            migrationBuilder.DropIndex(
                name: "IX_Detail_Product_GroupId",
                table: "Detail");

            migrationBuilder.DropColumn(
                name: "MultiSelect",
                table: "Product_Group");

            migrationBuilder.DropColumn(
                name: "Product_GroupId",
                table: "Detail");

            migrationBuilder.AddColumn<bool>(
                name: "MultiSelecte",
                table: "Product_Group",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
