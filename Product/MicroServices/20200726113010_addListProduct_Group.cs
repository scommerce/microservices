﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class addListProduct_Group : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProductId",
                table: "Product_Group",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Product_Group_ProductId",
                table: "Product_Group",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Product_Group_Product_ProductId",
                table: "Product_Group",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Product_Group_Product_ProductId",
                table: "Product_Group");

            migrationBuilder.DropIndex(
                name: "IX_Product_Group_ProductId",
                table: "Product_Group");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Product_Group");
        }
    }
}
