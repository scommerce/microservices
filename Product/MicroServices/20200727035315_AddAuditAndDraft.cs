﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Product.MicroServices
{
    public partial class AddAuditAndDraft : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "ProductId",
                table: "Product_Group",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Status",
                table: "Product",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "DraftProduct",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    ImageId = table.Column<long>(nullable: false),
                    NameTh = table.Column<string>(nullable: false),
                    NameEn = table.Column<string>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Desc = table.Column<string>(nullable: true),
                    Product_Group = table.Column<long>(nullable: true),
                    Status = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DraftProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DraftProduct_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DraftProduct_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DraftProduct_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Product_Audit",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    ImageId = table.Column<long>(nullable: false),
                    NameTh = table.Column<string>(nullable: false),
                    NameEn = table.Column<string>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Desc = table.Column<string>(nullable: true),
                    Product_Group = table.Column<long>(nullable: true),
                    Status = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product_Audit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_Audit_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_Audit_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_Audit_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DraftProduct_CreateUserId",
                table: "DraftProduct",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DraftProduct_CustomerId",
                table: "DraftProduct",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_DraftProduct_UpdateUserId",
                table: "DraftProduct",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Audit_CreateUserId",
                table: "Product_Audit",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Audit_CustomerId",
                table: "Product_Audit",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_Audit_UpdateUserId",
                table: "Product_Audit",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DraftProduct");

            migrationBuilder.DropTable(
                name: "Product_Audit");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Product");

            migrationBuilder.AlterColumn<long>(
                name: "ProductId",
                table: "Product_Group",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));
        }
    }
}
