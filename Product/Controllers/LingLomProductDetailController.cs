﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Product.DbContext;
using Product.Entity;
using Product.Models;

namespace Product.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LingLomProductDetailController : BaseController<DataContext>
    {
        [HttpPost]
        public IActionResult AddProductDetail(ProductDetailModel productDetailModel)
        {
            if (productDetailModel.ProductGroupId == null)
            {
                return Problem("Data Is Null");
            }
            Detail detail = new Detail();
            detail.NameEn = productDetailModel.NameEn;
            detail.NameTh = productDetailModel.NameTh;
            detail.Price = productDetailModel.Price;
            detail.Product_GroupId = productDetailModel.ProductGroupId;

            GetRepo().Add(detail);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpPost]
        public IActionResult EditProductDetail(ProductDetailModel productDetailModel)
        {
            if (productDetailModel.Id == null)
            {
                return Problem("DataIsNull");
            }
            var detail = GetRepo().GetData<Detail>(n=>n.Id == productDetailModel.Id && n.DeleteFlag == false);
            detail.NameEn = productDetailModel.NameEn;
            detail.NameTh = productDetailModel.NameTh;
            detail.Price = productDetailModel.Price;
            detail.Product_GroupId = productDetailModel.ProductGroupId;

            GetRepo().Update(detail);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public IActionResult DeleteProductDetail(long? Id)
        {
            if (Id == null)
            {
                return Problem("DataIsNull");
            }
            var detail = GetRepo().GetData<Detail>(n => n.Id == Id && n.DeleteFlag == false);
            detail.DeleteFlag = true;

            GetRepo().Update(detail);
            GetRepo().SaveChange();
            return Ok();
        }

    }
}
