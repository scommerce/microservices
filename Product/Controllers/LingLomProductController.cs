﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Product.DbContext;
using Product.Entity;
using Product.Models;

namespace Product.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]

    public class LingLomProductController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetProduct(long? ShopId)
        {
            var a = GetAll<Entity.Product>(n=>n.ShopId == ShopId && n.DeleteFlag == false && n.Status == (long)Status.Avaliable && (n.Product_Group.Any(c => c.DeleteFlag == false)) && (n.Product_Group.Any(c => c.ProductId != null))).ToList();
            return new ObjectResult(a) ;
        }
        [HttpPost]
        public async Task<IActionResult> AddProduct(ProductModel productModel)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + productModel.ApiKey, HttpMethod.Get, null, false);

            Entity.Product Product = new Entity.Product();
            Product.ImageId = productModel.ImageId;
            Product.NameEn = productModel.NameEn;
            Product.NameTh = productModel.NameTh;
            Product.Price = productModel.Price;
            Product.Desc = productModel.Desc;
            if (productModel.ProductActive == false)
            {
                Product.Status = (long)Status.Unavailable;
            }
            else if (productModel.ProductActive == true)
            {
                Product.Status = (long)Status.Avaliable;
            }
            else
            {
                Product.Status = (long)Status.Avaliable;
            }
            Product.ShopId = Shop.Id;
            Product.Customer = await GetRepo().GetCustomer(productModel.CusCode);

            GetRepo().Add(Product);
            GetRepo().SaveChange();

            var pg = GetRepo().GetData<Product_Group>(n=>n.DeleteFlag == false && n.Id == productModel.Product_Group);
            Product_Group pgroup = new Product_Group();
            pgroup.GroupNameTh = pg.GroupNameTh;
            pgroup.GroupNameEn = pg.GroupNameEn;
            pgroup.MultiSelect = pg.MultiSelect;
            pgroup.OrderBy = pg.OrderBy;
            pgroup.ProductId = Product.Id;
            pgroup.ShopId = Shop.Id;            


            GetRepo().Add(pgroup);
            GetRepo().SaveChange();

            Product_Audit product_Audit = new Product_Audit();
            product_Audit.ImageId = Product.ImageId;
            product_Audit.NameEn = Product.NameEn;
            product_Audit.NameTh = Product.NameTh;
            product_Audit.Price = Product.Price;
            product_Audit.Desc = Product.Desc;
            product_Audit.Status = (long)Status.Add;
            product_Audit.ShopId = Product.ShopId;
            product_Audit.Customer = Product.Customer;

            GetRepo().Add(product_Audit);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddDraftProduct(ProductModel productModel)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + productModel.ApiKey, HttpMethod.Get, null, false);

            DraftProduct draftProduct = new DraftProduct();
            draftProduct.ImageId = productModel.ImageId;
            draftProduct.NameEn = productModel.NameEn;
            draftProduct.NameTh = productModel.NameTh;
            draftProduct.Price = productModel.Price;
            draftProduct.Desc = productModel.Desc;
            draftProduct.Status = (long)Status.Wait;
            draftProduct.Approve = false;
            draftProduct.ShopId = Shop.Id;

            draftProduct.Customer = await  GetRepo().GetCustomer(productModel.CusCode);

            GetRepo().Add(draftProduct);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult ApproveDraft(long? Id)
        {
            var Draft = GetRepo().GetData<DraftProduct>(n => n.Id == Id && n.DeleteFlag == false);
            Draft.Approve = true;

            GetRepo().Update(Draft);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult AddProduct(long? Id)
        {

            var Draft = GetRepo().GetData<DraftProduct>(n=> n.Id == Id && n.DeleteFlag == false);
            if (Draft.Approve == false)
            {
                return Problem("Data Not Approve");
            }
            Entity.Product product = new Entity.Product();
            product.ImageId = Draft.ImageId;
            product.NameEn = Draft.NameEn;
            product.NameTh = Draft.NameTh;
            product.Price = Draft.Price;
            product.Desc = Draft.Desc;
            product.Status = (long)Status.Avaliable;
            product.ShopId = Draft.ShopId;
            product.Customer = Draft.Customer;


            GetRepo().Add(product);
            GetRepo().SaveChange();

            Draft.DeleteFlag = true;
            GetRepo().Update(Draft);
            GetRepo().SaveChange();

            Product_Audit product_Audit = new Product_Audit();
            product_Audit.ImageId = product.ImageId;
            product_Audit.NameEn = product.NameEn;
            product_Audit.NameTh = product.NameTh;
            product_Audit.Price = product.Price;
            product_Audit.Desc = product.Desc;
            product_Audit.Status = (long)Status.Add;
            product_Audit.ShopId = product.ShopId;
            product_Audit.Customer = Draft.Customer;

            GetRepo().Add(product_Audit);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpPost]
        public IActionResult EditProduct(EditProductModel productModel)
        {

            var product = GetRepo().GetData<Entity.Product>(n => n.Id == productModel.Id && n.DeleteFlag == false);

            product.ImageId = productModel.ImageId;
            product.NameEn = productModel.NameEn;
            product.NameTh = productModel.NameTh;
            product.Price = productModel.Price;
            product.Desc = productModel.Desc;
            if (productModel.ProductActive == false)
            {
                product.Status = (long)Status.Unavailable;
            }
            else if (productModel.ProductActive == true)
            {
                product.Status = (long)Status.Avaliable;
            }
            else
            {
                product.Status = (long)Status.Avaliable;
            }

            GetRepo().Update(product);
            GetRepo().SaveChange();


            var pg = GetRepo().GetData<Product_Group>(n => n.DeleteFlag == false && n.Id == productModel.Product_Group);
            pg.ProductId = product.Id;

            GetRepo().Update(pg);
            GetRepo().SaveChange();


            Product_Audit product_Audit = new Product_Audit();
            product_Audit.ImageId = product.ImageId;
            product_Audit.NameEn = product.NameEn;
            product_Audit.NameTh = product.NameTh;
            product_Audit.Price = product.Price;
            product_Audit.Desc = product.Desc;
            product_Audit.Status = (long)Status.Edit;
            product_Audit.ShopId = product.ShopId;
            product_Audit.Customer = product.Customer;

            GetRepo().Add(product_Audit);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult DeleteProduct(long? Id)
        {

            var product = GetRepo().GetData<Entity.Product>(n => n.Id == Id && n.DeleteFlag == false);
            product.DeleteFlag = true;

            GetRepo().SaveChange();

            var group = GetAll<Product_Group>(n=>n.ProductId == product.Id && n.DeleteFlag == false).ToList();
            foreach (var a in group)
            {
                a.DeleteFlag = true;
            }
            GetRepo().SaveChange();

            var detail = GetAll<Detail>(n=>n.Product_GroupId == group.FirstOrDefault().Id && n.DeleteFlag == false).ToList();
            foreach (var b in detail)
            {
                b.DeleteFlag = true;
            }
            GetRepo().SaveChange();

            Product_Audit product_Audit = new Product_Audit();
            product_Audit.ImageId = product.ImageId;
            product_Audit.NameEn = product.NameEn;
            product_Audit.NameTh = product.NameTh;
            product_Audit.Price = product.Price;
            product_Audit.Desc = product.Desc;
            product_Audit.Status = (long)Status.Delete;
            product_Audit.ShopId = product.ShopId;
            product_Audit.Customer = product.Customer;

            GetRepo().Add(product_Audit);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult GetProductById(long? id)
        {
            var data = GetRepo().GetDataAll<Entity.Product>(n=>n.DeleteFlag == false && n.Id == id).ToList();
            return new ObjectResult(data);
        }
        [HttpGet]
        public async Task<IActionResult> GetProductByApi(string ApiKey)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);

            var data = GetRepo().GetDataAll<Entity.Product>(n=>n.DeleteFlag == false && n.ShopId == Shop.Id).ToList();
            return new ObjectResult(data);
        }

        [HttpGet]
        public IActionResult ApproveProduct(long ProductId)
        { 
            var data = GetRepo().GetData<Entity.Product>(n => n.DeleteFlag == false && n.Id == ProductId && n.Status == (long)Status.Unavailable);

            data.Status = (long)Status.Avaliable;

            GetRepo().Update(data);
            GetRepo().SaveChange();

            return Ok();
        }


    }
}
