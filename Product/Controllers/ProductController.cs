﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.Startup;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using Product.DbContext;
using Product.Entity;
using Product.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Product.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : BaseController<DataContext>
    {
        // GET: /<controller>/
        [HttpGet]
        public IActionResult GetProduct(string cusCode)
        {
            var a = GetAll<Entity.Product>(n => n.Customer.CustomerCode == cusCode && n.DeleteFlag == false).ToList();
           var data= GetRepo().GetProperty(a);

            return new ObjectResult(data);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct(ProductModel productModel)
        {

            Product.Entity.Product product = new Product.Entity.Product();
            product.ImageId = productModel.ImageId;
            product.NameEn = productModel.NameEn;
            product.NameTh = productModel.NameTh;
            product.Price = productModel.Price;
            product.Desc = productModel.Desc;
            product.Status = (long)Status.Avaliable;

            product.Customer = await GetRepo().GetCustomer(productModel.CusCode);

            GetRepo().Add(product);
          
             GetRepo().SaveChange();


            return new ObjectResult(product);
        }
        [HttpGet]
        public IActionResult UpdateProperty(long productId, string Property, string Value, PropertyType PropertyType)
        {
            var a = GetAll<Entity.Product>(n => n.Id == productId && n.DeleteFlag == false).ToList();
            GetRepo().UpdateProperty(a.FirstOrDefault(), Property, Value, PropertyType);
            return Ok();
        }

        [HttpGet]
        public IActionResult RemoveProduct(long productId)
        {
            var a = GetAll<Entity.Product>(n => n.Id == productId && n.DeleteFlag == false).FirstOrDefault();
            a.DeleteFlag = true;
            GetRepo().Update(a);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public IActionResult CloseProduct(long? productid)
        {
            if ( productid == null)
            {
                return Problem();
            }
            var product = GetRepo().GetData<Entity.Product>(n=>n.DeleteFlag == false && n.Id == productid);
            product.Status = (long)Status.Unavailable;
            GetRepo().Update(product);
            GetRepo().SaveChange();
            return Ok();
        }
    }
}
