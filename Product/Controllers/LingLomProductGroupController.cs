﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Product.DbContext;
using Product.Entity;
using Product.Models;

namespace Product.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LingLomProductGroupController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddProductGroup(ProductGroupModel productGroupModel)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + productGroupModel.ApiKey, HttpMethod.Get, null, false);

            Product_Group product_Group = new Product_Group();
            product_Group.GroupNameTh = productGroupModel.GroupNameTh;
            product_Group.GroupNameEn = productGroupModel.GroupNameEn;
            product_Group.MultiSelect = productGroupModel.MultiSelect;
            product_Group.OrderBy = productGroupModel.OrderBy;
            product_Group.ProductId = productGroupModel.ProductId;
            product_Group.ShopId = Shop.Id;

            GetRepo().Add(product_Group);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> EditProductGroup(ProductGroupModel productGroupModel)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + productGroupModel.ApiKey, HttpMethod.Get, null, false);


            var product_Group = GetRepo().GetData<Product_Group>(n=>n.Id == productGroupModel.Id && n.DeleteFlag  == false);
            product_Group.GroupNameTh = productGroupModel.GroupNameTh;
            product_Group.GroupNameEn = productGroupModel.GroupNameEn;
            product_Group.MultiSelect = productGroupModel.MultiSelect;
            product_Group.OrderBy = productGroupModel.OrderBy;
            product_Group.ProductId = productGroupModel.ProductId;
            product_Group.ShopId = Shop.Id;

            GetRepo().Update(product_Group);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult DeleteProductGroup(long? Id)
        {
            if (Id == null)
            {
                return Problem("Data is null");
            }
            var product_Group = GetRepo().GetData<Product_Group>(n => n.Id == Id && n.DeleteFlag == false);
            product_Group.DeleteFlag = true;

            GetRepo().Update(product_Group);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetProductByGroupName(string ApiKey,string groupname)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            if (groupname != null) {
                //var a = GetRepo().GetDataAll<Product_Group>(n=> n.DeleteFlag == false && n.GroupNameTh.Contains(groupname) || n.GroupNameEn.Contains(groupname));
                var data = GetAll<Entity.Product>(n => n.DeleteFlag == false && n.ShopId == Shop.Id && (n.Product_Group.Any(c => c.DeleteFlag == false) && (n.Product_Group.Any(x => x.GroupNameTh.Contains(groupname)) || n.Product_Group.Any(x => x.GroupNameEn.Contains(groupname))))).ToList();


                foreach (var item in data)
                    {
                        l.Add(new
                        {
                            Id = item.Id,
                            NameTh = item.NameTh,
                            NameEn = item.NameEn,
                            Price = item.Price,
                            Desc = item.Desc,
                            Status = item.Status,
                            ImageId = item.ImageId,
                            GroupNameTh = item.Product_Group.FirstOrDefault().GroupNameTh,
                            Detail = item.Product_Group.FirstOrDefault().Detail.ToList()
                        });
                    }
                
            }
            else
            {
                var data = GetRepo().GetDataAll<Entity.Product>(n => n.DeleteFlag == false && n.ShopId == Shop.Id ).ToList();
                foreach (var item in data)
                {
                    l.Add(new
                    {
                        Id = item.Id,
                        NameTh = item.NameTh,
                        NameEn = item.NameEn,
                        Price = item.Price,
                        Desc = item.Desc,
                        Status = item.Status,
                        ImageId = item.ImageId,
                        GroupNameTh = item.Product_Group.FirstOrDefault().GroupNameTh,
                        Detail = item.Product_Group.FirstOrDefault().Detail.ToList()
                    });
                }
            }


            return new ObjectResult(l);
        }

        [HttpGet]
        public async Task<IActionResult> GetProductGroup(string ApiKey)
        {
            var lls = GetRepo().GetMicroServices("LingLomShop");
            var Shop = await GetRepo().sendRequest<ShopModel>(lls.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);

            var data = GetAll<Product_Group>(n=>n.DeleteFlag == false && n.ShopId == Shop.Id ).ToList().GroupBy(item => new { item.GroupNameTh, item.GroupNameEn })
                     .Select(group => group.OrderByDescending(item => item.Id).First());
            List<object> l = new List<object>();
            foreach (var item in data)
            {
                l.Add(new
                {
                    Id = item.Id,
                    GroupNameTh = item.GroupNameTh,
                    GroupNameEn = item.GroupNameEn,
                    MultiSelect = item.MultiSelect,
                    Detail = item.Detail.ToList() 
                }) ;
            }

            return new ObjectResult(l);
        }
    }
}
