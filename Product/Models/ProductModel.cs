﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Models
{
    public class ProductModel
    {
        public long? Id { get; set; }
      
        public long? ImageId { get; set; }
        [Required]
        public string NameTh { get; set; }
        [Required]
        public string NameEn { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Desc { get; set; }
        public long? Product_Group { get; set; }
        public bool Approve { get; set; }
        [Required]
        public string CusCode { get; set; }

        public long? ShopId { get; set; }
        public bool? ProductActive { get; set; }

        public string ApiKey { get; set; }
    }
}
