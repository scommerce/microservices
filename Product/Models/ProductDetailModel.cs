﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Models
{
    public class ProductDetailModel
    {

        public long? Id { get; set; }
        [Required]
        public string NameTh { get; set; }
        [Required]
        public string NameEn { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public long? ProductGroupId { get; set; }
    }
}
