﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Entity
{
    public class Detail : BaseEntityCusCode
    {
        [Required]
        public string NameTh { get; set; }
        [Required]
        public string NameEn { get; set; }
        [Required]
        public decimal Price { get; set; }
        public long? Product_GroupId { get; set; }
        [ForeignKey("Product_GroupId")]
        public virtual Product_Group Product_Group { get; set; }
    }
}
