﻿using Framework.BaseEntity;
using Framework.BaseInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Entity
{
    public class Product : BaseEntityCusCodeProperty
    {
        public long? ImageId { get; set; }
        [Required]
        public string NameTh { get; set; }
        [Required]
        public string NameEn { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Desc { get; set; }
        public virtual List<Product_Group> Product_Group { get; set; }
        [Required]
        public long? Status { get; set; }
        public long? ShopId { get; set; }

       
    }
}
