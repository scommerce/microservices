﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Entity
{
    public class Product_Group : BaseEntityCusCode
    {
        [Required]
        public string GroupNameTh { get; set; }
        [Required]
        public string GroupNameEn { get; set; }
        [Required]
        public bool MultiSelect { get; set; }
        public string OrderBy { get; set; }
        public long? ShopId { get; set; }
        [Required]
        public virtual List<Detail> Detail { get; set; }
        public long? ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}
