﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Entity
{
    public enum Status
    {
        Avaliable,
        Empty,
        Wait,
        Add,
        Edit,
        Delete,
        Unavailable
    }
}
