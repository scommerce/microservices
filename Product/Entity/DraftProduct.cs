﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Product.Entity
{
    public class DraftProduct : BaseEntityCusCode
    {
        [Required]
        public long? ImageId { get; set; }
        [Required]
        public string NameTh { get; set; }
        [Required]
        public string NameEn { get; set; }
        [Required]
        public decimal Price { get; set; }
        public string Desc { get; set; }
        public long? Product_Group { get; set; }
        [Required]
        public long? Status { get; set; }
        [Required]
        public long? ShopId { get; set; }
        [Required]
        public bool Approve { get; set; }
    }
}
