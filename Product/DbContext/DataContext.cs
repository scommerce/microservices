﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Product.Entity;

namespace Product.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.Product> Product { get; set; }
        public DbSet<Detail> Detail { get; set; }
        public DbSet<Product_Group> Product_Group { get; set; }
        public DbSet<Product_Audit> Product_Audit { get; set; }
        public DbSet<DraftProduct> DraftProduct { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Product;User Id=sa;Password=P@ssword1234;";
        }
    }
}
