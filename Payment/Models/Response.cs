﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Models
{
    public class Response
    {
        public PaymentRequestDetail PaymentResponse { get; set; }
    }
}
