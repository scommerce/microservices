﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Models
{
    public class PaymentRequestDetail
    {
        public string version { get; set; }
        public string payload { get; set; }
        public string signature { get; set; }
    }
}
