﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Models
{
    public class CardModel
    {
        public string Card { get; set; }
        public string CardHolder { get; set; }
        public string ExpMonth { get; set; }
        public string ExpYear { get; set; }
        public string EncryptedCardInfo { get; set; }
        public string MaskedCardInfo { get; set; }
        public string ExpMonthCardInfo { get; set; }
        public string ExpYearCardInfo { get; set; }
        public string ApiUser { get; set; }
    }
}
