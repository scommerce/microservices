﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Payment.MicroServices
{
    public partial class addTransection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Transections",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    timeStamp = table.Column<string>(nullable: true),
                    merchantID = table.Column<string>(nullable: true),
                    respCode = table.Column<string>(nullable: true),
                    pan = table.Column<string>(nullable: true),
                    amt = table.Column<string>(nullable: true),
                    uniqueTransactionCode = table.Column<string>(nullable: true),
                    tranRef = table.Column<string>(nullable: true),
                    approvalCode = table.Column<string>(nullable: true),
                    refNumber = table.Column<string>(nullable: true),
                    eci = table.Column<string>(nullable: true),
                    dateTime = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    failReason = table.Column<string>(nullable: true),
                    userDefined1 = table.Column<string>(nullable: true),
                    userDefined2 = table.Column<string>(nullable: true),
                    userDefined3 = table.Column<string>(nullable: true),
                    userDefined4 = table.Column<string>(nullable: true),
                    userDefined5 = table.Column<string>(nullable: true),
                    storeCardUniqueID = table.Column<string>(nullable: true),
                    ippPeriod = table.Column<string>(nullable: true),
                    ippInterestType = table.Column<string>(nullable: true),
                    ippInterestRate = table.Column<string>(nullable: true),
                    ippMerchantAbsorbRate = table.Column<string>(nullable: true),
                    paidChannel = table.Column<string>(nullable: true),
                    paidAgent = table.Column<string>(nullable: true),
                    paymentChannel = table.Column<string>(nullable: true),
                    backendInvoice = table.Column<string>(nullable: true),
                    issuerCountry = table.Column<string>(nullable: true),
                    issuerCountryA3 = table.Column<string>(nullable: true),
                    bankName = table.Column<string>(nullable: true),
                    cardType = table.Column<string>(nullable: true),
                    processBy = table.Column<string>(nullable: true),
                    paymentScheme = table.Column<string>(nullable: true),
                    rateQuoteID = table.Column<string>(nullable: true),
                    originalAmount = table.Column<string>(nullable: true),
                    fxRate = table.Column<string>(nullable: true),
                    currencyCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transections_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transections_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transections_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Transections_CreateUserId",
                table: "Transections",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Transections_CustomerId",
                table: "Transections",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Transections_UpdateUserId",
                table: "Transections",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transections");
        }
    }
}
