﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Payment.MicroServices
{
    public partial class addPem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PemPath",
                table: "_2C2Ps",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PemPath",
                table: "_2C2Ps");
        }
    }
}
