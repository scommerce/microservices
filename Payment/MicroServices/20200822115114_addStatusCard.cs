﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Payment.MicroServices
{
    public partial class addStatusCard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProcessBy",
                table: "Cards",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Cards",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProcessBy",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Cards");
        }
    }
}
