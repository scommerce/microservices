﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Payment.MicroServices
{
    public partial class addCertPath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CertPath",
                table: "_2C2Ps",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CertPath",
                table: "_2C2Ps");
        }
    }
}
