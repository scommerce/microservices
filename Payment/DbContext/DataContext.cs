﻿using Payment.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Payment.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity._2C2P> _2C2Ps { get; set; }
        public DbSet<Transection> Transections { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<InternalTransection> InternalTransection { get; set; }

        public override string GetConnectionString()
        {
            throw new NotImplementedException();
        }
    }
}
