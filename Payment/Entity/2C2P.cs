﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class _2C2P:BaseEntityCusCode
    {
        public string MerchantID { get; set; }
        public string SecretKey { get; set; }
        public string ApiUrl { get; set; }
        public string _3Ds { get; set; }
        public string ReturnUrl { get; set; }
        public string CertPath { get; set; }
        public string PemPath { get; set; }
    }
}
