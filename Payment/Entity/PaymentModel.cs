﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class PaymentModel
    {
        public string TransectionNo { get; set; }
        public string Desc { get; set; }
        public double Amount { get; set; }
        public string ApiKey { get; set; }
        public long CardId { get; set; }
    }
}
