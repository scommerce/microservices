﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class PayloadResponse
    {
        public string Payload { get; set; }
    }
}
