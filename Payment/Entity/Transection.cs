﻿using Framework.BaseEntity;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class Transection:BaseEntityCusCode
    {
        public string timeStamp { get; set; }
        public string merchantID { get; set; }
        public string respCode { get; set; }
        public string pan { get; set; }
        public string amt { get; set; }
        public string uniqueTransactionCode { get; set; }
        public string tranRef { get; set; }
        public string approvalCode { get; set; }
        public string refNumber { get; set; }
        public string eci { get; set; }
        public string dateTime { get; set; }
        public string status { get; set; }
        public string failReason { get; set; }
        public string userDefined1 { get; set; }
        public string userDefined2 { get; set; }
        public string userDefined3 { get; set; }
        public string userDefined4 { get; set; }
        public string userDefined5 { get; set; }
        public string storeCardUniqueID { get; set; }
        public string ippPeriod { get; set; }
        public string ippInterestType { get; set; }
        public string ippInterestRate { get; set; }
        public string ippMerchantAbsorbRate { get; set; }
        public string paidChannel { get; set; }
        public string paidAgent { get; set; }
        public string paymentChannel { get; set; }
        public string backendInvoice { get; set; }
        public string issuerCountry { get; set; }
        public string issuerCountryA3 { get; set; }
        public string bankName { get; set; }
        public string cardType { get; set; }
        public string processBy { get; set; }
        public string paymentScheme { get; set; }
        public string rateQuoteID { get; set; }
        public string originalAmount { get; set; }
        public string fxRate { get; set; }
        public string currencyCode { get; set; }
    }
}
