﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class Card:BaseEntityCusCode
    {
        public string CardPan { get; set; }
        public string StoreCardUniqueID { get; set; }
        public long UserId { get; set; }
        public string Status { get; set; }
        public string ProcessBy { get; set; }
        public string CardType { get; set; }
    }
}
