﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Entity
{
    public class InternalTransection:BaseEntityCusCode
    {
        public string InvoiceNo { get; set; }
        public string PaymentRef { get; set; }
        public string PaymentDate { get; set; }
        public string PaymentStatus { get; set; }
        public bool Test { get; set; }

    }
}
