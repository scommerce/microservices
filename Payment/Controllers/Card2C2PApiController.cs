﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Jose;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Payment.DbContext;
using Payment.Entity;

namespace Payment.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class Card2C2PApiController : BaseController<DataContext>
    {
        [HttpGet]
        public async Task<IActionResult> GetCard(string ApiKey)
        {
            var u = await GetRepo().GetUserFromApiKey(ApiKey, "th");

            var Card = GetRepo().GetDataAll<Payment.Entity.Card>(n => n.UserId == u.UserId).ToList();
            return new ObjectResult(Card);
        }
        //private static readonly Encoding encoding = Encoding.UTF8;
        //[HttpGet]
        //[NotDataMap]
        //public async Task<IActionResult> Test()
        //{
        //    var payload = new Dictionary<string, object>();

        //    payload.Add("merchantID", "764764000003976");
        //    payload.Add("invoiceNo", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
        //    payload.Add("description", "Test");
        //    payload.Add("amount", 1.0);
        //    payload.Add("currencyCode", "THB");



        //    var secretKey = encoding.GetBytes("7A821B7792B631FF4168CE7B861D76FF04B12F5FB2F672EE23910506B3524381"); 
        //    string token = Jose.JWT.Encode(payload, secretKey, JwsAlgorithm.HS256);

        //   var data = await GetRepo().sendRequestCustom<PayloadResponse>("https://sandbox-pgw.2c2p.com/payment/4.1/PaymentToken", HttpMethod.Post, new
        //    {
        //        payload = token
        //    }, true);
        //    string res = Jose.JWT.Decode(data.Payload, secretKey);
        //    JObject json = JObject.Parse(res);

        //    var payload2 = new Dictionary<string, object>();

     

        //    string token1 = Jose.JWT.Encode(payload2, secretKey, JwsAlgorithm.HS256);

        //    var res1 = await GetRepo().sendRequest("https://sandbox-pgw.2c2p.com/payment/4.1/cardtokeninfo", HttpMethod.Post, new
        //    {
        //        paymentToken = json["paymentToken"],
        //        clientID= Guid.NewGuid().ToString().Replace("-", ""),
        //        en= "en"
        //    }, true);
        //   // string res2 = Jose.JWT.Decode(res1.Payload, secretKey);
          
        //    //  json[""]

        //    return new ObjectResult(json);



        //}
    }
}