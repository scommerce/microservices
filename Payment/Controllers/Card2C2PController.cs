﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security;
using System.Security.Cryptography;

using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using CertificatesToDBandBack;
using Framework.Helper;
using Framework.Startup;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.VisualBasic;
using MimeKit;
using MimeKit.Cryptography;
using OpenSSL.PrivateKeyDecoder;
using Payment.DbContext;
using Payment.Entity;
using Payment.Models;
using SinaptIQPKCS7;

namespace Payment.Controllers
{
    public class Card2C2PController : BaseWebController<DataContext>
    {
        public Card2C2PController(IHostingEnvironment hostingEnvironment)
        {
            env = hostingEnvironment;
        }
        public readonly IHostingEnvironment env;
        public IActionResult Index(string ApiKey)
        {
            List<SelectListItem> year = new List<SelectListItem>();
            var startYear = DateTime.Now.Year;
            for (var i = DateTime.Now.Year; i < startYear + 10; i++)
            {
                year.Add(new SelectListItem()
                {
                    Text = i.ToString(),
                    Value = i.ToString(),
                });
            }
            ViewData["ApiUser"] = ApiKey;
            ViewData["Year"] = year;
            return View();
        }
        private static readonly Encoding encoding = Encoding.UTF8;
        [HttpPost]
        public async Task<IActionResult> AddCard(CardModel card2C2P)
        {
            var u = await GetRepo().GetUserFromApiKey(card2C2P.ApiUser,"th");
            var payment = GetRepo().GetData<_2C2P>(n => n.Customer.CustomerCode == u.Cuscode);

            var s= "<PaymentRequest>";
		    s=s+ "<merchantID>"+ payment.MerchantID+ "</merchantID>";
            s = s + "<uniqueTransactionCode>"+DateTime.Now.ToString("yyyyMMddHHmmssffff")+"</uniqueTransactionCode>";
            s = s + "<desc>Test Credit</desc>";
            s = s + "<amt>000000000100</amt>";
            s = s + "<currencyCode>764</currencyCode>  "; 
               //  s = s + "<pan>XXXXXXXXXXXX"+ card2C2P.Card.Substring(card2C2P.Card.Length - 4, 4) + "</pan>  ";
            s = s + "<panCountry>TH</panCountry>";
            s = s + "<cardholderName>"+card2C2P.CardHolder+"</cardholderName>";
           
            s = s + "<userDefined1>"+ card2C2P.ApiUser+ "</userDefined1>";

            s = s + "<userDefined2>Test</userDefined2>";
            s = s + "<storeCard>Y</storeCard>";
            s = s + "<encCardData>"+ card2C2P.EncryptedCardInfo+ "</encCardData>";
            s = s + "</PaymentRequest>";
            var paymentPayload = Encrypt.Base64(s);
    
            var keyByte = encoding.GetBytes(payment.SecretKey);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                hmacsha256.ComputeHash(encoding.GetBytes(paymentPayload));


                string sig = ByteToString(hmacsha256.Hash);
                s = "<PaymentRequest>";
                s = s + "  <version>9.9</version>";
                s = s + "  <payload>" + paymentPayload + "</payload>";
                s = s + "  <signature>" + sig + "</signature>";
                s = s + "</PaymentRequest>";
                ViewData["payload"] = Encrypt.Base64(s);
                ViewData["url"] = payment.ApiUrl;
                return View();
            }
        }

         string ByteToString(byte[] buff)
        {
            string sbinary = "";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* hex format */
            return sbinary;
        }
        public async Task<IActionResult> Return(string paymentResponse)
        {
            var s = Decrypt.Base64(paymentResponse);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(s);
            var data = xmlDoc["PaymentResponse"]["payload"].InnerText;
            XmlDocument xmlDoc1 = new XmlDocument();
            xmlDoc1.LoadXml(Decrypt.Base64(data));
            var Transection = new Transection();
            PropertyInfo[] propInfos = typeof(Transection).GetProperties();
            foreach(var p in propInfos.ToList())
            {
                if (xmlDoc1["PaymentResponse"][p.Name] != null)
                    p.SetValue(Transection, xmlDoc1["PaymentResponse"][p.Name].InnerText);
            }
            var u = await GetRepo().GetUserFromApiKey(Transection.userDefined1, "th");


            Transection.Customer = await GetRepo().GetCustomer(u.Cuscode);
            GetRepo().Add(Transection);
            var Card = GetRepo().GetData<Card>(n => n.StoreCardUniqueID == Transection.storeCardUniqueID);
            if (Card == null)
            {
                Card = new Card();
                Card.CardPan = Transection.pan;
                Card.StoreCardUniqueID = Transection.storeCardUniqueID;
                Card.UserId = u.UserId;
                GetRepo().Add(Card);
            }
            // GetRepo().SaveChange();
          
            if (Transection.status == "A")
            {
              
                return View("PaymentSuccess");


            }
           
            return View("PaymentFail");
        }

        public async Task<IActionResult> ServerReturn(string paymentResponse)
        {
            var s = Decrypt.Base64(paymentResponse);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(s);
            var data = xmlDoc["PaymentResponse"]["payload"].InnerText;
            XmlDocument xmlDoc1 = new XmlDocument();
            xmlDoc1.LoadXml(Decrypt.Base64(data));
            var Transection = new Transection();
            PropertyInfo[] propInfos = typeof(Transection).GetProperties();
            foreach (var p in propInfos.ToList())
            {
                if (xmlDoc1["PaymentResponse"][p.Name] != null)
                    p.SetValue(Transection, xmlDoc1["PaymentResponse"][p.Name].InnerText);
            }
            var u = await GetRepo().GetUserFromApiKey(Transection.userDefined1, "th");


            Transection.Customer = await GetRepo().GetCustomer(u.Cuscode);
            GetRepo().Add(Transection);
            var Card = GetRepo().GetData<Card>(n =>n.UserId == u.UserId && n.CardPan == Transection.pan);
            var payment = GetRepo().GetData<_2C2P>(n => n.Customer.CustomerCode == u.Cuscode);
            if (Card == null)
            {
              
                Card = new Card();
                Card.CardPan = Transection.pan;
                Card.StoreCardUniqueID = Transection.storeCardUniqueID;
                Card.UserId = u.UserId;
                Card.ProcessBy = Transection.processBy;
                Card.Status = Transection.status;
                Card.CardType = Transection.cardType;
                GetRepo().Add(Card);
            }
            else
            {
                Card.StoreCardUniqueID = Transection.storeCardUniqueID;
                Card.ProcessBy = Transection.processBy;
                Card.Status = Transection.status;
                Card.CardType = Transection.cardType;
            }



            GetRepo().SaveChange();
            if (Transection.userDefined2 == "Test")
            {
                await VoidInvoice(Transection.uniqueTransactionCode, Transection.Customer.CustomerCode);
            }
            try
            {
                if (payment.ReturnUrl != null)
                {
                    await GetRepo().sendRequest(payment.ReturnUrl, HttpMethod.Post, new
                    {
                        OrderId = Transection.uniqueTransactionCode,
                        PaymentRef = Transection.tranRef,
                        Status = Transection.status
                    }, true, GetHeaderLang());
                }
            }catch(Exception ex)
            {

            }

            return Ok();
        }




        [HttpGet]
        public async Task<IActionResult> Payment([FromQuery]PaymentModel paymentModel)
        {
           
            var u = await GetRepo().GetUserFromApiKey(paymentModel.ApiKey, "th");
            var payment = GetRepo().GetData<_2C2P>(n => n.Customer.CustomerCode == u.Cuscode);
            var car = GetRepo().GetData<Card>(n => n.UserId == u.UserId && n.Id == paymentModel.CardId);

            var s = "<PaymentRequest>";
            s = s + "<merchantID>" + payment.MerchantID + "</merchantID>";
            s = s + "<uniqueTransactionCode>" + paymentModel.TransectionNo + "</uniqueTransactionCode>";

            s = s + "<desc>"+ paymentModel .Desc+ "</desc>";
            var am = paymentModel.Amount.ToString().Split(".");
            string Amt = "";
            if (am.Length > 1)
            {
                Amt = ("000000000000" + am[0]).Substring(("000000000000" + am[0]).Length-12, 12) + am[1];
            }
            else
            {
                Amt = ("000000000000" + am[0]).Substring(("000000000000" + am[0]).Length-10, 10)+"00";
            }
            s = s + "<amt>"+ Amt + "</amt>";
            s = s + "<currencyCode>764</currencyCode>  ";
            // s = s + "  <request3DS>N</request3DS>";
            s = s + "<request3DS>" + payment._3Ds + "</request3DS>";
            //  s = s + "<pan>XXXXXXXXXXXX"+ card2C2P.Card.Substring(card2C2P.Card.Length - 4, 4) + "</pan>  ";
            s = s + "<panCountry>TH</panCountry>";
           // s = s + "<cardholderName>" + card2C2P.CardHolder + "</cardholderName>";
            s = s + "<storeCard>Y</storeCard>";
            s = s + "<userDefined1>" + paymentModel.ApiKey + "</userDefined1>";
            s = s + "<storeCardUniqueID>" + car.StoreCardUniqueID + "</storeCardUniqueID>";

            s = s + "<encCardData>" + "00acvI+yUpsc7wR5G+UIjbEJlQOkubgX45UAAGHvE/3ZghGEUXiSXOdV2q3VMdp3sCOLZL1WVAQBniDJS7YKXIBc436YLV2fc9e2jejW+USJa8gK35j6VmJZvxbbC/W+QbcQtZ95aEqtQDwXwrbi8rKiHh2f7b7HC0Gp/Q/UAzYOh9s=U2FsdGVkX1+hI6qlfyg0N+pXH21GQ+y0JUPF2y6NiA9xv/C/QqiPgi1GL0pchl89" + "</encCardData>";
            
            s = s + "</PaymentRequest>";
            var paymentPayload = Encrypt.Base64(s);

            var keyByte = encoding.GetBytes(payment.SecretKey);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                hmacsha256.ComputeHash(encoding.GetBytes(paymentPayload));


                string sig = ByteToString(hmacsha256.Hash);
                s = "<PaymentRequest>";
                s = s + "  <version>9.9</version>";
                s = s + "  <payload>" + paymentPayload + "</payload>";
                s = s + "  <signature>" + sig + "</signature>";
                s = s + "</PaymentRequest>";
                ViewData["payload"] = Encrypt.Base64(s);
                ViewData["url"] = payment.ApiUrl;

                //List<KeyValuePair<string, string>> valuePairs = new List<KeyValuePair<string, string>>();
                //valuePairs.Add(new KeyValuePair<string, string>( "paymentRequest", Encrypt.Base64(s)));
           //     var data = await GetRepo().sendRequest(payment.ApiUrl+ "/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx", HttpMethod.Post, valuePairs, false, GetHeaderLang());

                return View();
            }
        }
        [HttpGet]
        public async Task<IActionResult> GetInvoice(string InvoiceNo,string Cuscode)
        {
            var payment = GetRepo().GetData<_2C2P>(n => n.Customer.CustomerCode == Cuscode);

            var keyByte = encoding.GetBytes(payment.SecretKey);
            using (var hmacsha256 = new HMACSHA1(keyByte))
            {
               
                hmacsha256.ComputeHash(encoding.GetBytes("3.4" + payment.MerchantID + "I" + InvoiceNo));


                string hash = ByteToString(hmacsha256.Hash);

                String xml = "<PaymentProcessRequest>";
                xml = xml + "<version>3.4</version>";
                xml = xml + "<merchantID>" + payment.MerchantID + "</merchantID>";
                xml = xml + "<invoiceNo>" + InvoiceNo + "</invoiceNo>";
                xml = xml + "<processType>I</processType>";
                xml = xml + "<hashValue>"+ hash + "</hashValue>";
                xml = xml + "</PaymentProcessRequest>";

                PKCS7 pKCS7 = new PKCS7();
                
                string payload = pKCS7.encryptMessage(xml, pKCS7.getPublicCert(env.ContentRootPath + "/Cert/" + payment.CertPath));
                List<KeyValuePair<string, string>> valuePairs = new List<KeyValuePair<string, string>>();
                valuePairs.Add(new KeyValuePair<string, string>( "paymentRequest", payload));
                var data = await GetRepo().sendRequest(payment.ApiUrl + "/2C2PFrontend/PaymentActionV2/PaymentAction.aspx", HttpMethod.Post, valuePairs, false, GetHeaderLang());
                var response = await data.Content.ReadAsStringAsync();


  
                var resString = pKCS7.decryptMessage(response,pKCS7.getPrivateCert(env.ContentRootPath + "/Cert/"+payment.PemPath, "2c2p"));
                var s = resString;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(s);
                
                var Transection = new Transection();
                PropertyInfo[] propInfos = typeof(Transection).GetProperties();
                foreach (var p in propInfos.ToList())
                {
                    if (xmlDoc["PaymentProcessResponse"][p.Name] != null)
                    {
                     //   if(propInfos.Where(n=>n.Name== xmlDoc["PaymentProcessResponse"][p.Name].InnerText))
                        p.SetValue(Transection, xmlDoc["PaymentProcessResponse"][p.Name].InnerText);
                    }
                }

                var t = "";

            }
            return Ok();
           

        }
        
        public async Task<IActionResult> VoidInvoice(string InvoiceNo, string Cuscode)
        {
            var payment = GetRepo().GetData<_2C2P>(n => n.Customer.CustomerCode == Cuscode);

            var keyByte = encoding.GetBytes(payment.SecretKey);
            using (var hmacsha256 = new HMACSHA1(keyByte))
            {

                hmacsha256.ComputeHash(encoding.GetBytes("3.4" + payment.MerchantID + "V" + InvoiceNo));


                string hash = ByteToString(hmacsha256.Hash);

                String xml = "<PaymentProcessRequest>";
                xml = xml + "<version>3.4</version>";
                xml = xml + "<merchantID>" + payment.MerchantID + "</merchantID>";
                xml = xml + "<invoiceNo>" + InvoiceNo + "</invoiceNo>";
                xml = xml + "<processType>V</processType>";
                xml = xml + "<hashValue>" + hash + "</hashValue>";
                xml = xml + "</PaymentProcessRequest>";

                PKCS7 pKCS7 = new PKCS7();

                string payload = pKCS7.encryptMessage(xml, pKCS7.getPublicCert(env.ContentRootPath + "/Cert/" + payment.CertPath));
                List<KeyValuePair<string, string>> valuePairs = new List<KeyValuePair<string, string>>();
                valuePairs.Add(new KeyValuePair<string, string>("paymentRequest", payload));
                var data = await GetRepo().sendRequest(payment.ApiUrl + "/2C2PFrontend/PaymentActionV2/PaymentAction.aspx", HttpMethod.Post, valuePairs, false, GetHeaderLang());
                var response = await data.Content.ReadAsStringAsync();

                Console.WriteLine("PemPath::" + env.ContentRootPath + "/Cert/" + payment.PemPath);
                Console.WriteLine("CertPath::"+env.ContentRootPath + "/Cert/" + payment.CertPath);

                var resString = pKCS7.decryptMessage(response, pKCS7.getPrivateCert(env.ContentRootPath + "/Cert/" + payment.PemPath, "2c2p"));
                var s = resString;

                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(s);

                var Transection = new Transection();
                PropertyInfo[] propInfos = typeof(Transection).GetProperties();
                foreach (var p in propInfos.ToList())
                {
                    if (xmlDoc["PaymentProcessResponse"][p.Name] != null)
                    {
                        //   if(propInfos.Where(n=>n.Name== xmlDoc["PaymentProcessResponse"][p.Name].InnerText))
                        p.SetValue(Transection, xmlDoc["PaymentProcessResponse"][p.Name].InnerText);
                    }
                }

                var t = "";

            }
            return Ok();
        }
        static List<string> Split(string str, int chunkSize)
        {
            var m= Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize)).ToList();
            if(str.Length % chunkSize > 0)
            {
                m.Add(str.Substring(m.Count()*chunkSize, str.Length % chunkSize));
            }
            return m;
        }
        private  byte[] UnPem(string pem)
        {
            // This is a shortcut that assumes valid PEM
            // -----BEGIN words-----\nbase64\n-----END words-----
            const string Dashes = "-----";
            int index0 = pem.IndexOf(Dashes);
            int index1 = pem.IndexOf('\n', index0 + Dashes.Length);
            int index2 = pem.IndexOf(Dashes, index1 + 1);

            return Convert.FromBase64String(pem.Substring(index1, index2 - index1));
        }

        public  byte[] GetBytesFromPEM(string pemString, PemStringType type)
        {
            string header; string footer;
            switch (type)
            {
                case PemStringType.Certificate:
                    header = "-----BEGIN CERTIFICATE-----";
                    footer = "-----END CERTIFICATE-----";
                    break;
                case PemStringType.RsaPrivateKey:
                    header = "-----BEGIN RSA PRIVATE KEY-----";
                    footer = "-----END RSA PRIVATE KEY-----";
                    break;
                default:
                    return null;
            }

            int start = pemString.IndexOf(header) + header.Length;
            int end = pemString.IndexOf(footer, start) - start;
            return Convert.FromBase64String(pemString.Substring(start, end));
        }
      



    }
}