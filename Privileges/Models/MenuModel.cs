﻿using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Models
{
    [ExtMenu(width =200, plain=true,child = "items")]
    public class MenuModel
    {

 
        public List<MenuItemModel> Menus { get; set; }
    }
}
