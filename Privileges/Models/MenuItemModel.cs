﻿using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Models
{
    [ExtMenuItem()]
    public class MenuItemModel
    {

        public string MenuName { get; set; }

        public string Link { get; set; }

        public List<MenuItemModel> Menus { get; set; }
    }
}
