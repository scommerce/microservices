﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Privileges.MicroServices
{
    public partial class updatePrivilege : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuApi_Audit_Privileges_Audit_PrivilegesId",
                table: "MenuApi");

            migrationBuilder.DropTable(
                name: "Audit_MenuAction");

            migrationBuilder.DropTable(
                name: "Audit_MenuApi");

            migrationBuilder.DropTable(
                name: "Audit_Privileges");

            migrationBuilder.DropTable(
                name: "Audit_UserPrivileges");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_MenuApi_Audit_PrivilegesId",
                table: "MenuApi");

            migrationBuilder.DropColumn(
                name: "Audit_PrivilegesId",
                table: "MenuApi");

            migrationBuilder.DropColumn(
                name: "Submenu",
                table: "MenuApi");

            migrationBuilder.AddColumn<long>(
                name: "ParentMenuId",
                table: "MenuApi",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserPrivilegesId",
                table: "BaseUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MenuApi_ParentMenuId",
                table: "MenuApi",
                column: "ParentMenuId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UserPrivilegesId",
                table: "BaseUsers",
                column: "UserPrivilegesId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_UserPrivileges_UserPrivilegesId",
                table: "BaseUsers",
                column: "UserPrivilegesId",
                principalTable: "UserPrivileges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuApi_MenuApi_ParentMenuId",
                table: "MenuApi",
                column: "ParentMenuId",
                principalTable: "MenuApi",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseUsers_UserPrivileges_UserPrivilegesId",
                table: "BaseUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuApi_MenuApi_ParentMenuId",
                table: "MenuApi");

            migrationBuilder.DropIndex(
                name: "IX_MenuApi_ParentMenuId",
                table: "MenuApi");

            migrationBuilder.DropIndex(
                name: "IX_BaseUsers_UserPrivilegesId",
                table: "BaseUsers");

            migrationBuilder.DropColumn(
                name: "ParentMenuId",
                table: "MenuApi");

            migrationBuilder.DropColumn(
                name: "UserPrivilegesId",
                table: "BaseUsers");

            migrationBuilder.AddColumn<long>(
                name: "Audit_PrivilegesId",
                table: "MenuApi",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Submenu",
                table: "MenuApi",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Audit_MenuAction",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Action = table.Column<int>(type: "int", nullable: false),
                    Allow = table.Column<bool>(type: "bit", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    MenuApi = table.Column<long>(type: "bigint", nullable: true),
                    Privileges = table.Column<long>(type: "bigint", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audit_MenuAction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Audit_MenuAction_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_MenuAction_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_MenuAction_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Audit_MenuApi",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApiName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    MenuName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Submenu = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audit_MenuApi", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Audit_MenuApi_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_MenuApi_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_MenuApi_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Audit_Privileges",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApproveId = table.Column<long>(type: "bigint", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    PrivilegesName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RequiredApprove = table.Column<bool>(type: "bit", nullable: false),
                    SubPrivileges = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audit_Privileges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Audit_Privileges_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_Privileges_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_Privileges_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Audit_UserPrivileges",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    Privileges = table.Column<long>(type: "bigint", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true),
                    User = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audit_UserPrivileges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Audit_UserPrivileges_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_UserPrivileges_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Audit_UserPrivileges_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastLogin = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true),
                    UserName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UserPrivilegesId = table.Column<long>(type: "bigint", nullable: true),
                    UserStatus = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_UserPrivileges_UserPrivilegesId",
                        column: x => x.UserPrivilegesId,
                        principalTable: "UserPrivileges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuApi_Audit_PrivilegesId",
                table: "MenuApi",
                column: "Audit_PrivilegesId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuAction_CreateUserId",
                table: "Audit_MenuAction",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuAction_CustomerId",
                table: "Audit_MenuAction",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuAction_UpdateUserId",
                table: "Audit_MenuAction",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuApi_CreateUserId",
                table: "Audit_MenuApi",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuApi_CustomerId",
                table: "Audit_MenuApi",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_MenuApi_UpdateUserId",
                table: "Audit_MenuApi",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_Privileges_CreateUserId",
                table: "Audit_Privileges",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_Privileges_CustomerId",
                table: "Audit_Privileges",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_Privileges_UpdateUserId",
                table: "Audit_Privileges",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_UserPrivileges_CreateUserId",
                table: "Audit_UserPrivileges",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_UserPrivileges_CustomerId",
                table: "Audit_UserPrivileges",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Audit_UserPrivileges_UpdateUserId",
                table: "Audit_UserPrivileges",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_CreateUserId",
                table: "User",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_CustomerId",
                table: "User",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UpdateUserId",
                table: "User",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserPrivilegesId",
                table: "User",
                column: "UserPrivilegesId");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuApi_Audit_Privileges_Audit_PrivilegesId",
                table: "MenuApi",
                column: "Audit_PrivilegesId",
                principalTable: "Audit_Privileges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
