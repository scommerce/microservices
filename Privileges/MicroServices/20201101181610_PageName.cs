﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Privileges.MicroServices
{
    public partial class PageName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PageName",
                table: "MenuApi",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PageName",
                table: "MenuApi");
        }
    }
}
