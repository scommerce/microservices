﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Enum
{
    public enum Action
    {
        Add,
        Delete,
        Update,
        View
    }
}
