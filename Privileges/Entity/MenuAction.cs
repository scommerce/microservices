﻿using Framework.BaseEntity;
using Privileges.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Entity
{
    public class MenuAction : BaseEntityCusCode
    {
        public virtual MenuApi MenuApi { get; set; }
        public virtual Privileges Privileges { get; set; }
        public Enum.Action Action { get; set; }
        [NotMapped]
        public string ActionName { get {
                switch (Action)
                {
                    case Enum.Action.Add: return "Add";
                    case Enum.Action.Delete: return "Delete";
                    case Enum.Action.Update: return "Update";
                    case Enum.Action.View: return "View";
                    default: return "Unknown";
                }
            } 
        }
        public bool Allow { get; set; }
    }
}
