﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Entity
{
    public class UserPrivileges : BaseEntityCusCode
    {
        public virtual List<BaseUser> User { get; set; }
        public virtual Privileges Privileges { get; set; }
    }
}
