﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Entity
{
    public class Privileges : BaseEntityCusCode
    {
        public string PrivilegesName { get; set; }
        public virtual List<MenuApi> MenuApi { get; set; }
        public bool RequiredApprove { get; set; }
        public long? ApproveId { get; set; }
        public string SubPrivileges { get; set; }
    }
}
