﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Entity
{
    public class MenuApi : BaseEntityCusCode
    {
        public string ApiName { get; set; }
        public string MenuName { get; set; }
        public virtual MenuApi ParentMenu { get; set; }
        public string PageName { get; set; }
        public string Link { get; set; }

        public long? PrivilegesId { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.ForeignKey("PrivilegesId")]
        public virtual Privileges Privileges { get; set; }
    }
}
