﻿using Privileges.Entity;
using Privileges.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.Controllers
{
    public static class PrivilegesHelper
    {
        public static List<MenuItemModel> SubMenu(List<MenuApi> menus, MenuApi parent, List<Entity.UserPrivileges> privileges)
        {
            List<MenuItemModel> ret = new List<MenuItemModel>();
            foreach (var m in menus.Where(n => n.ParentMenu == parent))
            {
                MenuItemModel menuModel = new MenuItemModel();
                var sumMenu = SubMenu(menus, m, privileges);
                if (sumMenu != null)
                {

                    menuModel.Menus = sumMenu;

                }
                if (privileges.Any(n => n.Privileges == m.Privileges))
                {
                    menuModel.Link = (m.PageName == null) ? m.Link : "/Page?PageName=" + m.PageName;
                    menuModel.MenuName = m.MenuName;

                }
                if (menuModel.MenuName != null || menuModel.Menus.Count > 0)
                {
                    menuModel.Link = (m.PageName == null) ? m.Link : "/Page?PageName=" + m.PageName;
                    menuModel.MenuName = m.MenuName;
                    ret.Add(menuModel);
                }
            }
            return ret;
        }

    }
}
