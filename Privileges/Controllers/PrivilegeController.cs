﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Privileges.DbContext;
using Privileges.Entity;
using Privileges.Models;

namespace Privileges.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PrivilegeController : BaseController<DataContext>
    {
        [NotDataMap]
        public async Task<IActionResult> GetMenuAsync(string ApiKey)
        {
            var user = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var userPrivileges = GetRepo().GetDataAll<UserPrivileges>(n => n.User.Any(s => s == user.BaseUser) && n.DeleteFlag == false).ToList();
            var menu = GetRepo().GetDataAll<MenuApi>(n => n.Customer == user.BaseCustomer).ToList();
            List<MenuItemModel> ret = new List<MenuItemModel>();
            foreach (var m in menu.Where(n=>n.ParentMenu==null && n.DeleteFlag==false))
            {
                MenuItemModel menuModel = new MenuItemModel();
                menuModel.Menus = PrivilegesHelper.SubMenu(menu,m, userPrivileges);

                if (userPrivileges.Any(n => n.Privileges == m.Privileges))
                {
                    
                    menuModel.MenuName = m.MenuName;
                    menuModel.Link = (m.PageName == null) ?  m.Link : "/Page?PageName=" + m.PageName;
                }

                if (menuModel.MenuName != null || menuModel.Menus.Count>0)
                {
                    menuModel.Link = (m.PageName == null) ? m.Link : "/Page?PageName=" + m.PageName;
                    menuModel.MenuName = m.MenuName;
                    ret.Add(menuModel);
                }
               
                
            }
            MenuModel mainMenu = new MenuModel();
            mainMenu.Menus = ret;

            var a = GetUI(mainMenu, "default");


            string outputJson = JsonConvert.SerializeObject(a);


            return new JavaScriptResult(outputJson);

        }
   
        
        public async Task<IActionResult> AddUserToRoleAsync(string ApiKey,long RoleId)
        {
           var user = await GetRepo().GetUserFromApiKey(ApiKey,"th");
            var pri = GetRepo().GetData<Entity.Privileges>(n => n.Id == RoleId && n.Customer == user.BaseCustomer);
            UserPrivileges userPrivileges = GetData<UserPrivileges>(n => n.Privileges == pri && n.Customer ==user.BaseCustomer);
            if (userPrivileges == null)
            {
                userPrivileges = new UserPrivileges();
                userPrivileges.Privileges = pri;
                userPrivileges.Customer = user.BaseCustomer;
                userPrivileges.User = new List<Framework.BaseEntity.BaseUser>();
            }
            userPrivileges.User.Add(user.BaseUser);
            GetRepo().Add(userPrivileges);
            GetRepo().SaveChange();
            return Ok();
        }
    }
}