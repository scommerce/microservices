﻿using Privileges.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Privileges.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        //public DbSet<Audit_MenuAction> Audit_MenuAction { get; set; }
        //public DbSet<Audit_MenuApi> Audit_MenuApi { get; set; }
        //public DbSet<Audit_Privileges> Audit_Privileges { get; set; }
        //public DbSet<Audit_UserPrivileges> Audit_UserPrivileges { get; set; }
        public DbSet<MenuAction> MenuAction { get; set; }
        public DbSet<MenuApi> MenuApi { get; set; }
        public DbSet<Entity.Privileges> Privileges { get; set; }
        //public DbSet<User> User { get; set; }
        public DbSet<UserPrivileges> UserPrivileges { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Privileges;User Id=sa;Password=P@ssword1234;";
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Entity.Privileges>().HasMany(n => n.MenuApi).WithOne(n => n.Privileges).HasForeignKey(n => n.PrivilegesId);
        }
    }
}
