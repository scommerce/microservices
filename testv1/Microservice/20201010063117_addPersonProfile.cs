﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace testv1.Microservice
{
    public partial class addPersonProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "BaseProperties",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "PersonId",
                table: "BaseProperties",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_PersonId",
                table: "BaseProperties",
                column: "PersonId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_People_PersonId",
                table: "BaseProperties",
                column: "PersonId",
                principalTable: "People",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseProperties_People_PersonId",
                table: "BaseProperties");

            migrationBuilder.DropIndex(
                name: "IX_BaseProperties_PersonId",
                table: "BaseProperties");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "BaseProperties");

            migrationBuilder.DropColumn(
                name: "PersonId",
                table: "BaseProperties");
        }
    }
}
