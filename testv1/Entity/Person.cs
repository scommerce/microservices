﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace testv1.Entity
{
    public class Person:BaseEntityCusCodeProperty
    {
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
