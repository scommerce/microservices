﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using testv1.DbContext;
using testv1.Entity;

namespace testv1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class testController : BaseController<DataContext>

    {
        [HttpGet]
        public IActionResult getData()
        {
            //var a = GetAll<Person>().ToList();
            //var a = GetAll<Person>().ToList();
            var a = GetData<Person>(n => n.Id == 1);
            return new ObjectResult(a);
        }
        [HttpGet]
        public IActionResult add(string firstname, string lastname)
        {
            if (firstname == null || firstname =="")
            {
                return BadRequest("ต้องใส่ชื่อจริง");
            }
            Person person = new Person();
            person.FirstName = firstname;
            person.LastName = lastname;
            person.CreateDate = DateTime.Now;
            GetRepo().Add(person);
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public IActionResult addObject(Person person)
        {
            person.CreateDate = DateTime.Now;
            GetRepo().Add(person);
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public IActionResult updateObject(Person person)
        {
            var oldPerson = GetData<Person>(n => n.Id == person.Id);
            oldPerson.FirstName = person.FirstName;
            oldPerson.LastName = person.LastName;
            oldPerson.UpdateDate = DateTime.Now;
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public IActionResult updateProperty([FromQuery]long id,List<BasePropertyModel> BasePropertyModels)
        {
            var data = GetData<Person>(n => n.Id == id);
            GetRepo().UpdateProperties(data, BasePropertyModels);
          //  GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public IActionResult getProperty()
        {
           var data = GetAll<Person>();
           var d = GetRepo().GetProperty(data.ToList());
           return new ObjectResult(d);
        }
        [HttpPost]
        public IActionResult getDataTable(BaseDataTable dataTable)
        {
            var data = GetAll<Person>().ToDataTable(dataTable);
            return new ObjectResult(data);
        }
        [HttpGet]
        public IActionResult getDataTableByGet(int start ,int length)
        {
            BaseDataTable dataTable = new BaseDataTable();
            dataTable.Start = start;
            dataTable.Length = length;
            var data = GetAll<Person>().ToDataTable(dataTable);
            return new ObjectResult(data);
        }
        [HttpGet]
        public async Task<IActionResult> add1(string apikey ,string firstname, string lastname)
        {
            if (firstname == null || firstname == "")
            {
                return BadRequest("ต้องใส่ชื่อจริง");
            }
            var user = await GetRepo().GetUserFromApiKey(apikey, "th");
            var data = await GetRepo().sendRequest<BaseDataTable>("http://login.wfamework.com", HttpMethod.Get, null, false, "th");
            Person person = new Person();
            person.FirstName = firstname;
            person.LastName = lastname;
            person.CreateDate = DateTime.Now;
            person.CreateUser = user.BaseUser;
            person.Customer = user.BaseCustomer; 
            GetRepo().Add(person);
            GetRepo().SaveChange();
            return Ok();
        }
    }
    

}
