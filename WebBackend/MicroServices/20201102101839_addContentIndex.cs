﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBackend.MicroServices
{
    public partial class addContentIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContentIndex",
                table: "WebConfigs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentIndex",
                table: "WebConfigs");
        }
    }
}
