﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBackend.MicroServices
{
    public partial class changeMainNameToPageName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainName",
                table: "WebConfigs");

            migrationBuilder.AddColumn<string>(
                name: "PageName",
                table: "WebConfigs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PageName",
                table: "WebConfigs");

            migrationBuilder.AddColumn<string>(
                name: "MainName",
                table: "WebConfigs",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
