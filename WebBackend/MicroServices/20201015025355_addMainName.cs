﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebBackend.MicroServices
{
    public partial class addMainName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MainName",
                table: "WebConfigs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainName",
                table: "WebConfigs");
        }
    }
}
