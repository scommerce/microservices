﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebBackend.Entity;

namespace WebBackend.DbContext
{
    public class DataContext : Framework.BaseContext.BaseDbContext
    {
        public DbSet<WebConfig> WebConfigs { get; set; }
        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_WebBackend;User Id=sa;Password=P@ssword1234;";
        }
    }
}
