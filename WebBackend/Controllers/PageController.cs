﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Mvc;
using WebBackend.DbContext;
using WebBackend.Entity;

namespace WebBackend.Controllers
{
    public class PageController : BaseWebController<DataContext>
    {
        public IActionResult Index(string Cuscode,string PageName)
        {
           
            if (PageName == null)
                return Problem();
            var data = GetRepo().GetData<WebConfig>(n => n.Customer.CustomerCode == Cuscode && n.PageName == PageName && n.DeleteFlag == false);
            data.MainIndex = data.MainIndex + "?PageName="+ PageName;
            return View(data);
        }

    }
}
