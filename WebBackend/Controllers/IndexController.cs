﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using WebBackend.DbContext;
using WebBackend.Entity;

namespace WebBackend.Controllers
{
    public class IndexController : BaseWebController<DataContext>
    {
        public IActionResult Index(string CusCode)
        {
            if (CusCode == null)
                return Problem();
            var data = GetRepo().GetData<WebConfig>(n => n.Customer.CustomerCode == CusCode && n.PageName == "Index" && n.DeleteFlag == false);
            if (data != null)
                return View(data);
            return Problem();

        }

        public IActionResult InitCuscode(string CusCode)
        {
            GetRepo().GetCustomer(CusCode);
            return Ok();
        }
    }
}
