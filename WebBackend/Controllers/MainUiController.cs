﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebBackend.DbContext;
using WebBackend.Entity;
using WebBackend.Models;

namespace WebBackend.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MainUiController : BaseController<DataContext>
    {
        [HttpGet]
        [NotDataMap]
        public IActionResult GetMainUI(string PageName,string theme = "default")
        {
            MainModel login = new MainModel();
            login.ContentName = PageName;
            var a = GetUI(login, theme);


            string outputJson = JsonConvert.SerializeObject(a);

            return CreateExtjs("Ext.MainUi", outputJson);
        }

        [HttpGet]
        [NotDataMap]
        public IActionResult GetTestUI(string theme = "default")
        {
            MainModel login = new MainModel();

            var a = GetUI(login, theme);


            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }
        [HttpGet]
        [NotDataMap]
        public async Task<IActionResult> GetUiUrlAsync(string PageName,string ApiKey,string Cuscode, string theme = "default")
        {

            if (PageName == null)
                return Problem();
            var data = GetRepo().GetData<WebConfig>(n => n.PageName == PageName && n.DeleteFlag == false);

            string url = data.ContentIndex;
            if (url.Contains("?"))
            {
                url = data.ContentIndex + "&ApiKey="+ ApiKey+ "&Cuscode="+ Cuscode;
            }
            else
            {
                url = data.ContentIndex + "?ApiKey=" + ApiKey + "&Cuscode=" + Cuscode;
            }

            var response = await GetRepo().sendRequest(url, HttpMethod.Get, null, false);
            return new JavaScriptResult(await response.Content.ReadAsStringAsync());
        }


    }
}
