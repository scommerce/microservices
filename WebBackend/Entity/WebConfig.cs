﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBackend.Entity
{
    public class WebConfig:BaseEntityCusCode
    {
        public string MainIndex { get; set; }
        public string PageName { get; set; }
        public string ContentIndex { get; set; }
    }
}
