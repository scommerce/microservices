﻿using Framework.BaseExtjs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Framework.BaseAttrib.ExtAttribute;

namespace WebBackend.Models
{
    [ExtJsMainPage(width = Screen.width, height = Screen.height,showUserName =true)]
    public class MainModel
    {
        public string ContentName { get; set; }
        public string ContetntTheme { get; set; } = "default";
    }
}
