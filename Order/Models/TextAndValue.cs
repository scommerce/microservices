﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class TextAndValue
    {
        public string text { get; set; }
        public int value { get; set; }
    }
}
