﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class ShopModel
    {
        public long Id { get; set; }
        public string ShopName { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public long ShopImageId { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public int Commission { get; set; }
        public TimeSpan CookingTime { get; set; }
        public string Shopcode { get; set; }

    }
}
