﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class CalculateModel
    {
        public List<NewOrderDetail> Price { get; set; }
        public long ShopId { get; set; }
        public string Discount { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
