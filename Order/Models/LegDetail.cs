﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class LegDetail
    {
        public TextAndValue distance { get; set; }
        public TextAndValue duration { get; set; }
        public string end_address { get; set; }
        public string start_address { get; set; }
    }
}
