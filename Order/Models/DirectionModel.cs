﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class DirectionModel
    {
        public List<RouteDetail> routes { get; set; }
    }
}
