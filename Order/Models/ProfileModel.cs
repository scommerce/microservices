﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class ProfileModel
    {
        public long UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Phone { get; set; }
        public string HouseNumber { get; set; }
        public string Alley { get; set; }
        public string Area { get; set; }
        public string Province { get; set; }
        public string PostBox { get; set; }
    }
}
