﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class NewOrder
    {
   
        public long ShopId { get; set; }
        public string BuyerName { get; set; }
        public string BuyerTel { get; set; }
        public string BuyerLat { get; set; }
        public string BuyerLng { get; set; }
        public long BuyerId { get; set; }
        public string Addtional { get; set; }
        public string ApiKey { get; set; }
        public string AddressDesc { get; set; }
        public string PaymentType { get; set; }
        public  List<NewOrderDetail> NewOrderDetail { get; set; }
    }
}
