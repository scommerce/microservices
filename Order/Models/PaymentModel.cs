﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class PaymentModel
    {
        public string PaymentRef { get; set; }
        public string OrderNo { get; set; }
        public string PaymentStatus { get; set; }
    }
}
