﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Models
{
    public class RiderDetailModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CPRName { get; set; }
        public long CPRProvinceId { get; set; }

        public User CreateUser { get; set; }

        public Customer Customer { get; set; }

    }
}
