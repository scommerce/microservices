﻿using Framework.BaseEntity;
using Order.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Entity
{
    public class LinglomOrder : BaseEntityCusCode
    {
        public string OrderNo { get; set; }
        public long ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopTel { get; set; }
        public long ShopImage { get; set; }
        public string ShopAddress { get; set; }
        public double ShopLat { get; set; }
        public double ShopLng { get; set; }
        public string BuyerName { get; set; }
        public string BuyerTel { get; set; }
        public string BuyerLat { get; set; }
        public string BuyerLng { get; set; }
        public string BuyerAddress { get; set; }
        public long BuyerId { get; set; }
        public long? RiderId { get; set; }
        public string RiderName { get; set; }
        public string RiderLPR { get; set; }
        public long? RiderProvince { get; set; }
        public OrderStatus OrderStatus { get; set; }
        [NotMapped]
        public string OrderStatusName
        {
            get
            {

                switch (OrderStatus)
                {
                    case OrderStatus.Cancel: return "Cancel";
                    case OrderStatus.New: return "New";
                    case OrderStatus.Receive: return "Receive";
                    case OrderStatus.FoodComplate: return "FoodComplate";
                    case OrderStatus.Sending: return "Sending";
                    case OrderStatus.Complate: return "Complate";
                    default: return "Unknown";
                }
            }
        }
        public DateTime OrderDate { get; set; }
        public DateTime? OrderDateReceive { get; set; }
        public DateTime? OrderDateFinish { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? SendDateFinish { get; set; }
        public double Service { get; set; }
        public double Amount { get; set; }
        public double AmontCommission { get; set; }
        public double AmountAfter { get; set; }
        public double VatPercent { get; set; }
        public double Vat { get; set; }
        public double AmountNet { get; set; }
        public double AmountNetReturn { get; set; }
        [NotMapped]
        public double Profit
        {
            get
            {
                return ((Amount * 0.20) < 20) ? 20 : ((Amount * 0.20) > 60) ? 60 : (Amount * 0.20);
            }
        }

        public string PaymentRef { get; set; }
        public PaymentType PaymentType { get; set; }
        [NotMapped]
        public string PaymentTypeName {
            get
            {
                switch (PaymentType)
                {
                    case PaymentType.Credit_Debit: return "Credit_Debit";
                    case PaymentType.Cash: return "Cash";
                    default: return "Unknown";
                }
            }
        }
        public string Addtional { get; set; }
        public string AddressDesc {get; set;}
        public TimeSpan CookingTime { get; set; }
        public virtual List<LinglomOrderDetail> LinglomOrderDetails { get; set; }

    }
}
