﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Entity
{
    public class LinglomOrderDetail:BaseEntity
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string Detail { get; set; }
        public double Price { get; set; }
        public int Qty { get; set; }
        public string Addtional { get; set; }
    }
}
