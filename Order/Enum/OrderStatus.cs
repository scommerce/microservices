﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Order.Enum
{
    public enum OrderStatus
    {
        Cancel,
        New,
        Receive,
        FoodComplate,
        Sending,
        Complate,
        WaitPayment
        
    }
}
