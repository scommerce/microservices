﻿using Order.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Order.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.LinglomOrder> LinglomOrders { get; set; }
        public DbSet<LinglomOrderDetail> LinglomOrderDetails { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Order;User Id=sa;Password=P@ssword1234;";
        }
    }
}
