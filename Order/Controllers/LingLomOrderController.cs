﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Castle.DynamicProxy.Generators;
using Framework.BaseEntity;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Order.DbContext;
using Order.Entity;
using Order.Enum;
using Order.Models;

namespace Order.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LingLomOrderController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> NewOrder(NewOrder newOrder)
        {

            var key = await GetRepo().GetUserFromApiKey(newOrder.ApiKey, GetHeaderLang());
            if (key ==null)
            {
                return Problem("Data is null");
            }
            var user = GetRepo().GetData<BaseUser>(n=>n.UserName == key.UserName && n.DeleteFlag == false);
            if (user == null) {
                BaseUser baseUser = new BaseUser();
                baseUser.UserName = key.UserName;
                baseUser.BaseUserId = key.UserId;
                baseUser.Customer = await GetRepo().GetCustomer(key.Cuscode);

                GetRepo().Add(baseUser);
                GetRepo().SaveChange();
            }
            var Login = GetRepo().GetMicroServices("Login");
            var Profile = await GetRepo().sendRequest<ProfileModel>(Login.ServiceIp + "/api/User/GetProfile?ApiKey=" + newOrder.ApiKey, HttpMethod.Get, null, false);
            
            LinglomOrder linglomOrder = new LinglomOrder();
            linglomOrder.Addtional = newOrder.Addtional;
            linglomOrder.AddressDesc = newOrder.AddressDesc;
            linglomOrder.CreateUser = GetRepo().GetData<BaseUser>(n => n.UserName == key.UserName && n.DeleteFlag == false);
            linglomOrder.BuyerId = key.UserId;
            linglomOrder.BuyerLat = newOrder.BuyerLat;
            linglomOrder.BuyerLng = newOrder.BuyerLng;
            linglomOrder.BuyerName = Profile.Firstname + " " + Profile.Lastname;
            linglomOrder.BuyerTel = Profile.Phone;
            linglomOrder.BuyerAddress = Profile.HouseNumber + " " +Profile.Alley + " " + Profile.Area + " " + Profile.Province + " " + Profile.PostBox; 
            linglomOrder.OrderDate = DateTime.Now;
            linglomOrder.OrderNo = "L-INV-" + DateTime.Now.ToString("ddMMyyyyHHmmssffff");
            linglomOrder.CookingTime = new TimeSpan(0, 0, 15, 0, 0);
            if (newOrder.PaymentType == "Credit")
            {
                linglomOrder.OrderStatus = Enum.OrderStatus.WaitPayment;
            }
            else linglomOrder.OrderStatus = Enum.OrderStatus.New;

            if (newOrder.PaymentType == "Cash")
            {
                linglomOrder.PaymentType = Enum.PaymentType.Cash;
            }
            else if (newOrder.PaymentType == "Credit")
            {
                linglomOrder.PaymentType = Enum.PaymentType.Credit_Debit;
            }
            var microServices = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(microServices.ServiceIp + "/api/LinglomShop/GetShopById?shop=" + newOrder.ShopId, HttpMethod.Get, null, false);
            
            linglomOrder.ShopId = shop.Id;
            linglomOrder.ShopAddress = shop.Address;
            linglomOrder.ShopImage = shop.ShopImageId;
            linglomOrder.ShopLat = shop.Lat;
            linglomOrder.ShopLng = shop.Lng;
            linglomOrder.ShopName = shop.ShopName;
            linglomOrder.ShopTel = shop.Tel;

            var s = await GetRepo().sendRequestCustom<DirectionModel>("https://maps.googleapis.com/maps/api/directions/json?origin=" + shop.Lat + "," + shop.Lng + "&destination=" + linglomOrder.BuyerLat + "," + linglomOrder.BuyerLng + "&key=AIzaSyAtH53jFKMqpcYtiyxq6W4PolYLIaOyoDc", HttpMethod.Get, null, false);
            if (shop.Commission == 0)
                linglomOrder.Service = 37;
            else if (shop.Commission >= 15 || shop.Commission <= 19)
                linglomOrder.Service = 20;
            else if (shop.Commission >= 20 || shop.Commission <= 24)
                linglomOrder.Service = 15;
            else if (shop.Commission >= 25 && linglomOrder.Amount < 100)
                linglomOrder.Service = 10;
            else
                linglomOrder.Service = 0;


            var dis = Math.Floor(s.routes[0].legs[0].distance.value / 1000.0);
            for (var i = 0; i <= dis; i++)
            {
                if (i > 3 && i <= 5)
                {
                    linglomOrder.Service = linglomOrder.Service + 10;
                }
                else if (i > 5 && i <= 9)
                {
                    linglomOrder.Service = linglomOrder.Service + 15;
                }
                else if (i > 9)
                {
                    linglomOrder.Service = linglomOrder.Service + 20;
                }
            }


            linglomOrder.Amount = newOrder.NewOrderDetail.Sum(n => n.Price * n.Qty);
            if (linglomOrder.Amount >= 1000 && linglomOrder.PaymentType == Enum.PaymentType.Cash)
            {
                return Problem("You must pay by Credit Card or Debit Card");
            }
            var data = GetRepo().GetData<LinglomOrder>(n => n.DeleteFlag == false && n.CreateUser.UserName == key.UserName);
            if(data != null)
            {
                //TODO:Discount
            }
            var Commission = Math.Round(((linglomOrder.Amount * (shop.Commission / 100.0))), 2, MidpointRounding.AwayFromZero);
            //Todo : Set vat in Setting
            linglomOrder.VatPercent = 7;

            linglomOrder.Vat = Math.Round((Commission * 0.07), 2, MidpointRounding.AwayFromZero);
            linglomOrder.AmontCommission = Commission;

            linglomOrder.AmountAfter = linglomOrder.Amount - Commission - linglomOrder.Vat;

            linglomOrder.AmountNet = linglomOrder.Amount + linglomOrder.Service;

            linglomOrder.AmountNetReturn = Commission + linglomOrder.Vat + linglomOrder.Service;

            linglomOrder.LinglomOrderDetails = new List<LinglomOrderDetail>();
            foreach (var o in newOrder.NewOrderDetail)
            {
                LinglomOrderDetail linglomOrderDetail = new LinglomOrderDetail()
                {
                    Addtional = o.Addtional,
                    Detail = o.Detail,
                    Price = o.Price,
                    ProductId = o.ProductId,
                    ProductName = o.ProductName,
                    Qty = o.Qty,
                    CreateDate = DateTime.Now

                };
                linglomOrder.LinglomOrderDetails.Add(linglomOrderDetail);
            }

            GetRepo().Add(linglomOrder);
            GetRepo().SaveChange();
            var ShopMessageService = GetRepo().GetMicroServices("Login");
          
            string a = ShopMessageService.ServiceIp + "/api/Linglom/GetTokenFireBaseFromShopCode?ShopCode=" + shop.Shopcode+"&Cuscode=Linglom-001";
            var firebaseKey = await GetRepo().sendRequest<BaseApiToken>(a, HttpMethod.Get, null, false);

            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");

            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = newOrder.ApiKey,
                ToToken=firebaseKey.Token,
                Title="ร้านค้าลิงลม",
                Message="ร้านค้าลิงลม มีออเดอร์เข้ากรุณาตรวจสอบรายการ",
                //Topic="NewOrder"
            }, true) ;
            //var riderMicroService = GetRepo().GetMicroServices("Rider");
            //try
            //{
            //    //var rider = await GetRepo().sendRequest<RiderModel>(riderMicroService.ServiceIp + "/api/Rider/GetFirstRiderFromLocation", HttpMethod.Post, new
            //    //{
            //    //    Lat = shop.Lat,
            //    //    Lng = shop.Lng
            //    //}, true);
            //    //linglomOrder.RiderId = rider.Rider.Id;
            //    //linglomOrder.RiderName = rider.Rider.FirstName + " " + rider.Rider.LastName;
            //    //linglomOrder.RiderLPR = rider.Rider.CPRName;
            //    //linglomOrder.RiderProvince = rider.Rider.CPRProvinceId;

            //    //GetRepo().Update(linglomOrder);
            //    //GetRepo().SaveChange();


            //    var riderMessageMicroService = GetRepo().GetMicroServices("Login");
            //    string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode;
            //    var firebase = await GetRepo().sendRequest<Firebase>(riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUser?UserName=" + rider.Rider.CreateUser.UserName + "&Cuscode=" + rider.Rider.Customer.CustomerCode, HttpMethod.Get, null, true);
            //    GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //    {

            //        registration_ids = new string[] { firebase.Token },
            //        notification = new
            //        {
            //            title = "ลิงลม รับ Order",
            //            body = "มีรายการสั่งซื้อรหัส " + linglomOrder.OrderNo + " กรุณาตรวจสอบภายรายการ",
            //            mutable_content = true,
            //            sound = "Tri-tone"
            //        }
            //    }, true);

            //}
            //catch (Exception ex)
            //{

            //}


            return new ObjectResult(new {
                OrderNo = linglomOrder.OrderNo,
            });

        }
        [HttpGet]
        public async Task<IActionResult> GetCurrentOrder(string ApiKey)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            if (rider == null)
                return Problem("Rider Not Found");
            var order = GetRepo().GetData<LinglomOrder>(n => n.DeleteFlag == false && n.RiderId == rider.Id && (n.OrderStatus != Enum.OrderStatus.Complate && n.OrderStatus != Enum.OrderStatus.Cancel));
            return new ObjectResult(order);
        }

        [HttpGet]
        public async Task<IActionResult> GetSummaryToday(string ApiKey)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            if (rider == null)
                return Problem("Rider Not Found");
            var order = GetRepo().GetDataAll<LinglomOrder>(n => n.RiderId == rider.Id && n.DeleteFlag == false && n.OrderStatus == Enum.OrderStatus.Complate);

            return new ObjectResult(new
            {
                SumToday = order.Where(n => n.SendDateFinish.Value.Date == DateTime.Today).Sum(n => n.AmountNet),
                ProfitToday = order.Where(n => n.SendDateFinish.Value.Date == DateTime.Today).ToList().Sum(n => n.Profit),
                ReturnToday = order.Where(n => n.SendDateFinish.Value.Date == DateTime.Today).Sum(n => n.AmountNetReturn)
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetSummaryHistory(string ApiKey)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            if (rider == null)
                return Problem("Rider Not Found");
            var order = GetRepo().GetDataAll<LinglomOrder>(n => n.RiderId == rider.Id && n.DeleteFlag == false);


            var date = DateTime.Now;
            List<object> l = new List<object>();
            for (int i = 0; i < 7; i++)
            {
                var d = DateTime.Now.AddDays((-1 * i)).Date;
                l.Add(new
                {
                    Date = d.ToString("yyyy-MM-dd"),
                    SumToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).Sum(n => n.AmountNet),
                    ProfitToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).ToList().Sum(n => n.Profit),
                    ReturnToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).Sum(n => n.AmountNetReturn),
                    Deatil = order.Where(n => n.OrderDate.Date == d).OrderByDescending(n => n.Id).ToList()
                });
            }


            return new ObjectResult(l);
        }

        [HttpGet]
        public async Task<IActionResult> GetSummaryHistoryFromDate(string ApiKey, DateTime Date)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            if (rider == null)
                return Problem("Rider Not Found");
            var order = GetRepo().GetDataAll<LinglomOrder>(n => n.RiderId == rider.Id && n.DeleteFlag == false);





            var d = Date.Date;





            return new ObjectResult(new
            {
                Date = d.ToString("yyyy-MM-dd"),
                SumToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).Sum(n => n.AmountNet),
                ProfitToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).ToList().Sum(n => n.Profit),
                ReturnToday = order.Where(n => n.OrderStatus == Enum.OrderStatus.Complate && n.SendDateFinish.Value.Date == d).Sum(n => n.AmountNetReturn),
                Deatil = order.Where(n => n.OrderDate.Date == d).OrderByDescending(n => n.Id).ToList()
            });
        }


        [HttpPost]
        public async Task<IActionResult> ReceiveOrder(OrderModel orderModel)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            var order = GetRepo().GetData<LinglomOrder>(n => n.RiderId == rider.Id && n.OrderNo == orderModel.OrderId && n.OrderStatus == Enum.OrderStatus.FoodComplate);
            if (order == null)
            {
                return Problem("ไม่สามารถกดรับได้ เนื่องจากอาหารยังไม่เสร็จ หรือ Order อยู่ในสถานะอื่น");
            }

            order.OrderStatus = Enum.OrderStatus.Sending;
            order.SendDate = DateTime.Now;
            GetRepo().Update(order);
            GetRepo().SaveChange();
            await GetRepo().sendRequest(riderMicroService.ServiceIp + "/api/Rider/RiderRuning?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);

            var riderMessageMicroService = GetRepo().GetMicroServices("Login");

            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUserId?userId=" + order.BuyerId;
            var firebase = await GetRepo().sendRequest<Firebase>(a, HttpMethod.Get, null, true);
            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");
            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = orderModel.ApiKey,
                ToToken = firebase.Token,
                Title = "รับ Order",
                Message = "ลิงลมรับอาหารแล้ว กรุณาเตรียมตัว",
                //Topic="NewOrder"
            }, true);

            //GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //{

            //    registration_ids = new string[] { firebase.Token },
            //    notification = new
            //    {
            //        title = "รับ Order",
            //        body = "ลิงลมรับอาหารแล้ว กรุณาเตรียมตัว",
            //        mutable_content = true,
            //        sound = "Tri-tone"
            //    }
            //}, true);

            return new ObjectResult(order);
        }
        [HttpPost]
        public async Task<IActionResult> FinishOrder(OrderModel orderModel)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            var order = GetRepo().GetData<LinglomOrder>(n => n.RiderId == rider.Id && n.OrderNo == orderModel.OrderId && n.OrderStatus == Enum.OrderStatus.Sending);
            if (order == null)
            {
                return Problem("ไม่สามารถส่งอาหารได้ กรุณาลองใหม่อีกครั้ง");
            }

            order.OrderStatus = Enum.OrderStatus.Complate;
            order.SendDateFinish = DateTime.Now;
            GetRepo().Update(order);
            GetRepo().SaveChange();
            await GetRepo().sendRequest(riderMicroService.ServiceIp + "/api/Rider/RiderWorking?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);

            var riderMessageMicroService = GetRepo().GetMicroServices("Login");

            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUserId?userId=" + order.BuyerId;
            var firebase = await GetRepo().sendRequest<Firebase>(a, HttpMethod.Get, null, true);
            //GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //{

            //    registration_ids = new string[] { firebase.Token },
            //    notification = new
            //    {
            //        title = "รับ Order",
            //        body = "ลิงลมส่งอาหารให้คุณแล้ว",
            //        mutable_content = true,
            //        sound = "Tri-tone"
            //    }
            //}, true);

            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");
            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = orderModel.ApiKey,
                ToToken = firebase.Token,
                Title = "รับ Order",
                Message = "ลิงลมส่งอาหารให้คุณแล้ว",
                //Topic="NewOrder"
            }, true);

            return new ObjectResult(order);
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderCustomerOrdering(string ApiKey)
        {
            if (ApiKey == null)
            {
                return Problem();
            }
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            if(key == null)
            {
                return Problem();
            }
            var order = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.OrderStatus != Enum.OrderStatus.Cancel && n.OrderStatus != Enum.OrderStatus.Complate && n.CreateUser.UserName == key.UserName).OrderByDescending(n=>n.OrderDate).ToList();
            List<object> list = new List<object>();
            foreach (var item in order)
            {
                
                list.Add(new { 
                    ShopId = item.ShopId,
                    ShopImage = item.ShopImage,
                    ShopName = item.ShopName,
                    OrderNo = item.OrderNo,
                    OrderDate = item.OrderDate,
                    OrderStatus = (int)item.OrderStatus,
                    Amount = item.AmountNet,
                    RiderName = item.RiderName,
                    Addtional = item.Addtional, 
                    PaymentType = item.PaymentTypeName,
                    Detail = item.LinglomOrderDetails.ToList()
                });
            }
            
            return new ObjectResult(list);

        }

        [HttpGet]
        public async Task<IActionResult> GetOrderCustomerCancel(string ApiKey)
        {
            if (ApiKey == null)
            {
                return Problem();
            }
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            if (key == null)
            {
                return Problem();
            }
            var order = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.OrderStatus == Enum.OrderStatus.Cancel  && n.CreateUser.UserName == key.UserName).OrderByDescending(n => n.OrderDate).ToList();
            List<object> list = new List<object>();
            foreach (var item in order)
            {

                list.Add(new
                {
                    ShopId = item.ShopId,
                    ShopImage = item.ShopImage,
                    ShopName = item.ShopName,
                    OrderNo = item.OrderNo,
                    OrderDate = item.OrderDate,
                    OrderStatus = (int)item.OrderStatus,
                    Amount = item.AmountNet,
                    RiderName = item.RiderName,
                    Addtional = item.Addtional,
                    Detail = item.LinglomOrderDetails.ToList()
                });
            }
            return new ObjectResult(list);

        }

        [HttpGet]
        public async Task<IActionResult> GetOrderCustomerComplete(string ApiKey)
        {
            if (ApiKey == null)
            {
                return Problem();
            }
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            if (key == null)
            {
                return Problem();
            }
            var order = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.OrderStatus == Enum.OrderStatus.Complate && n.CreateUser.UserName == key.UserName).OrderByDescending(n => n.OrderDate).ToList();
            List<object> list = new List<object>();
            foreach (var item in order)
            {

                list.Add(new
                {
                    ShopId = item.ShopId,
                    ShopImage = item.ShopImage,
                    ShopName = item.ShopName,
                    OrderNo = item.OrderNo,
                    OrderDate = item.OrderDate,
                    OrderStatus = (int)item.OrderStatus,
                    Amount = item.AmountNet,
                    RiderName = item.RiderName,
                    Addtional = item.Addtional,
                    Detail = item.LinglomOrderDetails.ToList()
                });
            }
            return new ObjectResult(list);

        }


        [HttpGet]
        public async Task<IActionResult> GetOrderFromShop(string ApiKey)
        {
            //todo แก้ shopid
            // var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            //var shopId = 6;
            //  var MicroService = GetRepo().GetMicroServices("Login");
            //  var Userdata = await GetRepo().sendRequest(MicroService.ServiceIp + "/api/User/GetFromUserName?Username=" + key.UserName + "&Cuscode=" + key.Cuscode, HttpMethod.Get, null, false);

            // var response = JsonConvert.DeserializeObject<User>(await Userdata.Content.ReadAsStringAsync());

            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);


            var order = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.OrderStatus == Enum.OrderStatus.New && n.ShopId == shop.Id ).ToList();
            List<object> l = new List<object>();
            foreach (var item in order)
            {
                l.Add( new {
                    OrderNo = item.OrderNo,
                    PaymentType = item.PaymentTypeName,
                    SumPrice = Math.Round(item.AmountAfter,2),
                    Cash = Math.Round(order.Where(c => c.PaymentType == Enum.PaymentType.Cash && c.OrderNo == item.OrderNo).Sum(c => c.Amount),2),
                    Credit = Math.Round(order.Where(c => c.PaymentType == Enum.PaymentType.Credit_Debit && c.OrderNo == item.OrderNo).Sum(c => c.Amount),2),
                    GPWithVat = Math.Round(item.Vat + item.AmontCommission,2),
                    LossGP = Math.Round(item.AmontCommission,2),
                    Data = item.LinglomOrderDetails.ToList(),
                    Time = item.CookingTime,
                    CookingTime = item.OrderDate + item.CookingTime,
                    GPnum = shop.Commission
                });
            }
            return new ObjectResult(l); 
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderCookingFromShop(string ApiKey)
        {
            //  todo แก้ shopid

             var MicroService = GetRepo().GetMicroServices("LinglomShop");
             var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);


            var order = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.OrderStatus == Enum.OrderStatus.Receive && n.ShopId == shop.Id).ToList();
            List<object> l = new List<object>();
            foreach (var item in order)
            {
                l.Add(new
                {
                    OrderNo = item.OrderNo,
                    PaymentType = item.PaymentTypeName,
                    SumPrice = Math.Round(item.AmountAfter,2),
                    Cash = Math.Round(order.Where(c => c.PaymentType == Enum.PaymentType.Cash && c.OrderNo == item.OrderNo).Sum(c => c.Amount), 2),
                    Credit = Math.Round(order.Where(c => c.PaymentType == Enum.PaymentType.Credit_Debit && c.OrderNo == item.OrderNo).Sum(c => c.Amount), 2),
                    GPWithVat = Math.Round(item.Vat + item.AmontCommission, 2),
                    LossGP = Math.Round(item.AmontCommission, 2),
                    Data = item.LinglomOrderDetails.ToList(),
                    Time = item.CookingTime,
                    CookingTime = item.OrderDate + item.CookingTime,
                    GPnum = shop.Commission
                });
            }
            return new ObjectResult(l);
        }


        [HttpPost]
        public async Task<IActionResult> FoodReceiveOrder(OrderModel orderModel)
        {
            //   var riderMicroService = GetRepo().GetMicroServices("Rider");
            // var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            //todo แก้ shopid
            var shopId = 6;
            var order = GetRepo().GetData<LinglomOrder>(n =>  n.OrderNo == orderModel.OrderId && n.OrderStatus == Enum.OrderStatus.New);
            if (order == null)
            {
                return Problem("ไม่สามารถกดรับได้  Order อยู่ในสถานะอื่น");
            }

            order.OrderStatus = Enum.OrderStatus.Receive;
            order.SendDate = DateTime.Now;
            GetRepo().Update(order);
            GetRepo().SaveChange();


            var riderMessageMicroService = GetRepo().GetMicroServices("Login");

            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUserId?userId=" + order.BuyerId;
            var firebase = await GetRepo().sendRequest<Firebase>(a, HttpMethod.Get, null, true);
            //GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //{

            //    registration_ids = new string[] { firebase.Token },
            //    notification = new
            //    {
            //        title = "รับ Order",
            //        body = "ร้าน "+order.ShopName+" รับออเดอร์แล้วกรุณารอสักครู่",
            //        mutable_content = true,
            //        sound = "Tri-tone"
            //    }
            //}, true);

            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");
            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = orderModel.ApiKey,
                ToToken = firebase.Token,
                Title = "รับ Order",
                body = "ร้าน " + order.ShopName + " รับออเดอร์แล้วกรุณารอสักครู่",
                //Topic="NewOrder"
            }, true);

            // await GetRepo().sendRequest(riderMicroService.ServiceIp + "/api/Rider/RiderRuning?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            return new ObjectResult(order);
        }


        [HttpPost]
        public async Task<IActionResult> FoodRejectOrder(OrderModel orderModel)
        {
            //   var riderMicroService = GetRepo().GetMicroServices("Rider");
            // var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            //todo แก้ shopid
            var shopId = 6;
            var order = GetRepo().GetData<LinglomOrder>(n => n.OrderNo == orderModel.OrderId && n.OrderStatus == Enum.OrderStatus.New);
            if (order == null)
            {
                return Problem("ไม่สามารถกดรับได้  Order อยู่ในสถานะอื่น");
            }

            order.OrderStatus = Enum.OrderStatus.Cancel;
            order.SendDate = DateTime.Now;
            GetRepo().Update(order);
            GetRepo().SaveChange();

            var riderMessageMicroService = GetRepo().GetMicroServices("Login");
            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUserId?userId=" + order.BuyerId;
            var firebase = await GetRepo().sendRequest<Firebase>(a, HttpMethod.Get, null, true);
            //GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //{

            //    registration_ids = new string[] { firebase.Token },
            //    notification = new
            //    {
            //        title = "รับ Order",
            //        body = "ร้าน " + order.ShopName + " ปฏิเสธออเดอร์",
            //        mutable_content = true,
            //        sound = "Tri-tone"
            //    }
            //}, true);

            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");
            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = orderModel.ApiKey,
                ToToken = firebase.Token,
                Title = "รับ Order",
                body = "ร้าน " + order.ShopName + " ปฏิเสธออเดอร์",
                //Topic="NewOrder"
            }, true);

            // await GetRepo().sendRequest(riderMicroService.ServiceIp + "/api/Rider/RiderRuning?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            return new ObjectResult(order);
        }

        [HttpPost]
        public async Task<IActionResult> FoodFinishOrder(OrderModel orderModel)
        {
            //   var riderMicroService = GetRepo().GetMicroServices("Rider");
            // var rider = await GetRepo().sendRequest<RiderDetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            //todo แก้ shopid
            var shopId = 6;
            var order = GetRepo().GetData<LinglomOrder>(n =>  n.OrderNo == orderModel.OrderId && n.OrderStatus == Enum.OrderStatus.Receive);
            if (order == null)
            {
                return Problem("ไม่สามารถกดรับได้  Order อยู่ในสถานะอื่น");
            }

            order.OrderStatus = Enum.OrderStatus.FoodComplate;
            order.SendDate = DateTime.Now;
            GetRepo().Update(order);
            GetRepo().SaveChange();


            var riderMessageMicroService = GetRepo().GetMicroServices("Login");
            string a = riderMessageMicroService.ServiceIp + "/api/Login/GetTokenFireBaseFromUserId?userId=" + order.BuyerId;
            var firebase = await GetRepo().sendRequest<Firebase>(a, HttpMethod.Get, null, true);
            //GetRepo().sendRequestFireBase("https://fcm.googleapis.com/fcm/send", "=AAAARvl0r30:APA91bEQzVGl5OUE_mnoSuo8k2C-0I3zaapsDBY0_-HE7w7heSxoPKUNYK3ms4t1tMghdyvmqdGMTnQCnEBtcEI7EsADaIyyCVs4koHg_qisgJKk5tatWpif3qsTWSZ-OPTzgmwmY43M", new
            //{

            //    registration_ids = new string[] { firebase.Token },
            //    notification = new
            //    {
            //        title = "รับ Order",
            //        body = "ร้าน " + order.ShopName + " เตรียมอาหารเสร็จแล้ว ลิงลมกำลังมาส่ง",
            //        mutable_content = true,
            //        sound = "Tri-tone"
            //    }
            //}, true);

            var firebaseMicorServices = GetRepo().GetMicroServices("Firebase");
            await GetRepo().sendRequest(firebaseMicorServices.ServiceIp + "/api/Notification/send", HttpMethod.Post, new
            {
                ApiKey = orderModel.ApiKey,
                ToToken = firebase.Token,
                Title = "รับ Order",
                body = "ร้าน " + order.ShopName + " เตรียมอาหารเสร็จแล้ว ลิงลมกำลังมาส่ง",
                //Topic="NewOrder"
            }, true);
            // await GetRepo().sendRequest(riderMicroService.ServiceIp + "/api/Rider/RiderRuning?ApiKey=" + orderModel.ApiKey, HttpMethod.Get, null, false);
            return new ObjectResult(order);
        }

        [HttpPost]
        public async Task<IActionResult> CalculateOrder(CalculateModel Calculate)
        {
            LinglomOrder linglomOrder = new LinglomOrder();

            var microServices = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(microServices.ServiceIp + "/api/LinglomShop/GetShopById?shop=" + Calculate.ShopId, HttpMethod.Get, null, false);

            var s = await GetRepo().sendRequestCustom<DirectionModel>("https://maps.googleapis.com/maps/api/directions/json?origin="+shop.Lat+","+shop.Lng+"&destination="+ Calculate.Lat+","+ Calculate.Lng+ "&key=AIzaSyAtH53jFKMqpcYtiyxq6W4PolYLIaOyoDc", HttpMethod.Get, null, false);
           if(shop.Commission==0)
                linglomOrder.Service = 37;
            else if (shop.Commission >= 15 || shop.Commission <= 19)
                linglomOrder.Service = 20;
            else if (shop.Commission >= 20 || shop.Commission <= 24)
                linglomOrder.Service = 15;
            else if (shop.Commission >=25 && Calculate.Price.Sum(n=>n.Price)<100) 
                linglomOrder.Service = 10;
            else
                linglomOrder.Service = 0;


            var dis = Math.Floor(s.routes[0].legs[0].distance.value / 1000.0);
            for(var i = 0; i <= dis; i++)
            {
                if (i > 3 && i<=5)
                {
                    linglomOrder.Service = linglomOrder.Service +10;
                }
                else if (i > 5 && i <=9)
                {
                    linglomOrder.Service = linglomOrder.Service + 15;
                }
                else if (i >9)
                {
                    linglomOrder.Service = linglomOrder.Service + 20;
                }
            }
           


            double Discount = 0;
            linglomOrder.Amount = (double)Calculate.Price.Sum(n => n.Price * n.Qty) - Discount;
            var Commission = Math.Round(((linglomOrder.Amount * (shop.Commission / 100.0))), 2, MidpointRounding.AwayFromZero);
            //Todo : Set vat in Setting
            linglomOrder.VatPercent = 7;

            linglomOrder.Vat = Math.Round((Commission * 0.07), 2, MidpointRounding.AwayFromZero);
            linglomOrder.AmontCommission = Commission;

            linglomOrder.AmountAfter = linglomOrder.Amount - Commission - linglomOrder.Vat;

            linglomOrder.AmountNet = linglomOrder.Amount + linglomOrder.Service;

            linglomOrder.AmountNetReturn = Commission + linglomOrder.Vat + linglomOrder.Service;

            return new ObjectResult(new
            {
                Delivery = linglomOrder.Service,
                Discount = Discount,
                Sum = linglomOrder.Amount 
            });
        }

        [HttpPost]
        public IActionResult PaymentOrder(PaymentModel request)
        {
            var data = GetRepo().GetData<LinglomOrder>(n => n.DeleteFlag == false && n.OrderNo == request.OrderNo);
            if (data == null)
            {
                return Problem("Data is Null");
            }
            if (request.PaymentStatus != "A")
            {
                data.OrderStatus = Enum.OrderStatus.Cancel;
                return Problem("จ่ายเงินไม่สำเร็จ");
            }
            data.PaymentRef = request.PaymentRef;
            data.OrderStatus = Enum.OrderStatus.New;
            GetRepo().Update(data);
            GetRepo().SaveChange();
            return new ObjectResult(data);
        }
            
        [HttpGet]
        public IActionResult ContinueCooking(string OrderNo,TimeSpan num)
        {
            var data = GetRepo().GetData<LinglomOrder>(n=>n.DeleteFlag == false && n.OrderNo == OrderNo);
            data.CookingTime = data.CookingTime.Add(num);
            data.OrderDateFinish = data.OrderDateFinish + data.CookingTime;
            GetRepo().Update(data);
            GetRepo().SaveChange();

            return new ObjectResult(new
            {
                OrderDateFinish = data.OrderDateFinish,
                CookingTime = data.CookingTime,
            });
        }

        [HttpGet]
        public IActionResult SetTime(string OrderNo,TimeSpan num)
        {
            var data = GetRepo().GetData<LinglomOrder>(n=>n.DeleteFlag == false && n.OrderNo == OrderNo);
            data.CookingTime = data.CookingTime.Add(num);
            data.OrderDateFinish = DateTime.Now + data.CookingTime;
            GetRepo().Update(data);
            GetRepo().SaveChange();
            return new ObjectResult(new { 
                OrderDateFinish = data.OrderDateFinish ,
                CookingTime = data.CookingTime,
            });
        }
        
        [HttpGet]
        public async Task<IActionResult> OrderHistoryShop(string ApiKey)
        {
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);

            var data = GetAll<LinglomOrder>(n=>n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderStatus == Enum.OrderStatus.Complate).ToList().OrderByDescending(n=>n.OrderDate);
            List<object> l = new List<object>();
            foreach (var item in data)
            {
                l.Add(new
                {
                    OrderNo = item.OrderNo,
                    PaymentType = item.PaymentTypeName,
                    SumPrice = Math.Round(item.AmountAfter,2),
                    Data = item.LinglomOrderDetails.ToList(),
                    AllSold = item.Amount,
                    GPWithvat = item.AmontCommission + item.Vat,
                    LossGP = item.AmontCommission,
                    Cash = data.Where(n => n.PaymentType == Enum.PaymentType.Cash && n.OrderNo == item.OrderNo).Sum(c => c.AmountAfter),
                    Credit = data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit && n.OrderNo == item.OrderNo).Sum(c => c.AmountAfter),
                    OrderStatus = item.OrderStatusName,
                    GPnum = shop.Commission
                });
            }

            return new ObjectResult(l);
        }

        [HttpGet]
        public async Task<IActionResult> ShopOrderSummaryInDay(string ApiKey,DateTime? date)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            if (date == null)
            {
                return Problem("Data Is NULL");
            }
                var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Date.Date == date).ToList();
            List<object> l = new List<object>();
            foreach (var item in data)
            {
                l.Add(new
                {
                    OrderNo = item.OrderNo,
                    SumPrice = Math.Round(item.AmountAfter, 2),
                    RiderName = item.RiderName,
                    OrderDate = item.OrderDate.Date,
                    Data = item.LinglomOrderDetails.ToList(),
                    Cash = Math.Round(data.Where(c => c.PaymentType == Enum.PaymentType.Cash && c.OrderNo == item.OrderNo).Sum(c => c.Amount),2),
                    Credit = Math.Round(data.Where(c => c.PaymentType == Enum.PaymentType.Credit_Debit && c.OrderNo == item.OrderNo).Sum(c => c.Amount),2),
                    GPWithvat = Math.Round(item.AmontCommission + item.Vat,2),
                    LossGP = Math.Round(item.AmontCommission,2),
                    GPnum = shop.Commission,
                    Amount = Math.Round(item.Amount, 2),
                    AmountAfter = Math.Round(item.AmountAfter, 2),
                    Vat = Math.Round(item.Vat, 2),
                    AmountNet = Math.Round(item.AmountNet, 2),
                    AmountCommission = Math.Round(item.AmontCommission, 2),

                });
            }
            return new ObjectResult(l);
        }

        [HttpGet]
        public async Task<IActionResult> GetShopOrderSummaryDay(string ApiKey,DateTime? date)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Date.Date == date).ToList();

           
            
            return new ObjectResult(new {
                AllSold = Math.Round(data.Sum(c => c.Amount), 2),
                SoldWithGP = Math.Round(data.Sum(c => c.Amount) - data.Sum(c=>c.AmontCommission), 2),
                Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash).Sum(c => c.Amount), 2),
                Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit).Sum(c => c.Amount), 2)

            });
        }

        [HttpGet]
        public async Task<IActionResult> GetShopOrderSummaryMonth(string ApiKey, int? month,int? year)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Month == month && n.OrderDate.Year == year).ToList();

            return new ObjectResult(new
            {
                AllSold = Math.Round(data.Sum(c => c.Amount),2),
                SoldWithGP = Math.Round(data.Sum(c => c.Amount) - data.Sum(c=>c.AmontCommission),2),
                Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash).Sum(c => c.Amount) ,2),
                Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit).Sum(c => c.Amount),2),
                Vat = Math.Round(data.Sum(c => c.Vat),2)
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetShopOrderSummaryYear(string ApiKey,int? year)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Year == year).ToList();

            return new ObjectResult(new
            {
                AllSold = Math.Round(data.Sum(c => c.Amount), 2),
                SoldWithGP = Math.Round(data.Sum(c => c.Amount) - data.Sum(c => c.AmontCommission), 2),
                Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash).Sum(c => c.AmountAfter), 2),
                Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit).Sum(c => c.AmountAfter), 2),
                Vat = Math.Round(data.Sum(c => c.Vat), 2)
            });
        }

        [HttpGet]
        public IActionResult GetOrderById(long? id)
        {
            if(id == null)
            {
                return Problem("Data Is NULL");
            }
            var data = GetRepo().GetDataAll<LinglomOrder>(n=>n.DeleteFlag == false && n.Id == id).ToList().OrderByDescending(n=>n.Id);
            return new ObjectResult(data);
        }

        [HttpGet]
        public async Task<IActionResult> ShopOrderSummaryInMonth(string ApiKey, int? month,int? year)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            if (month == null && year == null)
            {
                return Problem("Data Is NULL");
            }
            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Month == month && n.OrderDate.Year == year).ToList();
            foreach (var item in data.Select(n => n.OrderDate).GroupBy(n => n.Day , c=>c.Month))
            {
                l.Add(new {
                    OrderDate = data.Where(n => n.OrderDate.Day == item.Key ).FirstOrDefault().OrderDate.Date,
                    AllSold = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.Amount),2),
                    SoldWithGP = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c=>c.AmountAfter), 2),
                    Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash && n.OrderDate.Day == item.Key).Sum(c => c.Amount),2),
                    Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit && n.OrderDate.Day == item.Key).Sum(c => c.Amount),2),
                    Amount = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.Amount), 2),
                    AmountAfter = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.AmountAfter), 2),
                    Vat = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.Vat), 2),
                    AmountNet = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.AmountNet), 2),
                    AmountCommission = Math.Round(data.Where(n => n.OrderDate.Day == item.Key).Sum(c => c.AmontCommission), 2),
                });
            }
            return new ObjectResult(l);
        }

        [HttpGet]
        public async Task<IActionResult> ShopOrderSummaryInYear(string ApiKey, int? year)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();
            if ( year == null)
            {
                return Problem("Data Is NULL");
            }
            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id && n.OrderDate.Year == year).ToList();
            foreach (var item in data.Select(n =>n.OrderDate).GroupBy(n => n.Month))
            {
                l.Add(new
                {
                    OrderDate = data.Where(n => n.OrderDate.Month == item.Key).FirstOrDefault().OrderDate.Date,
                    AllSold = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.Amount),2),
                    SoldWithGP = Math.Round(data.Where(n=> n.OrderDate.Month == item.Key).Sum(c => c.AmountAfter), 2),
                    Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash &&  n.OrderDate.Month == item.Key).Sum(c => c.Amount),2),
                    Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit && n.OrderDate.Month == item.Key).Sum(c => c.Amount),2),
                    Amount = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.Amount), 2),
                    AmountAfter = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.AmountAfter), 2),
                    Vat = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.Vat), 2),
                    AmountNet = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.AmountNet), 2),
                    AmountCommission = Math.Round(data.Where(n => n.OrderDate.Month == item.Key).Sum(c => c.AmontCommission), 2),
                });
            }
            return new ObjectResult(l);

        }

        [HttpGet]
        public async Task<IActionResult> ShopOrderSummaryAll(string ApiKey)
        {
            if (ApiKey == null)
            {
                return Problem("Key Is NULL");
            }
            var MicroService = GetRepo().GetMicroServices("LinglomShop");
            var shop = await GetRepo().sendRequest<ShopModel>(MicroService.ServiceIp + "/api/LinglomShop/GetShopByApi?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            List<object> l = new List<object>();

            var data = GetAll<LinglomOrder>(n => n.DeleteFlag == false && n.ShopId == shop.Id).ToList();

            return new ObjectResult(
                new
                {
                    AllSold = Math.Round(data.Sum(c => c.Amount), 2),
                    SoldWithGP = Math.Round(data.Sum(c=>c.AmountAfter), 2),
                    Cash = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Cash).Sum(c => c.Amount), 2),
                    Credit = Math.Round(data.Where(n => n.PaymentType == Enum.PaymentType.Credit_Debit).Sum(c => c.Amount), 2),
                    GPWithVat = Math.Round(data.Sum(c=>c.AmontCommission) + data.Sum(c => c.Vat), 2),
                    Amount = Math.Round(data.Sum(c => c.Amount), 2),
                    AmountAfter = Math.Round(data.Sum(c => c.AmountAfter), 2),
                    Vat = Math.Round(data.Sum(c => c.Vat), 2),
                    AmountNet = Math.Round(data.Sum(c => c.AmountNet), 2),
                    AmountCommission = Math.Round(data.Sum(c => c.AmontCommission), 2),

                });

        }
    }
}