﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Order.MicroServices
{
    public partial class RenameTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.CreateTable(
                name: "LinglomOrderDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    ProductId = table.Column<long>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    Detail = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    Qty = table.Column<int>(nullable: false),
                    Addtional = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinglomOrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LinglomOrderDetails_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LinglomOrderDetails_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LinglomOrders",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    OrderId = table.Column<string>(nullable: true),
                    ShopId = table.Column<long>(nullable: false),
                    ShopName = table.Column<string>(nullable: true),
                    ShopTel = table.Column<string>(nullable: true),
                    ShopAddress = table.Column<string>(nullable: true),
                    ShopLat = table.Column<double>(nullable: false),
                    ShopLng = table.Column<double>(nullable: false),
                    BuyerName = table.Column<string>(nullable: true),
                    BuyerTel = table.Column<string>(nullable: true),
                    BuyerLat = table.Column<string>(nullable: true),
                    BuyerLng = table.Column<string>(nullable: true),
                    BuyerId = table.Column<long>(nullable: false),
                    OrderStatus = table.Column<int>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    OrderDateReceive = table.Column<DateTime>(nullable: true),
                    OrderDateFinish = table.Column<DateTime>(nullable: true),
                    SendDate = table.Column<DateTime>(nullable: true),
                    SendDateFinish = table.Column<DateTime>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    AmountAfter = table.Column<double>(nullable: false),
                    VatPercent = table.Column<double>(nullable: false),
                    Vat = table.Column<double>(nullable: false),
                    AmountNet = table.Column<double>(nullable: false),
                    Addtional = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinglomOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LinglomOrders_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LinglomOrders_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LinglomOrders_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrderDetails_CreateUserId",
                table: "LinglomOrderDetails",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrderDetails_UpdateUserId",
                table: "LinglomOrderDetails",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrders_CreateUserId",
                table: "LinglomOrders",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrders_CustomerId",
                table: "LinglomOrders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrders_UpdateUserId",
                table: "LinglomOrders",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LinglomOrderDetails");

            migrationBuilder.DropTable(
                name: "LinglomOrders");

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Addtional = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    Detail = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<double>(type: "float", nullable: false),
                    ProductId = table.Column<long>(type: "bigint", nullable: false),
                    ProductName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Qty = table.Column<int>(type: "int", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderDetails_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderDetails_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Addtional = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Amount = table.Column<double>(type: "float", nullable: false),
                    AmountAfter = table.Column<double>(type: "float", nullable: false),
                    AmountNet = table.Column<double>(type: "float", nullable: false),
                    BuyerId = table.Column<long>(type: "bigint", nullable: false),
                    BuyerLat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuyerLng = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuyerName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuyerTel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    OrderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    OrderDateFinish = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OrderDateReceive = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OrderId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderStatus = table.Column<int>(type: "int", nullable: false),
                    SendDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SendDateFinish = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ShopAddress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShopId = table.Column<long>(type: "bigint", nullable: false),
                    ShopLat = table.Column<double>(type: "float", nullable: false),
                    ShopLng = table.Column<double>(type: "float", nullable: false),
                    ShopName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ShopTel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true),
                    Vat = table.Column<double>(type: "float", nullable: false),
                    VatPercent = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_CreateUserId",
                table: "OrderDetails",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_UpdateUserId",
                table: "OrderDetails",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CreateUserId",
                table: "Orders",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_UpdateUserId",
                table: "Orders",
                column: "UpdateUserId");
        }
    }
}
