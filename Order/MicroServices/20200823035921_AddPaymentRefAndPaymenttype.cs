﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Order.MicroServices
{
    public partial class AddPaymentRefAndPaymenttype : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PaymentRef",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentType",
                table: "LinglomOrders",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentRef",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "LinglomOrders");
        }
    }
}
