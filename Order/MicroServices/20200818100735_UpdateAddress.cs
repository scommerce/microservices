﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Order.MicroServices
{
    public partial class UpdateAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddressDesc",
                table: "LinglomOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressDesc",
                table: "LinglomOrders");
        }
    }
}
