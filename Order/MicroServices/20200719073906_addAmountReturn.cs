﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Order.MicroServices
{
    public partial class addAmountReturn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "AmontCommission",
                table: "LinglomOrders",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "AmountNetReturn",
                table: "LinglomOrders",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmontCommission",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "AmountNetReturn",
                table: "LinglomOrders");
        }
    }
}
