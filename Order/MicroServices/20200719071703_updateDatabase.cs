﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Order.MicroServices
{
    public partial class updateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "LinglomOrders");

            migrationBuilder.AddColumn<string>(
                name: "OrderNo",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RiderId",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RiderLPR",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RiderName",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RiderProvince",
                table: "LinglomOrders",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Service",
                table: "LinglomOrders",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "ShopImage",
                table: "LinglomOrders",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "LinglomOrderId",
                table: "LinglomOrderDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LinglomOrderDetails_LinglomOrderId",
                table: "LinglomOrderDetails",
                column: "LinglomOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_LinglomOrderDetails_LinglomOrders_LinglomOrderId",
                table: "LinglomOrderDetails",
                column: "LinglomOrderId",
                principalTable: "LinglomOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LinglomOrderDetails_LinglomOrders_LinglomOrderId",
                table: "LinglomOrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_LinglomOrderDetails_LinglomOrderId",
                table: "LinglomOrderDetails");

            migrationBuilder.DropColumn(
                name: "OrderNo",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "RiderId",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "RiderLPR",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "RiderName",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "RiderProvince",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "Service",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "ShopImage",
                table: "LinglomOrders");

            migrationBuilder.DropColumn(
                name: "LinglomOrderId",
                table: "LinglomOrderDetails");

            migrationBuilder.AddColumn<string>(
                name: "OrderId",
                table: "LinglomOrders",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
