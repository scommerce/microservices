﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LinglomBackend.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage ="กรุณาใส่ชื่อผู้ใช้งาน")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "กรุณาใส่รหัสผ่าน")]
        public string Password { get; set; }
        [Required(ErrorMessage = "กรุณาใส่รหัสลูกค้า")]
        public string CusCode { get; set; }
    }
}
