﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TestWin.MicroServics
{
    public partial class changeCuscodeToEmpCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
       
            migrationBuilder.DropColumn(
                name: "CusCode",
                table: "Employees");

            migrationBuilder.AddColumn<string>(
                name: "EmpCode",
                table: "Employees",
                maxLength: 5,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmpCode",
                table: "Employees");

            migrationBuilder.AddColumn<string>(
                name: "CusCode",
                table: "Employees",
                type: "nvarchar(5)",
                maxLength: 5,
                nullable: false,
                defaultValue: "");
        }
    }
}
