﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWin.Entity
{
    public class Product:BaseEntityCusCodeProperty
    {
        public double Price { get; set; }
        public double NewPrice { get { return Price * 10; } }
    }
}
