﻿using Framework.BaseEntity;
using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestWin.Entity
{
    [ExtLoginForm(title = "เพิ่มข้อมูลลูกค้า", order = 0, frame = true, child = "items", LoginText = "บันทึก", jsonSubmit = true, url = "https://localhost:44395/api/Employee/AddEmployee?apiKey=88cef35d126e450fa9ba8c1f1205f5c9", successUrl = @"http://203.150.107.134:9013/?Cuscode=LINGLOM-001")]
    [ExtLoginForm(theme ="dragable",draggable =true,width =500,title = "เพิ่มข้อมูลพนักงาน", order = 0, frame = true, child = "items", LoginText = "บันทึก", jsonSubmit = true, url = "https://localhost:44395/api/Employee/AddEmployee?apiKey=88cef35d126e450fa9ba8c1f1205f5c9", successUrl = @"http://203.150.107.134:9013/?Cuscode=LINGLOM-001")]

    public class Employee:BaseEntityCusCode
    {
        [Required]
        [MaxLength(5)]
        [ExtInputField(emptyText = "รหัสพนักงาน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "รหัสพนักงาน", order = 1)]
        [ExtInputField(theme = "dragable",labelWidth =200,hasValue =true,emptyText = "รหัสพนักงาน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "รหัสพนักงาน(EmpCode)", order = 1)]
        public string EmpCode { get; set; }
        [ExtInputField(emptyText = "ชื่อพนักงาน", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "ชื่อพนักงาน", order = 1)]
        public string Name { get; set; }
        [ExtInputField(emptyText = "นามสกุล", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "นามสกุล", order = 1)]
        public string LastName { get; set; }
        [ExtInputField(emptyText = "อายุ", blankText = "กรุณาใส่ข้อความ",inputType = ExtInputFieldAttribute.InputType.number, allowBlank = false, fieldLabel = "อายุ", order = 1)]
       
        public int Age { get; set; }

        [ForeignKey("DepartmentId")]
        [ExtPanel(title = "แผนก", order = 4, frame = false, child = "items")]
        public virtual Department Dept { get; set; }

        //[NotMapped]
        //public string DepartmentName
        //{
        //    get {
        //        return Dept.Name;
        //    }
        //}


        [NotMapped]
        public string FullName { get { return Name + " " + LastName; } }
    }
}
