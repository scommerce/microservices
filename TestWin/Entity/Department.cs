﻿using Framework.BaseEntity;
using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWin.Entity
{
  
    public class Department:BaseEntityCusCode
    {
        public string Name { get; set; }
        [ExtInputField(emptyText = "แผนก", blankText = "กรุณาใส่ข้อความ", allowBlank = false, fieldLabel = "แผนก", order = 1)]
        public virtual List<Employee> Employees { get; set; } = new List<Employee>();
    }
}
