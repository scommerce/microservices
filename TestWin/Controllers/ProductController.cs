﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestWin.DbContext;
using TestWin.Entity;
using TestWin.Model;

namespace TestWin.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : BaseController<DataContext>
    {
        //public IActionResult Add(Computer computer)
        //{
        //    List<BasePropertyModel> basePropertyModels = new List<BasePropertyModel>();
        //    BasePropertyModel Name = new BasePropertyModel();
        //    Name.Property = "Name";
        //    Name.PropertyType = Framework.BaseEntity.PropertyType.String;
        //    Name.Value = computer.Name;

        //    BasePropertyModel CPU = new BasePropertyModel();
        //    CPU.Property = "CPU";
        //    CPU.PropertyType = Framework.BaseEntity.PropertyType.String;
        //    CPU.Value = computer.CPU;


        //    BasePropertyModel Price = new BasePropertyModel();
        //    Price.Property = "Price";
        //    Price.PropertyType = Framework.BaseEntity.PropertyType.Double;
        //    Price.Value = computer.Price.ToString();

        //    basePropertyModels.Add(Name);

        //    basePropertyModels.Add(CPU);

        //    basePropertyModels.Add(Price);

        //    Product p = new Product();
        //    p.Properties = new List<BaseProperty>();
        //    GetRepo().Add(p);

        //    GetRepo().SaveChange();
        //    GetRepo().UpdateProperties(p, basePropertyModels);


  
            
        //    return Ok();


        //}
    
        public IActionResult Add(List<BasePropertyModel> basePropertyModels)
        {

            Product p = new Product();
            p.Properties = new List<BaseProperty>();
            p.Price = Double.Parse(basePropertyModels.Where(n => n.Property == "Price").FirstOrDefault().Value);
            GetRepo().Add(p);

            GetRepo().SaveChange();
            GetRepo().UpdateProperties(p, basePropertyModels);


            return Ok();


        }
        [NotDataMap]
        public IActionResult Get()
        {
            var data = GetAll<Product>().ToList();

            return new ObjectResult(GetRepo().GetProperty(data));
        }
    }
}