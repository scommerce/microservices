﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TestWin.DbContext;
using TestWin.Entity;
using TestWin.Model;

namespace TestWin.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeeController : BaseController<DataContext>
    {
        public IActionResult GetEmployeeUI(string theme)
        {
            var model = new Employee();
            model.EmpCode = "L001";
            var a = GetUI(model, theme);


            string outputJson = JsonConvert.SerializeObject(a);

            return CreateExtjs("Ext.Employee", outputJson);
      
        }
        [HttpPost]
        public IActionResult GetEmployee(BaseDataTable baseDataTable)
        {
            var data = GetAll<Employee>(n => n.DeleteFlag == false).ToDataTable(baseDataTable);
            return new ObjectResult(data);
        }

        [HttpPost]
        public IActionResult GetDepartment(BaseDataTable baseDataTable)
        {
            var data = GetAll<Department>(n => n.DeleteFlag == false).ToDataTable(baseDataTable);
           
            return new ObjectResult(data);
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployeeAsync([FromQuery]string apiKey,Employee employee)

        {
            var user = await GetRepo().GetUserFromApiKey(apiKey, "th");
            employee.CreateUser = user.BaseUser;
            employee.Customer = user.BaseCustomer;
            //   employee.Dept = GetData<Department>(n => n.Id == employee.Dept.Id);
            GetRepo().Add(employee);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpPost]
        public IActionResult UpdateEmployee(Employee employee)
        {
            var data = GetData<Employee>(n => n.Id == employee.Id);
            data.Name = employee.Name;
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpPost]
        public IActionResult AddEmployeeToDept(Employee employee)
        {
            var data = GetData<Department>(n => n.Id == employee.Dept.Id);
            var e = GetData<Employee>(n => n.Id == employee.Id);
            data.Employees.Add(e);
            GetRepo().SaveChange();
            return Ok();
        }
    }
}