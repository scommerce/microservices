﻿using Framework.BaseContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWin.Entity;

namespace TestWin.DbContext
{
    public class DataContext : BaseDbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Product> Products { get; set; }
        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=TestWin2;User Id=sa;Password=P@ssword1234;";
        }
    }
}
