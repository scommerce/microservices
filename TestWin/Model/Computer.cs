﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestWin.Model
{
    public class Computer
    {
        public string Name { get; set; }
        public string CPU { get; set; }
        public double Price { get; set; }
    }
}
