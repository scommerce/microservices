﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RiderQuota.Entity;

namespace RiderQuota.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.RiderQuota> RiderQuota { get; set; }
        public DbSet<RiderQdetail> RiderQdetail { get; set; }
        public DbSet<RiderRegisterQ> RiderRegisterQ { get; set; }

        public override string GetConnectionString()
        {
            throw new NotImplementedException();
        }
    }
}
