﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RiderQuota.MicroServices
{
    public partial class FixQdetail5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "QdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.AddColumn<long>(
                name: "RiderQdetailId",
                table: "RiderRegisterQ",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RiderRegisterQ_RiderQdetailId",
                table: "RiderRegisterQ",
                column: "RiderQdetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_RiderRegisterQ_RiderQdetail_RiderQdetailId",
                table: "RiderRegisterQ",
                column: "RiderQdetailId",
                principalTable: "RiderQdetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RiderRegisterQ_RiderQdetail_RiderQdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.DropIndex(
                name: "IX_RiderRegisterQ_RiderQdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.DropColumn(
                name: "RiderQdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.AddColumn<long>(
                name: "QdetailId",
                table: "RiderRegisterQ",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
