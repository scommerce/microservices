﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RiderQuota.MicroServices
{
    public partial class RenameAndfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Qdetail");

            migrationBuilder.DropTable(
                name: "RegisterQ");

            migrationBuilder.DropTable(
                name: "Quota");

            migrationBuilder.CreateTable(
                name: "RiderQuota",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    ZoneId = table.Column<long>(nullable: true),
                    ZoneName = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Status = table.Column<long>(nullable: true),
                    ApproveBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiderQuota", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RiderQuota_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderQuota_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderQuota_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RiderRegisterQ",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    RiderId = table.Column<long>(nullable: true),
                    RiderName = table.Column<string>(nullable: true),
                    QdetailId = table.Column<long>(nullable: true),
                    RegisterDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiderRegisterQ", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RiderRegisterQ_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderRegisterQ_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderRegisterQ_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RiderQdetail",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    TimeStart = table.Column<TimeSpan>(nullable: false),
                    TimeEnd = table.Column<TimeSpan>(nullable: false),
                    Limit = table.Column<int>(nullable: false),
                    Counter = table.Column<int>(nullable: false),
                    RiderQuotaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RiderQdetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RiderQdetail_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderQdetail_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderQdetail_RiderQuota_RiderQuotaId",
                        column: x => x.RiderQuotaId,
                        principalTable: "RiderQuota",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RiderQdetail_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RiderQdetail_CreateUserId",
                table: "RiderQdetail",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQdetail_CustomerId",
                table: "RiderQdetail",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQdetail_RiderQuotaId",
                table: "RiderQdetail",
                column: "RiderQuotaId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQdetail_UpdateUserId",
                table: "RiderQdetail",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQuota_CreateUserId",
                table: "RiderQuota",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQuota_CustomerId",
                table: "RiderQuota",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderQuota_UpdateUserId",
                table: "RiderQuota",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderRegisterQ_CreateUserId",
                table: "RiderRegisterQ",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderRegisterQ_CustomerId",
                table: "RiderRegisterQ",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_RiderRegisterQ_UpdateUserId",
                table: "RiderRegisterQ",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RiderQdetail");

            migrationBuilder.DropTable(
                name: "RiderRegisterQ");

            migrationBuilder.DropTable(
                name: "RiderQuota");

            migrationBuilder.CreateTable(
                name: "Quota",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ApproveBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    Status = table.Column<long>(type: "bigint", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true),
                    ZoneId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quota", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quota_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Quota_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Quota_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RegisterQ",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    QdetailId = table.Column<long>(type: "bigint", nullable: true),
                    RegisterDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RiderId = table.Column<long>(type: "bigint", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegisterQ", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RegisterQ_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RegisterQ_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RegisterQ_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Qdetail",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Counter = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateUserId = table.Column<long>(type: "bigint", nullable: true),
                    CustomerId = table.Column<long>(type: "bigint", nullable: true),
                    DeleteFlag = table.Column<bool>(type: "bit", nullable: false),
                    Limit = table.Column<int>(type: "int", nullable: false),
                    QuotaId = table.Column<long>(type: "bigint", nullable: true),
                    TimeEnd = table.Column<TimeSpan>(type: "time", nullable: false),
                    TimeStart = table.Column<TimeSpan>(type: "time", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateUserId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Qdetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Qdetail_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Qdetail_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Qdetail_Quota_QuotaId",
                        column: x => x.QuotaId,
                        principalTable: "Quota",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Qdetail_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Qdetail_CreateUserId",
                table: "Qdetail",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Qdetail_CustomerId",
                table: "Qdetail",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Qdetail_QuotaId",
                table: "Qdetail",
                column: "QuotaId");

            migrationBuilder.CreateIndex(
                name: "IX_Qdetail_UpdateUserId",
                table: "Qdetail",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Quota_CreateUserId",
                table: "Quota",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Quota_CustomerId",
                table: "Quota",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Quota_UpdateUserId",
                table: "Quota",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RegisterQ_CreateUserId",
                table: "RegisterQ",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_RegisterQ_CustomerId",
                table: "RegisterQ",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_RegisterQ_UpdateUserId",
                table: "RegisterQ",
                column: "UpdateUserId");
        }
    }
}
