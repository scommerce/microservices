﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RiderQuota.MicroServices
{
    public partial class FixQdetail2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RiderRegisterQ_RiderQdetail_RiderQdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.DropIndex(
                name: "IX_RiderRegisterQ_RiderQdetailId",
                table: "RiderRegisterQ");

            migrationBuilder.DropColumn(
                name: "RiderQdetailId",
                table: "RiderRegisterQ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RiderQdetailId",
                table: "RiderRegisterQ",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RiderRegisterQ_RiderQdetailId",
                table: "RiderRegisterQ",
                column: "RiderQdetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_RiderRegisterQ_RiderQdetail_RiderQdetailId",
                table: "RiderRegisterQ",
                column: "RiderQdetailId",
                principalTable: "RiderQdetail",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
