﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Models
{
    public class RiderQuotaModel
    {
        [Required]
        public long? ZoneId { get; set; }
        [Required]
        public string ZoneName { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public long? RiderQdetail { get; set; }
        [Required]
        public long? Status { get; set; }
        [Required]
        public string ApproveBy { get; set; }
    }
}
