﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Models
{
    public class ZoneModel
    {
        public string Name { get; set; }
        public long? ProvincesId { get; set; }
        public long? Id { get; set; }
    }
}
