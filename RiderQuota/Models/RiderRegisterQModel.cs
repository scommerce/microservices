﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Models
{
    public class RiderRegisterQModel
    {
        [Required]
        public long? RiderId { get; set; }
        [Required]
        public long? QdetailId { get; set; }

        public string TimeStart  { get; set; }
        public string TimeEnd { get; set; }
        public DateTime DatePick { get; set; }
    }
}
