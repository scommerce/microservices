﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Models
{
    public class RiderQdetailModel
    {
        public long? RiderId { get; set; }
        //[Required]
        //public long? QdetailId { get; set; }
        public long? ZoneId { get; set; }
        public string ApiKey { get; set; }
        public DateTime DatePick { get; set; }

    }
}
