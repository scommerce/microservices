﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RiderQuota.DbContext;
using RiderQuota.Entity;
using RiderQuota.Models;

namespace RiderQuota.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RiderQdetailController : BaseController<DataContext>
    {
        public class RiderReturn
        {
            public long? Id { get; set; }
            public TimeSpan TimeStart { get; set; }
            public TimeSpan TimeEnd { get; set; }
            public decimal Hours { get; set; }
            public int Limit { get; set; }
            public int Counter { get; set; }
            public string des { get; set; }
            public bool Registered { get; set; }
            //public List<RiderQdetail> RiderQuota { get; set; }
        }

        [HttpGet]
        public IActionResult GetQDetail(DateTime date)
        {
            var q = GetAll<RiderQdetail>(n=>n.DeleteFlag == false && n.RiderQuota.Date == date).ToList();

            return new ObjectResult(q);
        }

        [HttpPost]
        public async Task<IActionResult> GetQ(RiderQdetailModel riderQdetailModel)
        {
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderQdetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + riderQdetailModel.ApiKey, HttpMethod.Get, null, true);

            //List<RiderReturn> list = new List<RiderReturn>();
            var a = GetAll<RiderQdetail>().Where(n => n.RiderRegistersQ.Any(c => c.RiderId == rider.RiderId) && n.RiderQuota.Date == riderQdetailModel.DatePick.Date).ToList();
            return new ObjectResult(a);
        }

        [HttpPost]
        public async Task<IActionResult> GetQNextWeek(RiderQdetailModel riderQdetailModel)
        {
            if (riderQdetailModel.ZoneId == null)
            {
                return Problem("Data is null");
            }
            var riderMicroService = GetRepo().GetMicroServices("Rider");
            var rider = await GetRepo().sendRequest<RiderQdetailModel>(riderMicroService.ServiceIp + "/api/Rider/GetRiderFromApiKey?ApiKey=" + riderQdetailModel.ApiKey, HttpMethod.Get, null, true);

            List<RiderReturn> riderReturns = new List<RiderReturn>();
            var rq = GetAll<RiderQdetail>().Where(n => n.RiderQuota.ZoneId == riderQdetailModel.ZoneId && n.RiderQuota.Date.Date == riderQdetailModel.DatePick && n.DeleteFlag == false).ToList();
            foreach (var a in rq)
            {
                var rrq = GetAll<RiderRegisterQ>().Where(n => n.RiderQdetail.Id == a.Id && n.DeleteFlag == false).ToList();

                riderReturns.Add(new RiderReturn
                {
                    Id = a.Id,
                    TimeStart = a.TimeStart,
                    TimeEnd = a.TimeEnd,
                    Hours = (decimal)a.TimeEnd.Hours - (decimal)a.TimeStart.Hours,
                    Limit = a.Limit,
                    Counter = rrq.Count(),
                    Registered = (rrq.Where(n => n.RiderId == rider.RiderId).FirstOrDefault() == null) ? false : true,
                });
            }

            return new ObjectResult(riderReturns);

        }

        [HttpGet]
        public IActionResult CheckSystem()
        {
            var res = "ระบบจะเปิดให้บริการเร็วๆนี้";
            return new ObjectResult(new
            {
                Result = res
            });
        }
        //[HttpPost]
        //public IActionResult GetQ(RiderQdetailModel riderQdetailModel)
        //{
        //    DateTime dt = DateTime.Now;
        //    var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
        //    var diff = dt.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
        //    if (diff < 0)
        //    {
        //        diff += 7;
        //    }
        //    var da =  dt.AddDays(-diff).Date;

        //    if (riderQdetailModel.RiderId != null)
        //    {
        //        DateTime ba;
        //        List<RiderReturn> list = new List<RiderReturn>();
        //        var a = GetAll<RiderQdetail>().Where(n => n.RiderRegistersQ.FirstOrDefault().RiderId == riderQdetailModel.RiderId && n.RiderQuota.Date >= da.Date.Date && n.RiderQuota.Date <= da.AddDays(7).Date).ToList();

        //        for (int i = 0; i < 7; i++)
        //        {
        //            ba = da.AddDays(i).Date;
        //                list.Add(new RiderReturn()
        //                {
        //                    date = ba,
        //                    q = a.Where(n => n.RiderQuota.Date.Date == ba).ToList()
        //                });
        //        }
        //        return new ObjectResult(list);
        //    }
        //    else
        //    {
        //        return Problem("Data Is Null");
        //    }
        //}

        //[HttpPost]
        //public IActionResult GetQNextWeek(RiderQdetailModel riderQdetailModel)
        //{
        //    DateTime dt = DateTime.Now;
        //    var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
        //    var diff = dt.DayOfWeek - (culture.DateTimeFormat.FirstDayOfWeek + 1);
        //    if (diff < 0)
        //    {
        //        diff += 7;
        //    }

        //    var da = dt.AddDays(-diff).Date;
        //    var next = da.AddDays(1).Date;

        //    while (next.DayOfWeek != DayOfWeek.Monday)
        //        next = next.AddDays(1);

        //    if (riderQdetailModel.ZoneId != null)
        //    {

        //    }
        //        DateTime ba;
        //        List<RiderReturn> list = new List<RiderReturn>();
        //        var a = GetAll<RiderQdetail>().Where(n => n.DeleteFlag == false).ToList();

        //        for (int i = 0; i < 7; i++)
        //        {
        //            ba = next.AddDays(i).Date;

        //            list.Add(new RiderReturn()
        //            {
        //                date = ba,
        //                q = a.Where(n => n.RiderQuota.ZoneId == riderQdetailModel.ZoneId).ToList()
        //            });
        //        }
        //        return new ObjectResult(list);
        //}

    }
}