﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Rider.Models;
using RiderQuota.DbContext;
using RiderQuota.Entity;
using RiderQuota.Models;

namespace RiderQuota.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RiderRegisterQController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddRiderRegisterQ(RiderRegisterQModel RiderRegisterQModel)
        {
            //var counter = GetRepo().GetData<Entity.RiderQuota>().RiderQdetail.Where(n => n.Id == RiderRegisterQModel.QdetailId);
            //var counter = GetRepo().GetData<RiderQdetail>(n => n.Id == RiderRegisterQModel.QdetailId && n.DeleteFlag == false);

            var counter = GetRepo().GetData<RiderQdetail>(n => n.Id == RiderRegisterQModel.QdetailId && n.DeleteFlag == false);

            if (counter.Counter > counter.Limit)
            {
                return Problem("Full Quota!");
            }

            RiderRegisterQ riderRegisterQ = new RiderRegisterQ();

            var setting = GetContext().BaseMicroServices.Where(n => n.Name == "Rider" && n.Customer == null).FirstOrDefault();
            var rider = await GetRepo().sendRequest<List<RiderModel>>(setting.ServiceIp + "/api/Rider/GetRider?id=" + RiderRegisterQModel.RiderId, HttpMethod.Get, null, true);

            var re = GetRepo().GetDataAll<RiderRegisterQ>(n => n.RiderId == rider.FirstOrDefault().Id && n.RiderQdetail.RiderQuota.Date.Date == counter.RiderQuota.Date.Date && n.DeleteFlag == false).ToList();
            if (re != null)
            {
                foreach (var result in re)
                {
                    result.DeleteFlag = true;
                    GetRepo().Update(re);
                    GetRepo().SaveChange();
                }
            }
            riderRegisterQ.RiderId = rider.FirstOrDefault().Id;
            riderRegisterQ.RiderName = rider.FirstOrDefault().FirstName + " " + rider.FirstOrDefault().LastName;
            riderRegisterQ.RiderQdetail = counter;
            riderRegisterQ.RegisterDate = DateTime.Now;

            counter.Counter+=1;

            GetRepo().Add(riderRegisterQ);
            GetRepo().Update(counter);
            GetRepo().SaveChange();

            return Ok();
        }

    }
}