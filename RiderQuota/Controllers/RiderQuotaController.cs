﻿using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RiderQuota.DbContext;
using RiderQuota.Entity;
using RiderQuota.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RiderQuota.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RiderQuotaController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddRiderQuota(RiderQuotaModel riderQuotaModel)
        {
            //var counter = GetRepo().GetData<Entity.RiderQuota>().RiderQdetail.Where(n=>n.Id == riderQuotaModel.RiderQdetail);

            //if (counter.FirstOrDefault().Counter > counter.FirstOrDefault().Limit)
            //{
            //    return Problem("Full Quota!");
            //}

            Entity.RiderQuota riderQuota = new Entity.RiderQuota();
            
            var setting = GetContext().BaseMicroServices.Where(n => n.Name == "Rider" && n.Customer == null).FirstOrDefault();
            var response = await GetRepo().sendRequest(setting.ServiceIp + "/api/Zone/GetZone", HttpMethod.Post, new { Id = riderQuotaModel.ZoneId }, true);
            var zone = JsonConvert.DeserializeObject<BaseReturnData<ZoneModel>>(await response.Content.ReadAsStringAsync());
            riderQuota.ZoneId = zone.Data.Id;
            riderQuota.ZoneName = zone.Data.Name;
            DateTime dateTime = new DateTime();
            riderQuota.Date = dateTime.Date;
            riderQuota.Status = (long)Status.Active;
            riderQuota.ApproveBy = "null";

            GetRepo().Add(riderQuota);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> EditRiderQuota(RiderQuotaModel riderQuotaModel)
        {
            //var counter = GetRepo().GetData<Entity.RiderQuota>().RiderQdetail.Where(n=>n.Id == riderQuotaModel.RiderQdetail);

            //if (counter.FirstOrDefault().Counter > counter.FirstOrDefault().Limit)
            //{
            //    return Problem("Full Quota!");
            //}

            Entity.RiderQuota riderQuota = new Entity.RiderQuota();

            var setting = GetContext().BaseMicroServices.Where(n => n.Name == "Rider" && n.Customer == null).FirstOrDefault();
            var response = await GetRepo().sendRequest(setting.ServiceIp + "/api/Zone/GetZone", HttpMethod.Post, new { Id = riderQuotaModel.ZoneId }, true);
            var zone = JsonConvert.DeserializeObject<BaseReturnData<ZoneModel>>(await response.Content.ReadAsStringAsync());
            riderQuota.ZoneId = zone.Data.Id;
            riderQuota.ZoneName = zone.Data.Name;
            DateTime dateTime = new DateTime();
            riderQuota.Date = dateTime.Date;
            riderQuota.Status = (long)Status.Active;
            riderQuota.ApproveBy = "null";

            GetRepo().Update(riderQuota);
            GetRepo().SaveChange();

            return Ok();
        }

    }
}