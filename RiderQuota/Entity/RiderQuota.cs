﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Entity
{
    public class RiderQuota : BaseEntityCusCode
    {
        [Required]
        public long? ZoneId { get; set; }
        [Required]
        public string ZoneName { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }
        [NotMapped]
        public string DateString { get {
                return Date.ToString("yyyy-MM-dd");
            } }
        [Required]
        public virtual List<RiderQdetail> RiderQdetail { get; set; }
        [Required]
        public long? Status { get; set; }
        [Required]
        public string ApproveBy { get; set; }



    }
}
