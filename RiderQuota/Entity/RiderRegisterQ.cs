﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Entity
{
    public class RiderRegisterQ : BaseEntityCusCode
    {
        [Required]
        public long? RiderId { get; set; }
        [Required]
        public string RiderName { get; set; }
        [Required]
        public DateTime RegisterDate { get; set; }
        public long RiderQdetailId { get; set; }
        [ForeignKey("RiderQdetailId")]
        public virtual RiderQdetail RiderQdetail { get; set; }
    }
}
