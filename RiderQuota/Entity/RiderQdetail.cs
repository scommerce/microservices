﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RiderQuota.Entity
{
    public class RiderQdetail : BaseEntityCusCode
    {
        [Required]
        public TimeSpan TimeStart { get; set; }
        [Required]
        public TimeSpan TimeEnd { get; set; }
        [Required]
        public int Limit { get; set; }
        [Required]
        public int Counter { get; set; }
        public long RiderQuotaId { get; set; }
        [ForeignKey("RiderQuotaId")]
        public virtual RiderQuota RiderQuota { get; set; }
        [Required]
        public virtual List<RiderRegisterQ> RiderRegistersQ { get; set; }

    }
}
