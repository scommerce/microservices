﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IClass.Entity
{
    public class LogUpdate : BaseEntityCusCode
    {
        public long? ImageId { get; set; }
        public string FullNameTh { get; set; }
        public string FullNameEn { get; set; }
        public string NickName { get; set; }
        public string Age { get; set; }
        public string Birthday { get; set; }
        public string Address { get; set; }
        public string Tel { get; set; }
        public string Religion { get; set; }
        public string Position { get; set; }
        public string Departure { get; set; }
        public string CompanyName { get; set; }
        public string WorkAddress { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Study { get; set; }
        public string Duty { get; set; }
        public string IdCard { get; set; }
    }
}
