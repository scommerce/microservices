﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IClass.MicroServices
{
    public partial class updateEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogUpdate",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    ImageId = table.Column<long>(nullable: true),
                    FullNameTh = table.Column<string>(nullable: true),
                    FullNameEn = table.Column<string>(nullable: true),
                    NickName = table.Column<string>(nullable: true),
                    Age = table.Column<string>(nullable: true),
                    Birthday = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Tel = table.Column<string>(nullable: true),
                    Religion = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Departure = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    WorkAddress = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Study = table.Column<string>(nullable: true),
                    Duty = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogUpdate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LogUpdate_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LogUpdate_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LogUpdate_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LogUpdate_CreateUserId",
                table: "LogUpdate",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LogUpdate_CustomerId",
                table: "LogUpdate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_LogUpdate_UpdateUserId",
                table: "LogUpdate",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogUpdate");
        }
    }
}
