﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IClass.MicroServices
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseLanguage",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    Lang = table.Column<string>(nullable: true),
                    OrigianlText = table.Column<string>(nullable: true),
                    ToText = table.Column<string>(nullable: true),
                    Error = table.Column<bool>(nullable: false),
                    BaseApiReturnId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseLanguage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseApiTokens",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    TokenType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseApiTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseMicroServices",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ServiceIp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseMicroServices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseProperties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Property = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    PropetyType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseProperties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Section = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseTables",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Column = table.Column<string>(nullable: true),
                    Format = table.Column<string>(nullable: true),
                    Order = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseTables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseUsers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    BaseUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseUsers_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseUsers_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BaseApiReturns",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    PropertyName = table.Column<string>(nullable: true),
                    PropertyNameNew = table.Column<string>(nullable: true),
                    BaseApiReturnId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseApiReturns", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseApiReturns_BaseApiReturns_BaseApiReturnId",
                        column: x => x.BaseApiReturnId,
                        principalTable: "BaseApiReturns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseApiReturns_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseApiReturns_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BaseCustomers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    CustomerCode = table.Column<string>(nullable: true),
                    BaseCustomerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseCustomers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiReturns_BaseApiReturnId",
                table: "BaseApiReturns",
                column: "BaseApiReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiReturns_CreateUserId",
                table: "BaseApiReturns",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiReturns_UpdateUserId",
                table: "BaseApiReturns",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_CreateUserId",
                table: "BaseApiTokens",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_CustomerId",
                table: "BaseApiTokens",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_UpdateUserId",
                table: "BaseApiTokens",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_CreateUserId",
                table: "BaseCustomers",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_CustomerId",
                table: "BaseCustomers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_UpdateUserId",
                table: "BaseCustomers",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_CreateUserId",
                table: "BaseLanguage",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_UpdateUserId",
                table: "BaseLanguage",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_CreateUserId",
                table: "BaseMicroServices",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_CustomerId",
                table: "BaseMicroServices",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_UpdateUserId",
                table: "BaseMicroServices",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CreateUserId",
                table: "BaseProperties",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CustomerId",
                table: "BaseProperties",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_UpdateUserId",
                table: "BaseProperties",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_CreateUserId",
                table: "BaseSettings",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_CustomerId",
                table: "BaseSettings",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_UpdateUserId",
                table: "BaseSettings",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseTables_CreateUserId",
                table: "BaseTables",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseTables_CustomerId",
                table: "BaseTables",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseTables_UpdateUserId",
                table: "BaseTables",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_CreateUserId",
                table: "BaseUsers",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_CustomerId",
                table: "BaseUsers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UpdateUserId",
                table: "BaseUsers",
                column: "UpdateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseLanguage_BaseUsers_CreateUserId",
                table: "BaseLanguage",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseLanguage_BaseUsers_UpdateUserId",
                table: "BaseLanguage",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseLanguage_BaseApiReturns_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId",
                principalTable: "BaseApiReturns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseUsers_CreateUserId",
                table: "BaseApiTokens",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseUsers_UpdateUserId",
                table: "BaseApiTokens",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseCustomers_CustomerId",
                table: "BaseApiTokens",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseMicroServices_BaseUsers_CreateUserId",
                table: "BaseMicroServices",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseMicroServices_BaseUsers_UpdateUserId",
                table: "BaseMicroServices",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseMicroServices_BaseCustomers_CustomerId",
                table: "BaseMicroServices",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_BaseUsers_CreateUserId",
                table: "BaseProperties",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_BaseUsers_UpdateUserId",
                table: "BaseProperties",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_BaseCustomers_CustomerId",
                table: "BaseProperties",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseUsers_CreateUserId",
                table: "BaseSettings",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseUsers_UpdateUserId",
                table: "BaseSettings",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseCustomers_CustomerId",
                table: "BaseSettings",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseTables_BaseUsers_CreateUserId",
                table: "BaseTables",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseTables_BaseUsers_UpdateUserId",
                table: "BaseTables",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseTables_BaseCustomers_CustomerId",
                table: "BaseTables",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_BaseCustomers_CustomerId",
                table: "BaseUsers",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseCustomers_BaseUsers_CreateUserId",
                table: "BaseCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BaseCustomers_BaseUsers_UpdateUserId",
                table: "BaseCustomers");

            migrationBuilder.DropTable(
                name: "BaseApiTokens");

            migrationBuilder.DropTable(
                name: "BaseLanguage");

            migrationBuilder.DropTable(
                name: "BaseMicroServices");

            migrationBuilder.DropTable(
                name: "BaseProperties");

            migrationBuilder.DropTable(
                name: "BaseSettings");

            migrationBuilder.DropTable(
                name: "BaseTables");

            migrationBuilder.DropTable(
                name: "BaseApiReturns");

            migrationBuilder.DropTable(
                name: "BaseUsers");

            migrationBuilder.DropTable(
                name: "BaseCustomers");
        }
    }
}
