﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IClass.MicroServices
{
    public partial class UpdateIClassEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdCard",
                table: "LogUpdate",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCard",
                table: "LogUpdate");
        }
    }
}
