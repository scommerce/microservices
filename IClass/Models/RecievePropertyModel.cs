﻿using Framework.BaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IClass.Models
{
    public class RecievePropertyModel
    {
        public string nameTH { get; set; }
        public string nameEN { get; set; }
        public string nickname { get; set; }
        public string age { get; set; }
        public string dathOfBirth { get; set; }
        public string address { get; set; }
        public string phoneNumber { get; set; }
        public string religion { get; set; }
        public string career { get; set; }
        public string littleWork { get; set; }
        public string workplaceName { get; set; }
        public string workplaceAddress { get; set; }
        public string email { get; set; }
        public string houseNumber { get; set; }
        public string education { get; set; }
        public string duty { get; set; }
        public string iDCardNumber { get; set; }
    }
}
