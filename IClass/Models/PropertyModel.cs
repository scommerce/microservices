﻿using Framework.BaseModel;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IClass.Models
{
    public class PropertyModel
    {
        public string ApiKey { get; set; }
        public List<BasePropertyModel> Property { get; set; }
    }
}
