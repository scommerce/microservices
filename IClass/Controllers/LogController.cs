﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.Startup;
using IClass.DbContext;
using IClass.Entity;
using IClass.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace IClass.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LogController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddLogUpdate(PropertyModel request)
        {
            //List<object> l = new List<object>();
            //var property = await GetRepo().sendRequest<RecievePropertyModel>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + request.ApiKey, HttpMethod.Get, null, false);
            LogUpdate log = new LogUpdate();
            foreach (var data in request.Property)
            {
                if (data.Property == "ImageId")
                {
                    log.ImageId = Int32.Parse(data.Value);
                }
                if (data.Property == "nameTH")
                {
                    log.FullNameTh = data.Value;
                }
                if (data.Property == "nameEN")
                {
                    log.FullNameEn = data.Value;
                }
                if (data.Property == "nickname")
                {
                    log.NickName = data.Value;
                }
                if (data.Property == "age")
                {
                    log.Age = data.Value;
                }
                if (data.Property == "dathOfBirth")
                {
                    log.Birthday = data.Value;
                }
                if (data.Property == "address")
                {
                    log.Address = data.Value;
                }
                if (data.Property == "phoneNumber")
                {
                    log.Tel = data.Value;
                }
                if (data.Property == "religion")
                {
                    log.Religion = data.Value;
                }
                if (data.Property == "career")
                {
                    log.Position = data.Value;
                }
                if (data.Property == "littleWork")
                {
                    log.Departure = data.Value;
                }
                if (data.Property == "workplaceName")
                {
                    log.CompanyName = data.Value;
                }
                if (data.Property == "duty")
                {
                    log.Duty = data.Value;
                }
                if (data.Property == "email")
                {
                    log.Email = data.Value;
                }
                if (data.Property == "houseNumber")
                {
                    log.Phone = data.Value;
                }
                if (data.Property == "education")
                {
                    log.Study = data.Value;
                }
                if (data.Property == "workplaceAddress")
                {
                    log.WorkAddress = data.Value;
                }
                if (data.Property == "iDCardNumber")
                {
                    log.IdCard = data.Value;
                }
                var CusCode = await GetRepo().GetUserFromApiKey(request.ApiKey, GetHeaderLang());
                var user = GetData<BaseUser>(n => n.DeleteFlag == false && n.BaseUserId == CusCode.UserId);

                if (user == null)
                {
                    BaseUser baseUser = new BaseUser();
                    baseUser.UserName = CusCode.UserName;
                    baseUser.BaseUserId = CusCode.UserId;
                    baseUser.Customer = await GetRepo().GetCustomer(CusCode.Cuscode);
                    GetRepo().Add(baseUser);
                    GetRepo().SaveChange();
                }

                log.CreateUser = user;
            }
            
            GetRepo().Add(log);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetLog(string ApiKey)
        {
            var property = await GetRepo().sendRequest<RecievePropertyModel>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var CusCode = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var data = GetRepo().GetData<LogUpdate>(n=>n.DeleteFlag == false && n.CreateUser.UserName == CusCode.UserName);
            RecievePropertyModel r = new RecievePropertyModel();

                if (property.nameTH != data.FullNameTh)
                {
                   r.nameTH = property.nameTH;
                }
                if (property.nameEN != data.FullNameEn)
                {

                    r.nameEN = property.nameEN;

                }
                if (property.nickname != data.NickName)
                {

                    r.nickname = property.nickname;

                    
                }
                if (property.houseNumber != data.Phone)
                {

                    r.houseNumber = property.houseNumber;

                   
                }
                if (property.career != data.Position)
                {

                    r.career = property.career;

                    
                }
                if (property.religion != data.Religion)
                {

                    r.religion = property.religion;

                   
                }
                if (property.education != data.Study)
                {
                   
                    r.education = property.education;

                    
                }
                if (property.phoneNumber != data.Tel)
                {

                    r.phoneNumber = property.phoneNumber;

                    
                }
                if (property.workplaceAddress != data.WorkAddress)
                {

                    r.workplaceAddress = property.workplaceAddress;

                   
                }
                if (property.email != data.Email)
                {

                    r.email = property.email;

                    
                }
                if (property.duty != data.Duty)
                {

                    r.duty = property.duty;

                }
                if (property.littleWork != data.Departure)
                {

                    r.littleWork = property.littleWork;

                    
                }
                if (property.workplaceName != data.CompanyName)
                {

                    r.workplaceName = property.workplaceName;

                   
                }
                if (property.dathOfBirth != data.Birthday)
                {

                    r.dathOfBirth = property.dathOfBirth;

                   
                }
                if (property.age != data.Age)
                {

                    r.age = property.age;

                    
                }
                if (property.address != data.Address)
                {

                    r.address = property.address;

                    
                }
                if (property.iDCardNumber != data.IdCard)
                {

                    r.iDCardNumber = property.iDCardNumber;
                }
            
           
            return new ObjectResult(r); ;
        }

    }
}
