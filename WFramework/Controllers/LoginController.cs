﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using Services;

namespace WFramework.User.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet,HttpPost]
        public IActionResult Index(LoginModel loginModel)
        {
            ViewData["title"] = Environment.GetEnvironmentVariable("AppName");
            ViewData["cuscode"] = Environment.GetEnvironmentVariable("CusCode");
            if (loginModel.CusCode==null && ViewData["cuscode"] != null)
            {
                loginModel.CusCode = Environment.GetEnvironmentVariable("CusCode");
                ModelState.Remove("CusCode");
            }
            if(ModelState.ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid)
            {
                LoginServices loginServices = new LoginServices();
                var l = loginServices.Login(loginModel);
                if (l)
                {
                    return Redirect("http://google.co.th");
                }
            }
            return View(loginModel);
        }
      
    }
}