﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Anajan.DbContext;
using Anajan.Entity;
using Anajan.Models;
using Framework.BaseAttrib;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Anajan.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TestController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetTest()
        {
            var a = GetAll<TestData>(n=>n.DeleteFlag == false).ToList();
            

            return new ObjectResult(a);
        }
        [HttpPost]
        public IActionResult AddTest(TestModel testModel)
        {
            TestData testData = new TestData();
            testData.Name = testModel.Name;
            testData.Lname = testModel.Lname;
            testData.CreateDate = DateTime.Now;
            GetRepo().Add(testData);
            GetRepo().SaveChange();
            return  Ok();
        }
        [HttpPost]
        public IActionResult AddProfile([FromQuery]long Id , List<BasePropertyModel> BasePropertyModel)
        {
            var data = GetData<TestData>(n=>n.Id == Id);
            GetRepo().UpdateProperties(data, BasePropertyModel);
            return Ok();
        }

        [HttpPost]
        public IActionResult GetProp()
        {
            var data = GetAll<TestData>();
            var res = GetRepo().GetProperty(data.ToList());
            return new ObjectResult(res);
        }
        [HttpPost]
        public IActionResult Getdatatable(BaseDataTable baseDataTable)
        {
            var data = GetAll<TestData>().ToDataTable(baseDataTable);

            return new ObjectResult(data);
        }

        [HttpPost]
        public IActionResult Getdatatable1(int start , int lenght)
        {
            BaseDataTable baseDataTable = new BaseDataTable();
            baseDataTable.Start = start;
            baseDataTable.Length = lenght;
            var data = GetAll<TestData>().ToDataTable(baseDataTable);

            return new ObjectResult(data);
        }

        public IActionResult GetDataAA()
        {

        }
    }
}
