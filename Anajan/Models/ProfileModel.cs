﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Anajan.Models
{
    public class ProfileModel
    {
        public string Property { get; set; }
        public string Value { get; set; }
        public PropertyType PropetyType { get; set; }
    }
}
