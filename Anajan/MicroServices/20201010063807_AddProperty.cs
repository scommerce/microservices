﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Anajan.MicroServices
{
    public partial class AddProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TestDataId",
                table: "BaseProperties",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_TestDataId",
                table: "BaseProperties",
                column: "TestDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_TestData_TestDataId",
                table: "BaseProperties",
                column: "TestDataId",
                principalTable: "TestData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseProperties_TestData_TestDataId",
                table: "BaseProperties");

            migrationBuilder.DropIndex(
                name: "IX_BaseProperties_TestDataId",
                table: "BaseProperties");

            migrationBuilder.DropColumn(
                name: "TestDataId",
                table: "BaseProperties");
        }
    }
}
