﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Anajan.Entity
{
    public class TestData : BaseEntityCusCodeProperty
    {
        [Required]
        public string Name { get; set; }

        public string Lname { get; set; }
    }
}
