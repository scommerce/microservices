﻿using Anajan.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Anajan.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<TestData> TestData { get; set; }
    }
}
