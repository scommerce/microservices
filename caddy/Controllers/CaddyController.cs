﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using caddy.Entity;
using caddy.Models;
using caddy.DbContext;
using Framework.BaseEntity;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace caddy.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CaddyController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetCaddy()
        {
                var data = GetAll<Caddy>(n=> n.DeleteFlag == false).ToList();
                return new ObjectResult(data);
        }

        [HttpPost]
        public async Task<IActionResult> AddCaddy(CaddyModel request)
        {
            var data = GetData<Caddy>(n => n.PersonalCard == request.PersonalCard && n.DeleteFlag == false) ;
            if (data != null)
            {
                return BadRequest("Data is Already");
            }

            var CusCode = await GetRepo().GetUserFromApiKey(request.ApiKey, GetHeaderLang());
            var user = GetData<BaseUser>(n=>n.DeleteFlag == false && n.BaseUserId == CusCode.UserId);

            if (user == null)
            {
                BaseUser baseUser = new BaseUser();
                baseUser.UserName = CusCode.UserName;
                baseUser.BaseUserId = CusCode.UserId;
                baseUser.Customer = await GetRepo().GetCustomer(CusCode.Cuscode);
                GetRepo().Add(baseUser);
                GetRepo().SaveChange();
            }

            Caddy caddy = new Caddy();
            caddy.FirstName = request.FirstName;
            caddy.LastName = request.LastName;
            caddy.PersonalCard = request.PersonalCard;
            caddy.Phone = request.Phone;
            caddy.Price = request.Price;
            caddy.ZipCode = request.ZipCode;
            caddy.Rating = 0;
            caddy.Email = request.Email;
            caddy.BirthDate = request.BirthDate;
            caddy.BankName = request.BankName;
            caddy.BookBankName = request.BookBankName;
            caddy.BookBankNumber = request.BookBankNumber;
            caddy.Line = request.Line;
            caddy.Fun = request.Fun;
            caddy.Accuracy = request.Accuracy;
            caddy.Lang = request.Lang;
            caddy.IdLine = request.IdLine;
            caddy.Walk = request.Walk;
            caddy.Drive = request.Drive;
            caddy.Tips = request.Tips;
            caddy.FieldId = request.FieldId;
            caddy.CreateUser = user;
            caddy.Customer = await GetRepo().GetCustomer(CusCode.Cuscode);
            caddy.Images = request.Images;
            caddy.VdoId = request.VdoId;
            //Image image = new Image();
            //foreach (var sa in request.Images)
            //{
            //    image.ImageId = sa.ImageId;
            //    GetRepo().SaveChange();
            //}

            GetRepo().Add(caddy);
            GetRepo().SaveChange();

            return Ok("Success");
        }
        
        [HttpPost]
        public async Task<IActionResult> UpdateCaddy(CaddyModel request)
        {
            if (request.ApiKey == null)
            {
                return BadRequest("Data Is null");
            }
            var Key = await GetRepo().GetUserFromApiKey(request.ApiKey, GetHeaderLang());

            var caddy = GetData<Caddy>(n=>n.DeleteFlag == false && n.CreateUser.UserName == Key.UserName);

            caddy.FirstName = request.FirstName;
            caddy.LastName = request.LastName;
            caddy.PersonalCard = request.PersonalCard;
            caddy.Phone = request.Phone;
            caddy.Price = request.Price;
            caddy.ZipCode = request.ZipCode;
            caddy.Rating = request.Rating;
            caddy.Email = request.Email;
            caddy.BirthDate = request.BirthDate;
            caddy.BankName = request.BankName;
            caddy.BookBankName = request.BookBankName;
            caddy.BookBankNumber = request.BookBankNumber;
            caddy.Line = request.Line;
            caddy.Fun = request.Fun;
            caddy.Accuracy = request.Accuracy;
            caddy.IdLine = request.IdLine;
            caddy.Walk = request.Walk;
            caddy.Drive = request.Drive;
            caddy.Lang = request.Lang;
            caddy.Tips = request.Tips;
            caddy.Images = request.Images;
            caddy.VdoId = request.VdoId;
            caddy.FieldId = request.FieldId;

            GetRepo().Update(caddy);
            GetRepo().SaveChange();


            return Ok();
        }

        [HttpGet]
        public IActionResult DeleteCaddy(long Id)
        {
            if (Id == null)
            {
                return Problem("Data is Null");
            }
            var data = GetData<Caddy>(n=>n.DeleteFlag == false && n.Id == Id);
            data.DeleteFlag = true;
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetCaddyByApiKey(string ApiKey)
        {
            var Key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());

            var data = GetData<Caddy>(n=>n.DeleteFlag == false && n.CreateUser.UserName == Key.UserName);
            if (data == null)
            {
                return Problem("Data is Null");
            }
            return new ObjectResult(data);
        }
    }
}
