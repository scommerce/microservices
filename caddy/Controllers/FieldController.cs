﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using caddy.Entity;
using caddy.DbContext;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NetTopologySuite.Geometries;
using caddy.Models;

namespace caddy.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FieldController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetField()
        {
            var a = GetAll<Field>(n => n.DeleteFlag == false).ToList();
            return new ObjectResult(a);
        }
        [HttpGet]
        public IActionResult GetFieldByOption(string option,double Lat, double Lng)
        {
            if (option == "Rate")
            {
                var data = GetRepo().GetDataAll<Field>(n => n.DeleteFlag == false).OrderByDescending(n=>n.Rate).Skip(10).Take(20).ToList();
                return new ObjectResult(data);
            }
            //else if (option == "location")
            //{
            //    var Cusdis = new Point(Lat, Lng) { SRID = 4326 };
            //    var data = GetRepo().GetDataAll<Field>(n => n.DeleteFlag == false && n.LocationPoint.Distance(Cusdis) * 100000 <= 1000000).OrderByDescending(n=>n.LocationPoint.Distance(Cusdis) * 100000).ToList();
            //    return new ObjectResult(data);
            //}
            else if( option == null)
            {
                var data = GetRepo().GetDataAll<Field>(n => n.DeleteFlag == false).ToList();
                return new ObjectResult(data);
            }
            else
            {
                return Problem();
            }
        }

        [HttpPost]
        public IActionResult AddField(Field field)
        {
            Field data = new Field();
            data.ImageId = field.ImageId;
            data.Name = field.Name;
            data.Lat = field.Lat;
            data.Lng = field.Lng;
            data.Phone = field.Phone;
            data.ZoneId = field.ZoneId;
            data.Address = field.Address;
            data.Email = field.Email;
            GetRepo().Add(data);
            GetRepo().SaveChange();

            return Ok();
        }
        public IActionResult UpdateField(FieldModel field)
        {
            var data = GetData<Field>(n => n.DeleteFlag == false && n.Id == field.Id);

            data.ImageId = field.ImageId;
            data.Name = field.Name;
            data.Lat = field.Lat;
            data.Lng = field.Lng;
            data.Phone = field.Phone;
            data.ZoneId = field.ZoneId;
            data.Address = field.Address;
            data.Email = field.Email;

            GetRepo().Add(data);
            GetRepo().SaveChange();
            return Ok();
        }

    }
}
