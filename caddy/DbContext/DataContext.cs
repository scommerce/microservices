﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using caddy.Entity;

namespace caddy.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Caddy> Caddy { get; set; }
        public DbSet<Field> Field { get; set; }
        public DbSet<Review> Review { get; set; }
        public DbSet<Image> Images { get; set; }
        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Caddy;User Id=sa;Password=P@ssword1234;";
        }
    }
}
