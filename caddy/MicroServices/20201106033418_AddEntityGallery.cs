﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class AddEntityGallery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Other",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "Otherint",
                table: "Caddy");

            migrationBuilder.AddColumn<decimal>(
                name: "Drive",
                table: "Caddy",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "IdLine",
                table: "Caddy",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Walk",
                table: "Caddy",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "Gallery",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CaddyId = table.Column<long>(nullable: false),
                    ImageId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gallery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Gallery_Caddy_CaddyId",
                        column: x => x.CaddyId,
                        principalTable: "Caddy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Gallery_CaddyId",
                table: "Gallery",
                column: "CaddyId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Gallery");

            migrationBuilder.DropColumn(
                name: "Drive",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "IdLine",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "Walk",
                table: "Caddy");

            migrationBuilder.AddColumn<string>(
                name: "Other",
                table: "Caddy",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Otherint",
                table: "Caddy",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
