﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class Update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Caddy_Field_FieldId",
                table: "Caddy");

            migrationBuilder.DropIndex(
                name: "IX_Caddy_FieldId",
                table: "Caddy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Caddy_FieldId",
                table: "Caddy",
                column: "FieldId");

            migrationBuilder.AddForeignKey(
                name: "FK_Caddy_Field_FieldId",
                table: "Caddy",
                column: "FieldId",
                principalTable: "Field",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
