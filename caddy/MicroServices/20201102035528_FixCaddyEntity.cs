﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class FixCaddyEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Chinese",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "Korean",
                table: "Caddy");

            migrationBuilder.AlterColumn<string>(
                name: "Other",
                table: "Caddy",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddColumn<long>(
                name: "FieldId",
                table: "Caddy",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Lang",
                table: "Caddy",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Tips",
                table: "Caddy",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_Caddy_FieldId",
                table: "Caddy",
                column: "FieldId");

            migrationBuilder.AddForeignKey(
                name: "FK_Caddy_Field_FieldId",
                table: "Caddy",
                column: "FieldId",
                principalTable: "Field",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Caddy_Field_FieldId",
                table: "Caddy");

            migrationBuilder.DropIndex(
                name: "IX_Caddy_FieldId",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "FieldId",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "Lang",
                table: "Caddy");

            migrationBuilder.DropColumn(
                name: "Tips",
                table: "Caddy");

            migrationBuilder.AlterColumn<decimal>(
                name: "Other",
                table: "Caddy",
                type: "decimal(18,2)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Chinese",
                table: "Caddy",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Korean",
                table: "Caddy",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
