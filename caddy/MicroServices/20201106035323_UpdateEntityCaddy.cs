﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class UpdateEntityCaddy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdLine",
                table: "Caddy",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdLine",
                table: "Caddy");
        }
    }
}
