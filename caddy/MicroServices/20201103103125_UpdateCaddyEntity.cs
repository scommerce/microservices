﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class UpdateCaddyEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Otherint",
                table: "Caddy",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Otherint",
                table: "Caddy");
        }
    }
}
