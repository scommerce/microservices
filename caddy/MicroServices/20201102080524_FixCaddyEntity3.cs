﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.MicroServices
{
    public partial class FixCaddyEntity3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "FieldId",
                table: "BaseProperties",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_FieldId",
                table: "BaseProperties",
                column: "FieldId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseProperties_Field_FieldId",
                table: "BaseProperties",
                column: "FieldId",
                principalTable: "Field",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseProperties_Field_FieldId",
                table: "BaseProperties");

            migrationBuilder.DropIndex(
                name: "IX_BaseProperties_FieldId",
                table: "BaseProperties");

            migrationBuilder.DropColumn(
                name: "FieldId",
                table: "BaseProperties");
        }
    }
}
