﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace caddy.Microservices
{
    public partial class UpdateEntitycaddy2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Caddy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ImageId",
                table: "Caddy",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
