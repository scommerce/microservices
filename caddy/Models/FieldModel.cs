﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace caddy.Models
{
    public class FieldModel
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public long ImageId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public long ZoneId { get; set; }
    }
}
