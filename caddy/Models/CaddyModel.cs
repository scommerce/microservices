﻿using caddy.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace caddy.Models
{
    public class CaddyModel
    {
        public long? Id { get; set; }
        public string ApiKey { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PersonalCard { get; set; }
        [Required]
        public long ImageId { get; set; }
        public DateTime BirthDate { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal Rating { get; set; }
        [Required]
        public string BankName { get; set; }
        [Required]
        public string BookBankName { get; set; }
        [Required]
        public string BookBankNumber { get; set; }
        public string ZipCode { get; set; }
        public decimal Line { get; set; }
        public decimal Fun { get; set; }
        public decimal Accuracy { get; set; }
        public decimal Walk { get; set; }
        public decimal Drive { get; set; }
        public string Lang { get; set; }
        public decimal Tips { get; set; }
        public long? FieldId { get; set; }
        public string IdLine { get; set; }
        public List<Image> Images { get; set; }
        public long VdoId { get; set; }
    }
}
