﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace caddy.Entity
{
    public class Review : BaseEntityCusCode
    {
        public long Pid { get; set; }
        public string Comment { get; set; }
        public decimal Score { get; set; }
    }
}
