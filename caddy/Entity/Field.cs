﻿using Framework.BaseEntity;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace caddy.Entity
{
    public class Field : BaseEntityCusCodeProperty
    {
        public string Name { get; set; }
        public long ImageId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public long ZoneId { get; set; }
        public decimal Rate { get; set; }
        //[NotMapped]
        //public Point LocationPoint
        //{
        //    get
        //    {
        //        var Location = new Point(Lat, Lng) { SRID = 4264 };
        //        return Location;
        //    }
        //}
    }

}
