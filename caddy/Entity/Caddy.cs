﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace caddy.Entity
{
    public class Caddy : BaseEntityCusCode
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PersonalCard { get; set; }
        public DateTime BirthDate { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal Rating { get; set; }
        [Required]
        public string BankName { get; set; }
        [Required]
        public string BookBankName { get; set; }
        [Required]
        public string BookBankNumber { get; set; }
        public string ZipCode { get; set; }
        public decimal Line { get; set; }
        public decimal Fun { get; set; }
        public decimal Accuracy { get; set; }
        public decimal Walk { get; set; }
        public decimal Drive { get; set; }
        public string  Lang { get; set; }
        public decimal Tips { get; set; }
        public string IdLine { get; set; }
        public long  VdoId { get; set; }
        public virtual List<Image> Images { get; set; }
        public long? FieldId { get; set; }
        [ForeignKey("FieldId")]
        public virtual Field Field { get; set; }
    }
}
