﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase.DbContext;
using Firebase.Model;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Framework.Startup;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Firebase.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class NotificationController : BaseWebController<DataContext>
    {
        public NotificationController(IHostingEnvironment hostingEnvironment)
        {
            env = hostingEnvironment;
        }
        public readonly IHostingEnvironment env;

        public async Task<IActionResult> SendAsync(NotificationModel notification)
        {
            if (FirebaseApp.DefaultInstance == null)
            {
                FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.GetApplicationDefault(),
                });
            }
            Entity.Notification notification1 = new Entity.Notification();
            var u = await GetRepo().GetUserFromApiKey(notification.ApiKey, "th");
            notification1.Customer = u.BaseCustomer;
            notification1.CreateUser = u.BaseUser;
            notification1.Token = notification.ToToken;
            notification1.Image = notification.ImageUrl;
            notification1.Message = notification.Message;
            notification1.Title = notification.Title;
            notification1.Topic = notification.Topic;
            GetRepo().Add(notification1);
            GetRepo().SaveChange();
              //   FirebaseApp.Create();
              // This registration token comes from the client FCM SDKs.
              var registrationToken = notification.ToToken;

            // See documentation on defining a message payload.
            var message = new Message()
            {

                Token = registrationToken,
                Topic = notification.Topic,
                Android = new AndroidConfig()
                {
                    Notification = new AndroidNotification()
                    {
                        Title = notification.Title,
                        Body = notification.Message,
                        ImageUrl = notification.ImageUrl,
                        Sound = "monkey1.mp3",
                       ChannelId= "1000"
                    }
                },
               
            };

            // Send a message to the device corresponding to the provided
            // registration token.
            string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);
            // Response is a message ID string.
            Console.WriteLine("Successfully sent message: " + response);
            notification1.Send = true;
            notification1.SendDate = DateTime.Now;
            GetRepo().Update(notification1);
            GetRepo().SaveChange();
            return Ok();
        }
    }
}