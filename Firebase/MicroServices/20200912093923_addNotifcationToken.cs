﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Firebase.MicroServices
{
    public partial class addNotifcationToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Token",
                table: "Notifications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Token",
                table: "Notifications");
        }
    }
}
