﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Firebase.Entity
{
    public class Notification:BaseEntityCusCode
    {
        public string Topic { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
        public string Image { get; set; }
        public bool Send { get; set; }
        public DateTime? SendDate { get; set; }
    }
}
