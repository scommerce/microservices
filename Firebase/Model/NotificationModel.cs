﻿using NetTopologySuite.Geometries.Prepared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Firebase.Model
{
    public class NotificationModel
    {
        public string ApiKey { get; set; }
        public string ToToken { get; set; }
        public string Topic { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string ImageUrl { get; set; }

    }
}
