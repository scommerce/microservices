﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System;
using System.Security.Authentication;

namespace TestEmail
{
    class Program
    {
        static void Main(string[] args)
        {
            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress("KPI PMCU", "noreply.kpi@pmcu.co.th");
            message.From.Add(from);

            MailboxAddress to = new MailboxAddress("Marwin", "win05178@hotmail.com");
            message.To.Add(to);

            message.Subject = "Test Email";
            BodyBuilder bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<h1>Hello World!</h1>";
           // bodyBuilder.TextBody = "Hello World!";

            message.Body = bodyBuilder.ToMessageBody();

            SmtpClient client = new SmtpClient();
             client.SslProtocols = System.Security.Authentication.SslProtocols.Ssl3 | System.Security.Authentication.SslProtocols.Tls | System.Security.Authentication.SslProtocols.Tls11 | System.Security.Authentication.SslProtocols.Tls12 | System.Security.Authentication.SslProtocols.Tls13;
            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
            client.CheckCertificateRevocation = false;
          //  client.SslProtocols = SslProtocols.Ssl3 | SslProtocols.Ssl2 | SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
            client.Connect("mail.pmcu.co.th", 465, SecureSocketOptions.Auto);
            client.Authenticate("noreply.kpi@pmcu.co.th", "CUProperty");
            client.Send(message);
            client.Disconnect(true);
            client.Dispose();


        }
    }
}
