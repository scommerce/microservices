﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace TestRecord.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Entity.TestRecord> TestRec { get; set; }
    }

}
