﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRecord.Entity
{
    public class TestRecord : BaseEntityCusCode
    {
        public string Name { get; set; }
        public string Lname { get; set; }
    }
}
