﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Net.Http;
using System.Reflection.Metadata.Ecma335;
using System.Threading;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using NetTopologySuite.Geometries;
using Shop.DbContext;
using Shop.Entity;
using Shop.Models;
using Shop.Services;

namespace Shop.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class LinglomShopController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> AddShop(LinglomShopAddModel addModel)
        {
            LinglomShop linglomShop = new LinglomShop();
            linglomShop.Bank = GetRepo().GetData<Bank>(n => n.Id == addModel.BankId);
            linglomShop.BookBankImageId = addModel.BookBankImageId;
            linglomShop.BookBankNo = addModel.BookBankNo;
            linglomShop.Commission = addModel.Commission;
            linglomShop.Customer = await GetRepo().RegisterCustomer(addModel.CusCode);
            linglomShop.Desc = addModel.Desc;
            linglomShop.Lat = addModel.Lat;
            linglomShop.Lng = addModel.Lng;
            LingLomLocation lingLomLocation = new LingLomLocation();
            lingLomLocation.LocationPoint = new Point(linglomShop.Lng, linglomShop.Lat) { SRID = 4326 };
            linglomShop.Location = lingLomLocation;
            linglomShop.ShopImageId = addModel.ShopImageId;
            linglomShop.ShopName = addModel.ShopName;
            linglomShop.ShopNameEng = addModel.ShopNameEng;
            linglomShop.Tel = addModel.Tel;
            linglomShop.Address = addModel.Address;
            //var Count = GetAll<LinglomShop>(n => n.DeleteFlag == false).ToList();
            //linglomShop.Shopcode = "Food-" + (Count.Count() + 1).ToString("00000#");
            ProductGroup pg = new ProductGroup();
           
            //var MicroService =  GetRepo().GetMicroServices("Product");
            //var Shop = await GetRepo().sendRequest<ProductGroup>(MicroService.ServiceIp + "/api/LingLomProductGroup/AddProductGroup", , true, false);
            GetRepo().Add(linglomShop);
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> EditShop(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var property = await GetRepo().sendRequest<EditShopPropertyModel>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var shop = GetData<LinglomShop>(n => n.DeleteFlag == false && n.Shopcode == property.ShopCode);
            shop.ShopName = property.StoreName;
            shop.ShopImageId = property.ImageStore;
            shop.Address = property.Address;
            shop.Lat = property.Lat;
            shop.Lng = property.Lng;
            shop.Opentime = property.OpenTime;
            shop.Closetime = property.CloseTime;
            shop.Desc = property.Description;
            shop.Tel = property.Phone;
            shop.Monday = (property.Monday == "Open") ? true : false;
            shop.Tuesday = (property.Tuesday == "Open") ? true : false;
            shop.Wednesday = (property.Wednesday == "Open") ? true : false;
            shop.Thursday = (property.Thursday == "Open") ? true : false;
            shop.Friday = (property.Friday == "Open") ? true : false;
            shop.Saturday = (property.Saturnday == "Open") ? true : false;
            shop.Sunday = (property.Sunday == "Open") ? true : false;

            GetRepo().Update(shop);
            GetRepo().SaveChange();

            if (shop.Location == null)
            {
                LingLomLocation location = new LingLomLocation();
                location.Id = shop.Id;
                location.LocationPoint = new Point(shop.Lng, shop.Lat) { SRID = 4326 };
               
                GetRepo().Add(location);
                GetRepo().SaveChange();

                shop.Location = location;

                GetRepo().Update(shop);
                GetRepo().SaveChange();
            }
            else if(shop.Location != null) {

                var lo = GetData<LingLomLocation>(n=>n.Id == shop.Location.Id);
                lo.LocationPoint = new Point(shop.Lng, shop.Lat) { SRID = 4326};

                GetRepo().SaveChange();
            }
            

            return Ok("Success");
        }


        [HttpGet]
        public IActionResult GetShopById(long Shop)
        {
            var d = GetRepo().GetData<LinglomShop>(n => n.Id == Shop && n.DeleteFlag == false);

            return new ObjectResult(d);
        }
        [HttpGet]
        public async Task<IActionResult> GetShopByApi(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var cusCOde = await GetRepo().sendRequest<ShopCus>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var d = GetRepo().GetData<LinglomShop>(n => n.Shopcode == cusCOde.ShopCode && n.DeleteFlag == false);
            return new ObjectResult(d);
        }

        [HttpGet]
        public IActionResult GetShopByShopCode(string shopcode)
        {
            var d = GetRepo().GetData<LinglomShop>(n => n.Shopcode == shopcode && n.DeleteFlag == false);
            return new ObjectResult(d);
        }

        [HttpPost]
        public IActionResult GetShopByCate(LingLomGetShopModel request)
        {
            var Cusdis = new Point(request.Lng, request.Lat) { SRID = 4326 };
            if (request.CateId != null) {
                var cate = GetAll<ShopCate>(n => n.CategoryId == request.CateId && n.Category.DeleteFlag == false && n.Shop.ShopStatus == Enum.ShopStatus.Open && n.Shop.DeleteFlag == false && n.Shop.Location.LocationPoint.Distance(Cusdis) * 100000 <= 1000000)
                    .OrderBy(n => n.Shop.Location.LocationPoint.Distance(Cusdis) * 100000).Skip(request.Skip).Take(20).ToList().Select(n => LinglomServices.GetShopObject(n.Shop, Cusdis, n.CategoryId));
                var res = cate.ToList();

                return new ObjectResult(res);
            }
            else {
                var cate = GetAll<LinglomShop>(n => n.DeleteFlag == false && n.ShopStatus == Enum.ShopStatus.Open  && n.Location.LocationPoint.Distance(Cusdis) * 100000 <= 1000000)
                 .OrderBy(n => n.Location.LocationPoint.Distance(Cusdis) * 100000).Skip(request.Skip).Take(20)
                .ToList().Select(n => LinglomServices.GetShopObject(n, Cusdis, 0));
                var res = cate.ToList();

                return new ObjectResult(res);
            }
        }
        [HttpPost]
        // [NotDataMap]
        public IActionResult GetNearShop(LingLomGetShopModel location)
        {
            var l = new Point(location.Lng, location.Lat) { SRID = 4326 };
            var shop = GetAll<LinglomShop>(n => n.DeleteFlag == false && n.ShopStatus == Enum.ShopStatus.Open && n.Location.LocationPoint.Distance(l) * 100000  <= 10000)
                .OrderBy(n => n.Location.LocationPoint.Distance(l) * 100000).Skip(location.Skip).Take(20).ToList().Select(n => LinglomServices.GetShopObject(n, l, 0));
            var shope = shop.ToList();

            return new ObjectResult(shope);
        }


        [HttpGet]
        public IActionResult SearchShop(string Name, double Lat, double lng)
        {
            var l = new Point(lng, Lat) { SRID = 4326 };
            var res = GetAll<LinglomShop>(n => n.DeleteFlag == false &&( n.ShopName.Contains(Name) || n.ShopNameEng.Contains(Name) || n.Desc.Contains(Name) || n.ShopCate.FirstOrDefault().Category.CategoryName.Contains(Name) )&& n.ShopStatus == Enum.ShopStatus.Open && n.Location.LocationPoint.Distance(l) * 100000 <= 1000000)
                .OrderBy(n => n.Location.LocationPoint.Distance(l) * 100000)
                .ToList().Select(n => LinglomServices.GetShopObject(n, l, 0));
            var result = res.ToList();
            return new ObjectResult(result);
        }

        [HttpGet]
        public async Task<IActionResult> OpenShop(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var cusCOde = await GetRepo().sendRequest<ShopCus>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var shop = GetRepo().GetData<LinglomShop>(n => n.DeleteFlag == false  && n.Shopcode == cusCOde.ShopCode);
            shop.ShopStatus = Enum.ShopStatus.Open;

            GetRepo().Update(shop);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> CloseShop(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var cusCOde = await GetRepo().sendRequest<ShopCus>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var shop = GetRepo().GetData<LinglomShop>(n => n.DeleteFlag == false && n.Shopcode == cusCOde.ShopCode);
            shop.ShopStatus = Enum.ShopStatus.Close;

            GetRepo().Update(shop);
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetShopStatus(string ApiKey)
        {
            var key = await GetRepo().GetUserFromApiKey(ApiKey, GetHeaderLang());
            var cusCOde = await GetRepo().sendRequest<ShopCus>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey=" + ApiKey, HttpMethod.Get, null, false);
            var shop = GetRepo().GetData<LinglomShop>(n => n.DeleteFlag == false && n.Shopcode == cusCOde.ShopCode);
            return new ObjectResult( new { 
                data = (shop.ShopStatus == Enum.ShopStatus.Open)? "เปิด" : "ปิด"
            });

        }

        [HttpGet]
        public IActionResult GetContact()
        {
            var Tel = "021495260";
            var Line = "https://line.me/R/ti/p/%40262ccxxv";
            var fb = "https://www.facebook.com/linglomdelivery/";
            
            return new ObjectResult(new
            {
                Tel = Tel,
                Line = Line,
                Facebook = fb
            });

        }


    }

}