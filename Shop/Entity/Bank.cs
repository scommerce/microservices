﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Entity
{
    public class Bank
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public string NameEng { get; set; }
    }
}
