﻿using System;
using System.Collections.Generic;
using Framework.BaseEntity;

namespace Shop.Entity
{
    public class ShopCate
    {

        public long CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public long ShopId { get; set; }
        public virtual LinglomShop Shop { get; set; }

    }
}
