﻿using Framework.BaseEntity;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;
using Shop.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Z.BulkOperations;

namespace Shop.Entity
{
    public class LinglomShop:BaseEntityCusCode
    {
        public string ShopName { get; set; }
        public string ShopNameEng { get; set; }
        public string Desc { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Address { get; set; }

        public virtual LingLomLocation Location { get; set; }
        public long ShopImageId { get; set; }
        public int Commission { get; set; }
        public string Tel { get; set; }

        public virtual Bank Bank { get; set; }
        public string BookBankNo { get; set; }
        public long BookBankImageId { get; set; }
        public long ZoneId { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
        public TimeSpan Opentime { get; set; }
        public TimeSpan Closetime { get; set; }
        public string Shopcode { get; set; }
        public ShopStatus ShopStatus { get; set; }
        [NotMapped]
        public bool ShopClose { get {
                if (ShopStatus == ShopStatus.Close)
                    return true;
                if ((DateTime.Now.TimeOfDay.Hours - Closetime.Hours) <= 0)
                {
                    return true;
                }
                return false;
            
            } }
        public decimal Rating { get; set; }
        public virtual List<ShopCate> ShopCate { get; set; }

    }
}
