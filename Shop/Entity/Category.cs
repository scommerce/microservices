﻿using System;
using Framework.BaseEntity;

namespace Shop.Entity
{
    public class Category: BaseEntityCusCode
    {
        public string CategoryName { get; set; }
    }
}
