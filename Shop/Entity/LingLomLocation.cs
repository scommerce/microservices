﻿using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Entity
{
    public class LingLomLocation
    {
        [Key]
        public long Id { get; set; }
        [Column(TypeName = "geometry")]
        public Point LocationPoint { get; set; }
    }
}
