﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Enum
{
    public enum ShopStatus
    {
        Close,
        Open
    }
}
