﻿using Framework.Helper;
using NetTopologySuite.Geometries;
using Shop.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services
{
    public static class LinglomServices
    {
        public static object GetShopObject(LinglomShop linglom, Geometry l, long cat = 0)
        {
            return new
            {
                CateId = cat,
                ShopId = linglom.Id,
                ShopName = linglom.ShopName,
                ShopNameEn = linglom.ShopNameEng,
                ShopImage = linglom.ShopImageId,
                Address = linglom.Address,
                OpenTime = linglom.Opentime.ToString(@"hh\:mm"),
                CloseTime = linglom.Closetime.ToString(@"hh\:mm"),
                ShopStatus = (int)linglom.ShopStatus,
                ShopClose = linglom.ShopClose,
                Rating = linglom.Rating,
                Desc = linglom.Desc,
                RealDistance = Calculation.CalDistance(linglom.Location.LocationPoint, l),
                Distinct = (Calculation.CalDistance(linglom.Location.LocationPoint, l) < 1000) ? Math.Round((Calculation.CalDistance(linglom.Location.LocationPoint, l)),2) + " ม." : Math.Round(Calculation.CalDistance(linglom.Location.LocationPoint, l)/1000,2,MidpointRounding.AwayFromZero) + " กม."
            };
        }
       
    }
}
