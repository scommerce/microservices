﻿
using Microsoft.EntityFrameworkCore;
using Shop.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Shop.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<LinglomShop> LinglomShop { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ShopCate> ShopCate { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=MicroServices_Shop;User Id=sa;Password=P@ssword1234;";
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //   modelBuilder.Entity<LinglomShop>().HasMany(n => n.ShopCate);
            modelBuilder.Entity<ShopCate>().HasKey(n => new { n.ShopId, n.CategoryId });
        }
    }
}
