﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class addCate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ShopCateId",
                table: "LinglomShop",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BaseLanguage",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    Lang = table.Column<string>(nullable: true),
                    OrigianlText = table.Column<string>(nullable: true),
                    ToText = table.Column<string>(nullable: true),
                    Error = table.Column<bool>(nullable: false),
                    BaseApiReturnId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseLanguage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseApiReturns_BaseApiReturnId",
                        column: x => x.BaseApiReturnId,
                        principalTable: "BaseApiReturns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseLanguage_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    CategoryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Categories_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Categories_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ShopCate",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    CategoryId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShopCate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ShopCate_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ShopCate_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ShopCate_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ShopCate_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LinglomShop_ShopCateId",
                table: "LinglomShop",
                column: "ShopCateId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_BaseApiReturnId",
                table: "BaseLanguage",
                column: "BaseApiReturnId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_CreateUserId",
                table: "BaseLanguage",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseLanguage_UpdateUserId",
                table: "BaseLanguage",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_CreateUserId",
                table: "Categories",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_CustomerId",
                table: "Categories",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_UpdateUserId",
                table: "Categories",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_CategoryId",
                table: "ShopCate",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_CreateUserId",
                table: "ShopCate",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_CustomerId",
                table: "ShopCate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_UpdateUserId",
                table: "ShopCate",
                column: "UpdateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_LinglomShop_ShopCate_ShopCateId",
                table: "LinglomShop",
                column: "ShopCateId",
                principalTable: "ShopCate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LinglomShop_ShopCate_ShopCateId",
                table: "LinglomShop");

            migrationBuilder.DropTable(
                name: "BaseLanguage");

            migrationBuilder.DropTable(
                name: "ShopCate");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropIndex(
                name: "IX_LinglomShop_ShopCateId",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "ShopCateId",
                table: "LinglomShop");
        }
    }
}
