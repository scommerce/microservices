﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class addCate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LinglomShop_ShopCate_ShopCateId",
                table: "LinglomShop");

            migrationBuilder.DropForeignKey(
                name: "FK_ShopCate_BaseUsers_CreateUserId",
                table: "ShopCate");

            migrationBuilder.DropForeignKey(
                name: "FK_ShopCate_BaseCustomers_CustomerId",
                table: "ShopCate");

            migrationBuilder.DropForeignKey(
                name: "FK_ShopCate_BaseUsers_UpdateUserId",
                table: "ShopCate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShopCate",
                table: "ShopCate");

            migrationBuilder.DropIndex(
                name: "IX_ShopCate_CreateUserId",
                table: "ShopCate");

            migrationBuilder.DropIndex(
                name: "IX_ShopCate_CustomerId",
                table: "ShopCate");

            migrationBuilder.DropIndex(
                name: "IX_ShopCate_UpdateUserId",
                table: "ShopCate");

            migrationBuilder.DropIndex(
                name: "IX_LinglomShop_ShopCateId",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "CreateUserId",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "DeleteFlag",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "UpdateUserId",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "ShopCateId",
                table: "LinglomShop");

            migrationBuilder.AlterColumn<long>(
                name: "CategoryId",
                table: "ShopCate",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ShopId",
                table: "ShopCate",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShopCate",
                table: "ShopCate",
                columns: new[] { "ShopId", "CategoryId" });

            migrationBuilder.AddForeignKey(
                name: "FK_ShopCate_LinglomShop_ShopId",
                table: "ShopCate",
                column: "ShopId",
                principalTable: "LinglomShop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShopCate_LinglomShop_ShopId",
                table: "ShopCate");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ShopCate",
                table: "ShopCate");

            migrationBuilder.DropColumn(
                name: "ShopId",
                table: "ShopCate");

            migrationBuilder.AlterColumn<long>(
                name: "CategoryId",
                table: "ShopCate",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "ShopCate",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "ShopCate",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreateUserId",
                table: "ShopCate",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "CustomerId",
                table: "ShopCate",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DeleteFlag",
                table: "ShopCate",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "ShopCate",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UpdateUserId",
                table: "ShopCate",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ShopCateId",
                table: "LinglomShop",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ShopCate",
                table: "ShopCate",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_CreateUserId",
                table: "ShopCate",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_CustomerId",
                table: "ShopCate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_ShopCate_UpdateUserId",
                table: "ShopCate",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_LinglomShop_ShopCateId",
                table: "LinglomShop",
                column: "ShopCateId");

            migrationBuilder.AddForeignKey(
                name: "FK_LinglomShop_ShopCate_ShopCateId",
                table: "LinglomShop",
                column: "ShopCateId",
                principalTable: "ShopCate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShopCate_BaseUsers_CreateUserId",
                table: "ShopCate",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShopCate_BaseCustomers_CustomerId",
                table: "ShopCate",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ShopCate_BaseUsers_UpdateUserId",
                table: "ShopCate",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
