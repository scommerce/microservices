﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class UpdateShopCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Shopcode",
                table: "LinglomShop",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Shopcode",
                table: "LinglomShop");
        }
    }
}
