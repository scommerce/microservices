﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class UpdateLinglomShop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "Closetime",
                table: "LinglomShop",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<bool>(
                name: "Friday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Monday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "Opentime",
                table: "LinglomShop",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<bool>(
                name: "Saturday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Sunday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Thursday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Tuesday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Wednesday",
                table: "LinglomShop",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "BaseProperties",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Property = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    PropetyType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseProperties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseProperties_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CreateUserId",
                table: "BaseProperties",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_CustomerId",
                table: "BaseProperties",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseProperties_UpdateUserId",
                table: "BaseProperties",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaseProperties");

            migrationBuilder.DropColumn(
                name: "Closetime",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Friday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Monday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Opentime",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Saturday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Sunday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Thursday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Tuesday",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "Wednesday",
                table: "LinglomShop");
        }
    }
}
