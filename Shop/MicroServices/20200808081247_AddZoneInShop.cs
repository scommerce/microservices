﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class AddZoneInShop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ZoneId",
                table: "LinglomShop",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ZoneId",
                table: "LinglomShop");
        }
    }
}
