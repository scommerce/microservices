﻿using Microsoft.EntityFrameworkCore.Migrations;
using NetTopologySuite.Geometries;

namespace Shop.MicroServices
{
    public partial class UpdateLocation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location",
                table: "LinglomShop");

            migrationBuilder.AddColumn<long>(
                name: "LocationId",
                table: "LinglomShop",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LingLomLocation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LocationPoint = table.Column<Point>(type: "geometry", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LingLomLocation", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LinglomShop_LocationId",
                table: "LinglomShop",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_LinglomShop_LingLomLocation_LocationId",
                table: "LinglomShop",
                column: "LocationId",
                principalTable: "LingLomLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LinglomShop_LingLomLocation_LocationId",
                table: "LinglomShop");

            migrationBuilder.DropTable(
                name: "LingLomLocation");

            migrationBuilder.DropIndex(
                name: "IX_LinglomShop_LocationId",
                table: "LinglomShop");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "LinglomShop");

            migrationBuilder.AddColumn<Point>(
                name: "Location",
                table: "LinglomShop",
                type: "geometry",
                nullable: true);
        }
    }
}
