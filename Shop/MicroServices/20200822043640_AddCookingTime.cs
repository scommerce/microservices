﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class AddCookingTime : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<TimeSpan>(
                name: "CookingTime",
                table: "LinglomShop",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CookingTime",
                table: "LinglomShop");
        }
    }
}
