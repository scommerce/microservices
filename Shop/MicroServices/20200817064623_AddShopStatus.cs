﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.MicroServices
{
    public partial class AddShopStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ShopStatus",
                table: "LinglomShop",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopStatus",
                table: "LinglomShop");
        }
    }
}
