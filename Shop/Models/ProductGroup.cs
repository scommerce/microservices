﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ProductGroup
    {
        [Required]
        public string GroupNameTh { get; set; }
        [Required]
        public string GroupNameEn { get; set; }
        [Required]
        public bool MultiSelect { get; set; }
        public string OrderBy { get; set; }

        public long? DetailId { get; set; }
        public long? ProductId { get; set; }
        public string ApiKey { get; set; }
    }
}
