﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class LingLomGetShopModel
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
        public long ZoneId { get; set; }
        public long CateId { get; set; }
        public int Skip { get; set; }
    }
}
