﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class LinglomShopAddModel
    {
        [Required]
        public string ShopName { get; set; }
        public string ShopNameEng { get; set; }
        public string Desc { get; set; }
        [Required]
        public long ShopImageId { get; set; }
        [Required]
        public double Lat { get; set; }
        [Required]
        public double Lng { get; set; }
        [Required]
        public int Commission { get; set; }
        [Required]
        public string Tel { get; set; }
        [Required]
        public long BankId { get; set; }
        [Required]
        public string BookBankNo { get; set; }
        [Required]
        public long BookBankImageId { get; set; }
        public string Address { get; set; }
        public string CusCode { get; set; }


    }
}
