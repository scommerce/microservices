﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ShopDetail
    {
        public string ShopCode { get; set; }
        public string Line { get; set; }
        public string Facebook { get; set; }
        public string Tel { get; set; }
    }
}
