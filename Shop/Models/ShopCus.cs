﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models
{
    public class ShopCus
    {
        public long UserId { get; set; }
        public string ShopCode { get; set; }
    }
}
