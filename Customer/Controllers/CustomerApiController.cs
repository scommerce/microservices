﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Customer.DbContext;
using Framework.BaseAttrib;
using Framework.BaseEntity;
using Framework.BaseFramework;
using Framework.BaseInterface;
using Framework.BaseModel;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Customer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerApiController : BaseController<DataContext>
    {

       // [ResponseCache(Duration = 60)]
        [HttpPost]
        public IBaseReturnDatatable GetCustomersAll([FromForm]BaseDataTable baseDataTable)
        {




            var d = GetAll<Entity.Customer>().ToDataTable(baseDataTable);
            var bra = new BaseReturnApi(GetContext());
            return d;
        }

       // [ResponseCache(Duration = 60)]
        [HttpGet]
        public IActionResult GetCustomer(string CusCode)
        {
            var d = GetAll<Entity.Customer>(n=>n.CustomerCode == CusCode).FirstOrDefault();
          
            return new ObjectResult(d);
        }

         [HttpGet]
        
        public IActionResult GetLogoPath(string CusCode)
        {
            using (WebClient webClient = new WebClient())
            {
                var d = GetAll<Entity.Customer>(n => n.CustomerCode == CusCode).FirstOrDefault();

                byte[] data = webClient.DownloadData(d.LogoPath);

                return File(data, "image/png");
            }
        }
    }
}