﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Customer.MicroServices
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseApiTokens",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    TokenType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseApiTokens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Section = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseSettings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseUsers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    BaseUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseUsers_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseUsers_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BaseCustomers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    CustomerCode = table.Column<string>(nullable: true),
                    BaseCustomerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseCustomers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseCustomers_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    CustomerName = table.Column<string>(nullable: false),
                    CustomerCode = table.Column<string>(nullable: false),
                    TAX = table.Column<string>(nullable: true),
                    TAXType = table.Column<int>(nullable: false),
                    Tel = table.Column<string>(nullable: true),
                    AddressFull = table.Column<string>(nullable: true),
                    Address1 = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    Address3 = table.Column<string>(nullable: true),
                    Address4 = table.Column<string>(nullable: true),
                    ProvinceId = table.Column<long>(nullable: true),
                    ProvinceName = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    LineId = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FAX = table.Column<string>(nullable: true),
                    LogoPath = table.Column<string>(nullable: true),
                    ParentCustomerId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_Customers_ParentCustomerId",
                        column: x => x.ParentCustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_CreateUserId",
                table: "BaseApiTokens",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_CustomerId",
                table: "BaseApiTokens",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseApiTokens_UpdateUserId",
                table: "BaseApiTokens",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_CreateUserId",
                table: "BaseCustomers",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_CustomerId",
                table: "BaseCustomers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseCustomers_UpdateUserId",
                table: "BaseCustomers",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_CreateUserId",
                table: "BaseSettings",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_CustomerId",
                table: "BaseSettings",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseSettings_UpdateUserId",
                table: "BaseSettings",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_CreateUserId",
                table: "BaseUsers",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_CustomerId",
                table: "BaseUsers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseUsers_UpdateUserId",
                table: "BaseUsers",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CreateUserId",
                table: "Customers",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CustomerId",
                table: "Customers",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ParentCustomerId",
                table: "Customers",
                column: "ParentCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_UpdateUserId",
                table: "Customers",
                column: "UpdateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseUsers_CreateUserId",
                table: "BaseApiTokens",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseUsers_UpdateUserId",
                table: "BaseApiTokens",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseApiTokens_BaseCustomers_CustomerId",
                table: "BaseApiTokens",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseUsers_CreateUserId",
                table: "BaseSettings",
                column: "CreateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseUsers_UpdateUserId",
                table: "BaseSettings",
                column: "UpdateUserId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseSettings_BaseCustomers_CustomerId",
                table: "BaseSettings",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BaseUsers_BaseCustomers_CustomerId",
                table: "BaseUsers",
                column: "CustomerId",
                principalTable: "BaseCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BaseCustomers_BaseUsers_CreateUserId",
                table: "BaseCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BaseCustomers_BaseUsers_UpdateUserId",
                table: "BaseCustomers");

            migrationBuilder.DropTable(
                name: "BaseApiTokens");

            migrationBuilder.DropTable(
                name: "BaseSettings");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "BaseUsers");

            migrationBuilder.DropTable(
                name: "BaseCustomers");
        }
    }
}
