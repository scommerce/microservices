﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Customer.MicroServices
{
    public partial class addBaseMicroServices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BaseMicroServices",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ServiceIp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseMicroServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseMicroServices_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseMicroServices_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BaseMicroServices_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_CreateUserId",
                table: "BaseMicroServices",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_CustomerId",
                table: "BaseMicroServices",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BaseMicroServices_UpdateUserId",
                table: "BaseMicroServices",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BaseMicroServices");
        }
    }
}
