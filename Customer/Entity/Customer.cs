﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Customer.Entity
{
    public class Customer: BaseEntityCusCode
    {
        [Required]
        public string CustomerName { get; set; }
        [Required]
        public string CustomerCode { get; set; }

        public string TAX { get; set; }
        public virtual TAXType TAXType { get; set; }
        public string Tel { get; set; }
        public string AddressFull { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public long? ProvinceId { get; set; }
        public string ProvinceName { get; set; }
        public string ZipCode { get; set; }
        public string LineId { get; set; }
        public string Email { get; set; }
        public string FAX { get; set; }
        public string LogoPath { get; set; }

        public virtual ICollection<Customer> SubCustomer { get; set; }

        public virtual Customer ParentCustomer { get; set; }

        public long? ParentCustomerId { get; set; }


        [NotMapped]
        public string CustomerCodeAndName { get
            {
                return CustomerCode+" " + CustomerName;
            }
        }

    }
}
