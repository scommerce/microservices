﻿using System;
using System.Collections.Generic;
using Framework.BaseEntity;

namespace Swane.Entity
{
    public class QrCode: BaseEntityCusCode
    {
        public long ProductId { get; set; }
       public string ProductName { get; set; }
        public String Code { get; set; }
        public virtual List<QrCode> SubCode { get; set; }
        public byte[] ByteCode { get; set; }
        public bool GenSuccess { get; set; }
        public virtual List<Scan> UserScan { get; set; }
    }
}
