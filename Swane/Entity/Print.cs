﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swane.Entity
{
    public class Print: BaseEntityCusCode
    {
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public int Qty { get; set; }
        public int Size { get; set; }
        public int Progress { get; set; }
        public virtual List<QrCode> SubCode { get; set; }

        public bool GenSuccess { get; set; }
    }
}
