﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Swane.Entity
{
    public class Scan: BaseEntityCusCode
    {
        public string UserName { get; set; }
        public bool Member { get; set; }
        [NotMapped]
        public string CreateDateString { get
            {
                return CreateDate.ToString("dd-MM-yyyy HH:mm:ss");
            }
        }
        //public int MyProperty { get; set; }
    }
}
