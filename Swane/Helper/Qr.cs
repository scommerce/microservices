﻿using System;
using System.Drawing;
using System.IO;
using QRCoder;

namespace Swane.Helper
{
    public static class Qr
    {
        public static byte[] GetQrCode(string code)
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(code,
            QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);

            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            Bitmap resized = new Bitmap(qrCodeImage, new Size(50, 50));


            return ImageToByte2(resized);
        }
        public static byte[] ImageToByte2(Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }
    }
}
