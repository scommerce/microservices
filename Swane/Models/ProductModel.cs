﻿using NetTopologySuite.Geometries.Prepared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swane.Models
{
    public class ProductModel
    {
        public long Id { get; set; }
        public string NameTh { get; set; }
        public int Size { get; set; }
    }
}
