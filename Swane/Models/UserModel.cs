﻿using System;
namespace Swane.Models
{
    public class UserModel
    {
        public string UserName { get; set; }
        public long Id { get; set; }
        public string Password { get; set; }
        public string CusCode { get; set; }
    }
}
