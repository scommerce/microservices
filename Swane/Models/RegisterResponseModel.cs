﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Swane.Models
{
    public class RegisterResponseModel
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }
    
        public DateTime? Birthday { get; set; }

        public string Facebook { get; set; }

        public string Ig { get; set; }

        public string Address { get; set; }
        public string IdCard { get; set; }

        public string Tel { get; set; }

        public string Line { get; set; }

        public int Sex { get; set; }


        public long ImageFile { get; set; }

        public long IdCardFile { get; set; }
        public DateTime? ApproveDate { get; set; }
        public bool Approve { get; set; }
        public string ParentName { get; set; }
        public string Provice { get; set; }
        public string Province { get; set; }
    }
}
