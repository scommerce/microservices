﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Swane.Models
{
    public class RegisterModel
    {
        public long Id { get; set; }
        // [Required(ErrorMessage ="กรุณาใส่ชื่อผู้ใช้งาน")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "กรุณาใส่รหัสผ่าน")]
        public string Password { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ยืนยันรหัสผ่าน")]
        [Compare("Password",ErrorMessage ="รหัสผ่านไม่ตรงกัน")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ชื่อ")]
        public string Name { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ชื่อเล่น")]
        public string NickName { get; set; }
        [Required(ErrorMessage = "กรุณาใส่วันเกิด วว/ดด/ปปปป")]
        public DateTime? Birthday { get; set; }
   
        public string Facebook { get; set; }
        [Required(ErrorMessage = "กรุณาใส่จังหวัด")]
        public string Provice { get; set; }

        public string Ig { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ที่อยู่")]
        public string Address { get; set; }
        public string IdCard { get; set; }
        [Required(ErrorMessage = "กรุณาใส่เบอร์โทรศัพท์")]
        public string Tel { get; set; }
        [Required(ErrorMessage = "กรุณาใส่ไลน์ไอดี")]
        public string Line { get; set; }
        [Required(ErrorMessage = "กรุณาใส่เพศ")]
        public int Sex { get; set; }


        [Required(ErrorMessage = "กรุณาอัพโหลดภาพหน้าตรง")]
        public IFormFile ImageFile { get; set; }
        [Required(ErrorMessage = "กรุณาอัพโหลดภาพบัตรประจำตัวประชาขน")]
        public IFormFile IdCardFile { get; set; }

    

    }
}
