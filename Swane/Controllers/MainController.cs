﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup.BaseAttrib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Swane.Controllers
{
    public class MainController : Controller
    {
        [Authorize(Policy = "Login")]
        public IActionResult Index()
        {
            return View("Dashboard");
        }
    }
}
