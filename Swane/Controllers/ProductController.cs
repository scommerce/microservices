﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Login.DbContext;
using Microsoft.AspNetCore.Mvc;
using Swane.Models;

namespace Swane.Controllers
{
    public class ProductController : BaseWebController<DataContext>
    {
        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Add(ProductModel product)
        {

            var p = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9009/api/Product/AddProduct", HttpMethod.Post, new {
                NameEn=product.NameTh,
                NameTh=product.NameTh,
                CusCode= "SWANE-001",
                Price=0
            }, true);

            await GetRepo().sendRequest("http://203.150.107.134:9009/api/Product/UpdateProperty?productId="+p.Id.ToString()+"&Property=Size&Value=" + product.Size.ToString()+"&PropertyType=2", HttpMethod.Get, null, true);


            return RedirectToAction("Index");
        }
    }
}
