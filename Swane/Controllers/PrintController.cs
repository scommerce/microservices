﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Swane.Controllers
{
    public class PrintController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> GetReportAsync(string id, string type)
        {

            using (HttpClient client = new HttpClient())
            {
                var file = await client.GetAsync("http://203.154.83.161:5000/api/Report/PrintQr?id=" + id );

                return new FileContentResult(await file.Content.ReadAsByteArrayAsync(), file.Content.Headers.ContentType.MediaType)
                {
                    FileDownloadName = DateTime.Now.ToString("ddMMyyyyHHmmss")+"." + type
                };
            }
        }
    }
}