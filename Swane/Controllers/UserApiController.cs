﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swane.DbContext;
using Swane.Models;

namespace Swane.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserApiController : BaseController<DataContext> { 
        [NotDataMap]
        public async Task<IActionResult> GetUser()
        {
            var data = await GetRepo().sendRequest<List<UserModel>>("http://203.150.107.134:9002/api/User/GetUserAdminSwane", HttpMethod.Get, null, false);
            return new ObjectResult(data);
        }

        [NotDataMap]
        public async Task<IActionResult> GetUserRegister()
        {
            try
            {
                var data = await GetRepo().sendRequest<List<RegisterResponseModel>>("http://203.150.107.134:9002/api/User/GetUserSwane", HttpMethod.Get, null, false);
                return new ObjectResult(data);
            }catch(Exception ex)
            {
                return new ObjectResult(new List<RegisterResponseModel>()); 
            }
        }
    }
}
