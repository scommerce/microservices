﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Login.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Swane.DbContext;
using Swane.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Swane.Controllers
{
    public class RegisterController : BaseWebController<DataContext>
    {
        // GET: /<controller>/

        public RegisterController(IWebHostEnvironment env)
        {

            _env = env;

        }
        public IActionResult Index(string User)
        {
            ViewData["user"] = User;
            return View();
        }
        public IActionResult Registered(string user)
        {
            ViewData["User"] = user;
            return View();
        }
        public IActionResult Html(string html)
        {

            return GetHtml("test");
        }
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> UpdateProfileAsync(string id)
        {
            RegisterModel registerModel = new RegisterModel();
            var um = await GetRepo().sendRequest<List<Models.RegisterResponseModel>>("http://203.150.107.134:9002/api/User/GetUserSwane", HttpMethod.Get, null, false);
            var user = um.Where(n => n.Id.ToString() == id).FirstOrDefault();
            if(user != null)
            {
                registerModel.Id = long.Parse(id);
                registerModel.Name = user.Name;
                registerModel.NickName = user.NickName;
                registerModel.Address = user.Address;
              
                registerModel.Facebook = user.Facebook;
                registerModel.IdCard = user.IdCard;
                registerModel.Ig = user.Ig;
                registerModel.Line = user.Line;
                registerModel.Provice = user.Province;
                registerModel.Sex = user.Sex;
                registerModel.Tel = user.Tel;
                registerModel.UserName = user.UserName;
                registerModel.Password = "1234";
                registerModel.ConfirmPassword = "1234";
            }

            return View(registerModel);
        }
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> UpdateRegisterAsync(RegisterModel registerModel)
        {
            InsertProfile insertProfile = new InsertProfile();
            insertProfile.UserId = registerModel.Id;
            insertProfile.ProfileProperty = new List<BasePropertyModel>();
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Address",
                Value = registerModel.Address,

                PropertyType = PropertyType.String
            });
          
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Facebook",
                Value = registerModel.Facebook,
                PropertyType = PropertyType.String
            });
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "IdCard",
                Value = registerModel.IdCard,
                PropertyType = PropertyType.String
            });
            //insertProfile.ProfileProperty.Add(new BasePropertyModel()
            //{
            //    Property = "IdCardFile",
            //    Value = idCard.ImageId.ToString(),
            //    PropertyType = PropertyType.Long
            //});
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Ig",
                Value = registerModel.Ig,
                PropertyType = PropertyType.String
            });
            //insertProfile.ProfileProperty.Add(new BasePropertyModel()
            //{
            //    Property = "ImageFile",
            //    Value = image.ImageId.ToString(),
            //    PropertyType = PropertyType.Long
            //});
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Line",
                Value = registerModel.Line,
                PropertyType = PropertyType.String
            });
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Name",
                Value = registerModel.Name,
                PropertyType = PropertyType.String
            });
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "NickName",
                Value = registerModel.NickName,
                PropertyType = PropertyType.String
            });
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Sex",
                Value = registerModel.Sex.ToString(),
                PropertyType = PropertyType.Int
            });
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Tel",
                Value = registerModel.Tel,
                PropertyType = PropertyType.String
            });
          
            insertProfile.ProfileProperty.Add(new BasePropertyModel()
            {
                Property = "Province",
                Value = registerModel.Provice,
                PropertyType = PropertyType.String
            });
           

            await GetRepo().sendRequest("http://203.150.107.134:9002/api/User/UpdateProfilesForSwane", HttpMethod.Post, insertProfile, true);


            return View();
        }
        public async Task<IActionResult> Register(RegisterModel registerModel, [FromQuery]string User)
        {
            if (HttpContext.Request.Method == "GET")
            {
                // The action is a POST.
                ModelState.Clear();
            }
            else
            {
                try
                {
                    var user = await GetRepo().sendRequest<Models.UserModel>("http://203.150.107.134:9002/api/User/GetFromUserName?Username=" + User + "&Cuscode=SWANE-001", HttpMethod.Get, null, true);


                    var um = await GetRepo().sendRequest<List<Models.RegisterResponseModel>>("http://203.150.107.134:9002/api/User/GetUserSwane", HttpMethod.Get,null,false);
                    if(um.Where(n=>(n.Approve == true || n.ApproveDate == null)  && n.IdCard == registerModel.IdCard).Count()>0)
                    {
                        ModelState.AddModelError("Error", "รหัสบัตรประชาชนซ้ำ");
                        return View(registerModel);
                    }

                    if (ModelState.IsValid)
                    {



                        var idCard = await GetRepo().uploadFile<BaseDataModel>("http://203.150.107.134:9003/api/Upload/Image?Cuscode=SWANE-001", HttpMethod.Put, registerModel.IdCardFile);
                        var image = await GetRepo().uploadFile<BaseDataModel>("http://203.150.107.134:9003/api/Upload/Image?Cuscode=SWANE-001", HttpMethod.Put, registerModel.ImageFile);
                        var u = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/User/AddSwane", HttpMethod.Post, new
                        {
                            CusCode = "SWANE-001",
                            UserName = "test",
                            Password = registerModel.Password,
                            ConfirmPassword = registerModel.ConfirmPassword
                        }, true);

                        var api = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/Login/Login", HttpMethod.Post, new
                        {
                            CusCode = "SWANE-001",
                            UserName = u.data,
                            Password = registerModel.Password
                        }, true, GetHeaderLang());


                        InsertProfile insertProfile = new InsertProfile();
                        insertProfile.ApiKey = api.data;
                        insertProfile.ProfileProperty = new List<BasePropertyModel>();
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Address",
                            Value = registerModel.Address,

                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Birthday",
                            Value = registerModel.Birthday.Value.ToString("yyyy-MM-dd"),
                            PropertyType = PropertyType.Date
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Facebook",
                            Value = registerModel.Facebook,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "IdCard",
                            Value = registerModel.IdCard,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "IdCardFile",
                            Value = idCard.ImageId.ToString(),
                            PropertyType = PropertyType.Long
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Ig",
                            Value = registerModel.Ig,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "ImageFile",
                            Value = image.ImageId.ToString(),
                            PropertyType = PropertyType.Long
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Line",
                            Value = registerModel.Line,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Name",
                            Value = registerModel.Name,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "NickName",
                            Value = registerModel.NickName,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Sex",
                            Value = registerModel.Sex.ToString(),
                            PropertyType = PropertyType.Int
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Tel",
                            Value = registerModel.Tel,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "ParentId",
                            Value = user.Id.ToString(),
                            PropertyType = PropertyType.Long
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "ParentName",
                            Value = user.UserName,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Province",
                            Value = registerModel.Provice,
                            PropertyType = PropertyType.String
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "Approve",
                            Value = "false",
                            PropertyType = PropertyType.Bool
                        });
                        insertProfile.ProfileProperty.Add(new BasePropertyModel()
                        {
                            Property = "ApproveDate",
                            Value = null,
                            PropertyType = PropertyType.DateTime
                        }); ;

                        await GetRepo().sendRequest("http://203.150.107.134:9002/api/User/UpdateProfiles", HttpMethod.Post, insertProfile, true);


                        var d = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/User/AddParentUser?ApiKey=" + api.data + "&UserId=" + user.Id, HttpMethod.Get, null, true);

                        return RedirectToAction("Registered", new { user = d.data });


                    }
                      

                    }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.Message);
                }
            }



            return View(registerModel);
        }
    }
}
