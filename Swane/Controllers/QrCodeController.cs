﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AspNetCore.Report;
using Framework.Startup;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.CompilerServices;
using Swane.DbContext;
using Swane.Entity;
using Swane.Helper;

namespace Swane.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class QrCodeController : BaseController<DataContext>
    {
        [HttpGet]
        public IActionResult GetQrCode(long productId, string proudctName, int size, int qty)
        {
            Thread thread = new Thread(delegate ()
            {
                Print print = new Print();
                print.ProductId = productId;
                print.ProductName = proudctName;
                print.Size = size;
                print.Qty = qty;
                print.SubCode = new List<QrCode>();
                GetRepo().Add(print);
                GetRepo().SaveChange();

                for (int i = 0; i < qty; i++)
                {
                    QrCode qr = new QrCode();
                    qr.ProductId = productId;
                    qr.ProductName = proudctName;
                    qr.Code = Guid.NewGuid().ToString().Replace("-", "");
                    qr.ByteCode = Qr.GetQrCode(qr.Code);
                    qr.GenSuccess = false;
                    qr.SubCode = new List<QrCode>();
                    qr.CreateDate = DateTime.Now;
                    for (int j = 0; j < size; j++)
                    {
                        QrCode subQr = new QrCode();
                        subQr.ProductId = productId;
                        subQr.ProductName = proudctName;
                        subQr.CreateDate = DateTime.Now;
                        subQr.Code = Guid.NewGuid().ToString().Replace("-", "");
                        subQr.ByteCode = Qr.GetQrCode(subQr.Code);
                        qr.SubCode.Add(subQr);

                    }

                    //qr.GenSuccess = true;
                    //
                    print.SubCode.Add(qr);
                    print.Progress++;
                    GetRepo().Update(print);

                    GetRepo().SaveChange();
                    Console.WriteLine("Gen " + qr.Code + " Success");

                }
                print.GenSuccess = true;
                GetRepo().Update(print);
                GetRepo().SaveChange();

            });
            thread.Start();

            return Ok();
        }

        [HttpGet]
        public IActionResult GetQrCodeAll()
        {
            var data = GetRepo().GetDataAll<Print>(n => n.DeleteFlag == false).OrderBy(n => n.Id).ToList();
            return new ObjectResult(data);
        }

        [HttpGet]
        public IActionResult ScanQr(string qr,string UserName,bool member)
        {
          

            var data = GetRepo().GetData<QrCode>(n => n.Code == qr);

            if (data.UserScan.Where(n => n.Member == false).Count() > 0)
            {
                return Problem("ไม่สามารถแสกนได้ เนื่องจากขายให้ลูกค้าทั่วไปแล้ว");

            }

            if (data.UserScan.Where(n=>n.UserName == UserName).Count()>0)
            {
                return Problem("ชื่อผู้ใช้ นี้ สแกนไปแล้ว");

            }

           

            if (data.SubCode.Count() > 0)
            {
                foreach(var q in data.SubCode)
                {
                    Scan scan1 = new Scan();
                    scan1.UserName = UserName;
                    scan1.CreateDate = DateTime.Now;
                    scan1.Member = member;
                    if (data.UserScan == null)
                        data.UserScan = new List<Scan>();
                    q.UserScan.Add(scan1);
                    GetRepo().Update(data);
                }
            }
            
                Scan scan = new Scan();
                scan.UserName = UserName;
                scan.CreateDate = DateTime.Now;
            scan.Member = member;
            if (data.UserScan == null)
                    data.UserScan = new List<Scan>();
                data.UserScan.Add(scan);
                GetRepo().Update(data);
              
            
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpGet]
        public IActionResult GetQr(string qr)
        {
            var data = GetRepo().GetData<QrCode>(n => n.Code == qr);



            return new ObjectResult(data);
        }


       
    }
}