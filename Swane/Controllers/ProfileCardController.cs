﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.Startup;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SixLabors.ImageSharp.Processing;
using Swane.DbContext;
using Swane.Models;

namespace Swane.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProfileCardController :  BaseController<DataContext>
    {
        public ProfileCardController(IHostingEnvironment hostingEnvironment)
        {
            env = hostingEnvironment;
        }
        public readonly IHostingEnvironment env;

        public async Task<IActionResult> GetProfileAsync(String ApiKey)
        {

            var data = await GetRepo().sendRequest<RegisterResponseModel>("http://203.150.107.134:9002/api/User/GetProfile?ApiKey="+ ApiKey, HttpMethod.Get,null, false, GetHeaderLang());





            var im = DownloadImage("http://203.150.107.134:9003/api/Upload/GetImage?Cuscode=SWANE-001&id=" + data.ImageFile);
            
            im.NormalizeOrientation();
            


            string imageSaveFilePath = env.WebRootPath+ "\\profile_card";
            if (!Directory.Exists(imageSaveFilePath))
            {
                Directory.CreateDirectory(imageSaveFilePath);
            }
            imageSaveFilePath = imageSaveFilePath + "\\S"+data.UserId+".jpg";
            String imageFilePath = env.WebRootFileProvider.GetFileInfo("img/profile_card.jpg")?.PhysicalPath;
            Bitmap newBitmap;
            using (var bitmap = (Bitmap)Image.FromFile(imageFilePath))//load the image file
            {
                using (Graphics graphics = Graphics.FromImage(bitmap))
                {
                    using (Font arialFont = new Font("Tahoma", 250))
                    {
                        graphics.DrawString("S"+data.UserId, arialFont, Brushes.White, new PointF(3300f, 1600f));
                       

                    }
                    string[] s = data.Address.Split(' ');
                    using (Font arialFont = new Font("Tahoma", 120))
                    {
                        
                        graphics.DrawString(data.Facebook, arialFont, Brushes.White, new PointF(3300f, 2150f));
                        graphics.DrawString(data.Ig, arialFont, Brushes.White, new PointF(3300f, 2600f));
                        graphics.DrawString(data.Line, arialFont, Brushes.White, new PointF(3300f, 3100f));
                        graphics.DrawString(data.Tel, arialFont, Brushes.White, new PointF(3300f, 3550f));
                        graphics.DrawString(data.Provice??s[s.Length-1], arialFont, Brushes.White, new PointF(3300f, 4050f));
                    }
                    graphics.DrawImage(im, 770f, 1690f,1730f, 1700f);
                }
                newBitmap = new Bitmap(bitmap);
            }

            newBitmap.Save(imageSaveFilePath);
            byte[] b = System.IO.File.ReadAllBytes(imageSaveFilePath);
            return File(b, "image/jpg") ;
        }

        Image DownloadImage(string fromUrl)
        {
            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                using (Stream stream = webClient.OpenRead(fromUrl))
                {
                    return Image.FromStream(stream);
                }
            }
        }
        public SixLabors.ImageSharp.Image GetReducedImage(int width, int height, Stream image1)
        {

            //Image image = System.Drawing.Image.FromStream(image1);

            //Image thumb = image.GetThumbnailImage(width, height, () => false, IntPtr.Zero);
            var image = SixLabors.ImageSharp.Image.Load(image1);
            image.Mutate(x => x.Resize(width, height));
            return image;


            //  return destImage;

        }

    }
}