﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Login.Model;
using Microsoft.AspNetCore.Mvc;
using Swane.DbContext;

namespace Swane.Controllers
{
    public class LoginController : BaseWebController<DataContext>
    {
        public async Task<IActionResult> Index(LoginModel loginModel)
        {
            loginModel.CusCode = "SWANE-001";
            if (HttpContext.Request.Method == "GET")
            {
                // The action is a post
                ModelState.Clear();
                return View();
            }
            try
            {
                var data = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/Login/Login", HttpMethod.Post, loginModel, true, GetHeaderLang());
                LoginSession(data.data);
                return RedirectToAction("Index", "Main");
              //  var key = await GetRepo().GetUserFromApiKey(data.data, GetHeaderLang());
            }catch(Exception ex)
            {
                ModelState.AddModelError("LoginFail", "ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบข้อมูล");
            }
        
            return View();
        }
        public async Task<IActionResult> Logout()
        {
            LogoutSession();
            return RedirectToAction("Index");
        }
    }
}
