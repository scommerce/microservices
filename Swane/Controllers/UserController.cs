﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Login.Model;
using Microsoft.AspNetCore.Mvc;
using Swane.DbContext;

namespace Swane.Controllers
{
    public class UserController : BaseWebController<DataContext>
    {
        public IActionResult Index()
        {
            return View();
        }


        public async Task<IActionResult> Add(UserModel user)
        {

            var u = await GetRepo().sendRequest<BaseDataModel>("http://203.150.107.134:9002/api/User/AddUser", HttpMethod.Post, new
            {
                CusCode = "SWANE-001",
                UserName = user.UserName,
                Password = user.Password,
                ConfirmPassword = user.Password
            }, true);


            return RedirectToAction("Index");
        }
    }
}
