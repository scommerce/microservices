﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseAttrib;
using Framework.Startup;
using Login.DbContext;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swane.Models;

namespace Swane.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]


    public class ProductApiController :  BaseController<DataContext>
    {
        [NotDataMap]
        public async Task<IActionResult> GetProduct()
        {
            var data = await GetRepo().sendRequest<List<ProductModel>>("http://203.150.107.134:9009/api/Product/GetProduct?cusCode=SWANE-001", HttpMethod.Get, null, false);
            return new ObjectResult(data);
        }
    }
}