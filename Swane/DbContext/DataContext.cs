﻿
using Microsoft.EntityFrameworkCore;
using Swane.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Swane.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {

        public DbSet<Print> Print { get; set; }
        public DbSet<QrCode> QrCodes { get; set; }

        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=Swane;User Id=sa;Password=P@ssword1234;";
        }
    }
}
