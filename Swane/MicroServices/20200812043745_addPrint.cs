﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Swane.MicroServices
{
    public partial class addPrint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PrintId",
                table: "QrCodes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Print",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    ProductId = table.Column<long>(nullable: false),
                    ProductName = table.Column<string>(nullable: true),
                    Qty = table.Column<int>(nullable: false),
                    Size = table.Column<int>(nullable: false),
                    GenSuccess = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Print", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Print_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Print_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Print_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QrCodes_PrintId",
                table: "QrCodes",
                column: "PrintId");

            migrationBuilder.CreateIndex(
                name: "IX_Print_CreateUserId",
                table: "Print",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Print_CustomerId",
                table: "Print",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Print_UpdateUserId",
                table: "Print",
                column: "UpdateUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_QrCodes_Print_PrintId",
                table: "QrCodes",
                column: "PrintId",
                principalTable: "Print",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QrCodes_Print_PrintId",
                table: "QrCodes");

            migrationBuilder.DropTable(
                name: "Print");

            migrationBuilder.DropIndex(
                name: "IX_QrCodes_PrintId",
                table: "QrCodes");

            migrationBuilder.DropColumn(
                name: "PrintId",
                table: "QrCodes");
        }
    }
}
