﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Swane.MicroServices
{
    public partial class addMember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Member",
                table: "Scan",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Member",
                table: "Scan");
        }
    }
}
