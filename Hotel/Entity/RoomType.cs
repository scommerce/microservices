<<<<<<< HEAD
﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entity
{
    public class RoomType:BaseEntityCusCode
    {
        public string RoomTypeName { get; set; }
        public string Desc { get; set; }
        public RoomImage ThumnailPath { get; set; }
        public List<RoomImage> RoomImages { get; set; }
        public List<RoomNo> RoomNos { get; set; }
        [NotMapped]
        public int RoomAvaliable { get
            {
                return RoomNos.Where(n => n.DeleteFlag == false && n.RoomStatus == RoomStatus.Avaliable).Count();
            }
        }
    }
}
=======
﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hotel.Entity
{
    public class RoomType:BaseEntityCusCode
    {
        public string RoomTypeName { get; set; }
        public string Desc { get; set; }
        public RoomImage ThumnailPath { get; set; }
        public List<RoomImage> RoomImages { get; set; }
        public List<RoomNo> RoomNos { get; set; }
        [NotMapped]
        public int RoomAvaliable { get
            {
                return RoomNos.Where(n => n.DeleteFlag == false && n.RoomStatus == RoomStatus.Avaliable).Count();
            }
        }
    }
}
>>>>>>> 3ec56854a629783b4c249ce0b2ca184ef9147a77
