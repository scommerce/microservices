<<<<<<< HEAD
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Hotel.DbContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Hotel
{
    public class Startup : Framework.Startup.Startup
    {
        public override IAuthorizationRequirement GetAuthorization()
        {
            return null;
        }
        public Startup(IConfiguration configuration) : base(configuration)
        {

        }
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSingleton<DataContext>();
            services.AddDbContext<DataContext>();
            services.AddTransient<DataContext>();
            services.AddMvc(config =>
            {
                config.Filters.Add(new Filtter(new DataContext()));
            });



        }
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (IServiceScope serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {

                DataContext context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                System.Collections.Generic.IEnumerable<string> s = context.Database.GetPendingMigrations();
                context.Database.Migrate();
            }
            base.Configure(app, env);


        }

       

    }

}
=======
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Hotel.DbContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Hotel
{
    public class Startup : Framework.Startup.Startup
    {
        public override IAuthorizationRequirement GetAuthorization()
        {
            return null;
        }
        public Startup(IConfiguration configuration) : base(configuration)
        {

        }
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSingleton<DataContext>();
            services.AddDbContext<DataContext>();
            services.AddTransient<DataContext>();
            services.AddMvc(config =>
            {
                config.Filters.Add(new Filtter(new DataContext()));
            });



        }
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (IServiceScope serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {

                DataContext context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                System.Collections.Generic.IEnumerable<string> s = context.Database.GetPendingMigrations();
                context.Database.Migrate();
            }
            base.Configure(app, env);


        }

       

    }

}
>>>>>>> 3ec56854a629783b4c249ce0b2ca184ef9147a77
