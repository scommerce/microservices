﻿using Framework.BaseEnum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseApiToken:BaseEntityCusCode
    {
        public string Token { get; set; }
        public TokenType TokenType { get; set; }
    }
}
