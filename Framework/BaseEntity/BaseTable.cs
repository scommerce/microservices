﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseTable:BaseEntityCusCode
    {
        public string Name { get; set; }
        public string Column { get; set; }
        public string Format { get; set; }
        public string Order { get; set; }
       
    }
}
