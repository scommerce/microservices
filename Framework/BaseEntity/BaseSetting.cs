﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseSetting:BaseEntityCusCode
    {
        public string Section { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
