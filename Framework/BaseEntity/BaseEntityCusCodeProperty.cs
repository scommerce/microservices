﻿using System;
using System.Collections.Generic;
using Framework.BaseEntity;

namespace Framework.BaseEntity
{
    public class BaseEntityCusCodeProperty : BaseEntityCusCode
    {
        public virtual List<BaseProperty> Properties { get; set; }
        public List<BaseProperty> GetProperties()
        {
            return Properties;
        }
    }
}
