﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseCustomer:BaseEntityCusCode
    {
        public string CustomerCode { get; set; }
        public string BaseCustomerId { get; set; }
        public virtual List<BaseUser> Users { get; set; }
    }
}
