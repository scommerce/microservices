﻿using System;
namespace Framework.BaseEntity
{
    public enum PropertyType
    {
      Char,
      String,
      Int,
      Long,
      Double,
      Date,
      DateTime,
      Bool
    }
}
