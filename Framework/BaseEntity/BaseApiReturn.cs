﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseApiReturn:BaseEntity
    {
        public string Name { get; set; }
        public string PropertyName { get; set; }
        public string PropertyNameNew { get; set; }
      
        public long? BaseApiReturnId { get; set; }
        [ForeignKey("BaseApiReturnId")]
        public virtual List<BaseApiReturn> SubClass { get; set; }

        public virtual List<BaseLanguage> BaseLanguage { get; set; }
    }
}
