﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseEntityCusCode:BaseEntity
    {
        public long? CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual BaseCustomer Customer { get; set; }

       
    }
}
