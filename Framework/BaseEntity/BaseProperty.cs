﻿using System;
using Framework.BaseEntity;

namespace Framework.BaseEntity
{
    public class BaseProperty: BaseEntityCusCode
    {
        
        public string Property { get; set; }
        public string Value { get; set; }
        public PropertyType PropetyType { get; set; }
    }
}
