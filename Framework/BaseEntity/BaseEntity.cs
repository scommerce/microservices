﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseEntity: BaseEntityModel
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public bool DeleteFlag { get; set; }


        public DateTime CreateDate
        {
            get
            {
                return this.createDate.HasValue
                   ? this.createDate.Value
                   : DateTime.Now;
            }

            set { this.createDate = value; }
        }
        private DateTime? createDate = null;

        public DateTime? UpdateDate { get; set; }

        [ForeignKey("CreateUserId")]
        public virtual BaseUser CreateUser { get; set; }
        [ForeignKey("UpdateUserId")]
        public virtual BaseUser UpdateUser { get; set; }

        public long GetId()
        {
            return Id;
        }
    }
}
