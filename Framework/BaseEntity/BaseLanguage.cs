﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEntity
{
    public class BaseLanguage:BaseEntity
    {
        public string Lang { get; set; }
        public string OrigianlText { get; set; }
        public string ToText { get; set; }
        public bool Error { get; set; }
    }
}
