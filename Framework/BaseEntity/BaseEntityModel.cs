﻿using System;
namespace Framework.BaseEntity
{
    public interface BaseEntityModel
    {
        public long GetId();
    }
}
