﻿using Castle.Core.Internal;
using Castle.DynamicProxy.Generators.Emitters;
using CertificatesToDBandBack;
using Framework.BaseExtjs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Framework.BaseAttrib
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]

    public class ExtAttribute : Attribute
    {
        public enum Screen
        {
            width,
            height
        }

        public virtual string GetExtend()
        {
            return null;
        }

        public virtual string GetXtypeSring()
        {
            return "";
        }
        public bool itemsFixTheme { get; set; }



        public string theme { get; set; } = "default";
        [ExtDefault]
        public int order { get; set; } = 0;


        public string name { get; set; }


        public bool hidden { get; set; }



        public string child { get; set; }


        public object width { get; set; } = null;


        public object height { get; set; } = null;



        public ExtJsListener listeners { get; set; }
        public virtual ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = new ExpandoObject();

            extJs.TryAdd("xtype", GetXtypeSring());
            extJs.TryAdd("order", order);
            if (listeners != null)

                extJs.TryAdd("listeners", listeners);
            if (width != null)
            {
                if (width.IsNumericType())
                {
                    extJs.TryAdd("width", width);
                }
           
                else if(width.GetType().IsEnum)
                {
                    if (width is Screen screen)
                    {
                        string size = "";
                        if (screen == Screen.width)
                        {
                            size = "Ext.getBody().getViewSize().width";
                        }
                        extJs.TryAdd("width", new JRaw(size));
                    }
                }
                else
                {
                    extJs.TryAdd("width", new JRaw(width));
                }
            }
            if (height != null)
            {
                if (height.IsNumericType())
                {
                    extJs.TryAdd("height", height);
                }
                else if (width.GetType().IsEnum)
                {
                    if (height is Screen screen)
                    {
                        string size = "";
                        if (screen == Screen.height)
                        {
                            size = "Ext.getBody().getViewSize().height";
                        }
                        extJs.TryAdd("height", new JRaw(size));
                    }

                }
                else
                {
                    extJs.TryAdd("height", new JRaw(height));
                }
            }
            if (name != null)
            {
                extJs.TryAdd("name", name);
            }
            else
            {
                if (p is PropertyInfo prop)
                    extJs.TryAdd("name", prop.Name);

                else if (p is Type c)
                    extJs.TryAdd("name", c.Name);
            }

            foreach (var myprop in GetType().GetProperties())
            {

                var a = myprop.GetCustomAttributes(true).Where(n => n is ExtDefaultAttribute);
                foreach (var mya in a)
                {

                    if (myprop.GetValue(this) != null)
                    {
                        extJs.TryAdd(myprop.Name, myprop.GetValue(this));
                    }

                }
            }


            if (GetExtend() != null)
            {
                extJs.TryAdd("extend", GetExtend());
            }




            if (child != null)
            {
                if (p is IList list)
                {
                    foreach (var l in list)
                    {
                        SetClass(l.GetType(), extJs, uiClass);
                    }
                }
                else if (p is Type c)
                {
                    SetClass(c, extJs, uiClass);
                }
                if (p is PropertyInfo pinfo)
                {
                    if (pinfo.PropertyType is Type c1)
                    {
                        SetClass(c1, extJs, uiClass);
                    }
                }
            }

            return extJs;
        }
        private void SetClass(Type c, ExpandoObject extJs, object uiClass = null)
        {
            var prop = c.GetProperties();
            List<ExpandoObject> listItemJs = new List<ExpandoObject>();
            foreach (var pp in prop)
            {
                ExpandoObject itemJs = null;
                if (pp.GetValue(uiClass) is IList list)
                {
                    foreach (var l in list)
                    {

                        foreach (var a in l.GetType().GetCustomAttributes(true).Where(n => n is ExtAttribute).ToList())
                        {

                            if (a is ExtAttribute extAttr)
                            {
                                if (extAttr.theme == theme || (extAttr.theme == "default" && itemJs == null))
                                {
                                    itemJs = new ExpandoObject();
                                    itemJs = extAttr.SetObject(theme, itemJs, l.GetType(), l);


                                }
                            }
                        }
                        if (itemJs != null)
                            listItemJs.Add(itemJs);

                    }

                }
                else
                {
                    foreach (var a in pp.GetCustomAttributes(true).Where(n => n is ExtAttribute).ToList())
                    {

                        if (a is ExtAttribute extAttr)
                        {
                            if (!itemsFixTheme)
                            {
                                if (extAttr.theme == theme || (extAttr.theme == "default" && itemJs == null))
                                {
                                    itemJs = new ExpandoObject();
                                    itemJs = extAttr.SetObject(theme, itemJs, pp, uiClass);


                                }
                            }
                            else
                            {
                                if (extAttr.theme == theme  && itemJs == null)
                                {
                                    itemJs = new ExpandoObject();
                                    itemJs = extAttr.SetObject(theme, itemJs, pp, uiClass);


                                }
                            }
                        }
                    }
                    if (itemJs != null)
                        listItemJs.Add(itemJs);
                }


            }
            listItemJs = listItemJs.OrderBy(n => ((dynamic)n).order).ToList();
            extJs.TryAdd(child, listItemJs);
        }
    }

    internal class ExtDefaultAttribute : Attribute
    {
    }
}
