﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseEnum
{
    public enum TokenType
    {
        BaseLogin,
        Firebase,
        Facebook,
        AD,
        LDAP
    }
}
