﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Framework.Helper
{
    public static class Encrypt
    {
        private enum EncryptType
        {
            MD5,
            SHA1
        }

        public static string md5(string source)
        {
            return cale(source, EncryptType.MD5);
        }

        public static string sha1(string source)
        {
            return cale(source, EncryptType.SHA1);
        }
        public static string Base64(string plainText)
        {
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }



        private static string cale(string source, EncryptType encryptType)
        {
            // step 1, calculate MD5 hash from input
            HashAlgorithm hashAlgorithm = null;
            switch (encryptType)
            {
                case EncryptType.MD5: hashAlgorithm = System.Security.Cryptography.MD5.Create(); break;
                case EncryptType.SHA1: hashAlgorithm = System.Security.Cryptography.SHA1.Create(); break;
                default: hashAlgorithm = null; break;
            }

            if (hashAlgorithm == null)
            {
                throw new Exception("Unknow EncryptType");
            }

            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(source);
            byte[] hash = hashAlgorithm.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static string EncryptString(string key, string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }
    }
}
