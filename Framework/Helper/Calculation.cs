﻿using GeoCoordinatePortable;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Helper
{
    public static class Calculation
    {
        public static double CalDistance(Geometry LocationPoint, Geometry l)
        {
            //var R = 6371; // km

            //var a = LocationPoint.Coordinate.Distance(l.Coordinate);
            //var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            //var d = R * c;
            //
            // double distanceInKm = c1.GetDistanceTo(c2) / 1000;

            GeoCoordinate c1 = new GeoCoordinate(LocationPoint.Coordinate.Y,LocationPoint.Coordinate.X);
            GeoCoordinate c2 = new GeoCoordinate(l.Coordinate.Y, l.Coordinate.X);

            double distanceInm = c1.GetDistanceTo(c2) ;

            return distanceInm;

            //return Math.Round(LocationPoint.Distance(l) * 100000, 2, MidpointRounding.AwayFromZero);
        }
    }
}
