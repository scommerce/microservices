﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseInterface
{
    public interface IToJsonable
    {
        public JRaw ToJson();
    }
}
