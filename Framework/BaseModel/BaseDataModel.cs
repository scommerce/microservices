﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseDataModel
    {
        public string data { get; set; }
        public long ImageId { get; set; }

        public long Id { get; set; }
    }
}
