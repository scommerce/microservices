﻿using Framework.BaseEntity;


namespace Framework.BaseModel
{
    
    public class BasePropertyModel
    {
        public string Property { get; set; }
        public string Value { get; set; }
        public PropertyType PropertyType { get; set; }

    }
}
