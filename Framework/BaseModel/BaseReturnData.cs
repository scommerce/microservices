﻿using Framework.BaseInterface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseReturnData<T> 
    {
        public T Data { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public int Count { get; set; }

    }
}
