﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseDataTable
    {
        public int Start { get; set; }
        public int Length { get; set; }

        public List<BaseSortDatatable> Sort { get; set; }
        public List<BaseFilterDatatable> Filter { get; set; }

    }
}
