﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseHttpCustomer
    {
        public long Id { get; set; }
        public string CustomerCode { get; set; }
    }
}
