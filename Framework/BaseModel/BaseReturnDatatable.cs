﻿using Framework.BaseInterface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseReturnDatatable: IBaseReturnDatatable
    {
        public IList Data { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public int Count { get; set; }

        public IList GetData()
        {
            return Data;
        }

        public void SetData(IList data)
        {
            Data = data;
        }
    }
}
