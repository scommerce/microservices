﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseModel
{
    public class BaseUserReturn
    {
        public long UserId { get; set; }
        public long CuscodeId { get; set; }
        public string UserName { get; set; }
        public string Cuscode { get; set; }
        public BaseUser BaseUser { get; set; }
        public BaseCustomer BaseCustomer { get; set; }
    }
}
