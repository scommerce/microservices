﻿using Framework.BaseEntity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.BaseContext
{
    public abstract  class BaseDbContext: DbContext
    {
        public DbSet<BaseUser> BaseUsers { get; set; }
        public DbSet<BaseCustomer> BaseCustomers { get; set; }
        public DbSet<BaseApiToken> BaseApiTokens { get; set; }
        public DbSet<BaseSetting> BaseSettings { get; set; }
        public DbSet<BaseTable> BaseTables { get; set; }
        public DbSet<BaseApiReturn> BaseApiReturns { get; set; }
        public DbSet<BaseLanguage> BaseLanguage { get; set; }
        public DbSet<BaseMicroServices> BaseMicroServices { get; set; }

        public DbSet<BaseProperty> BaseProperties { get; set; }

        public abstract string GetConnectionString();
        protected  override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            string conn = Environment.GetEnvironmentVariable("ConnectionString");
            if (conn == null)



            conn = "Server=203.150.107.134,1500; Database=Caddy;User Id=sa;Password=P@ssword1234;";

         //     conn = "Server=203.150.107.134,1500; Database=MicroServices_Login;User Id=sa;Password=P@ssword1234;";

                //conn = "Server=203.150.107.134,1500; Database=MicroServices_Shop;User Id=sa;Password=P@ssword1234;";

                conn = GetConnectionString();
             //   conn = "Server=203.150.107.134,1500; Database=PMCU_Kpi;User Id=sa;Password=P@ssword1234;";

                //     conn = "Server=203.150.107.134,1500; Database=MicroServices_Login;User Id=sa;Password=P@ssword1234;";

            //conn = "Server=203.150.107.134,1500; Database=MicroServices_Product;User Id=sa;Password=P@ssword1234;";



          // conn = "Server=203.150.107.134,1500; Database=MicroServices_Login;User Id=sa;Password=P@ssword1234;";
            //conn = "Server=203.150.107.134,1500; Database=MicroServices_Cutomer;User Id=sa;Password=P@ssword1234;";
            //string conn = "Server=203.150.107.134,1500; Database=MicroServices_LinglomOrder;User Id=sa;Password=P@ssword1234;";
            //string conn = "Server=203.150.107.134,1500; Database=MicroServices_Upload;User Id=sa;Password=P@ssword1234;";


            optionsBuilder.UseLazyLoadingProxies();

            Console.WriteLine(conn);
            optionsBuilder.UseSqlServer(conn, x => x.UseNetTopologySuite());
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            foreach (Microsoft.EntityFrameworkCore.Metadata.IMutableForeignKey relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            modelBuilder.Entity<BaseUser>().HasOne(n => n.Customer).WithMany(n => n.Users);
        }
    }
}
