﻿using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtMenuAttribute: ExtPanelAttribute
    {
        [ExtDefault]
        public bool plain { get; set; }
        [ExtDefault]
        public bool floating { get; set; } = false;
        public override string GetXtypeSring()
        {
            return "menu";
        }
        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs =  base.SetObject(theme, extJs, p, uiClass);
            extJs.TryAdd("renderTo", new JRaw("Ext.getBody()"));
            return extJs;
        }
    }
}
