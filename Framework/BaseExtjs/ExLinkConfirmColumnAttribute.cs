﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using Framework.BaseAttrib;

namespace Framework.BaseExtjs
{
    public class ExLinkConfirmColumnAttribute : ExtGridColumn
    {
        public enum option
        {
            Reload
        }
        public string ReportText { get; set; }
        public string ConfirmTitle { get; set; }
        public string ConfirmMsg { get; set; }

        public override string GetXtypeSring()
        {
            return "templatecolumn";
        }
        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);
            //   extJs.TryAdd("items", prop.Name);
            if (p is PropertyInfo prop)
            {
                // var button = ;
                //   var button = @"{text: '" + ReportText + "',href='" + prop.GetValue(uiClass) + "'}";

                extJs.TryAdd("tpl", "<a onclick=\"confirm('"+ConfirmTitle+"','"+ConfirmMsg+"',null)\" href='#' target=\"_blank\">" + ReportText + "</a>");
            }
            return extJs;
        }

    }
}
