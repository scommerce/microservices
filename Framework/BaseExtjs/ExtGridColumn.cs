﻿using Framework.BaseAttrib;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtGridColumn:ExtAttribute
    {
        public enum Fillter
        {
            None,
            String,
            Number,
            Date,
            List,
            Boolean
        }
        public override string GetXtypeSring()
        {
            return "gridcolumn";
        }
        [ExtDefault]
        public string text { get; set; }
        [ExtDefault]
        public string format { get; set; }

        public string tpl { get; set; }

        public string dataIndex { get; set; }

        public Fillter fillter { get; set; }
        [ExtDefault]
        public bool hidden { get; set; }


        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);
            if(dataIndex!=null && tpl==null)
                extJs.TryAdd("dataIndex", dataIndex);
            else
            {
                if (tpl == null)
                {
                    if (p is PropertyInfo prop)
                        extJs.TryAdd("dataIndex", prop.Name);

                    else if (p is Type c)
                        extJs.TryAdd("dataIndex", c.Name);
                }
            }
            switch (fillter)
            {
                case Fillter.String: extJs.TryAdd("filter", "string");break;
                case Fillter.Number: extJs.TryAdd("filter", "number"); break;
                case Fillter.Date: extJs.TryAdd("filter", "date"); break;
                case Fillter.List: extJs.TryAdd("filter", "list"); break;
                case Fillter.Boolean: extJs.TryAdd("filter", "boolean"); break;
                default:break;
            }

          
            return extJs;
        }
    }
}
