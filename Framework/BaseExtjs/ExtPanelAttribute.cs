﻿using Framework.BaseAttrib;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtPanelAttribute : ExtAttribute
    {
        [ExtDefault]
        public string title { get; set; }
        [ExtDefault]
        public bool frame { get; set; }
        [ExtDefault]
        public bool jsonSubmit { get; set; }
        [ExtDefault]
        public bool suspendLayout { get; set; }
        [ExtDefault]
        public bool draggable { get; set; }

    }
}
