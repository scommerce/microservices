﻿using Framework.BaseInterface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtProxy : IToJsonable
    {
        public string type { get; set; } = "ajax";
        public object url { get; set; }
        public string rootProperty { get; set; } = "data";
        public bool withCredentials { get; set; } = true;
        public bool paramsAsJson { get; set; }
        public enum ProxyType
        {
            LocalStorage,
            SessionStorage,
            Memory,
            Ajax,
            JsonP,
            Rest,
            Direct,
        }
        public enum Method
        {
            GET,
            POST,
            PUT,
            DELETE
        }
        public ProxyType proxyType { get; set; } = ProxyType.Ajax;
        private string renderType { get; set; }
        public bool useDefaultXhrHeader { get; set; } = false;
        public JRaw reader { get; set; }
        public JRaw actionMethods { get; set; }
        public Method createMethod { get; set; } = Method.POST;
        public Method readMethod { get; set; } = Method.GET;
        public Method updateMethod { get; set; } = Method.PUT;
        public Method deleteMethod { get; set; } = Method.DELETE;
        public string totalProperty { get; set; } = "recordsTotal";
        public JRaw ToJson() {
            switch (proxyType)
            {
                case ProxyType.Ajax: type = "ajax"; break;
                case ProxyType.Direct: type = "direct"; break;
                case ProxyType.JsonP: type = "jsonp"; break;
                case ProxyType.LocalStorage: type = "localstorage"; break;
                case ProxyType.Memory: type = "memory"; break;
                case ProxyType.Rest: type = "rest"; break;
                case ProxyType.SessionStorage: type = "sessionstorage"; break;

            }
            actionMethods = new JRaw(@"{
                create: '"+ GetMethod(createMethod)+ @"',
                read: '" + GetMethod(readMethod) + @"',
                update: '" + GetMethod(updateMethod) + @"',
                destroy: '" + GetMethod(deleteMethod) + @"',
            }");

            reader = new JRaw(@"{
             type: 'json',
             rootProperty: '" + rootProperty + @"',
             totalProperty:'"+ totalProperty + @"'
            }");
            return new JRaw(Newtonsoft.Json.JsonConvert.SerializeObject(this));
        }
        private string GetMethod(Method method)
        {
            switch (method)
            {
                case Method.DELETE:return "DELETE";
                case Method.GET: return "GET";
                case Method.POST: return "POST";
                case Method.PUT: return "PUT";
            }
            return null;
        }
    }
}
