﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;

namespace Framework.BaseExtjs
{
    public class ExtReportColumnAttribute: ExtGridColumn
    {
        public string ReportText { get; set; }
        public override string GetXtypeSring()
        {
            return "templatecolumn";
        }
        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);
            //   extJs.TryAdd("items", prop.Name);
            if (p is PropertyInfo prop)
            {
                // var button = ;
                //   var button = @"{text: '" + ReportText + "',href='" + prop.GetValue(uiClass) + "'}";
                
                   extJs.TryAdd("tpl", "<a href=\"{"+ prop.Name + "}\" target=\"_blank\">"+ReportText+"</a>");
            }
            return extJs;
        }

    }
}
