﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseExtjs
{
    public static class ExtJsHelper
    {
        
       
        public static JRaw SubmitForm(string form,string method,string nextUrl)
        {
            return new JRaw(@" function(){
                var form = this.up('"+ form + @"').getForm();
                if (form.isValid()) {
               
                  Ext.MessageBox.show({
                        msg: 'กรุณารอสักครู่...',
                        progressText: 'Saving...',
                        width: 300,
                        wait: {
                            interval: 200
                        },
                       
                    });
                    form.submit({
                        method: '" + method + @"',
                        success: function (form, action) {
                            Ext.MessageBox.hide();
                            Ext.util.Cookies.set('ApiKey', action.result.data.data);
                            Ext.Msg.alert('สำเร็จ', 'เข้าสู่ระบบสำเร็จ',function(){
                                 window.location.href = '" + nextUrl + @"'
                            });
                         
                        },
                        failure: function (form, action) {
                            console.log(action);
                            Ext.MessageBox.hide();
                            Ext.Msg.alert('ผิดพลาด', action.response.responseText ? JSON.parse(action.response.responseText).message : 'No response');
                           
                        }
                    });
                }
                }");
        }
    }
}
