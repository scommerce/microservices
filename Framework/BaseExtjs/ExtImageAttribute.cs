﻿using Framework.BaseAttrib;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtImageAttribute :ExtAttribute
    {
        public override string GetXtypeSring()
        {
            return "image";
        }

     
        public int? _flex { get; set; } = null;

        public int flex
        {
            get
            {
                return this._flex.Value;
            }
            set
            {
                this._flex = value;
            }
        }


        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            listeners = new ExtJsListener();
            listeners.load = new Newtonsoft.Json.Linq.JRaw(@"{
               element: 'el',
               fn: function(){
                    Ext.ComponentQuery.query('panel').forEach(function(data){
                    data.suspendLayout = false;
                    data.updateLayout();
                    })
                }
            }");
            extJs = base.SetObject(theme, extJs, p, uiClass);
            if (p is PropertyInfo prop)
                extJs.TryAdd("src", prop.GetValue(uiClass));
            if(_flex.HasValue)
                extJs.TryAdd("flex", _flex.Value);

            
            return extJs;
        }
    }
}
