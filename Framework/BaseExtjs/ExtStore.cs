﻿using Framework.BaseInterface;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtStore: IToJsonable
    {
        public int id { get; set; }

        public bool autoLoad { get; set; }
        public int pageSize { get; set; } = 0;

        public JRaw model { get; set; }
        public JRaw proxy { get; set; }
        public bool remoteSort { get; set; }
        public bool remoteFilter { get; set; }
        public virtual  JRaw ToJson()
        {
           
            return new JRaw("new Ext.data.Store(" + Newtonsoft.Json.JsonConvert.SerializeObject(this) + ")");
        }
    }
}
