﻿using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtInputFieldAttribute : ExtAttribute
    {
        public InputType inputType { get; set; } = InputType.none;
        [ExtDefault]
        public bool allowBlank { get; set; }
        [ExtDefault]
        public string fieldLabel { get; set; }

        [ExtDefault]
        public string emptyText { get; set; }
        [ExtDefault]
        public string blankText { get; set; }
        [ExtDefault]
        public string msgTarget { get; set; }
    
        public string regex { get; set; }
        [ExtDefault]
        public string regexText { get; set; }

        [ExtDefault]
        public string validationMessage { get; set; }
        

        public bool hasValue { get; set; }
       
        public int labelWidth { get; set; }



        public override string GetXtypeSring()
        {
            return "textfield";
        }

        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);
            
            if (inputType != InputType.none)
            {
                extJs.TryAdd("inputType", GetInputType());
                if (inputType == InputType.hidden)
                {
                    extJs.TryAdd("hidden", true);
                }
            }
            if (regex != null)
            {
                extJs.TryAdd("regex", new JRaw(regex));
            }
          
           extJs.TryAdd("allowBlank", allowBlank);
        
            if (hasValue)
            {
                if (p is PropertyInfo prop)
                    extJs.TryAdd("value", prop.GetValue(uiClass));

            }
            if (labelWidth > 0)
            {
                extJs.TryAdd("labelWidth", labelWidth);
            }
         
            return extJs;

        }
        public string GetInputType()
        {
            switch (inputType)
            {
                case InputType.datetimeLocal: return "datetime-local";

                default: return Enum.GetName(typeof(InputType), inputType);
            }

        }
        public enum InputType
        {
            none,
            button,
            checkbox,
            color,
            date,
            datetimeLocal,
            email,
            file,
            hidden,
            image,
            month,
            number,
            password,
            radio,
            range,
            reset,
            search,
            submit,
            tel,
            text,
            time,
            url,
            week

        }
    }
}
