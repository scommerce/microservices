﻿using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Framework.BaseExtjs
{
   
    public class ExtLoginFormAttribute: ExtPanelAttribute
    {
        [ExtDefault]
        public int bodyPadding { get; set; } = 10;
        public override string GetExtend()
        {
            return "Ext.form.Panel";
        }
        public override string GetXtypeSring()
        {
            return "panel";
        }
        public string LoginText { get; set; }
        [ExtDefault]
        public string url { get; set; }

        public string successUrl { get; set; }

        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);
            List<ExpandoObject> buttons = new List<ExpandoObject>();
            if (LoginText != null) {
                ExpandoObject button = new ExpandoObject();
                button.TryAdd("text", LoginText);
                button.TryAdd("handler", ExtJsHelper.SubmitForm("form", "post", successUrl));
                buttons.Add(button);
            }
            extJs.TryAdd("buttons", buttons);
            return extJs;
        }
    }
}
