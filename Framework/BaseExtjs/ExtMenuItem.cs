﻿using Framework.BaseAttrib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtMenuItem:ExtAttribute
    {
        public override string GetXtypeSring()
        {
            return "menuitem";
        }

        
        public string MenuProperty { get; set; } = "Menus";
        public string TextProperty { get; set; } = "MenuName";
        public string LinkProperty { get; set; } = "Link";
        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs =  base.SetObject(theme, extJs, p, uiClass);

            if (p is Type c)
            {
               
                extJs.TryAdd("text", c.GetProperty(TextProperty).GetValue(uiClass));
                var link = c.GetProperty(LinkProperty).GetValue(uiClass);
                if (link != null) 
                    extJs.TryAdd("href", link);
                
                var listMenu = c.GetProperty(MenuProperty).GetValue(uiClass);
                if(listMenu is IList list)
                {
                    List<ExpandoObject> menuList = new List<ExpandoObject>();
                    foreach (var l in list)
                    {
                        var menu = SetObject(theme, extJs, l.GetType(), l);
                        menuList.Add(menu);
                    }
                    if(menuList.Count>0)
                            extJs.TryAdd("menu", menuList);
                }


            }


            return extJs;
        }
    }
}
