﻿using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtComboBoxAttribute: ExtInputPickerAttribute
    {
        public enum QueryMode
        {
            Local,
            Remote
        }
        public QueryMode queryMode { get; set; }
        public int dataSoucreId { get; set; }
        /// <summary>
        /// Json String example:
        /// [['0','Cuscode1'],['1','Cuscode2']]
        /// </summary>
        public string localDataSource { get; set; }
        [ExtDefault]
        public string displayField { get; set; } = "display";
        [ExtDefault]
        public string valueField { get; set; } = "value";

        public string remoteUrl { get; set; }
        public string rootProperty { get; set; }
        public bool paramsAsJson { get; set; }
        public ExtProxy.ProxyType proxyType { get; set; } = ExtProxy.ProxyType.Ajax;
        public ExtProxy.Method loadMethod { get; set; }
        [ExtDefault]
        public bool autoLoad { get; set; }
        public override string GetXtypeSring()
        {
            return "combobox";
        }

        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs= base.SetObject(theme, extJs, p, uiClass);
            switch (queryMode)
            {
                case QueryMode.Local: extJs.TryAdd("queryMode", "local");break;
                case QueryMode.Remote: extJs.TryAdd("queryMode", "remote"); break;

            }
            if(queryMode== QueryMode.Local)
            {
                if (localDataSource != null)
                {
                    ExtArrayDataSource extArrayDataSource = new ExtArrayDataSource();
                    extArrayDataSource.id = dataSoucreId;
                    extArrayDataSource.fields = new string[] { valueField, displayField };
                    extArrayDataSource.dataSource = localDataSource;
                    extJs.TryAdd("store", extArrayDataSource.ToJson());
                }
            }
            else
            {
                if (remoteUrl != null)
                {
                    ExtStore extStore = new ExtStore();
                    extStore.id = dataSoucreId;
                    extStore.autoLoad = true;

                    ExtProxy extProxy = new ExtProxy();
                    extProxy.url = remoteUrl;
                    extProxy.rootProperty = rootProperty;
                    extProxy.proxyType = proxyType;
                    extProxy.readMethod = loadMethod;
                    extProxy.paramsAsJson = paramsAsJson;
                    extStore.proxy = extProxy.ToJson();
                    

                    extJs.TryAdd("store", extStore.ToJson());
                }
            }

            return extJs;
        }
    }
}
