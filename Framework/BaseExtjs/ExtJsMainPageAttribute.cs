﻿using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtJsMainPageAttribute:ExtPanelAttribute
    {
        public override string GetExtend()
        {
            return "Ext.form.Panel";
        }
        public override string GetXtypeSring()
        {
            return "layout-border";
        }
        [ExtDefault]
        public string layout { get { return "border"; } }
        [ExtDefault]
        public bool bodyBorder { get; set; }
        public JRaw defaults { get {
                return new JRaw(@"{
                    collapsible: true,
                    split: true,
                    bodyPadding: 10
                }");
            } }
        public int MenuSize { get; set; } = 200;
        public bool showUserName { get; set; }
        public string contentProperty { get; set; } = "ContentName";
        public string contentTheme { get; set; } = "ContetntTheme";
        public string baseUrl { get; set; } = "/api/MainUi/GetUiUrl";
        public string checkLoginUrl { get; set; } = "https://login.wframwork.com/api/Login/CheckToken";
        public string privilegeMenu { get; set; } = "https://privilege.wframwork.com/api/Privilege/GetMenu";
        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            extJs = base.SetObject(theme, extJs, p, uiClass);


            List<Object> item = new List<object>();
            item.Add(new JRaw(@"{
             itemId:'mainMenu',
             title: 'เมนู',
             region: 'west',
             floatable: false,
             margin: '5 5 5 5',
             width: "+ MenuSize + @",
             minWidth: 100,
             maxWidth: 250,
             items: [
                    {
                 loader: {
                     url: '" + privilegeMenu + @"?ApiKey='+Ext.util.Cookies.get('ApiKey'),
                     renderer: 'component',
                     autoLoad: true
                 }
             }
            ]
         }"));

            string onrender = "";
            if (showUserName)
            {
                onrender = @" afterrender: {
                                     fn: function () {
                                         var button = this;
                                         Ext.Ajax.request({
                                             url: '"+ checkLoginUrl + @"',
                                             method: 'POST',
                                             jsonData : {
                                                 ApiKey:   Ext.util.Cookies.get('ApiKey')
                                             },
                                             success: function (response, opts) {
                                                 var data = JSON.parse(response.responseText);
                                                 button.setText(data.data.UserName);
                                             },

                                             failure: function (response, opts) {
                                                 console.log('server-side failure with status code ' + response.status);
                                             }
                                         });

                                     }
                                 }";
            }

            item.Add(new JRaw(@"{

             region: 'center',

             margin: '5 5 5 5',

             height: 50,
             items: [{

                 region: 'top',

                 items: [{
                     xtype: 'toolbar',

                     width: Ext.getBody().getViewSize().width - "+ MenuSize + @" - 20,
                     items: [
                         'ยินดีต้อนรับ',
                         '->', {
                             itemId: 'userLogin',
                             xtype: 'splitbutton',
                             text: 'UserName',
                            listeners: {
                              "+ onrender + @"
                             },
                             menu: {
                                 xtype: 'menu',
                                 items: [
                                     // these will render as dropdown menu items when the arrow is clicked:
                                     {
                                         text: 'ออกจากระบบ',
                                         style: {
                'color': 'red !important',
                                         },
                                         handler: function() {
                Ext.Msg.confirm('ยืนยัน', 'ยืนยันการออกจากระบบ', function(buttonId) {
                    if (buttonId == 'yes')
                        alert('logout');
                });
            }
        }
                                 ]
                             }
}
                     ]
                 }],

             },  {
                 loader: {
                     url: '" + baseUrl + @"?PageName="+ uiClass.GetType().GetProperty(contentProperty).GetValue(uiClass) + @"&theme="+ uiClass.GetType().GetProperty(contentTheme).GetValue(uiClass) + @"&ApiKey='+Ext.util.Cookies.get('ApiKey'),
                     renderer: 'component',
                     autoLoad: true
                 }
             }],
             layout: 'vbox',

         }"));

            extJs.TryAdd("items", item);

            return extJs;
        }
    }
}
