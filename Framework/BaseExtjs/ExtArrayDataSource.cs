﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtArrayDataSource: ExtStore
    {
        public int id { get; set; }
        public string[] fields { get; set; }
        public string dataSource { get; set; }
        public JRaw data { get; set; }
        public override JRaw ToJson()
        {
            if(data == null)
                data = new JRaw(dataSource);
            return new JRaw("new Ext.data.ArrayStore(" + Newtonsoft.Json.JsonConvert.SerializeObject(this) + ")");
        }
    }
}
