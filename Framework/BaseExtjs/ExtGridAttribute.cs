﻿using CertificatesToDBandBack;
using Framework.BaseAttrib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;

namespace Framework.BaseExtjs
{
    public class ExtGridAttribute : ExtPanelAttribute
    {
        public override string GetXtypeSring()
        {
            return "gridpanel";
        }

        public enum CUD
        {
            NotAllow,
            Create,
            CreateUpdate,
            CrearteUpdateDelete,
            Update,
            UpdateDelete,
            Delete
        }
        [ExtDefault]
        public string title { get; set; }

        public int dataSoucreId { get; set; }
        public string remoteUrl { get; set; }
        public string rootProperty { get; set; } = "data";
        public ExtProxy.ProxyType proxyType { get; set; } = ExtProxy.ProxyType.Ajax;
        public ExtProxy.Method loadMethod { get; set; }
        public bool paramsAsJson { get; set; }
        public bool scrollable { get; set; }
        public bool paging { get; set; }
        public int pageSize { get; set; }
        [ExtDefault]
        public bool remoteSort { get; set; }
        [ExtDefault]
        public bool remoteFilter { get; set; }
        public bool filter { get; set; }
        [ExtDefault]
        public string emptyText { get; set; } = "ไม่พบรายการที่ค้นหา";
        [ExtDefault]
        public bool loadMask { get; set; } = true;
        public CUD cud { get; set; }
        public bool changeUrlAdd { get; set; }
        public string addUiUrl { get; set; }

        public string controller { get; set; }
        public object addWidth { get; set; } = 100;


        public object addHeight { get; set; } = 50;


        public override ExpandoObject SetObject(string theme, ExpandoObject extJs, object p, object uiClass = null)
        {
            if (controller != null)
                remoteUrl = controller + "/Get";
            extJs = base.SetObject(theme, extJs, p, uiClass);
            if (cud == CUD.CrearteUpdateDelete || cud == CUD.CreateUpdate || cud == CUD.Update || cud == CUD.UpdateDelete || cud == CUD.Delete)
            {
                var data = (List<ExpandoObject>)((dynamic)extJs).columns;
                


                if (cud == CUD.CrearteUpdateDelete || cud == CUD.UpdateDelete || cud == CUD.Delete)
                {
                    int width = 0;
                    var actionUpdate = new ExpandoObject();
                    var button = "";
                    width = 30;

                    button += @"{iconCls: 'fas fa-minus-circle',tooltip: 'Remove',handler: 'onRemoveClick'}";
                    actionUpdate.TryAdd("xtype", "actioncolumn");

                    actionUpdate.TryAdd("sortable", false);
                    actionUpdate.TryAdd("menuDisabled", true);
                    actionUpdate.TryAdd("width", width);
                    actionUpdate.TryAdd("items", new JRaw("[" + button + "]"));
                    data.Insert(0, actionUpdate);

                }

                if (cud == CUD.CrearteUpdateDelete || cud == CUD.CreateUpdate || cud == CUD.Update  || cud == CUD.UpdateDelete)
                {
                    int width = 0;
                    var actionUpdate = new ExpandoObject();
                    var button = "";

                    width = 30;
                   
                    button = @"{iconCls: 'fas fa-edit',tooltip: 'Update',handler: 'onUpdateClick'}";
                    actionUpdate.TryAdd("xtype", "actioncolumn");

                    actionUpdate.TryAdd("sortable", false);
                    actionUpdate.TryAdd("menuDisabled", true);
                    actionUpdate.TryAdd("width", width);
                    actionUpdate.TryAdd("items", new JRaw("[" + button + "]"));
                    data.Insert(0, actionUpdate);

                }
                

                
                extJs.TryAdd("columns", data);
            }

            ExtStore extStore = new ExtStore();
            extStore.id = dataSoucreId;
            extStore.autoLoad = true;
            extStore.pageSize = pageSize;
            extStore.remoteFilter = remoteFilter;
            extStore.remoteSort = remoteSort;
            if (remoteUrl != null)
            {
                ExtProxy extProxy = new ExtProxy();
                var url = remoteUrl;
                if (url.Contains("?") == false)
                {
                    url = url + "?1=1";
                }
                extProxy.url = new JRaw("'" + url + "&ApiKey='+Ext.util.Cookies.get('ApiKey')");
                extProxy.rootProperty = rootProperty;
                extProxy.proxyType = proxyType;
                extProxy.readMethod = loadMethod;
                extProxy.paramsAsJson = paramsAsJson;
                extStore.proxy = extProxy.ToJson();
            }
            if (paging)
            {
                extStore.remoteFilter = true;
                extStore.remoteSort = true;
                extJs.TryAdd("bbar", new JRaw(@"{
                     xtype: 'pagingtoolbar',
                     displayInfo: true,
                     displayMsg: 'แสดงข้อมูล {0} - {1} จาก {2}',
                     emptyMsg: 'ไม่พบข้อมูล'}"));
            }
            var tbar = "";
            if(cud == CUD.Create || cud == CUD.CreateUpdate || cud == CUD.CrearteUpdateDelete)
            {
                tbar = @"{
                     text: 'เพิ่มข้อมูล',
                     tooltip: 'เพิ่มข้อมูล',
                     iconCls: 'fas fa-plus-circle',
                     handler: function () {";
                if (!changeUrlAdd)
                {
                    var url = addUiUrl;
                  
                    if (addUiUrl == null)
                        url = controller + "/GetAddUI";

                    if (url.Contains("?") == false)
                    {
                        url = url + "?1=1";
                    }

                    
                  

                    tbar += @"Ext.create('Ext.Window', {
                    title: 'เพิ่มข้อมูลผู้ใช้งาน',
                        width: ";

                    if (addWidth != null)
                    {
                        if (addWidth.IsNumericType())
                        {
                            tbar += addWidth.ToString();
                        }

                        else if (addWidth.GetType().IsEnum)
                        {
                            if (addWidth is Screen screen)
                            {
                                string size = "";
                                if (screen == Screen.width)
                                {
                                    size = "Ext.getBody().getViewSize().width/2";
                                }
                                tbar += new JRaw(size);
                                
                            }
                        }
                        else
                        {
                            tbar += new JRaw(addWidth);
                        }
                    }

                    tbar += @",
                        closable: true,
                        height: ";
                    if (addHeight != null)
                    {
                        if (addHeight.IsNumericType())
                        {
                            tbar += addHeight.ToString();
                        }

                        else if (addHeight.GetType().IsEnum)
                        {
                            if (addHeight is Screen screen)
                            {
                                string size = "";
                                if (screen == Screen.height)
                                {
                                    size = "Ext.getBody().getViewSize().height/2";
                                }
                                tbar += new JRaw(size);

                            }
                        }
                        else
                        {
                            tbar += new JRaw(addHeight);
                        }
                    }
                    tbar +=@",
                        items: [{
                            loader: {
                                                 url: '"+ url + @"&ApiKey='+Ext.util.Cookies.get('ApiKey'),
                                                 renderer: 'component',
                                                 autoLoad: true
                                             }
                        }],
                        ownerCt: this,
                        modal: true,
                     

                    }).show();";
                }

                tbar += @"}
                    }";
            }
            
            if (filter)
            {
              //  extJs.TryAdd("requires", "['Ext.grid.filters.Filters']");
       
                extJs.TryAdd("plugins", new JRaw(@"['gridfilters']"));

                if (tbar != "")
                    tbar += ",";

                tbar += @"{
                     text: 'ล้างการค้นหาทั้งหมด',
                     tooltip: 'ล้างการค้นหาทั้งหมด',
                     iconCls: 'fas fa-sync-alt',
                     handler: function () {
                      
                         this.up('panel').filters.clearFilters();
                     }
                    }";

                
              
            };

            if(tbar!="")
                extJs.TryAdd("tbar", new JRaw(@"["+ tbar + "]"));
            extJs.TryAdd("store", extStore.ToJson());
            extJs.TryAdd("layout", "fit");

            return extJs;
        }
    }
}
