﻿using Framework.BaseContext;
using Framework.BaseEntity;
using Framework.BaseInterface;
using Framework.BaseModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Framework.BaseFramework
{
    public class BaseRepository
    {
        BaseDbContext dbContext;
        HttpContext context;
        public BaseRepository(BaseDbContext dbContext, HttpContext context = null)
        {
            this.dbContext = dbContext;
            this.context = context;
        }

        public async Task<BaseDataModel> Login(string cusCode, string userName, string password,string lang)
        {
            var setting = dbContext.BaseMicroServices.Where(n => n.Name == "Login" && n.Customer == null).FirstOrDefault();
            return await sendRequest<BaseDataModel>(setting.ServiceIp+ "/api/Login/Login", HttpMethod.Post, new
            {
                CusCode=cusCode,
                UserName=userName,
                Password=password
            }, true, lang);
        }

        public async Task ChangePassword(string apikey, string password, string confirmPassword, string lang)
        {
            var setting = dbContext.BaseMicroServices.Where(n => n.Name == "Login" && n.Customer == null).FirstOrDefault();
            await sendRequest(setting.ServiceIp + "/api/User/ChangePassword", HttpMethod.Post, new
            {
                ApiKey = apikey,
                Password = password,
                ConfirmPassword = confirmPassword
            }, true, lang);
        }

        public IQueryable<TEntity> GetDataAll<TEntity>(Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
               new TransactionOptions()
               {
                   IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
               }))
            {
                if (where == null)
                    return dbContext.Set<TEntity>().Where(n => true);
                return dbContext.Set<TEntity>().Where(where);
            }
        }

        public TEntity GetData<TEntity>(Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
               new TransactionOptions()
               {
                   IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
               }))
            {
                if (where == null)
                    return dbContext.Set<TEntity>().Where(n => true).FirstOrDefault();
                return dbContext.Set<TEntity>().Where(where).FirstOrDefault();
            }
        }
        public async Task<BaseCustomer> GetCustomer(string cuscode)
        {
            var cus = dbContext.BaseCustomers.Where(n => n.CustomerCode == cuscode).FirstOrDefault();
            if (cus == null)
                cus=await RegisterCustomer(cuscode);

            return cus;
        }
        public List<BaseSetting> GetSetting(string cuscode,string section )
        {

         
            return dbContext.BaseSettings.Where(n=>n.Customer.CustomerCode ==  cuscode && n.Section == section).ToList();
        }

        public BaseSetting GetSetting(string cuscode, string section,string key)
        {


            return dbContext.BaseSettings.Where(n => n.Customer.CustomerCode == cuscode && n.Section == section && n.Key == key).FirstOrDefault();
        }

        public TEntity Add<TEntity>(TEntity data) where TEntity : class, new()
        {

            if(data is Framework.BaseEntity.BaseEntity a)
            {
                a.CreateDate = DateTime.Now;
                
            }
            if (data is Framework.BaseEntity.BaseEntityCusCode c)
            {
                if (c.CreateUser != null)
                    c.Customer = c.CreateUser.Customer;
            }

            dbContext.Add(data);
            return data;
        }


        public TEntity Update<TEntity>(TEntity data) where TEntity : class, new()
        {

            if (data is Framework.BaseEntity.BaseEntity a)
            {
                a.UpdateDate = DateTime.Now;
            }
            if (data is Framework.BaseEntity.BaseEntityCusCode c)
            {
                if (c.CreateUser != null)
                    c.Customer = c.CreateUser.Customer;
            }

            return data;
        }
        public void SaveChange()
        {
            dbContext.SaveChanges();
        }

        public async Task<BaseCustomer> RegisterCustomer(string Cuscode)
        {
            BaseCustomer c = GetData<BaseCustomer>(n => n.CustomerCode == Cuscode);
            if (c != null)
                return c;

            var setting= dbContext.BaseMicroServices.Where(n => n.Name == "Customer" && n.Customer == null).FirstOrDefault();
            var response = await sendRequest(setting.ServiceIp + "/api/CustomerApi/GetCustomer?CusCode=" + Cuscode, HttpMethod.Get, null, false);


            if (response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<BaseReturnData<BaseHttpCustomer>>(await response.Content.ReadAsStringAsync());

                BaseCustomer baseCustomer = GetData<BaseCustomer>(n => n.CustomerCode == Cuscode);

                bool insert = false;
                if (baseCustomer == null)
                {
                    insert = true;
                    baseCustomer = new BaseCustomer();
                }

                baseCustomer.DeleteFlag = false;
              //  baseCustomer.CreateDate = DateTime.Now;
                baseCustomer.CustomerCode = Cuscode;
                baseCustomer.BaseCustomerId = res.Data.Id.ToString();

                if (insert)
                {
                    Add(baseCustomer);
                }
                //else
                //{
                //    Update(baseCustomer);
                //}
                SaveChange();

                return baseCustomer;
            }
            else
            {
                throw new Exception("Micro Services Error");
            }

        }

        public BaseMicroServices GetMicroServices(string MicroName)
        {
            return dbContext.BaseMicroServices.Where(n => n.Name == MicroName && n.Customer == null).FirstOrDefault();
        }
        

        public async Task<BaseUserReturn> GetUserFromApiKey(string ApiKey,string lang)
        {
           
            var setting = dbContext.BaseMicroServices.Where(n => n.Name == "CheckLogin" && n.Customer == null).FirstOrDefault();
            var response = await sendRequest(setting.ServiceIp + "/api/Login/CheckToken", HttpMethod.Post, new { ApiKey=ApiKey }, true, lang);


            if (response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<BaseReturnData<BaseUserReturn>>(await response.Content.ReadAsStringAsync());
                if (res.Count == 0)
                {
                    throw new Exception("User Not Found");
                }

                BaseCustomer baseCustomer = GetData<BaseCustomer>(n => n.BaseCustomerId == res.Data.CuscodeId.ToString());
                bool insert = false;
                if (baseCustomer == null)
                {
                    insert = true;
                    baseCustomer = new BaseCustomer();
                }

                

                //  baseCustomer.CreateDate = DateTime.Now;
                baseCustomer.CustomerCode = res.Data.Cuscode;
                baseCustomer.BaseCustomerId = res.Data.CuscodeId.ToString();
           
                if (insert)
                {
                    Add(baseCustomer);
                }
               
                BaseUser baseUser = GetData<BaseUser>(n => n.BaseUserId == res.Data.UserId && n.Customer.CustomerCode == res.Data.Cuscode);
                 insert = false;
                if (baseUser == null)
                {
                    insert = true;
                    baseUser = new BaseUser();
                }

                baseUser.UserName = res.Data.UserName;
                baseUser.BaseUserId = res.Data.UserId;
                baseUser.Customer = baseCustomer;
            
                if (insert)
                {
                    Add(baseUser);
                }
             
                //var old = GetDataAll<BaseApiToken>(n => n.DeleteFlag == false && n.TokenType == Framework.BaseEnum.TokenType.BaseLogin && n.CreateUser.UserName == res.Data.UserName && n.Customer.CustomerCode == res.Data.Cuscode);
                //foreach (var o in old)
                //{
                //    o.DeleteFlag = true;
                //   Update(o);
                //}
                //SaveChange();

                //BaseApiToken baseApiToken = new BaseApiToken();
                //baseApiToken.Token = ApiKey;
                //baseApiToken.TokenType = Framework.BaseEnum.TokenType.BaseLogin;
                //baseApiToken.CreateUser = baseUser;
                //baseApiToken.Customer = baseCustomer;


                //Add(baseApiToken);



                SaveChange();


                res.Data.BaseCustomer = baseCustomer;
                res.Data.BaseUser = baseUser;
                return res.Data;

              
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new Exception("User Not Found");
            }
            else
            {
                throw new Exception("Micro Services Error");
            }

        }

        public BaseSetting AddSetting(string Cuscode,string Section, string Key,string Value)
        {
            BaseSetting baseSetting = GetData<BaseSetting>(n => n.Customer.CustomerCode == Cuscode && n.Section == Section && n.Key == Key);
            bool insert = false;
            if (baseSetting == null)
            {
                insert = true;
                baseSetting = new BaseSetting();
            }
           
            baseSetting.DeleteFlag = false;
            baseSetting.Customer = GetData<BaseCustomer>(n=>n.CustomerCode== Cuscode);
            baseSetting.Section = Section;
            baseSetting.Key = Key;
            baseSetting.Value = Value;
            if(insert)
                Add(baseSetting);
            else
                Update(baseSetting);
            SaveChange();

            return baseSetting;
        }

        public async Task<HttpResponseMessage> sendRequest(string url, HttpMethod httpMethod, object obj,bool json, string lang = "th")
        {
            var request = new HttpRequestMessage(httpMethod,  url);


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);

                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }
            //if (base64 != null)
            //    request.Headers.Add("api-key", GetApiKey());

            request.Headers.Add("lang", lang);

            var response = await client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<BaseReturnError>(await response.Content.ReadAsStringAsync());
                 throw new Exception(res.message);
            }
            return response;
        }


        public async Task<T> sendRequest<T>(string url, HttpMethod httpMethod, object obj, bool json,string lang="th")
        {
            Console.WriteLine("Url:"+url);

            var request = new HttpRequestMessage(httpMethod, url);


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);
                    Console.WriteLine("Param:"+newUserJson);
                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }
           //if (base64 != null)
            request.Headers.Add("lang",lang);


            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<BaseReturnData<T>>(await response.Content.ReadAsStringAsync());
                return res.Data;
            }
            else
            {
                var res = JsonConvert.DeserializeObject<BaseReturnError>(await response.Content.ReadAsStringAsync());
                throw new Exception(res.message);
            }
        }

        public async Task<T> sendRequestCustom<T>(string url, HttpMethod httpMethod, object obj, bool json, string lang = "th")
        {
            var request = new HttpRequestMessage(httpMethod, url);

            Console.WriteLine("Url:" + url);
            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);
                    Console.WriteLine("Param:" + newUserJson);
                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }
            //if (base64 != null)
            request.Headers.Add("lang", lang);


            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                return res;
            }
            else
            {
                var res = JsonConvert.DeserializeObject<BaseReturnError>(await response.Content.ReadAsStringAsync());
                throw new Exception(res.message);
            }
        }

        public async Task<BaseReturnData<T>> sendRequestFull<T>(string url, HttpMethod httpMethod, object obj, bool json, string lang = "th")
        {
            var request = new HttpRequestMessage(httpMethod, url);

            Console.WriteLine("Url:" + url);
            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);
                    Console.WriteLine("Param:" + newUserJson);
                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }
            //if (base64 != null)
            request.Headers.Add("lang", lang);


            var response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode)
            {
                var res = JsonConvert.DeserializeObject<BaseReturnData<T>>(await response.Content.ReadAsStringAsync());
                return res;
            }
            else
            {
                var res = JsonConvert.DeserializeObject<BaseReturnError>(await response.Content.ReadAsStringAsync());
                throw new Exception(res.message);
            }
        }

        public async Task<T> uploadFile<T>(string url, HttpMethod httpMethod, Microsoft.AspNetCore.Http.IFormFile obj, string lang = "th")
        {


            Console.WriteLine("Url:" + url);
            HttpClient client = new HttpClient();
            using (var content =
             new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
            {
                var m = new MemoryStream();
                await obj.CopyToAsync(m);

              

                content.Add(new ByteArrayContent(m.GetBuffer()), "file", obj.FileName);
                

                //if (base64 != null)

                //   request.Content = content;
                var response = await client.PutAsync(url,content);
                if (response.IsSuccessStatusCode)
                {
                    var res = JsonConvert.DeserializeObject<BaseReturnData<T>>(await response.Content.ReadAsStringAsync());
                    return res.Data;
                }
                else
                {
                    var res = JsonConvert.DeserializeObject<BaseReturnError>(await response.Content.ReadAsStringAsync());
                    throw new Exception(res.message);
                }

            }
           
        }

        public void sendRequestFireBase(string url,string SaverKey, object obj, bool json)
        {
            Console.WriteLine("Url:" + url);
            var request = new HttpRequestMessage(HttpMethod.Post, url);


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);
                    Console.WriteLine("Param:" + newUserJson);
                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }

            var authValue = new AuthenticationHeaderValue("key", SaverKey);

            request.Headers.Authorization = authValue;


            client.SendAsync(request);
            //if (response.IsSuccessStatusCode)
            //{
            //    var res = JsonConvert.DeserializeObject<BaseReturnData<T>>(await response.Content.ReadAsStringAsync());
            //    return res.Data;
            //}
            //else
            //{
            //    throw new Exception(response.StatusCode.ToString());
            //}
        }

        public void UpdateProperty(BaseEntityCusCodeProperty entity , string property,string value, PropertyType propertyType)
        {
            var p = entity.GetProperties().Where(n => n.Property == property && n.DeleteFlag == false).FirstOrDefault();

            if (p == null)
            {
                p = new BaseProperty();
                p.CreateDate = DateTime.Now;
                entity.Properties.Add(p);
                p.Customer = entity.Customer;
            }
            p.Property = property;
            p.Value = value;
            p.PropetyType = propertyType;

            p.UpdateDate = DateTime.Now;
            SaveChange();
        }

        public void UpdateProperties(BaseEntityCusCodeProperty entity, List<BasePropertyModel> propertyModels)
        {

           // var u = GetRepo().GetData<User>(n => n.UserName == a.CreateUser.UserName && n.Customer.CustomerCode == a.CreateUser.Customer.CustomerCode);
            foreach (var ip in entity.GetProperties())
            {
                ip.DeleteFlag = true;
                Update(ip);
            }
            foreach (var ip in propertyModels)
            {

                var p = entity.GetProperties().Where(n => n.Property == ip.Property).FirstOrDefault();
                if (p == null)
                {
                    p = new BaseProperty();
                    entity.GetProperties().Add(p);
                    p.CreateDate = DateTime.Now;
                    p.Customer = entity.Customer;
                }
                p.DeleteFlag = false;
                p.Property = ip.Property;
                p.Value = ip.Value;
                p.PropetyType = ip.PropertyType;
                Update(entity);
            }
            
            SaveChange();
        }
        public ExpandoObject GetProperty<T>(T entity) where T: BaseEntityCusCodeProperty
        {
            ExpandoObject v = new ExpandoObject();

            foreach (var p in entity.GetType().GetProperties())
            {
                v.TryAdd(p.Name, p.GetValue(entity));
            }

            foreach (var p in entity.GetProperties().Where(n => n.DeleteFlag == false))
            {
                if (p.Value == null)
                    v.TryAdd(p.Property, null);
                else
                {
                    switch (p.PropetyType)
                    {
                        case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value == "0") ? "false" : (p.Value == "1") ? "true" : p.Value)); break;
                        case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                        case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                        case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                        case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                        case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                        case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                        default: v.TryAdd(p.Property, p.Value); break;
                    }
                }


            }
           
           
            return v;
        }

        public List<ExpandoObject> GetProperty<T>(List<T> entity) where T : BaseEntityCusCodeProperty
        {
            List<ExpandoObject> lv = new List<ExpandoObject>();
            foreach (var e in entity)
            {
                ExpandoObject v = new ExpandoObject();

                foreach (var p in e.GetType().GetProperties())
                {
                    v.TryAdd(p.Name, p.GetValue(e));
                }

                foreach (var p in e.GetProperties().Where(n => n.DeleteFlag == false))
                {
                    if (p.Value == null)
                        v.TryAdd(p.Property, null);
                    else
                    {
                        switch (p.PropetyType)
                        {
                            case PropertyType.Bool: v.TryAdd(p.Property, bool.Parse((p.Value == "0") ? "false" : (p.Value == "1") ? "true" : p.Value)); break;
                            case PropertyType.Char: v.TryAdd(p.Property, char.Parse(p.Value)); break;
                            case PropertyType.Date: v.TryAdd(p.Property, DateTime.Parse(p.Value).Date); break;
                            case PropertyType.DateTime: v.TryAdd(p.Property, DateTime.Parse(p.Value)); break;
                            case PropertyType.Double: v.TryAdd(p.Property, Double.Parse(p.Value)); break;
                            case PropertyType.Int: v.TryAdd(p.Property, int.Parse(p.Value)); break;
                            case PropertyType.Long: v.TryAdd(p.Property, long.Parse(p.Value)); break;
                            default: v.TryAdd(p.Property, p.Value); break;
                        }
                    }


                }
                lv.Add(v);
            }


            return lv;
        }
    }
}
