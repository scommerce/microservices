﻿using Framework.BaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Framework.BaseFramework
{
    public class BaseReturnApi
    {
        BaseRepository baseRepository;
        public BaseReturnApi(BaseDbContext dbContext)
        {
            baseRepository = new BaseRepository(dbContext);
        }
        public IQueryable<BaseEntity.BaseApiReturn> GetBaseReturnApis(string name)
        {
           return  baseRepository.GetDataAll<BaseEntity.BaseApiReturn>(n => n.Name == name && n.DeleteFlag == false && n.BaseApiReturnId ==null);
        }

    
    }
}
