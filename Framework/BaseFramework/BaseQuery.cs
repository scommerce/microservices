﻿using Framework.BaseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace System.Linq
{
    public static class BaseQuery
    {
        public static IQueryable<TEntity> Paging<TEntity>(this IQueryable<TEntity> query, int start, int end)
        {
            return query.Skip(start).Take(end);
        }

        public static IQueryable<TEntity> Paging<TEntity>(this IQueryable<TEntity> query, BaseDataTable baseDataTable)
        {
            return query.Paging(baseDataTable.Start, baseDataTable.Length);
        }


        public static BaseReturnDatatable ToDataTable<TEntity>(this IQueryable<TEntity> query, BaseDataTable baseDataTable)
        {

            if (baseDataTable.Filter != null)
            {
                var expression = query.Expression;
         
                foreach (var f in baseDataTable.Filter)
                {
                    var parameter = Expression.Parameter(typeof(TEntity), "x");
                    var selector = Expression.PropertyOrField(parameter, f.property);
                    
                    Expression operation = query.Expression;
                    object value;
                    if (selector.Type.IsEnum)
                        value = Enum.Parse(selector.Type, f.value, true); 
                    else
                     value = Convert.ChangeType(f.value, selector.Type); 
            
                    switch (f.@operator)
                    {
                        case "eq":
                            {
                                var contant = Expression.Constant(value);
                                operation = Expression.Equal(selector, contant);
                            }
                            break;
                        case "like":
                            {
                                MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                                var contant = Expression.Constant(value, typeof(string));
                               
                                operation = Expression.Call(selector, method, contant);
                            }
                            break;
                        case "lt":
                            {
                                var contant = Expression.Constant(value);
                                operation = Expression.LessThanOrEqual(selector, contant);
                            }
                            break;
                        case "gt":
                            {
                                var contant = Expression.Constant(value);
                                operation = Expression.GreaterThanOrEqual(selector, contant);
                            }
                            break;
                    }
                    var whereLambda = Expression.Lambda<Func<TEntity, bool>>(operation, parameter);

                   
                    expression = Expression.Call(
                        typeof(Queryable),
                        "Where",
                        new[] { typeof(TEntity) },
                        expression,
                        whereLambda
                    );
                }
                query = query.Provider.CreateQuery<TEntity>(expression);
            }
           
            if (baseDataTable.Sort != null)
            {
                var expression = query.Expression;
                int count = 0;
                foreach (var s in baseDataTable.Sort)
                {
                    var parameter = Expression.Parameter(typeof(TEntity), "x");
                    var selector = Expression.PropertyOrField(parameter, s.property);
                    var method = string.Equals(s.direction, "DESC", StringComparison.OrdinalIgnoreCase) ?
                     (count == 0 ? "OrderByDescending" : "ThenByDescending") :
                     (count == 0 ? "OrderBy" : "ThenBy");
                    expression = Expression.Call(typeof(Queryable), method,
                        new Type[] { query.ElementType, selector.Type },
                        expression, Expression.Quote(Expression.Lambda(selector, parameter)));

                    count++;
                }
                query = query.Provider.CreateQuery<TEntity>(expression);
            }
            

            BaseReturnDatatable ret = new BaseReturnDatatable();
            ret.Count = query.Count();
            ret.RecordsFiltered = ret.Count;
            ret.RecordsTotal = ret.Count;

            if (baseDataTable.Length>0)
                ret.Data = query.Paging(baseDataTable.Start, baseDataTable.Length).ToList();
            else
                ret.Data = query.ToList();
            return ret;
        }
        public static BaseReturnDatatable ToDataTable<TEntity>(this List<TEntity> query, BaseDataTable baseDataTable)
        {
            BaseReturnDatatable ret = new BaseReturnDatatable();
            ret.Count = query.Count();
            ret.RecordsFiltered = ret.Count;
            ret.RecordsTotal = ret.Count;
            ret.Data = query.Skip(baseDataTable.Start).Take(baseDataTable.Length).ToList();
            return ret;
        }
    }
}
