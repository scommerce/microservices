﻿
using Kpi.Entity;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Kpi.DbContext
{
    public class DataContext:Framework.BaseContext.BaseDbContext
    {
        public DbSet<Assessor> Assessors { get; set; }
        public DbSet<Evaluation> Evaluations { get; set; }
        public DbSet<EmployeeLeave> EmployeeLeave { get; set; }
        public DbSet<EvaluationData> EvaluationDatas { get; set; }
        public DbSet<EvaluationDataDetail> EvaluationDataDetails { get; set; }
        public DbSet<EvaluationDataDetailTemplate> EvaluationDataDetailTemplates { get; set; }
        public DbSet<EvaluationDataTemplate> EvaluationDataTemplates { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EmployeeType> EmployeeType { get; set; }
        public DbSet<Prefix> Prefix { get; set; }
        public DbSet<Flow> Flows { get; set; }
        public DbSet<EvaluationPoint> EvaluationPoint { get; set; }
        public DbSet<SurveyTemplate> SurveyTemplates { get; set; }
        public DbSet<SurveyDetailTemplate> SurveyDetailTemplate { get; set; }
        public DbSet<SurveyDetailChooseTemplate> SurveyDetailChooseTemplate { get; set; }
        public DbSet<FeedBackTemplate> FeedBackTemplates { get; set; }     




        public DbSet<Survey> Survey { get; set; }
        public DbSet<SurveyDetails> SurveyDetails { get; set; }
        public DbSet<SurveyDetailChoose> SurveyDetailChoose { get; set; }

        public DbSet<SurveyEmp> SurveyEmp { get; set; }

        public DbSet<SurveySelect> SurveySelect { get; set; }        public DbSet<FeedBack> FeedBacks { get; set; }        public DbSet<FeedBackDetail> FeedBackDetails { get; set; }        public DbSet<FeedbackEmp> FeedbackEmp { get; set; }        public DbSet<FeedBackFromAsseert> FeedBackFromAsseert { get; set; }
        public override string GetConnectionString()
        {
            return "Server=203.150.107.134,1500; Database=PMCU_Kpi;User Id=sa;Password=P@ssword1234; ";
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<SurveyDetailTemplate>().HasMany(n => n.SurveyDetailChooseTemplates).WithOne(n => n.SurveyDetail);
            modelBuilder.Entity<SurveyDetailChooseTemplate>().HasOne(n => n.Next);

            modelBuilder.Entity<SurveyDetails>().HasMany(n => n.SurveyDetailChoose).WithOne(n => n.SurveyDetail);
            modelBuilder.Entity<SurveyDetailChoose>().HasOne(n => n.Next);

        }

    }
}
