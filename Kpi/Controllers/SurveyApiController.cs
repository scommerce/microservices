﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SurveyApiController : BaseController<DataContext>
    {

        [HttpPost]
        public IActionResult GetDataTableTemplate([FromForm]BaseDataTable baseDataTable)
        {
            var data = GetRepo().GetDataAll<SurveyTemplate>(n => n.DeleteFlag == false).ToDataTable(baseDataTable);
            return new ObjectResult(data);
        }

        [HttpPost]
        public async Task<IActionResult> GetServeyAsync([FromForm] BaseDataTable baseDataTable)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetDataAll<Survey>(n => n.DeleteFlag == false && n.Employees.Any(e=>e.Employee.User==user.BaseUser && e.DeleteFlag==false)).ToDataTable(baseDataTable);
           
            foreach(Survey s in data.Data)
            {
                if(s.Employees.Where(n=>n.Employee.User == user.BaseUser).First().finish == true)
                {
                    s.EmpSend = true;
                }
            }
            
            return new ObjectResult(data);
        }

      

    }
}