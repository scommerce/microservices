﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    public class MainController : Controller
    {
        [Authorize(Policy = "Login")]
        public IActionResult Index()
        {
            return View("Dashboard");
        }

        [Authorize(Policy = "Login")]
        public IActionResult Index1()
        {
            return View("Dashboard1");
        }
    }
}