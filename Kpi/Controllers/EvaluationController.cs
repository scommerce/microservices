﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Kpi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Kpi.Controllers
{
    public class EvaluationController : BaseWebController<DataContext>
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReportSummary()
        {
            return View();
        }
        public IActionResult RawReport()
        {
            return View();
        }

        [Authorize(Policy = "Login")]
        public async Task<IActionResult> Make(long id)
        {
            MakeEvaluationModel makeEvaluationModel = new MakeEvaluationModel();

            var eval = GetRepo().GetData<Entity.Evaluation>(n => n.Id == id);
           var data = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var emp = eval.Emp.Where(n=>n.Employee.User == data.BaseUser);
            makeEvaluationModel.Evaluation = eval;
            makeEvaluationModel.Employee = emp.FirstOrDefault().Employee;
            ViewData["UserId"] = data.UserId;
            ViewData["Assersor"] = false;
            ViewData["EmpCode"] = data.UserId;
            return View(makeEvaluationModel);
        }

        [Authorize(Policy = "Login")]
        public async Task<IActionResult> MakeAssessor(long id,string EmpCode)
        {
            MakeEvaluationModel makeEvaluationModel = new MakeEvaluationModel();

            var eval = GetRepo().GetData<Entity.Evaluation>(n => n.Id == id);
            var data = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var emp = eval.Emp.Where(n => n.Employee.EmpCode == EmpCode);
            makeEvaluationModel.Evaluation = eval;
            makeEvaluationModel.Employee = emp.FirstOrDefault().Employee;
            foreach(var e in makeEvaluationModel.Evaluation.Data.Where(n=>n.EvaluationType == EvaluationType.Free && n.DeleteFlag==false ).ToList())
            {
                var femp = e.EvaluationPoint.Where(n => n.UserId == emp.FirstOrDefault().Employee.User && n.DeleteFlag == false);
                var fuse = e.EvaluationPoint.Where(n => n.UserId == data.BaseUser && n.DeleteFlag == false);
                if (femp.Count() != fuse.Count()) {
                  
                 
                    foreach (var p in femp.ToList()) {
                        EvaluationPoint evaluationPoint = new EvaluationPoint();
                        evaluationPoint.Name = p.Name;
                        evaluationPoint.SubPoint = new List<SubPoint>();
                        evaluationPoint.Weigth = p.Weigth;
                        evaluationPoint.UserId = data.BaseUser;
                        evaluationPoint.Owner = p.Owner;
                        evaluationPoint.EvaluationDataDetail = new EvaluationDataDetail();
                      //  evaluationPoint.Point = p.Point;
                        if (p.SubPoint.Count() > 0)
                        {
                            foreach (var s in p.SubPoint)
                            {
                                evaluationPoint.SubPoint.Add(new SubPoint()
                                {
                                    Name = s.Name
                                });
                            }
                        }

                        e.EvaluationPoint.Add(evaluationPoint);
                    }
                }
            }
            ViewData["EmpCode"] = emp.FirstOrDefault().Employee.User.BaseUserId;
            ViewData["Assersor"] = true;
            ViewData["UserId"] = data.BaseUser.BaseUserId;
            return View("Make",makeEvaluationModel);
        }

        public IActionResult Template()
        {
            return View();
        }
        public IActionResult ReportStatus()
        {
            return View();
        }
        public IActionResult ReportStatusDetail(long id)
        {
            var eval = GetRepo().GetData<Entity.Evaluation>(n => n.Id == id);
            return View(eval);
        }
        public IActionResult ManageTemplate()
        {
            return View();
        }
        public IActionResult OrderAssersor(long eval)
        {
            var e = GetRepo().GetData<Entity.Evaluation>(n => n.Id == eval);
            return View(e);
        }

        public IActionResult ManageTemplate1(EvaluationDataTemplate evaluation)
        {
            ModelState.Clear();
            if (evaluation.Id > 0)
            {
                var e = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == evaluation.Id);
                e.Name = evaluation.Name;
                e.EvaluationType = evaluation.EvaluationType;
                evaluation = e;
            }
            if (evaluation.EvaluationType == EvaluationType.Provide)
            {
                return View("ManageTemplate1", evaluation);
            }
            evaluation.Wording = "";
            GetRepo().Add(evaluation);
            GetRepo().SaveChange();
            return RedirectToAction("Template");
        }
        public IActionResult EditTemplate(long id)
        {

          var d=  GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == id);
            return View("ManageTemplate",d);
        }

        public IActionResult SaveTemplate([FromBody]EvaluationDataTemplate evaluation)
        {
            EvaluationDataTemplate eData = new EvaluationDataTemplate();
            if (evaluation.Id > 0)
            {
                 eData = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == evaluation.Id);
                eData.Name = evaluation.Name;
            }

            foreach (var e in evaluation.Details)
            {

                var ed = eData.Details.Where(n => n.Id == e.Id).FirstOrDefault();
                if (ed != null)
                {
                    ed.UpdateDate = DateTime.Now;
                    ed.Name = e.Name;
                    ed.Link = e.Link;
                    ed.Order = e.Order;
                    
                }
                else
                { 
                if (e.CreateDate == null)
                    e.CreateDate = DateTime.Now;
                    
                }
                foreach(var u in ed.SubDetail)
                {
                    u.DeleteFlag = true;
                }
                foreach (var ee in e.SubDetail)
                {
                   
                    if (ed != null)
                    {
                     
                        var edd = ed.SubDetail.Where(n => n.Id == ee.Id).FirstOrDefault();
                        if (edd != null)
                        {
                            ee.DeleteFlag = false;
                            edd.UpdateDate = DateTime.Now;
                            edd.Name = ee.Name;
                            edd.Link = ee.Link;
                            edd.Order = ee.Order;
                        }
                        else
                        {
                            edd = new EvaluationDataDetailTemplate();
                            edd.Name = ee.Name;
                            edd.Link = ee.Link;
                            edd.Order = ee.Order;
                            edd.CreateDate = DateTime.Now;
                            ed.SubDetail.Add(edd);

                        }
                    }
                    else
                    {
                        if (ee.CreateDate == null)
                            ee.CreateDate = DateTime.Now;
                    }
                }

            }
            if(evaluation.Id==0)
             GetRepo().Add(evaluation);

            GetRepo().SaveChange();
            return Ok();
        }


        public IActionResult Evaluation()
        {
            return View();
        }
        public IActionResult NewEvaluation()
        {
            return View();
        }

        public IActionResult EditEvaluation(long eval)
        {
            var e = GetRepo().GetData<Entity.Evaluation>(n=>n.Id == eval);
            return View("NewEvaluation",e);
        }

        public IActionResult SaveEvaluation(Entity.Evaluation evaluation)
        {
            return View();
        }
    }
}