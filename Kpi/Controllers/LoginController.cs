﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Kpi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    public class LoginController : BaseWebController<DataContext>
    {
        public async Task<IActionResult> Index(LoginModel loginModel)
        {
            loginModel.CusCode = GetCusCode();
            if (HttpContext.Request.Method == "GET")
            {
                // The action is a post
                ModelState.Clear();
                return View();
            }
            try
            {
                var data = await GetRepo().Login(loginModel.CusCode, loginModel.UserName, loginModel.Password, GetHeaderLang());
                var u = await GetRepo().GetUserFromApiKey(data.data, GetHeaderLang());
                var  e = GetRepo().GetData<Employee>(n => n.User == u.BaseUser);
               
                LoginSession(data.data);
                if (e != null)
                {
                    if (!e.ChangePassword)
                    {
                        return RedirectToAction("ChangePassword");
                    }
                    return RedirectToAction("Index1", "Main");
                }
               
                return RedirectToAction("Index", "Main");
                //  var key = await GetRepo().GetUserFromApiKey(data.data, GetHeaderLang());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                ModelState.AddModelError("LoginFail", "ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบข้อมูล");
            }

            return View();
        }
        public async Task<IActionResult> Logout()
        {
            LogoutSession();
            return RedirectToAction("Index");
        }
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel changePassword)
        {
            //  LogoutSession();
            if (HttpContext.Request.Method == "GET")
            {
                // The action is a post
                ModelState.Clear();
                return View();
            }
            else
            {
                try
                {
                    await GetRepo().ChangePassword(GetApiKey(),changePassword.Password,changePassword.ConfirmPassword, GetHeaderLang());
                    var u = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
                    var e = GetRepo().GetData<Employee>(n => n.User == u.BaseUser);
                    e.ChangePassword = true;
                    GetRepo().SaveChange();
                    return RedirectToAction("Index");
                }catch(Exception ex)
                {
                    ModelState.AddModelError("LoginFail", ex.Message);
                    return View();
                }
            }

        }


    }
}