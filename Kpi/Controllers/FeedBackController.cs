﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    public class FeedBackController : BaseWebController<DataContext>
    {
        public async Task<IActionResult> FeedBackAssertAsync(long FeedbackId)
        {
            var feeback = GetRepo().GetData<FeedBack>(n => n.Id == FeedbackId && n.DeleteFlag == false);
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
           
            ViewData["BaseUser"] = user.BaseUser.Id;


            return View(feeback);
        }
        public async Task<IActionResult> FeedBackAccept(long FeedbackId,long AssertId)
        {
            var feeback = GetRepo().GetData<FeedBack>(n => n.Id == FeedbackId && n.DeleteFlag == false);
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var f = feeback.FeedBackFromAsseerts.Where(n => n.Assert.Id == AssertId && n.Employee.employee.User == user.BaseUser).ToList();
            ViewData["FeedbackName"] = f.First().Assert.Employee.Name+" "+ f.First().Assert.Employee.LastName + " " + feeback.Name;
            ViewData["FeedbackId"] = FeedbackId;
            ViewData["AssertId"] = AssertId;
            return View(f);
        }

      

    }
}