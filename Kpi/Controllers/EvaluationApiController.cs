﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.Report.ReportExecutionService;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Kpi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Evaluation = Kpi.Entity.Evaluation;

namespace Kpi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EvaluationApiController : BaseController<DataContext>
    {
        [HttpPost]
        public IActionResult GetDataTableTemplate([FromForm]BaseDataTable baseDataTable)
        {
            var data = GetRepo().GetDataAll<EvaluationDataTemplate>(n => n.DeleteFlag == false).ToDataTable(baseDataTable);
            return new ObjectResult(data);
        }

        [HttpPost]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> GetEvaluation([FromForm]BaseDataTable baseDataTable)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var emp = GetRepo().GetData<Employee>(n => n.User == user.BaseUser);
            var data = GetRepo().GetDataAll<Entity.Evaluation>(n =>n.Emp.Any(e=>e.Employee == emp && e.DeleteFlag==false) && n.DeleteFlag == false).ToDataTable(baseDataTable);
            
            
            foreach(Entity.Evaluation e in data.Data)
            {
                var u = e.Emp.Where(n => n.Employee.User == user.BaseUser && n.DeleteFlag==false).FirstOrDefault();

                var a = e.Assessors.Where(n => n.Emp.User == user.BaseUser && n.DeleteFlag == false && n.Working==true).FirstOrDefault();
                if (u!=null && u.Send && a!=null)
                {
                    e.EmpSend = true;
                }
                else
                {
                    e.EmpSend = false;
                }
            }
            return new ObjectResult(data);
        }
        [HttpPost]
        //  [Authorize(Policy = "Login")]
        public async Task<IActionResult> SaveEvaluation(EvaluationModel evaluation)
        {
            Evaluation eval = new Evaluation();
            if(evaluation.Id != null)
            {
                eval = GetRepo().GetData<Evaluation>(n => n.Id == evaluation.Id);
            }
            eval.EvaluationName = evaluation.Name;
            eval.Year = evaluation.Year;
            eval.No = evaluation.No;
            eval.EvaluationDetail = evaluation.Desc;
            eval.RequestTimeSheet = evaluation.RequireTimesheet;
            eval.TimeSheetStart = evaluation.TimeSheetStart;
            eval.TimeSheetEnd = evaluation.TimeSheetEnd;
            eval.BeforeNoti = evaluation.BeforeNoti;
            eval.ExpireNoti = evaluation.ExpireNoti;

            foreach(var e in eval.Emp)
            {
                e.DeleteFlag = true;
            }
            foreach (var a in eval.Assessors)
            {
                a.DeleteFlag = true;
            }
            foreach (var e in evaluation.Emp)
            {
                var emp = GetRepo().GetData<Employee>(n => n.EmpCode == e);
                EvaluationEmp evaluationEmp = new EvaluationEmp();
                evaluationEmp.Employee = emp;
                evaluationEmp.CreateDate = DateTime.Now;
                if (eval.Emp == null)
                    eval.Emp = new List<EvaluationEmp>();
                eval.Emp.Add(evaluationEmp);
                int i = 0;
                foreach (var a in evaluation.Assessor)
                {
                    var emp1 = GetRepo().GetData<Employee>(n => n.EmpCode == a);
                    Assessor assessor = new Assessor();
                    assessor.Emp = emp;
                    assessor.EmpAssessor = emp1;
                    assessor.CreateDate = DateTime.Now;
                    
                    if (eval.Assessors.Where(n => n.Emp == emp && n.EmpAssessor == emp1 && n.Weigth>0 ).FirstOrDefault()!=null)
                    assessor.Weigth = eval.Assessors.Where(n => n.Emp == emp && n.EmpAssessor == emp1 && n.Weigth > 0).FirstOrDefault().Weigth;
                    assessor.Order = i++;
                    if (eval.Assessors == null)
                        eval.Assessors = new List<Assessor>();
                    eval.Assessors.Add(assessor);
                }
            }
            foreach (var d in eval.Data)
            {
                d.DeleteFlag = true;
            }
            int t = 0;
            foreach(var d in evaluation.Template)
            {
                var data = GetRepo().GetData<EvaluationDataTemplate>(n => n.Name == d.Name);
                EvaluationData ed = new EvaluationData();
                ed.Weigth = d.Weigth;
                ed.Name = data.Name;
                ed.EvaluationType = data.EvaluationType;
                ed.Wording = data.Wording;
                ed.CreateDate = DateTime.Now;
                ed.Order = t++;
                foreach (var dt in data.Details)
                {
                    EvaluationDataDetail dataDetail = new EvaluationDataDetail();
                    dataDetail.Name = dt.Name;
                    dataDetail.Weigth = dt.Weigth;
                    dataDetail.FileId = int.Parse(dt.Link);
                
                    dataDetail.CreateDate = DateTime.Now;
                    foreach(var dtt in dt.SubDetail)
                    {
                        EvaluationDataDetail dataDetailDetail = new EvaluationDataDetail();
                        dataDetailDetail.Name = dtt.Name;
                        dataDetailDetail.Weigth = dtt.Weigth;
                        dataDetailDetail.FileId = int.Parse(dtt.Link);
                    
                        dataDetailDetail.CreateDate = DateTime.Now;
                        if (dataDetail.SubDetail == null)
                            dataDetail.SubDetail = new List<EvaluationDataDetail>();
                        dataDetail.SubDetail.Add(dataDetailDetail);
                    }
                    if (ed.Details == null)
                        ed.Details = new List<EvaluationDataDetail>();
                    ed.Details.Add(dataDetail);
                }
                if (eval.Data == null)
                {
                    eval.Data = new List<EvaluationData>();
                }
                eval.Data.Add(ed);
            }
           
            if(evaluation.Id!=null || evaluation.Id > 0)
            {
                GetRepo().Update(eval);
            }
            else
            {
                GetRepo().Add(eval);
            }
            GetRepo().SaveChange();

            return Ok();
        }

        [HttpPost]
        //  [Authorize(Policy = "Login")]
        public IActionResult SaveOrder([FromQuery]long eval,List<EmpOrderModel> data)
        {
            var e = GetRepo().GetData<Evaluation>(n => n.Id == eval);

           
            foreach (var d in data.GroupBy(n=>n.Emp))
            {
                var emp = GetRepo().GetData<Employee>(n => n.EmpCode == d.Key);
                foreach (var a in e.Assessors.Where(n=>n.Emp == emp))
                {
                    a.DeleteFlag = true;
                }
                int o = 0;
                foreach (var a in d)
                {

                    var emp1 = GetRepo().GetData<Employee>(n => n.EmpCode == a.Assesor);
                    Assessor assessor = new Assessor();
                    assessor.Emp = emp;
                    assessor.EmpAssessor = emp1;
                    assessor.CreateDate = DateTime.Now;
                    assessor.Weigth =a.Weigth;
                    assessor.Order = o++;
                    if (e.Assessors == null)
                        e.Assessors = new List<Assessor>();
                    e.Assessors.Add(assessor);
                }
                
            }
            GetRepo().SaveChange();

            return new ObjectResult(data);
        }

        [HttpPost]
      //  [Authorize(Policy = "Login")]
        public async Task<IActionResult> GetEvaluationAll([FromForm]BaseDataTable baseDataTable)
        {
       
            var data = GetRepo().GetDataAll<Entity.Evaluation>(n =>  n.DeleteFlag == false).ToDataTable(baseDataTable);

            return new ObjectResult(data);
        }

        [HttpPost]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> GetAssessor([FromForm]BaseDataTable baseDataTable)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var emp = GetRepo().GetData<Employee>(n => n.User == user.BaseUser);
          //  var test= GetRepo().GetDataAll<Entity.Assessor>(n => n.EmpAssessor.User == user.BaseUser && n.Working == true && n.DeleteFlag == false);
            var data = GetRepo().GetDataAll<Entity.Assessor>(n => n.EmpAssessor.User == user.BaseUser && n.Working == true && n.DeleteFlag == false).ToDataTable(baseDataTable);
            List<object> i = new List<object>();
            foreach(Entity.Assessor d in data.Data)
            {
                // d.Evaluation.EvaluationName = d.Evaluation.EvaluationName+"("+d.EmpAssessor.Name +" "+d.EmpAssessor.LastName+")";
                var next = GetRepo().GetDataAll<Entity.Assessor>(n => n.Order >= d.Order + 1&& n.Weigth>0 && n.Emp == d.Emp && n.DeleteFlag == false).OrderBy(n=>n.Order).FirstOrDefault();
                var send = d.Send; 
                if (next!=null){
                    if (next.RejectDate != null)
                    {
                        send = false;
                    }
                }

                i.Add(new {
                    Id = d.Evaluation.Id,
                    EvaluationName = d.Evaluation.EvaluationName + "(" + d.Emp.Name + " " + d.Emp.LastName + ")",
                    Year = d.Evaluation.Year,
                    No = d.Evaluation.No,
                    StartDateString = d.Evaluation.StartDateString,
                    EndDateString = d.Evaluation.EndDateString,
                    EmpSend = send,
                   
                    EmpCode = d.Emp.EmpCode
                });
            }
            BaseReturnDatatable ret = new BaseReturnDatatable();
            ret.Count = data.Count;
            ret.Data = i;
            ret.RecordsFiltered = data.RecordsFiltered;
            ret.RecordsTotal = data.RecordsTotal;
            return new ObjectResult(ret);
        }

        [HttpGet]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> SendEvent(long eventId)
        {

            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var e = GetRepo().GetData<Entity.Evaluation>(n => n.Id == eventId);

            foreach(var ee in e.Data.Where(n=>n.DeleteFlag==false))
            {
                if (ee.EvaluationType == EvaluationType.Provide)
                {
                    if (ee.SumWeigth < 100)
                    {
                        return Problem("กรุณาใส่น้ำหนักให้ครบ 100");
                    }
                }
                else
                {
                    if (ee.EvaluationPoint.Where(n=>n.DeleteFlag== false && n.Owner == n.UserId && n.UserId== user.BaseUser).Sum(n=>n.Weigth) < 100)
                    {
                        return Problem("กรุณาใส่น้ำหนักให้ครบ 100");
                    }
                }
                if (ee.EvaluationType == EvaluationType.Free)
                {
                    foreach (var p in ee.EvaluationPoint.Where(n => n.DeleteFlag == false && n.Owner == n.UserId && n.UserId == user.BaseUser))
                    {
                        if (p.Point == 0)
                        {
                            return Problem("กรุณาประเมินให้ครบทุกข้อ");
                        }
                    }
                }
                else
                {
                    foreach (var p in ee.Details.Where(n => n.DeleteFlag == false))
                    {
                        if (p.EvaluationPoint.Where(n=>n.DeleteFlag==false && n.Owner == n.UserId && n.UserId == user.BaseUser).FirstOrDefault().Point == 0)
                        {
                            return Problem("กรุณาประเมินให้ครบทุกข้อ");
                        }
                    }
                }
            }




            var emp = e.Emp.Where(n => n.Employee.User == user.BaseUser && n.DeleteFlag==false).FirstOrDefault();
            emp.UpdateDate = DateTime.Now;
            emp.Send = true;
            emp.SendateTime = DateTime.Now;


            var a = e.Assessors.Where(n => n.Emp == emp.Employee && n.DeleteFlag == false && n.Send == false && n.Weigth > 0).OrderBy(n => n.Order).FirstOrDefault();
            a.Working = true;
            a.RejectComment = null;

            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> Reject(long eventId, long empCode,string comment)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var e = GetRepo().GetData<Entity.Evaluation>(n => n.Id == eventId);
            
            var emp = e.Assessors.Where(n => n.EmpAssessor.User == user.BaseUser && n.DeleteFlag == false && n.Emp.User.BaseUserId == empCode).FirstOrDefault();
            emp.UpdateDate = DateTime.Now;
            emp.Send = false;
            emp.RejectDate = DateTime.Now;
            emp.Working = false;
            emp.RejectComment = comment;
            GetRepo().SaveChange();

            var a = e.Assessors.Where(n => n.Emp.User.BaseUserId == empCode && n.DeleteFlag == false && n.Send == true && n.Weigth > 0).OrderBy(n => n.Order).FirstOrDefault();
            if (a == null)
            {
                var emp1 = e.Emp.Where(n => n.Employee.User.BaseUserId  == empCode && n.DeleteFlag==false).FirstOrDefault();
                emp1.UpdateDate = DateTime.Now;
                emp1.Send = false;
                GetRepo().SaveChange();
            }

            return Ok();
        }

        [HttpGet]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> SendAssesor(long eventId,long empCode)
        {

            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var e = GetRepo().GetData<Entity.Evaluation>(n => n.Id == eventId);
            var emp = e.Assessors.Where(n =>  n.EmpAssessor.User == user.BaseUser && n.DeleteFlag == false && n.Emp.User.BaseUserId == empCode).FirstOrDefault();
            emp.UpdateDate = DateTime.Now;
            emp.Send = true;
            emp.SendDate = DateTime.Now;
            GetRepo().SaveChange();

            var a = e.Assessors.Where(n => n.Emp.User.BaseUserId == empCode && n.DeleteFlag == false && n.Send == false && n.Weigth>0).OrderBy(n => n.Order).FirstOrDefault();
            if (a != null)
            {
                a.Working = true;
                a.RejectDate = null;
                a.RejectComment = null;
                GetRepo().SaveChange();
            }

     
            return Ok();
        }

        [HttpPost]
        [Authorize(Policy = "Login")]
        public async Task<IActionResult> SavePointEvaluation(Models.Evaluation data)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            foreach (var e in data.Data)
            {
               var d = GetRepo().GetData<EvaluationData>(n => n.DeleteFlag == false && n.Id == e.Id);
                var dd= GetRepo().GetDataAll<EvaluationPoint>(n => n.DeleteFlag == false && n.Owner.BaseUserId == data.EmpCode && n.UserId.BaseUserId == user.BaseUser.BaseUserId && n.Evaluation.Id == d.Evaluation.Id);
                foreach (var d1 in dd)
                {
                    d1.DeleteFlag = true;

                }
         
                GetRepo().SaveChange();
            }

            foreach (var e in data.Data)
            {
                var d = GetRepo().GetData<EvaluationData>(n => n.DeleteFlag == false && n.Id == e.Id);
              
             
                foreach (var ee in e.Data) {
                    EvaluationPoint ep = new EvaluationPoint();
                    ep.Evaluation = d.Evaluation;
                    if(ee.Id!=null)
                    ep.EvaluationDataDetail = GetRepo().GetData<EvaluationDataDetail>(n => n.DeleteFlag == false && n.Id == ee.Id);
                    else
                    ep.EvaluationData = GetRepo().GetData<EvaluationData>(n => n.DeleteFlag == false && n.Id == e.Id);
                    ep.Name = ee.Name;
                    ep.Point = ee.Point;
                    if(ee.Weigth.HasValue)
                    ep.Weigth = ee.Weigth.Value;
                    ep.UserId = user.BaseUser;
                    ep.Customer = user.BaseCustomer;
                    ep.Order = ee.Order;
                    ep.CreateDate = DateTime.Now;
                    ep.SubPoint = new List<SubPoint>();
                    if (ee.Subname != null)
                    {
                        foreach (var n in ee.Subname)
                        {
                            ep.SubPoint.Add(new SubPoint() { Name = n.Name,Order=n.Order });
                        }
                    }

                    if (ep.Owner == ep.UserId)
                    {
                        var emp = d.Evaluation.Emp.Where(n => n.DeleteFlag==false && n.Employee.User == user.BaseUser).FirstOrDefault();
                        emp.Recommend = data.Recommend;
                        ep.Owner = user.BaseUser;
                    }
                    else
                    {
                        var emp = d.Evaluation.Emp.Where(n => n.DeleteFlag == false && n.Employee.User.BaseUserId == data.EmpCode).FirstOrDefault();
                        emp.Recommend = data.Recommend;
                        ep.Owner = GetRepo().GetData<BaseUser>(n => n.BaseUserId == data.EmpCode);
                    }
                    
                    GetRepo().Add(ep);
                }
            }
            GetRepo().SaveChange();
          
            return Ok();
        }


        [HttpPost]
        public async Task<IActionResult> SaveEvaluationImport(Entity.Evaluation data)
        {
            //var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            var e = GetRepo().GetData<Entity.Evaluation>(n =>n.DeleteFlag==false && n.EvaluationName == data.EvaluationName);
            if (e == null)
            {
                GetRepo().Add(data);

                GetRepo().SaveChange();
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SaveFlowImport(Models.EmployeeFlowImport data)
        {
            //var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            var e = GetRepo().GetData<Entity.Evaluation>(n => n.DeleteFlag == false && n.EvaluationName == data.Name);

            if (e.Assessors == null)
                e.Assessors = new List<Assessor>();
            //foreach (var ee in e.Assessors)
            //{
            //    ee.DeleteFlag = true;

            //}
            GetRepo().SaveChange();
            var owner = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.Owner.Trim().Replace("\n","").Replace("\r",""));
            if (owner != null)
            {
                if (e != null)
                {

                    if (data.F1 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F1.Trim().Substring(0,5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (emp == null)
                        {
                            Console.WriteLine("Not Found");
                        }
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W1.Value;
                            Assessor.Order = 1;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }
                    if (data.F2 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F2.Trim().Substring(0, 5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W2.Value;
                            Assessor.Order = 2;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }
                    if (data.F3 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F3.Trim().Substring(0, 5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W3.Value;
                            Assessor.Order = 3;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }
                    if (data.F4 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F4.Trim().Substring(0, 5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W4.Value;
                            Assessor.Order = 4;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }
                    if (data.F5 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F5.Trim().Substring(0, 5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W5.Value;
                            Assessor.Order = 5;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }
                    if (data.F6 != null)
                    {
                        var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag == false && n.EmpCode == data.F6.Trim().Substring(0, 5).Trim().Replace("\n", "").Replace("\r", ""));
                        if (GetRepo().GetData<Assessor>(n => n.Emp == owner && n.EmpAssessor == emp && n.Evaluation == e) == null)
                        {
                            Assessor Assessor = new Assessor();
                            Assessor.EmpAssessor = emp;
                            Assessor.Emp = owner;
                            Assessor.Weigth = data.W6.Value;
                            Assessor.Order = 6;
                            Assessor.CreateDate = DateTime.Now;
                            e.Assessors.Add(Assessor);
                            GetRepo().SaveChange();
                        }
                    }



                }
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SaveEmpImport(Models.EmployeeImport data)
        {
            //var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            var e = GetRepo().GetData<Entity.Evaluation>(n => n.DeleteFlag==false && n.EvaluationName == data.Name);


            if (e != null)
            {

                var emp = GetRepo().GetData<Entity.Employee>(n => n.DeleteFlag==false&& n.EmpCode == data.EmpCode);
             
                if (emp != null)
                {
                    var ee = GetRepo().GetData<Entity.EvaluationEmp>(n => n.DeleteFlag == false && n.Evaluation == e && n.Employee == emp);
                    if (ee == null)
                    {
                        EvaluationEmp evaluationEmp = new EvaluationEmp();
                        evaluationEmp.Employee = emp;
                        evaluationEmp.Evaluation = e;
                        evaluationEmp.CreateDate = DateTime.Now;
                        e.Emp.Add(evaluationEmp);


                        GetRepo().SaveChange();
                    }
                    else
                    {
                        Console.WriteLine("Error");
                    }
                }
                else
                {
                    Console.WriteLine("Error");
                }
            }
            else
            {
                Console.WriteLine("Error");
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SaveEvaluationTemplateImport(EvaluationImportModel data)
        {
            //var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            var e = GetRepo().GetData<Entity.Evaluation>(n => n.DeleteFlag==false && n.EvaluationName == data.Name);
          
            GetRepo().SaveChange();
            if (e != null)
            {
                foreach (var d in e.Data)
                {
                    d.DeleteFlag = true;

                }
                if (data.T1)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 27);

                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }

                if (data.T2)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 28);

                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }

                if (data.T3)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 27);

                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }


                if (data.T4)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 29);

                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }


                if (data.T5)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 33);
               
                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }
                if (data.T6)
                {
                    var T = GetRepo().GetData<EvaluationDataTemplate>(n => n.Id == 34);
                    EvaluationData evaluationData = new EvaluationData();
                    evaluationData.CreateDate = DateTime.Now;
                    evaluationData.Name = T.Name;
                    evaluationData.BasePoint = T.BasePoint;
                    evaluationData.EvaluationType = T.EvaluationType;
                    evaluationData.Evaluation = e;
                    evaluationData.Order = 0;
                    evaluationData.Weigth = 0;
                    evaluationData.Wording = T.Wording;
                    evaluationData.Details = new List<EvaluationDataDetail>();
                    foreach (var i in T.Details)
                    {
                        EvaluationDataDetail edd = new EvaluationDataDetail();
                        edd.Name = i.Name;
                        edd.Weigth = i.Weigth;
                        edd.CreateDate = DateTime.Now;
                        if (i.Link != null)
                            edd.FileId = int.Parse(i.Link);
                        edd.Order = 0;
                        edd.SubDetail = new List<EvaluationDataDetail>();
                        foreach (var ed in i.SubDetail)
                        {
                            EvaluationDataDetail edd1 = new EvaluationDataDetail();
                            edd1.Name = ed.Name;
                            edd1.Weigth = ed.Weigth;
                            edd1.CreateDate = DateTime.Now;
                            if (ed.Link != null)
                                edd1.FileId = int.Parse(ed.Link);
                            edd1.Order = 0;
                            edd.SubDetail.Add(edd1);
                        }


                        evaluationData.Details.Add(edd);
                    }
                    GetRepo().Add(evaluationData);
                    GetRepo().SaveChange();
                }

            }

            return Ok();
        }
    }
}