﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCore.Report.ReportService2010_;
using Framework.BaseEntity;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;

namespace Kpi.Controllers
{
    public class SurveryController : BaseWebController<DataContext>
    {
        [Authorize(Policy = "Login")]
        public IActionResult Index(long? ServeyId,long? ServeyDetailId)
        {
            SurveyDetails survey;
            if (ServeyDetailId.HasValue)
                survey=GetRepo().GetData<SurveyDetails>(n => n.Survey.Id == ServeyId && n.Id == ServeyDetailId && n.DeleteFlag == false);
            else
                survey = GetRepo().GetDataAll<SurveyDetails>(n => n.Survey.Id == ServeyId && n.DeleteFlag==false).OrderBy(n=>n.Order).FirstOrDefault();
            return View(survey);
        }

        [Authorize(Policy = "Login")]
        public async Task<IActionResult> Next(long Choose,string comment)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetData<SurveyDetailChoose>(n => n.Id == Choose);
            SurveySelect surveySelect = new SurveySelect();
            surveySelect.Choose = data;
            surveySelect.SurveyDetails = data.SurveyDetail;
            surveySelect.Survey = data.SurveyDetail.Survey;
            surveySelect.User = user.BaseUser;
            surveySelect.Comment = comment;
            var d = GetRepo().GetDataAll<SurveySelect>(n => n.User == user.BaseUser && n.Choose.SurveyDetail == data.SurveyDetail);
            foreach(var c in d)
            {
                c.DeleteFlag = true;
            }
           
            GetRepo().SaveChange();
            GetRepo().Add(surveySelect);
            GetRepo().SaveChange();


            if (data.Next != null)
                return RedirectToAction("Index", new { ServeyId = data.Next.Survey.Id, ServeyDetailId = data.Next.Id });
            else
            {
                var surveys = GetRepo().GetDataAll<SurveyDetails>(n => n.Survey.Id == data.SurveyDetail.Survey.Id && n.DeleteFlag == false).OrderBy(n => n.Order).ToList();
       
                var positon = surveys.FindIndex(n => n.Id == data.SurveyDetail.Id);
                if (positon + 1 >= surveys.Count())
                {
                    var emp =GetRepo().GetData<SurveyEmp>(n => n.Survey== data.SurveyDetail.Survey && n.Employee.User == user.BaseUser);
                    emp.finish = true;
                    GetRepo().SaveChange();
                    return RedirectToAction("Index1", "Main");
                }
                var next = surveys[positon + 1];
               

                return RedirectToAction("Index", new { ServeyId = next.Survey.Id, ServeyDetailId = next.Id });

            }
        
        }
    }
}
