﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseModel;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Kpi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FeedBackApiController : BaseController<DataContext>
    {
        [HttpPost]
        public async Task<IActionResult> GetFeedBackAssert([FromForm] BaseDataTable baseDataTable)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetDataAll<FeedBack>(n => n.DeleteFlag == false && n.Asseert.Any(e => e.Working==true && e.Employee.User == user.BaseUser && e.DeleteFlag == false)).ToDataTable(baseDataTable);


            foreach (FeedBack s in data.Data)
            {
                if (s.Asseert.Where(n => n.Employee.User == user.BaseUser).First().Finish == true)
                {
                    s.EmpSent = true;
                }
                if(s.Asseert.Where(n => n.Employee.User == user.BaseUser).First().Parent != null)
                    s.Name = s.Name+"("+ s.Asseert.Where(n => n.Employee.User == user.BaseUser).First().Parent.Employee.Name +" "+ s.Asseert.Where(n => n.Employee.User == user.BaseUser).First().Parent.Employee.LastName+ ")";
                s.FeedBackFromAsseerts = s.FeedBackFromAsseerts.Where(n => n.DeleteFlag == false).ToList();
            }
            return new ObjectResult(data);
        }
        [HttpPost]
        public async Task<IActionResult> GetFeedBack([FromForm] BaseDataTable baseDataTable)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetDataAll<FeedBack>(n => n.DeleteFlag == false && n.Feedback.Any(e => e.employee.User == user.BaseUser && e.DeleteFlag == false)).ToList();


            List<FeedbackResponseModel> list = new List<FeedbackResponseModel>();
            

            foreach (FeedBack s in data.Where(n=>n.FeedBackFromAsseerts.Any(s=>s.FeedBack!="" && s.DeleteFlag==false && s.FeedBack!=null )))
            {

                if (s.Asseert.Where(n => n.Finish == false).Count() == 0)
                {
                    var f = s.Asseert.OrderBy(n => n.Ordering).ToList().Last();
                        FeedbackResponseModel feedbackResponseModel = new FeedbackResponseModel();
                        feedbackResponseModel.AssertId = f.Id;
                        feedbackResponseModel.FeedbackId = s.Id;
                        feedbackResponseModel.FeedBackName =  s.Name;
                        var emp = s.FeedbackAccept.Where(n => n.Emp.employee.User == user.BaseUser && n.Assert == f).FirstOrDefault();
                        if (emp != null && emp.Accept.HasValue)
                        {
                            feedbackResponseModel.Accept = true;
                        }
                        //feedbackResponseModel.Accept 
                        list.Add(feedbackResponseModel);
                    
                }
            }
            return new ObjectResult(list.ToDataTable(baseDataTable));
        }
        [HttpPost]
        public async Task<IActionResult> SaveFeedback(FeedbackModel feedbackModel)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetData<FeedBack>(n => n.DeleteFlag == false && n.Id == feedbackModel.feedBackId);
            foreach(var f in data.FeedBackFromAsseerts.Where(n=>n.Assert.Employee.User == user.BaseUser)){
                f.DeleteFlag = true;
            }
            GetRepo().SaveChange();
            var assert = data.Asseert.Where(n => n.Employee.User == user.BaseUser);
            foreach(var e in feedbackModel.feedback)
            {
                var emp = data.Feedback.Where(n => n.Id == e.emp);
                foreach (var f in e.feedback)
                {
                    FeedBackFromAsseert feedBackFromAsseert = new FeedBackFromAsseert();
                    feedBackFromAsseert.CreateDate = DateTime.Now;
                    feedBackFromAsseert.Assert = assert.First();
                    feedBackFromAsseert.Employee = emp.First();
                    feedBackFromAsseert.FeedBack = f.feedback;
                   
                    feedBackFromAsseert.FeedBackDetail = data.FeedBackDetails.Where(n => n.Id == f.feedbackId).First();
                    if (data.FeedBackFromAsseerts == null)
                        data.FeedBackFromAsseerts = new List<FeedBackFromAsseert>();
                    data.FeedBackFromAsseerts.Add(feedBackFromAsseert);
                }

            }

            GetRepo().SaveChange();

            return Ok();
        }


        [HttpPost]
        public async Task<IActionResult> SendFeedback(FeedbackModel feedbackModel)
        {
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());
            var data = GetRepo().GetData<FeedBack>(n => n.DeleteFlag == false && n.Id == feedbackModel.feedBackId);
            foreach (var f in data.FeedBackFromAsseerts.Where(n => n.Assert.Employee.User == user.BaseUser))
            {
                f.DeleteFlag = true;
            }
            GetRepo().SaveChange();
            var assert = data.Asseert.Where(n => n.Employee.User == user.BaseUser);
            foreach (var e in feedbackModel.feedback)
            {
                var emp = data.Feedback.Where(n => n.Id == e.emp);
                foreach (var f in e.feedback)
                {
                    FeedBackFromAsseert feedBackFromAsseert = new FeedBackFromAsseert();
                    feedBackFromAsseert.CreateDate = DateTime.Now;
                    feedBackFromAsseert.Assert = assert.First();
                    feedBackFromAsseert.Employee = emp.First();
                    feedBackFromAsseert.FeedBack = f.feedback;

                    feedBackFromAsseert.FeedBackDetail = data.FeedBackDetails.Where(n => n.Id == f.feedbackId).First();
                    if (data.FeedBackFromAsseerts == null)
                        data.FeedBackFromAsseerts = new List<FeedBackFromAsseert>();
                    data.FeedBackFromAsseerts.Add(feedBackFromAsseert);


                    if (assert.First().NextEmployee != null)
                    {
                        FeedBackFromAsseert feedBackFromAsseert1 = new FeedBackFromAsseert();
                        feedBackFromAsseert1.CreateDate = DateTime.Now;
                        feedBackFromAsseert1.Assert = assert.First().NextEmployee;
                        feedBackFromAsseert1.Employee = emp.First();
                        feedBackFromAsseert1.FeedBack = f.feedback;

                        feedBackFromAsseert1.FeedBackDetail = data.FeedBackDetails.Where(n => n.Id == f.feedbackId).First();
                        if (data.FeedBackFromAsseerts == null)
                            data.FeedBackFromAsseerts = new List<FeedBackFromAsseert>();
                        data.FeedBackFromAsseerts.Add(feedBackFromAsseert1);

                    }
                }

            }
            assert.First().Finish = true;
            //assert.First().Working = false;
            if(assert.First().NextEmployee!=null)
            assert.First().NextEmployee.Working = true;
            GetRepo().SaveChange();
           
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public async Task<IActionResult> Accept(FeedbackRequest feedbackRequest)
        {
            var feeback = GetRepo().GetData<FeedBack>(n => n.Id == feedbackRequest.FeedbackId && n.DeleteFlag == false);
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            FeedbackAccept feedbackAccept = new FeedbackAccept();
            feedbackAccept.Assert = feeback.Asseert.Where(n => n.Id == feedbackRequest.AssertId).FirstOrDefault();
            feedbackAccept.Emp = feeback.Feedback.Where(n => n.employee.User == user.BaseUser).FirstOrDefault();
            feedbackAccept.Accept = true;
            feedbackAccept.CreateDate = DateTime.Now;
            if (feeback.FeedbackAccept == null)
                feeback.FeedbackAccept = new List<FeedbackAccept>();
            feeback.FeedbackAccept.Add(feedbackAccept);
            GetRepo().SaveChange();
            return Ok();
        }
        [HttpPost]
        public async Task<IActionResult> Reject(FeedbackRequest feedbackRequest)
        {
            var feeback = GetRepo().GetData<FeedBack>(n => n.Id == feedbackRequest.FeedbackId && n.DeleteFlag == false);
            var user = await GetRepo().GetUserFromApiKey(GetApiKey(), GetHeaderLang());

            FeedbackAccept feedbackAccept = new FeedbackAccept();
            feedbackAccept.Assert = feeback.Asseert.Where(n => n.Id == feedbackRequest.AssertId).FirstOrDefault();
            feedbackAccept.Emp = feeback.Feedback.Where(n => n.employee.User == user.BaseUser).FirstOrDefault();
            feedbackAccept.Accept = false;
            feedbackAccept.ResonReject = feedbackRequest.reson;
            feedbackAccept.CreateDate = DateTime.Now;
            if (feeback.FeedbackAccept == null)
                feeback.FeedbackAccept = new List<FeedbackAccept>();
            feeback.FeedbackAccept.Add(feedbackAccept);
            GetRepo().SaveChange();
            return Ok();
        }
        public async Task<IActionResult> GetReportAsync(string id,string type)
        {

            using (HttpClient client = new HttpClient())
            {
                var file = await client.GetAsync("http://203.154.83.161:5000/api/Report/GetReport?reportName=/KPI/Feedback&id=" + id + "&type=" + type);

                return new FileContentResult(await file.Content.ReadAsByteArrayAsync(), file.Content.Headers.ContentType.MediaType)
                {
                    FileDownloadName="Feedback."+type
                };
            }
        }
        public async Task<IActionResult> GetExcelAsync(string id, string type)
        {

            using (HttpClient client = new HttpClient())
            {
                var file = await client.GetAsync("http://203.154.83.161:5000/api/Report/GetReport?reportName=/KPI/Feedback&id=" + id + "&type=xlsx" );

                return new FileContentResult(await file.Content.ReadAsByteArrayAsync(), "application/xlsx")
                {
                    FileDownloadName = "Feedback." + type
                };
            }
        }
    }
}