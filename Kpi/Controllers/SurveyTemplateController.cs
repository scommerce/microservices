﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    public class SurveyTemplateController : BaseWebController<DataContext>
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult EditTemplate(long id)
        {
            var d = GetRepo().GetData<SurveyTemplate>(n => n.Id == id);
            return View(d);
        }
        public IActionResult SurveyDetailTemplate(long id)
        {
            var d = GetRepo().GetData<SurveyTemplate>(n => n.Id == id);
            return View(d);
        }
    }
}