﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Framework.BaseEntity;
using Framework.BaseModel;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Kpi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kpi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeeController  :BaseController<DataContext>
    {
        [HttpPost]
        public IActionResult GetEmployee([FromForm]BaseDataTable baseDataTable)
        {
            var data = GetRepo().GetDataAll<Employee>().OrderBy(n=>n.EmpCode).ToDataTable(baseDataTable);
            return new ObjectResult(data);

        }
        [HttpGet]
        public async Task<IActionResult> LoadEmployee()
        {
            var emp = await GetRepo().sendRequestCustom<List<EmployeeOracle>>("http://113.53.247.76/KPI/Controller/getEmployeeOracleNew.php", HttpMethod.Get, null, false);
           
            foreach(var e in emp)
            {
                Employee newEmp = GetRepo().GetData<Employee>(n => n.DeleteFlag == false && n.EmpCode == e.EMPLOYEE_NUMBER);
                if (newEmp == null)
                {
                    newEmp = new Employee();
                   
                    GetRepo().Add(newEmp);
                }
                if (!newEmp.ChangePassword)
                {
                    try
                    {
                        await GetRepo().sendRequest("http://203.150.107.134:9002/api/User/AddUser", HttpMethod.Post, new
                        {
                            CusCode = "PMCU-KPI01",
                            UserName = e.USER_NAME,
                            Password = "1234",
                            ConfirmPassword = "1234"
                        }, true);
                        var api = await GetRepo().Login("PMCU-KPI01", e.USER_NAME, "1234", GetHeaderLang());
                        var u = await GetRepo().GetUserFromApiKey(api.data, GetHeaderLang());
                        newEmp.User = u.BaseUser;
                    }catch(Exception ex)
                    {

                    }
                }
                Prefix prefix = GetRepo().GetData<Prefix>(n => n.DeleteFlag == false && n.Name == e.TITLE_MEANING);
                if (prefix == null)
                {
                    prefix = new Prefix();
                    prefix.Name = e.TITLE_MEANING;
                    GetRepo().Add(prefix);
                }
                newEmp.EmpCode = e.EMPLOYEE_NUMBER;
                newEmp.Prefix = prefix;
                newEmp.Name = e.FIRST_NAME;
                newEmp.LastName = e.LAST_NAME;

                Position position = GetRepo().GetData<Position>(n => n.DeleteFlag == false && n.Name == e.POSITION_NAME);
                if (position == null)
                {
                    position = new Position();
                    position.Name = e.POSITION_NAME;
                    GetRepo().Add(position);
                }


                newEmp.Position = position;

                Department department = GetRepo().GetData<Department>(n => n.DeleteFlag == false && n.Name == e.ORG_NAME);
                if (department == null)
                {
                    department = new Department();
                    department.Name = e.ORG_NAME;
                    GetRepo().Add(department);
                }


                newEmp.Department = department;
                newEmp.BirthDate = e.DATE_OF_BIRTH;
                newEmp.StartDate = e.START_DATE;
                newEmp.HireDate = e.HIRE_DATE;
                newEmp.No = e.MANP_NO;
               
                newEmp.Level = e.TYPE;

                EmployeeType employeeType = GetRepo().GetData<EmployeeType>(n => n.DeleteFlag == false && n.Name == e.GROUP_NAME);
                if (employeeType == null)
                {
                    employeeType = new EmployeeType();
                    employeeType.Name = e.GROUP_NAME;
                    GetRepo().Add(employeeType);
                }
                newEmp.EmployeeType = employeeType;
                newEmp.UserName = e.USER_NAME;
                newEmp.Email = e.EMAIL_ADDRESS_T;
                // GetRepo().Add(newEmp);


              
                GetRepo().SaveChange();

            }
            //GetRepo().SaveChange();
            
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> LoadLeaveEmployee()
        {
            var lastDate = GetRepo().GetData<BaseSetting>(n => n.Key == "LastDate");
            var Leave = await GetRepo().sendRequestCustom<List<EmployeeLeaveOracle>>("http://113.53.247.76/KPI/Controller/getLeaveUserNew.php", HttpMethod.Get, new {
                leave_start = lastDate.Value
            }, true);

            foreach (var e in Leave)
            {
                EmployeeLeave newLeave = GetRepo().GetData<EmployeeLeave>(n => n.DeleteFlag == false && n.LeaveEnd ==e.LEAVE_END_DATE && n.LeaveStart == e.LEAVE_START_DATE && n.Employee.EmpCode == e.EMP_CODE);
                if(newLeave == null)
                {
                    newLeave = new EmployeeLeave();
                    Employee emp = GetRepo().GetData<Employee>(n => n.DeleteFlag == false && n.EmpCode == e.EMP_CODE);
                    newLeave.LeaveName = e.LEAVE_NAME;
                    newLeave.LeaveStart = e.LEAVE_START_DATE;
                    newLeave.LeaveEnd = e.LEAVE_END_DATE;
                    newLeave.AbsenceDay = e.ABSENCE_DAYS;
                    newLeave.AbsenceTypeId = e.ABSENCE_TYPE_ID;
                    newLeave.CreateDate = DateTime.Now;
                    emp.EmployeeLeaves.Add(newLeave);
                }

            }
           // GetRepo().SaveChange();

            lastDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
            lastDate.UpdateDate = DateTime.Now;
            GetRepo().SaveChange();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> LoadLateEmployee()
        {
            var lastDate = GetRepo().GetData<BaseSetting>(n => n.Key == "LateDate");
            var Leave = await GetRepo().sendRequestCustom<List<EmployeeLateOracle>>("http://113.53.247.76/KPI/Controller/getLateUser.php", HttpMethod.Get, new
            {
                leave_start = lastDate.Value
            }, true);

            foreach (var e in Leave)
            {
                EmployeeLate newLeave = GetRepo().GetData<EmployeeLate>(n => n.DeleteFlag == false && n.DateIn == e.DATE_IN && n.DateOut == e.DATE_OUT && n.Employee.EmpCode == e.EMP_CODE);
                if (newLeave == null)
                {
                    newLeave = new EmployeeLate();
                    Employee emp = GetRepo().GetData<Employee>(n => n.DeleteFlag == false && n.EmpCode == e.EMP_CODE);

                    newLeave.LeaveName = e.LEAVE_NAME;
                    newLeave.DateIn = e.DATE_IN;
                    newLeave.DateOut = e.DATE_OUT;
                    newLeave.TimeIn = e.TIME_IN;
                    newLeave.LateTime = e.LATE_TIME;
                    newLeave.TimeOut = e.TIME_OUT;
                    newLeave.CreateDate = DateTime.Now;
                    emp.EmployeeLates.Add(newLeave);
                }

            }
            // GetRepo().SaveChange();

            lastDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
            lastDate.UpdateDate = DateTime.Now;
            GetRepo().SaveChange();
            return Ok();
        }



        [HttpGet]
        public async Task<IActionResult> LoadShifEmployee()
        {
            var lastDate = GetRepo().GetData<BaseSetting>(n => n.Key == "ShifDate");
            var Leave = await GetRepo().sendRequestCustom<List<EmployeeShifOracle>>("http://113.53.247.76/KPI/Controller/getShifUser.php", HttpMethod.Get, new
            {
                leave_start = lastDate.Value
            }, true);

            foreach (var e in Leave)
            {
                EmployeeShif newLeave = GetRepo().GetData<EmployeeShif>(n => n.DeleteFlag == false && n.ShifDate == e.SHIF_DATE && n.Employee.EmpCode == e.EMP_CODE);
                if (newLeave == null)
                {
                    newLeave = new EmployeeShif();
                    Employee emp = GetRepo().GetData<Employee>(n => n.DeleteFlag == false && n.EmpCode == e.EMP_CODE);

                    newLeave.LeaveName = e.LEAVE_NAME;
                    newLeave.ShifDate = e.SHIF_DATE;
                    newLeave.Amount = e.AMOUNT;
                    newLeave.CreateDate = DateTime.Now;
                    emp.EmployeeShifs.Add(newLeave);
                }

            }
            // GetRepo().SaveChange();

            lastDate.Value = DateTime.Now.ToString("yyyy-MM-dd");
            lastDate.UpdateDate = DateTime.Now;
            GetRepo().SaveChange();
            return Ok();
        }
    }
}