﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Kpi.Entity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Kpi.Controllers
{
    public class FeedbackCRUDController : BaseCRUD<FeedBack, FeedBack, DataContext>
    {
        // GET: /<controller>/

        public FeedbackCRUDController()
        {
           // FieldMap.Add("CurrentUser", "");
        }
    }
}
