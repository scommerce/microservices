﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class changeEmployeeAssessor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assessors_BaseUsers_EmpId",
                table: "Assessors");

            migrationBuilder.AddForeignKey(
                name: "FK_Assessors_Employees_EmpId",
                table: "Assessors",
                column: "EmpId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assessors_Employees_EmpId",
                table: "Assessors");

            migrationBuilder.AddForeignKey(
                name: "FK_Assessors_BaseUsers_EmpId",
                table: "Assessors",
                column: "EmpId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
