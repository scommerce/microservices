﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addFeedBackTemplate5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AssertId",
                table: "FeedBackFromAsseert",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_AssertId",
                table: "FeedBackFromAsseert",
                column: "AssertId");

            migrationBuilder.AddForeignKey(
                name: "FK_FeedBackFromAsseert_FeedbackAssert_AssertId",
                table: "FeedBackFromAsseert",
                column: "AssertId",
                principalTable: "FeedbackAssert",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeedBackFromAsseert_FeedbackAssert_AssertId",
                table: "FeedBackFromAsseert");

            migrationBuilder.DropIndex(
                name: "IX_FeedBackFromAsseert_AssertId",
                table: "FeedBackFromAsseert");

            migrationBuilder.DropColumn(
                name: "AssertId",
                table: "FeedBackFromAsseert");
        }
    }
}
