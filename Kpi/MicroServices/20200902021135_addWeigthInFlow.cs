﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addWeigthInFlow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmpCode",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Assessors");

            migrationBuilder.AddColumn<long>(
                name: "EmpId",
                table: "Assessors",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Weigth",
                table: "Assessors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_EmpId",
                table: "Assessors",
                column: "EmpId");

            migrationBuilder.AddForeignKey(
                name: "FK_Assessors_BaseUsers_EmpId",
                table: "Assessors",
                column: "EmpId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assessors_BaseUsers_EmpId",
                table: "Assessors");

            migrationBuilder.DropIndex(
                name: "IX_Assessors_EmpId",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "EmpId",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "Weigth",
                table: "Assessors");

            migrationBuilder.AddColumn<string>(
                name: "EmpCode",
                table: "Assessors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Assessors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "Assessors",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Assessors",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
