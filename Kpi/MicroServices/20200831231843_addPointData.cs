﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addPointData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "EvaluationDataId",
                table: "EvaluationPoint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_EvaluationDataId",
                table: "EvaluationPoint",
                column: "EvaluationDataId");

            migrationBuilder.AddForeignKey(
                name: "FK_EvaluationPoint_EvaluationDatas_EvaluationDataId",
                table: "EvaluationPoint",
                column: "EvaluationDataId",
                principalTable: "EvaluationDatas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EvaluationPoint_EvaluationDatas_EvaluationDataId",
                table: "EvaluationPoint");

            migrationBuilder.DropIndex(
                name: "IX_EvaluationPoint_EvaluationDataId",
                table: "EvaluationPoint");

            migrationBuilder.DropColumn(
                name: "EvaluationDataId",
                table: "EvaluationPoint");
        }
    }
}
