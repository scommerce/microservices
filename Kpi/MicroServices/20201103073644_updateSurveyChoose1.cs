﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class updateSurveyChoose1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SurveySelect",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    UserId = table.Column<long>(nullable: true),
                    SurveyId = table.Column<long>(nullable: true),
                    SurveyDetailsId = table.Column<long>(nullable: true),
                    ChooseId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveySelect", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveySelect_SurveyDetailChoose_ChooseId",
                        column: x => x.ChooseId,
                        principalTable: "SurveyDetailChoose",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveySelect_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveySelect_SurveyDetails_SurveyDetailsId",
                        column: x => x.SurveyDetailsId,
                        principalTable: "SurveyDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveySelect_Survey_SurveyId",
                        column: x => x.SurveyId,
                        principalTable: "Survey",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveySelect_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveySelect_BaseUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_ChooseId",
                table: "SurveySelect",
                column: "ChooseId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_CreateUserId",
                table: "SurveySelect",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_SurveyDetailsId",
                table: "SurveySelect",
                column: "SurveyDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_SurveyId",
                table: "SurveySelect",
                column: "SurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_UpdateUserId",
                table: "SurveySelect",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveySelect_UserId",
                table: "SurveySelect",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SurveySelect");
        }
    }
}
