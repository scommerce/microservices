﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addEvaluation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EvaluationDataTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    BasePoint = table.Column<int>(nullable: false),
                    Wording = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluationDataTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluationDataTemplates_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataTemplates_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataTemplates_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Evaluations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    EvaluationName = table.Column<string>(nullable: false),
                    EvaluationDetail = table.Column<string>(nullable: true),
                    RequestTimeSheet = table.Column<bool>(nullable: false),
                    TimeSheetStart = table.Column<DateTime>(nullable: true),
                    TimeSheetEnd = table.Column<DateTime>(nullable: true),
                    AllowNull = table.Column<bool>(nullable: false),
                    SkipFirst = table.Column<bool>(nullable: false),
                    RecommandWording = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evaluations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Evaluations_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evaluations_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Evaluations_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EvaluationDataDetailTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Weigth = table.Column<int>(nullable: false),
                    EvaluationDataDetailTemplateId = table.Column<long>(nullable: true),
                    EvaluationDataTemplateId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluationDataDetailTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetailTemplates_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetailTemplates_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetailTemplates_EvaluationDataDetailTemplates_EvaluationDataDetailTemplateId",
                        column: x => x.EvaluationDataDetailTemplateId,
                        principalTable: "EvaluationDataDetailTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetailTemplates_EvaluationDataTemplates_EvaluationDataTemplateId",
                        column: x => x.EvaluationDataTemplateId,
                        principalTable: "EvaluationDataTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetailTemplates_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Assessors",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    EmpCode = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    EvaluationId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assessors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Assessors_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assessors_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assessors_Evaluations_EvaluationId",
                        column: x => x.EvaluationId,
                        principalTable: "Evaluations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Assessors_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EvaluationDatas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    BasePoint = table.Column<int>(nullable: false),
                    Wording = table.Column<string>(nullable: false),
                    EvaluationId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluationDatas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluationDatas_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDatas_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDatas_Evaluations_EvaluationId",
                        column: x => x.EvaluationId,
                        principalTable: "Evaluations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDatas_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Flows",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    EmpCode = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    EvaluationId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flows", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Flows_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Flows_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Flows_Evaluations_EvaluationId",
                        column: x => x.EvaluationId,
                        principalTable: "Evaluations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Flows_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EvaluationDataDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Weigth = table.Column<int>(nullable: false),
                    EvaluationDataDetailId = table.Column<long>(nullable: true),
                    EvaluationDataId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluationDataDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetails_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetails_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetails_EvaluationDataDetails_EvaluationDataDetailId",
                        column: x => x.EvaluationDataDetailId,
                        principalTable: "EvaluationDataDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetails_EvaluationDatas_EvaluationDataId",
                        column: x => x.EvaluationDataId,
                        principalTable: "EvaluationDatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationDataDetails_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_CreateUserId",
                table: "Assessors",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_CustomerId",
                table: "Assessors",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_EvaluationId",
                table: "Assessors",
                column: "EvaluationId");

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_UpdateUserId",
                table: "Assessors",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetails_CreateUserId",
                table: "EvaluationDataDetails",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetails_CustomerId",
                table: "EvaluationDataDetails",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetails_EvaluationDataDetailId",
                table: "EvaluationDataDetails",
                column: "EvaluationDataDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetails_EvaluationDataId",
                table: "EvaluationDataDetails",
                column: "EvaluationDataId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetails_UpdateUserId",
                table: "EvaluationDataDetails",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetailTemplates_CreateUserId",
                table: "EvaluationDataDetailTemplates",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetailTemplates_CustomerId",
                table: "EvaluationDataDetailTemplates",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetailTemplates_EvaluationDataDetailTemplateId",
                table: "EvaluationDataDetailTemplates",
                column: "EvaluationDataDetailTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetailTemplates_EvaluationDataTemplateId",
                table: "EvaluationDataDetailTemplates",
                column: "EvaluationDataTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataDetailTemplates_UpdateUserId",
                table: "EvaluationDataDetailTemplates",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDatas_CreateUserId",
                table: "EvaluationDatas",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDatas_CustomerId",
                table: "EvaluationDatas",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDatas_EvaluationId",
                table: "EvaluationDatas",
                column: "EvaluationId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDatas_UpdateUserId",
                table: "EvaluationDatas",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataTemplates_CreateUserId",
                table: "EvaluationDataTemplates",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataTemplates_CustomerId",
                table: "EvaluationDataTemplates",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationDataTemplates_UpdateUserId",
                table: "EvaluationDataTemplates",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Evaluations_CreateUserId",
                table: "Evaluations",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Evaluations_CustomerId",
                table: "Evaluations",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Evaluations_UpdateUserId",
                table: "Evaluations",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Flows_CreateUserId",
                table: "Flows",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Flows_CustomerId",
                table: "Flows",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Flows_EvaluationId",
                table: "Flows",
                column: "EvaluationId");

            migrationBuilder.CreateIndex(
                name: "IX_Flows_UpdateUserId",
                table: "Flows",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Assessors");

            migrationBuilder.DropTable(
                name: "EvaluationDataDetails");

            migrationBuilder.DropTable(
                name: "EvaluationDataDetailTemplates");

            migrationBuilder.DropTable(
                name: "Flows");

            migrationBuilder.DropTable(
                name: "EvaluationDatas");

            migrationBuilder.DropTable(
                name: "EvaluationDataTemplates");

            migrationBuilder.DropTable(
                name: "Evaluations");
        }
    }
}
