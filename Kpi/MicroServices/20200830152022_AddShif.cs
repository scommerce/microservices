﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class AddShif : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeShif",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true),
                    LeaveName = table.Column<string>(nullable: true),
                    ShifDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeShif", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EmployeeShif_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeShif_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeShif_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EmployeeShif_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeShif_CreateUserId",
                table: "EmployeeShif",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeShif_CustomerId",
                table: "EmployeeShif",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeShif_EmployeeId",
                table: "EmployeeShif",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeShif_UpdateUserId",
                table: "EmployeeShif",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeShif");
        }
    }
}
