﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Send",
                table: "EvaluationEmp",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "EvaluationPoint",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    UserIdId = table.Column<long>(nullable: true),
                    EvaluationId = table.Column<long>(nullable: true),
                    EvaluationDataDetailId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Weigth = table.Column<int>(nullable: false),
                    Point = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EvaluationPoint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_EvaluationDataDetails_EvaluationDataDetailId",
                        column: x => x.EvaluationDataDetailId,
                        principalTable: "EvaluationDataDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_Evaluations_EvaluationId",
                        column: x => x.EvaluationId,
                        principalTable: "Evaluations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EvaluationPoint_BaseUsers_UserIdId",
                        column: x => x.UserIdId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_CreateUserId",
                table: "EvaluationPoint",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_CustomerId",
                table: "EvaluationPoint",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_EvaluationDataDetailId",
                table: "EvaluationPoint",
                column: "EvaluationDataDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_EvaluationId",
                table: "EvaluationPoint",
                column: "EvaluationId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_UpdateUserId",
                table: "EvaluationPoint",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_UserIdId",
                table: "EvaluationPoint",
                column: "UserIdId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EvaluationPoint");

            migrationBuilder.DropColumn(
                name: "Send",
                table: "EvaluationEmp");
        }
    }
}
