﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addCommentSurvey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "SurveySelect",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Comment",
                table: "SurveyDetailChooseTemplate",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Comment",
                table: "SurveyDetailChoose",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "SurveySelect");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "SurveyDetailChooseTemplate");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "SurveyDetailChoose");
        }
    }
}
