﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addSurveyEMp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Survey",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    SurveyName = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    TemplateName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Survey", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Survey_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Survey_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Survey_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MutiSelect = table.Column<bool>(nullable: false),
                    oid = table.Column<long>(nullable: false),
                    SurveyId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyDetails_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetails_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetails_Survey_SurveyId",
                        column: x => x.SurveyId,
                        principalTable: "Survey",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetails_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyEmp",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    SurveyId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyEmp", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyEmp_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyEmp_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyEmp_Survey_SurveyId",
                        column: x => x.SurveyId,
                        principalTable: "Survey",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyEmp_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyDetailChoose",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    SurveyDetailId = table.Column<long>(nullable: true),
                    NextId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyDetailChoose", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChoose_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChoose_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChoose_SurveyDetails_NextId",
                        column: x => x.NextId,
                        principalTable: "SurveyDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChoose_SurveyDetails_SurveyDetailId",
                        column: x => x.SurveyDetailId,
                        principalTable: "SurveyDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChoose_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Survey_CreateUserId",
                table: "Survey",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Survey_CustomerId",
                table: "Survey",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Survey_UpdateUserId",
                table: "Survey",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChoose_CreateUserId",
                table: "SurveyDetailChoose",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChoose_CustomerId",
                table: "SurveyDetailChoose",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChoose_NextId",
                table: "SurveyDetailChoose",
                column: "NextId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChoose_SurveyDetailId",
                table: "SurveyDetailChoose",
                column: "SurveyDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChoose_UpdateUserId",
                table: "SurveyDetailChoose",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetails_CreateUserId",
                table: "SurveyDetails",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetails_CustomerId",
                table: "SurveyDetails",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetails_SurveyId",
                table: "SurveyDetails",
                column: "SurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetails_UpdateUserId",
                table: "SurveyDetails",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmp_CreateUserId",
                table: "SurveyEmp",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmp_EmployeeId",
                table: "SurveyEmp",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmp_SurveyId",
                table: "SurveyEmp",
                column: "SurveyId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyEmp_UpdateUserId",
                table: "SurveyEmp",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SurveyDetailChoose");

            migrationBuilder.DropTable(
                name: "SurveyEmp");

            migrationBuilder.DropTable(
                name: "SurveyDetails");

            migrationBuilder.DropTable(
                name: "Survey");
        }
    }
}
