﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addNextEmp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "NextEmployeeId",
                table: "FeedbackAssert",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_NextEmployeeId",
                table: "FeedbackAssert",
                column: "NextEmployeeId");

            migrationBuilder.AddForeignKey(
                name: "FK_FeedbackAssert_Employees_NextEmployeeId",
                table: "FeedbackAssert",
                column: "NextEmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeedbackAssert_Employees_NextEmployeeId",
                table: "FeedbackAssert");

            migrationBuilder.DropIndex(
                name: "IX_FeedbackAssert_NextEmployeeId",
                table: "FeedbackAssert");

            migrationBuilder.DropColumn(
                name: "NextEmployeeId",
                table: "FeedbackAssert");
        }
    }
}
