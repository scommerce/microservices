﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class AddSessesor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "EmpAssessorId",
                table: "Assessors",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Assessors_EmpAssessorId",
                table: "Assessors",
                column: "EmpAssessorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Assessors_Employees_EmpAssessorId",
                table: "Assessors",
                column: "EmpAssessorId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Assessors_Employees_EmpAssessorId",
                table: "Assessors");

            migrationBuilder.DropIndex(
                name: "IX_Assessors_EmpAssessorId",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "EmpAssessorId",
                table: "Assessors");
        }
    }
}
