﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addEvaluationTypeAndSumWeighInData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvaluationType",
                table: "EvaluationDataDetails");

            migrationBuilder.AddColumn<int>(
                name: "EvaluationType",
                table: "EvaluationDatas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Weigth",
                table: "EvaluationDatas",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvaluationType",
                table: "EvaluationDatas");

            migrationBuilder.DropColumn(
                name: "Weigth",
                table: "EvaluationDatas");

            migrationBuilder.AddColumn<int>(
                name: "EvaluationType",
                table: "EvaluationDataDetails",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
