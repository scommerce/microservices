﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addOwnerPoint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "OwnerId",
                table: "EvaluationPoint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EvaluationPoint_OwnerId",
                table: "EvaluationPoint",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_EvaluationPoint_BaseUsers_OwnerId",
                table: "EvaluationPoint",
                column: "OwnerId",
                principalTable: "BaseUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EvaluationPoint_BaseUsers_OwnerId",
                table: "EvaluationPoint");

            migrationBuilder.DropIndex(
                name: "IX_EvaluationPoint_OwnerId",
                table: "EvaluationPoint");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "EvaluationPoint");
        }
    }
}
