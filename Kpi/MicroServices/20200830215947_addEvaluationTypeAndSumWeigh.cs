﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addEvaluationTypeAndSumWeigh : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EvaluationType",
                table: "EvaluationDataDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Wording",
                table: "EvaluationDataDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvaluationType",
                table: "EvaluationDataDetails");

            migrationBuilder.DropColumn(
                name: "Wording",
                table: "EvaluationDataDetails");
        }
    }
}
