﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class AddLinkSurvey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SurveyDetailId",
                table: "SurveyDetailChooseTemplate",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChooseTemplate_SurveyDetailId",
                table: "SurveyDetailChooseTemplate",
                column: "SurveyDetailId");

            migrationBuilder.AddForeignKey(
                name: "FK_SurveyDetailChooseTemplate_SurveyDetailTemplate_SurveyDetailId",
                table: "SurveyDetailChooseTemplate",
                column: "SurveyDetailId",
                principalTable: "SurveyDetailTemplate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SurveyDetailChooseTemplate_SurveyDetailTemplate_SurveyDetailId",
                table: "SurveyDetailChooseTemplate");

            migrationBuilder.DropIndex(
                name: "IX_SurveyDetailChooseTemplate_SurveyDetailId",
                table: "SurveyDetailChooseTemplate");

            migrationBuilder.DropColumn(
                name: "SurveyDetailId",
                table: "SurveyDetailChooseTemplate");
        }
    }
}
