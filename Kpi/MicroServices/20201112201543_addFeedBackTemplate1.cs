﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addFeedBackTemplate1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FeedBackTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    TemplateName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBackTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplates_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplates_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplates_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeedBackTemplateDetail",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FeedBackTemplateId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBackTemplateDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplateDetail_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplateDetail_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplateDetail_FeedBackTemplates_FeedBackTemplateId",
                        column: x => x.FeedBackTemplateId,
                        principalTable: "FeedBackTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackTemplateDetail_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplateDetail_CreateUserId",
                table: "FeedBackTemplateDetail",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplateDetail_CustomerId",
                table: "FeedBackTemplateDetail",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplateDetail_FeedBackTemplateId",
                table: "FeedBackTemplateDetail",
                column: "FeedBackTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplateDetail_UpdateUserId",
                table: "FeedBackTemplateDetail",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplates_CreateUserId",
                table: "FeedBackTemplates",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplates_CustomerId",
                table: "FeedBackTemplates",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackTemplates_UpdateUserId",
                table: "FeedBackTemplates",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeedBackTemplateDetail");

            migrationBuilder.DropTable(
                name: "FeedBackTemplates");
        }
    }
}
