﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addSurveyChoose : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "finish",
                table: "SurveyEmp",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "finish",
                table: "SurveyEmp");
        }
    }
}
