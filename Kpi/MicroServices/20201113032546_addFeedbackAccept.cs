﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addFeedbackAccept : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Accept",
                table: "FeedbackEmp");

            migrationBuilder.DropColumn(
                name: "ResonReject",
                table: "FeedbackEmp");

            migrationBuilder.CreateTable(
                name: "FeedbackAccept",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    AssertId = table.Column<long>(nullable: true),
                    EmpId = table.Column<long>(nullable: true),
                    Accept = table.Column<bool>(nullable: true),
                    ResonReject = table.Column<string>(nullable: true),
                    FeedBackId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedbackAccept", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_FeedbackAssert_AssertId",
                        column: x => x.AssertId,
                        principalTable: "FeedbackAssert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_FeedbackEmp_EmpId",
                        column: x => x.EmpId,
                        principalTable: "FeedbackEmp",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_FeedBacks_FeedBackId",
                        column: x => x.FeedBackId,
                        principalTable: "FeedBacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAccept_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_AssertId",
                table: "FeedbackAccept",
                column: "AssertId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_CreateUserId",
                table: "FeedbackAccept",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_CustomerId",
                table: "FeedbackAccept",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_EmpId",
                table: "FeedbackAccept",
                column: "EmpId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_FeedBackId",
                table: "FeedbackAccept",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAccept_UpdateUserId",
                table: "FeedbackAccept",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeedbackAccept");

            migrationBuilder.AddColumn<bool>(
                name: "Accept",
                table: "FeedbackEmp",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResonReject",
                table: "FeedbackEmp",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
