﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addOrdering : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeedbackAssert_Employees_NextEmployeeId",
                table: "FeedbackAssert");

            migrationBuilder.AddColumn<int>(
                name: "Ordering",
                table: "FeedbackAssert",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_FeedbackAssert_FeedbackAssert_NextEmployeeId",
                table: "FeedbackAssert",
                column: "NextEmployeeId",
                principalTable: "FeedbackAssert",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FeedbackAssert_FeedbackAssert_NextEmployeeId",
                table: "FeedbackAssert");

            migrationBuilder.DropColumn(
                name: "Ordering",
                table: "FeedbackAssert");

            migrationBuilder.AddForeignKey(
                name: "FK_FeedbackAssert_Employees_NextEmployeeId",
                table: "FeedbackAssert",
                column: "NextEmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
