﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class AddSendDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "SendateTime",
                table: "EvaluationEmp",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SendDate",
                table: "Assessors",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Working",
                table: "Assessors",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SendateTime",
                table: "EvaluationEmp");

            migrationBuilder.DropColumn(
                name: "SendDate",
                table: "Assessors");

            migrationBuilder.DropColumn(
                name: "Working",
                table: "Assessors");
        }
    }
}
