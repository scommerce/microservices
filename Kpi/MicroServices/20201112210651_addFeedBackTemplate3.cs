﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addFeedBackTemplate3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_FeedBacks_FeedBackId",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Employees_FeedBackId",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "FeedBackId",
                table: "Employees");

            migrationBuilder.CreateTable(
                name: "FeedbackAssert",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true),
                    Finish = table.Column<bool>(nullable: false),
                    FeedBackId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedbackAssert", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedbackAssert_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAssert_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAssert_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAssert_FeedBacks_FeedBackId",
                        column: x => x.FeedBackId,
                        principalTable: "FeedBacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackAssert_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_CreateUserId",
                table: "FeedbackAssert",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_CustomerId",
                table: "FeedbackAssert",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_EmployeeId",
                table: "FeedbackAssert",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_FeedBackId",
                table: "FeedbackAssert",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackAssert_UpdateUserId",
                table: "FeedbackAssert",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FeedbackAssert");

            migrationBuilder.AddColumn<long>(
                name: "FeedBackId",
                table: "Employees",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employees_FeedBackId",
                table: "Employees",
                column: "FeedBackId");

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_FeedBacks_FeedBackId",
                table: "Employees",
                column: "FeedBackId",
                principalTable: "FeedBacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
