﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class addFeedBackTemplate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        

            migrationBuilder.AddColumn<long>(
                name: "FeedBackId",
                table: "Employees",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FeedBacks",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    TemplateName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBacks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBacks_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBacks_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBacks_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });


            migrationBuilder.CreateTable(
                name: "FeedBackDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FeedBackId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBackDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBackDetails_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackDetails_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackDetails_FeedBacks_FeedBackId",
                        column: x => x.FeedBackId,
                        principalTable: "FeedBacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackDetails_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FeedbackEmp",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    employeeId = table.Column<long>(nullable: true),
                    Accept = table.Column<bool>(nullable: false),
                    ResonReject = table.Column<string>(nullable: true),
                    FeedBackId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedbackEmp", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedbackEmp_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackEmp_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackEmp_FeedBacks_FeedBackId",
                        column: x => x.FeedBackId,
                        principalTable: "FeedBacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackEmp_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedbackEmp_Employees_employeeId",
                        column: x => x.employeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

     

           

            migrationBuilder.CreateTable(
                name: "FeedBackFromAsseert",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    FeedBackDetailId = table.Column<long>(nullable: true),
                    EmployeeId = table.Column<long>(nullable: true),
                    FeedBack = table.Column<string>(nullable: true),
                    FeedBackId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FeedBackFromAsseert", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FeedBackFromAsseert_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackFromAsseert_FeedbackEmp_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "FeedbackEmp",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackFromAsseert_FeedBackDetails_FeedBackDetailId",
                        column: x => x.FeedBackDetailId,
                        principalTable: "FeedBackDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackFromAsseert_FeedBacks_FeedBackId",
                        column: x => x.FeedBackId,
                        principalTable: "FeedBacks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FeedBackFromAsseert_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

          


            migrationBuilder.CreateIndex(
                name: "IX_Employees_FeedBackId",
                table: "Employees",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackDetails_CreateUserId",
                table: "FeedBackDetails",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackDetails_CustomerId",
                table: "FeedBackDetails",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackDetails_FeedBackId",
                table: "FeedBackDetails",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackDetails_UpdateUserId",
                table: "FeedBackDetails",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackEmp_CreateUserId",
                table: "FeedbackEmp",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackEmp_CustomerId",
                table: "FeedbackEmp",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackEmp_FeedBackId",
                table: "FeedbackEmp",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackEmp_UpdateUserId",
                table: "FeedbackEmp",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedbackEmp_employeeId",
                table: "FeedbackEmp",
                column: "employeeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_CreateUserId",
                table: "FeedBackFromAsseert",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_EmployeeId",
                table: "FeedBackFromAsseert",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_FeedBackDetailId",
                table: "FeedBackFromAsseert",
                column: "FeedBackDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_FeedBackId",
                table: "FeedBackFromAsseert",
                column: "FeedBackId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBackFromAsseert_UpdateUserId",
                table: "FeedBackFromAsseert",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_CreateUserId",
                table: "FeedBacks",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_CustomerId",
                table: "FeedBacks",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FeedBacks_UpdateUserId",
                table: "FeedBacks",
                column: "UpdateUserId");

       

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_FeedBacks_FeedBackId",
                table: "Employees",
                column: "FeedBackId",
                principalTable: "FeedBacks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employees_FeedBacks_FeedBackId",
                table: "Employees");

            migrationBuilder.DropTable(
                name: "FeedBackFromAsseert");


            migrationBuilder.DropTable(
                name: "SurveySelect");

            migrationBuilder.DropTable(
                name: "FeedbackEmp");

            migrationBuilder.DropTable(
                name: "FeedBackDetails");

          

            migrationBuilder.DropTable(
                name: "FeedBacks");


            migrationBuilder.DropIndex(
                name: "IX_Employees_FeedBackId",
                table: "Employees");

    
            migrationBuilder.DropColumn(
                name: "FeedBackId",
                table: "Employees");
        }
    }
}
