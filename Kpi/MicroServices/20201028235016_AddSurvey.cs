﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kpi.MicroServices
{
    public partial class AddSurvey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SurveyTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    SurveryName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyTemplates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyTemplates_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyTemplates_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyTemplates_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyDetailTemplate",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    MutiSelect = table.Column<bool>(nullable: false),
                    SurveyTemplateId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyDetailTemplate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyDetailTemplate_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailTemplate_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailTemplate_SurveyTemplates_SurveyTemplateId",
                        column: x => x.SurveyTemplateId,
                        principalTable: "SurveyTemplates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailTemplate_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SurveyDetailChooseTemplate",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeleteFlag = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    CreateUserId = table.Column<long>(nullable: true),
                    UpdateUserId = table.Column<long>(nullable: true),
                    CustomerId = table.Column<long>(nullable: true),
                    Order = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    NextId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurveyDetailChooseTemplate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChooseTemplate_BaseUsers_CreateUserId",
                        column: x => x.CreateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChooseTemplate_BaseCustomers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "BaseCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChooseTemplate_SurveyDetailTemplate_NextId",
                        column: x => x.NextId,
                        principalTable: "SurveyDetailTemplate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurveyDetailChooseTemplate_BaseUsers_UpdateUserId",
                        column: x => x.UpdateUserId,
                        principalTable: "BaseUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChooseTemplate_CreateUserId",
                table: "SurveyDetailChooseTemplate",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChooseTemplate_CustomerId",
                table: "SurveyDetailChooseTemplate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChooseTemplate_NextId",
                table: "SurveyDetailChooseTemplate",
                column: "NextId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailChooseTemplate_UpdateUserId",
                table: "SurveyDetailChooseTemplate",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailTemplate_CreateUserId",
                table: "SurveyDetailTemplate",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailTemplate_CustomerId",
                table: "SurveyDetailTemplate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailTemplate_SurveyTemplateId",
                table: "SurveyDetailTemplate",
                column: "SurveyTemplateId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyDetailTemplate_UpdateUserId",
                table: "SurveyDetailTemplate",
                column: "UpdateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyTemplates_CreateUserId",
                table: "SurveyTemplates",
                column: "CreateUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyTemplates_CustomerId",
                table: "SurveyTemplates",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_SurveyTemplates_UpdateUserId",
                table: "SurveyTemplates",
                column: "UpdateUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SurveyDetailChooseTemplate");

            migrationBuilder.DropTable(
                name: "SurveyDetailTemplate");

            migrationBuilder.DropTable(
                name: "SurveyTemplates");
        }
    }
}
