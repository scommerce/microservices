﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeImport
    {
        public string Name { get; set; }
        public string EmpCode { get; set; }
    }
}
