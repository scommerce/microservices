﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class FeedbackRequest
    {
        public long AssertId { get; set; }
        public long FeedbackId { get; set; }
        public string reson { get; set; }
    }
}
