﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class FeedbackModel
    {
        public long feedBackId { get; set; }
        public List<FeedbackDetailModel> feedback { get; set; }
    }
}
