﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EvaluationModel
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string No { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Desc { get; set; }
        public bool RequireTimesheet { get; set; }
        public DateTime TimeSheetStart { get; set; }
        public DateTime TimeSheetEnd { get; set; }
        public int BeforeNoti { get; set; }
        public int ExpireNoti { get; set; }
        public List<String> Emp { get; set; }
        public List<String> Assessor { get; set; }
        public List<EvalTemplateModel> Template { get; set; }
    }
}
