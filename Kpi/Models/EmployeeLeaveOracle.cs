﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeLeaveOracle
    {
        public string EMP_CODE { get; set; }
        public int ABSENCE_TYPE_ID { get; set; }
        public string LEAVE_NAME { get; set; }
        public DateTime? LEAVE_START_DATE { get; set; }
        public DateTime? LEAVE_END_DATE { get; set; }
        public double? ABSENCE_DAYS { get; set; }
    }
}
