﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EvaluationDetailData
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public int? Weigth { get; set; }
        public int Point { get; set; }
        public int Order { get; set; }
        public List<EvaluationDetailData> Subname { get; set; }
    }
}
