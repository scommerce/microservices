﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EvaluationDetail
    {
        public long Id { get; set; }
        public List<EvaluationDetailData> Data { get; set; }

    }
}
