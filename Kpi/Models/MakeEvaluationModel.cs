﻿using Kpi.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class MakeEvaluationModel
    {
        public Kpi.Entity.Evaluation Evaluation { get; set; }
        public Employee Employee { get; set; }
    }
}
