﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class FeedbackDetailModel
    {
        public long emp { get; set; }
        public List<FeedbackDetailModel2> feedback { get; set; }
    }
}
