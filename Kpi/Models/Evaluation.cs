﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class Evaluation
    {
        public string Recommend { get; set; }
        public long? EmpCode { get; set; }
        public List<EvaluationDetail> Data { get; set; }
    }
}
