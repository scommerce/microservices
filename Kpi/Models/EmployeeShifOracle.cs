﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeShifOracle
    {
        public string EMP_CODE { get; set; }
        public string LEAVE_NAME { get; set; }
        public DateTime SHIF_DATE { get; set; }
        public double AMOUNT { get; set; }
    }
}
