﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class FeedbackResponseModel
    {
        public long FeedbackId { get; set; }
        public string FeedBackName { get; set; }
        public long AssertId { get; set; }
        public bool Accept { get; set; }

    }
}
