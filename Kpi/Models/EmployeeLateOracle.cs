﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeLateOracle
    {
        public string EMP_CODE { get; set; }
        public string LEAVE_NAME { get; set; }
        public DateTime? DATE_IN { get; set; }
        public DateTime? DATE_OUT { get; set; }
        public string TIME_IN { get; set; }
        public string TIME_OUT { get; set; }
        public double LATE_TIME { get; set; }
    }
}
