﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeFlowImport
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        public string F1 { get; set; }
        public int? W1 { get; set; }

        public string F2 { get; set; }
        public int? W2 { get; set; }

        public string F3 { get; set; }
        public int? W3 { get; set; }

        public string F4 { get; set; }
        public int? W4 { get; set; }

        public string F5 { get; set; }
        public int? W5 { get; set; }

        public string F6 { get; set; }
        public int? W6 { get; set; }
    }
}
