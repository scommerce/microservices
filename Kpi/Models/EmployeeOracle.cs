﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Models
{
    public class EmployeeOracle
    {
        public string EMPLOYEE_NUMBER { get; set; }
        public string TITLE_MEANING { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string POSITION_NAME { get; set; }
        public string ORG_NAME { get; set; }
        public DateTime DATE_OF_BIRTH { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime HIRE_DATE { get; set; }
        public string MANP_NO { get; set; }
        public string GROUP_NAME { get; set; }
        public string USER_NAME { get; set; }
        public int TYPE { get; set; }
        public string EMAIL_ADDRESS_T { get; set; }
    }
}
