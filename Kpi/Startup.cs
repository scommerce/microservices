using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Startup;
using Kpi.DbContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Kpi
{
    public class Startup : Framework.Startup.Startup
    {
        public Startup(IConfiguration configuration) : base(configuration)
        {

        }
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddSingleton<DataContext>();
            services.AddDbContext<DataContext>();
            services.AddTransient<DataContext>();
            services.AddMvc(config =>
            {
                config.Filters.Add(new Filtter(new DataContext()));
            });



        }
        public override void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (IServiceScope serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {

                DataContext context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                System.Collections.Generic.IEnumerable<string> s = context.Database.GetPendingMigrations();
                context.Database.Migrate();
            }
            base.Configure(app, env);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Login}/{action=Index}");
            });
        }

        public override IAuthorizationRequirement GetAuthorization()
        {
            return new LoginRequirement("/Login", new DataContext());
        }
    }
}
