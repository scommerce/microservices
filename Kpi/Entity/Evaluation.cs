﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{

    public class Evaluation:BaseEntityCusCode
    {
        public string Year { get; set; }
        public string No { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        [NotMapped]
        public string StartDateString { get {
                return StartDate.ToString("dd-MM-yyyy");
            } }
        [NotMapped]
        public string EndDateString { get
            {
                return EndDate.ToString("dd-MM-yyyy");
            }
        }
        [NotMapped]
        public bool EmpSend { get; set; }
        public string EvaluationName { get; set; }
        public string EvaluationDetail { get; set; }
        public bool RequestTimeSheet { get; set; }
        public DateTime? TimeSheetStart { get; set; }
        public DateTime? TimeSheetEnd { get; set; }
        public virtual List<Flow> Flows { get; set; }
        public virtual List<Assessor> Assessors { get; set; }
        public int BeforeNoti { get; set; }
        public int ExpireNoti { get; set; }
        public bool AllowNull { get; set; }
        public bool SkipFirst { get; set; }
       
        public string RecommandWording { get; set; }
        [Required]
        public virtual List<EvaluationData> Data { get; set; }
        public virtual List<EvaluationEmp> Emp { get; set; }

    }
}
