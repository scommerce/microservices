﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{

    public class EvaluationDataDetailTemplate : BaseEntityCusCode
    {
        public int  Order { get; set; }
        [Required]
        public string Name { get; set; }
        public string Link { get; set; }

        public int Weigth { get; set; }
        public virtual List<EvaluationDataDetailTemplate> SubDetail { get; set; }
    }
}
