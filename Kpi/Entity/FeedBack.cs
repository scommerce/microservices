﻿using Framework.BaseEntity;
using Framework.BaseExtjs;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    [ExtGrid(title = "Feedback", controller = "https://localhost:5001/api/FeedBackCRUD", cud = ExtGridAttribute.CUD.NotAllow, addWidth = "Ext.getBody().getViewSize().width-220", addHeight = "Ext.getBody().getViewSize().height-200", width = "Ext.getBody().getViewSize().width-220", height = "Ext.getBody().getViewSize().height-50", pageSize = 20, child = "columns", paging = true, filter = true)]
    public class FeedBack:BaseEntityCusCode
    {
        [ExtGridColumn(text = "ชื่อ Feedback", fillter = ExtGridColumn.Fillter.String)]
        public string Name { get; set; }
        [ExtGridColumn(text = "วันที่เริ่มต้น", fillter = ExtGridColumn.Fillter.Date)]
        public DateTime StartDate { get; set; }
        [ExtGridColumn(text = "วันที่สิ้นสุด", fillter = ExtGridColumn.Fillter.Date)]
        public DateTime EndDate { get; set; }
        public virtual List<FeedbackAssert> Asseert { get; set; }
        public virtual List<FeedbackEmp> Feedback { get; set; }
        public string TemplateName { get; set; }
        public virtual List<FeedBackDetail> FeedBackDetails { get; set; }
        public virtual List<FeedBackFromAsseert> FeedBackFromAsseerts { get; set; }
        public virtual List<FeedbackAccept> FeedbackAccept { get; set; }
        [NotMapped]
        [ExtGridColumn(text = "ผู้ประเมิน Feedback ปัจจุบัน", fillter = ExtGridColumn.Fillter.String)]
        public string CurrentUser { get
            {
                if (Asseert == null)
                    return null;
                var emp = Asseert.Where(n => n.Finish == false).OrderBy(n => n.Ordering).FirstOrDefault();
                if (emp == null) 
                    return "ทำครบแล้ว";
                return emp.Employee.Name + " " + emp.Employee.LastName;
            }
        }
        
        [NotMapped]
        [ExtReportColumn(text = "PDF",ReportText ="Download PDF")]
        public string Report { get {
                return "https://kpi.pmcu.co.th/api/FeedBackApi/GetReport?id="+Id.ToString()+"&type=pdf";
                    }
        }
        [NotMapped]
        [ExtReportColumn(text = "Excel",ReportText ="Download Excel")]
        public string Excel { get
            {
                return "https://kpi.pmcu.co.th/api/FeedBackApi/GetExcel?id="+Id.ToString()+"&type=xls";
            }
        }
        [NotMapped]
        [ExLinkConfirmColumn(text = "Reset3", ReportText = "Reset",ConfirmTitle ="แจ้งเตือน",ConfirmMsg ="กรุณายืนยันเพื่อรีเซ็ต")]
        public string Reset
        {
            get
            {
                return "https://kpi.pmcu.co.th/api/FeedBackApi/GetReport?id=" + Id.ToString() + "&type=pdf";
            }
        }
        [NotMapped]
        public bool EmpSent { get; set; }

    }
}
