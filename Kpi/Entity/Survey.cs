﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Survey:BaseEntityCusCode
    {
        public string SurveyName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TemplateName { get; set; }
        public virtual List<SurveyDetails> SurveyDetails { get; set; }
        public virtual List<SurveyEmp> Employees { get; set; }

        [NotMapped]
        public bool EmpSend { get; set; }
    }
}
