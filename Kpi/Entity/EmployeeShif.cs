﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EmployeeShif : BaseEntityCusCode
    {
        public virtual Employee Employee { get; set; }
        public string LeaveName { get; set; }
        public DateTime ShifDate { get; set; }
        public double Amount { get; set; }
    }
}
