﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SubPoint
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
}
