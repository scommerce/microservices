﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Department:BaseEntityCusCode
    {
        public string Name { get; set; }
    }
}
