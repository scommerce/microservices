﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EvaluationData : BaseEntityCusCode
    {
        public int Order { get; set; }

        [Required]
        public string Name { get; set; }
        public int BasePoint { get; set; }
        [Required]
        public string Wording { get; set; }
        public EvaluationType EvaluationType { get; set; }
        public int Weigth { get; set; }
        [NotMapped]
        public int SumWeigth
        {
            get
            {
               
                return Details.Sum(n => n.Weigth);
            }
        }
        public virtual Evaluation Evaluation { get; set; }
        public virtual List<EvaluationDataDetail> Details { get; set; }
        public virtual List<EvaluationPoint> EvaluationPoint { get; set; }
        [NotMapped]
        public bool HavePoint { get
            {
                if (EvaluationType == EvaluationType.Free)
                    return EvaluationPoint.Where(n=>n.DeleteFlag==false).Count() > 0 ? true : false;
                return Details.Where(n=>n.HavePoint).Count()>0 ?true: false;
            }
        }

        [NotMapped]
        public double SumPoint
        {
            get
            {
                if (EvaluationType == EvaluationType.Free)
                    return EvaluationPoint.Where(n => n.DeleteFlag == false).Sum(n => n.SumPoint);
                return Details.Sum(n => n.SumPoint);
            }
        }
    }
}
