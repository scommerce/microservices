﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Prefix:BaseEntityCusCode
    {
        public String Name { get; set; }
    }
}
