﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class FeedbackAccept : BaseEntityCusCode
    {
        public virtual FeedbackAssert Assert { get; set; }
        public virtual FeedbackEmp Emp { get; set; }
        public bool? Accept { get; set; }
        public string ResonReject
        {
            get; set;
        }
    }
}
