﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EmployeeLeave: BaseEntityCusCode
    {
        public virtual Employee Employee { get; set; }
        public int AbsenceTypeId { get; set; }
        public string LeaveName { get; set; }
        public DateTime? LeaveStart { get; set; }
        public DateTime? LeaveEnd { get; set; }
        public double? AbsenceDay { get; set; }

    }
    
}
