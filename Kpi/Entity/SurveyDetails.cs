﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveyDetails:BaseEntityCusCode
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public bool MutiSelect { get; set; }
        public virtual List<SurveyDetailChoose> SurveyDetailChoose { get; set; }
        public long oid { get; set; }
        public virtual Survey Survey { get; set; }

       
    }
}
