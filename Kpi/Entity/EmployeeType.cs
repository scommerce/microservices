﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EmployeeType:BaseEntityCusCode
    {
        public string Name { get; set; }
    }
}
