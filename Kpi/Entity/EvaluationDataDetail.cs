﻿using Framework.BaseAttrib;
using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{

    public class EvaluationDataDetail : BaseEntityCusCode
    {
        public int  Order { get; set; }
        [Required]
        public string Name { get; set; }

        public int Weigth { get; set; }
        [NotMapped]
        public int SumWeigth { get
            {
                return SubDetail.Where(n=>n.DeleteFlag==false).Sum(n => n.Weigth);
            }
        }
        public int FileId { get; set; }

        public string Wording { get; set; }
        public virtual List<EvaluationDataDetail> SubDetail { get; set; }
        public virtual List<EvaluationPoint> EvaluationPoint { get; set; }
        [NotMapped]
        public double SumPoint
        {
            get
            {
                return EvaluationPoint.Where(n=>n.DeleteFlag==false).Sum(n => n.SumPoint);
            }
        }
        public EvaluationDataDetail()
        {
            EvaluationPoint = new List<EvaluationPoint>();

        }
        [NotMapped]
        public bool HavePoint
        {
            get
            {
               
                    return EvaluationPoint.Where(n => n.DeleteFlag == false).Count() > 0 ? true : false;
         
            }
        }

    }
}
