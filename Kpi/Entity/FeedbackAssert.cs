﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class FeedbackAssert:BaseEntityCusCode
    {
        public virtual Employee Employee { get; set; }
        public bool Finish { get; set; }
        public int Ordering { get; set; }
        public bool Working { get; set; }
        public virtual FeedbackAssert NextEmployee { get; set; }
        [ForeignKey("NextEmployeeId")]
        public virtual FeedbackAssert Parent { get; set; }

    }
}
