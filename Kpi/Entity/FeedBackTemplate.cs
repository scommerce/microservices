﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class FeedBackTemplate:BaseEntityCusCode
    {
        public string TemplateName { get; set; }
        public virtual List<FeedBackTemplateDetail> FeedBackTemplateDetails { get; set; }
    }
}
