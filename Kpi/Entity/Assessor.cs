﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Assessor : BaseEntityCusCode
    {
        public virtual Employee Emp { get; set; }
        public virtual Employee EmpAssessor { get; set; }
        public int Order { get; set; }
        public int Weigth { get; set; }
        public bool Send { get; set; }
        public bool Working { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? RejectDate { get; set; }
        public string RejectComment { get; set; }
        public virtual Evaluation Evaluation { get; set; }
    }
}
