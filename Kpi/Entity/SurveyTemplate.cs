﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveyTemplate: BaseEntityCusCode
    {
        public string SurveryName { get; set; }
        public virtual List<SurveyDetailTemplate> SurveyDetailTemplate { get; set; }
    }
}
