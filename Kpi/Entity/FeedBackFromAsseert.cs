﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class FeedBackFromAsseert:BaseEntity
    {
        public virtual FeedBackDetail FeedBackDetail { get; set; }
        public virtual FeedbackEmp Employee { get; set; }
        public virtual FeedbackAssert Assert { get; set; }
        public string FeedBack { get; set; }

    }
}
