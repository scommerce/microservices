﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EmployeeLate : BaseEntityCusCode
    {
        public virtual Employee Employee { get; set; }
        public string LeaveName { get; set; }
        public DateTime? DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public double LateTime { get; set; }
    }
}
