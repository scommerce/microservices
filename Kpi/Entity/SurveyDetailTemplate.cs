﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveyDetailTemplate: BaseEntityCusCode
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public bool MutiSelect { get; set; }
        public virtual List<SurveyDetailChooseTemplate> SurveyDetailChooseTemplates { get; set; }
        public long oid { get; set; }
    }
}
