﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Flow : BaseEntityCusCode
    {
        public string UserName { get; set; }
        public long UserId { get; set; }
        public string EmpCode { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
    }
}
