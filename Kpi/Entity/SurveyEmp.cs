﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveyEmp:BaseEntity
    {
        public virtual Survey Survey { get; set; }
        public virtual Employee Employee { get; set; }
        public bool finish { get; set; }
    }
}
