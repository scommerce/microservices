﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveySelect:BaseEntity
    {
        public virtual BaseUser User { get; set; }
        public virtual Survey Survey { get; set; }
        public virtual SurveyDetails SurveyDetails { get; set; }
        public virtual SurveyDetailChoose Choose { get; set; }
        public String Comment { get; set; }
    }
}
