﻿using Framework.BaseAttrib;
using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EvaluationDataTemplate : BaseEntityCusCode
    {
        public int Order { get; set; }

        [Required]
        public string Name { get; set; }
        public int BasePoint { get; set; }
        [Required]
        public string Wording { get; set; }
        public virtual List<EvaluationDataDetailTemplate> Details { get; set; }
        public EvaluationType EvaluationType { get; set; }
      
    }
}
