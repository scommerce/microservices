﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EvaluationPoint:BaseEntityCusCode
    {
        public virtual BaseUser UserId { get; set; }
        public virtual BaseUser Owner { get; set; }
        public virtual Evaluation Evaluation { get; set; }
        public virtual EvaluationDataDetail EvaluationDataDetail { get; set; }
        public virtual EvaluationData EvaluationData { get; set; }
        public string Name { get; set; }
        public int Weigth { get; set; }
        public int Point { get; set; }
        public virtual List<SubPoint> SubPoint { get; set; }
        public int Order { get; set; }
        [NotMapped]
        public double SumPoint { get
            {
                if (DeleteFlag == true)
                {
                    return 0;
                }
                if(EvaluationData==null)
                    return ((EvaluationDataDetail.Weigth * Point) / 5.0);
                else
                    return (Weigth * Point) / 5.0;
            }
        }
        
    }
}
