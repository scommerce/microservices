﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class EvaluationEmp:BaseEntity
    {
        public virtual Evaluation Evaluation { get; set; }
        public virtual Employee Employee { get; set; }
        public bool Send { get; set; }
        public string Recommend { get; set; }
        public DateTime? SendateTime { get; set; }
    }
}
