﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class Employee:BaseEntityCusCode
    {
        public virtual BaseUser User { get; set; }
        public string EmpCode { get; set; }
        public virtual Prefix Prefix { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public virtual Position Position { get; set; }
        public virtual Department Department { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? HireDate { get; set; }
        public string No { get; set; }
        public virtual EmployeeType EmployeeType { get; set; }
        public long Level { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public virtual List<EmployeeLeave> EmployeeLeaves { get; set; }
        public virtual List<EmployeeLate> EmployeeLates { get; set; }
        public virtual List<EmployeeShif> EmployeeShifs { get; set; }
        public bool ChangePassword { get; set; }
        
    }
}
