﻿using Framework.BaseEntity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{
    public class SurveyDetailChooseTemplate:BaseEntityCusCode
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        [ForeignKey("SurveyDetailId")]
        public virtual SurveyDetailTemplate SurveyDetail { get; set; }
        [ForeignKey("NextId")]
        public virtual SurveyDetailTemplate Next { get; set; }
        public bool Comment { get; set; }
    }
}
