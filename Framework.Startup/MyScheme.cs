﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Startup
{
    public class MyScheme : IAuthenticationHandler
    {
        public Task<AuthenticateResult> AuthenticateAsync()
        {
            return null;
        }

        public Task ChallengeAsync(AuthenticationProperties properties)
        {

            return Task.CompletedTask;
        }

        public Task ForbidAsync(AuthenticationProperties properties)
        {
            return Task.CompletedTask;
        }

    
        public Task InitializeAsync(AuthenticationScheme scheme, HttpContext context)
        {
            if (context.Request.ContentType == "application/json")
            {  context.Response.Clear();
                context.Response.StatusCode = 401;

                context.Response.ContentType = "application/json";
           
                    

                    string response = JsonConvert.SerializeObject(new { message="ไม่ได้เข้าสู่ระบบ" }, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });




                    context.Response.WriteAsync(response);


                
            }
            return Task.CompletedTask;

        }

    }
}
