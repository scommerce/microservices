﻿using Framework.BaseAttrib;
using Framework.BaseContext;
using Framework.BaseEntity;
using Framework.BaseFramework;
using Framework.BaseInterface;
using Framework.BaseModel;
using Framework.Startup.BaseAttrib;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace Framework.Startup
{
    public class Filtter : IActionFilter
    {
        BaseDbContext dataContext;
        string lang = "th";
        public  void OnActionExecuted(ActionExecutedContext context)
        {
            var hl = context.HttpContext.Request.Headers["lang"].ToString();
            if (hl != null && hl != "")
                lang = hl;
            if (context.Result != null)
            {
                if (!(context.Result is RedirectResult))
                {
                    var langError = dataContext.BaseLanguage.Where(n => n.DeleteFlag == false && n.Error == true);
                    if (context.Result is ViewResult r)
                    {
                        var micro = dataContext.BaseMicroServices.ToList();
                        r.ViewData["MicroService"] = micro.ToDictionary(n=>n.Name,n=>n.ServiceIp);
                        r.ViewData["CusCode"] = Environment.GetEnvironmentVariable("CusCode");
                    }
                    else if (context.Result is OkResult o)
                    {
                        context.Result = new ObjectResult(new
                        {
                            success = true,
                            data = "Success"
                        });
                    }
                    else if (context.Result is OkObjectResult oo)
                    {
                        context.Result = new ObjectResult(new
                        {
                            success = true,
                            data = "Success"
                        });
                    }
                    else if (context.Exception is Exception e)
                    {
                        context.HttpContext.Response.Clear();
                        context.HttpContext.Response.StatusCode = 500;
                        context.HttpContext.Response.ContentType = "application/json";

                        string mes = e.Message;
                        var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);

                        if (l.FirstOrDefault() != null)
                            mes = l.FirstOrDefault().ToText;

                        context.Result = new ObjectResult(new
                        {
                            message = mes
                        });
                        context.Exception = null;
                        //  context.Result
                        //context.HttpContext.Response.Clear();
                        //context.HttpContext.Response.StatusCode = 500;
                        //context.HttpContext.Response.ContentType = "application/json";
                        ////string errorResponse = JsonConvert.SerializeObject(new
                        ////{
                        ////    message = e.Message
                        ////}, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                        // context.HttpContext.Response.WriteAsync("{\"message\":\"" + e.Message + "\"}");

                    }
                    else if (context.Result is BadRequestResult cb)
                    {
                        context.HttpContext.Response.StatusCode = 400;
                        string mes = cb.ToString();
                        var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                        if (l.FirstOrDefault() != null)
                            mes = l.FirstOrDefault().ToText;
                        context.Result = new ObjectResult(new
                        {
                            message = mes
                        });
                        context.Exception = null;
                    }
                    else if (context.Result is BadRequestObjectResult cbo)
                    {
                        context.HttpContext.Response.StatusCode = 400;
                        string mes = cbo.Value.ToString();
                        var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                        if (l.FirstOrDefault() != null)
                            mes = l.FirstOrDefault().ToText;
                        context.Result = new ObjectResult(new
                        {
                            message = mes
                        });
                        context.Exception = null;
                    }
                    else if (context.Result is ObjectResult content)
                    {

                        //var data = new
                        //{
                        //    Status = 200,
                        //    Value = context.Result
                        //};
                        if (content.Value is IBaseReturnDatatable data)
                        {


                            List<Object> ret = new List<object>();
                            BaseReturnApi baseReturnApi = new BaseReturnApi(dataContext);
                            var setting = baseReturnApi.GetBaseReturnApis(context.ActionDescriptor.AttributeRouteInfo.Template).ToList();
                            if (setting.Count() > 0)
                            {
                                foreach (var d in data.GetData())
                                {


                                    ret.Add(SetModel(setting, d));

                                }

                            }
                            data.SetData(ret);
                        }
                        else if (content.Value is ProblemDetails p)
                        {
                            string mes = p.Detail;
                            var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                            if (l.FirstOrDefault() != null)
                                mes = l.FirstOrDefault().ToText;
                            content.Value = new
                            {
                                message = mes
                            };
                        }
                        else if (content.Value is Exception ex)
                        {

                            string mes = ex.Message;
                            var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                            if (l.FirstOrDefault() != null)
                                mes = l.FirstOrDefault().ToText;
                            content.Value = new
                            {
                                message = mes
                            };
                        }
                        else if (content.Value is BadRequestResult br)
                        {

                            string mes = br.ToString();
                            var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                            if (l.FirstOrDefault() != null)
                                mes = l.FirstOrDefault().ToText;
                            content.Value = new
                            {
                                message = mes
                            };
                        }
                        else if (content.Value is BadRequestObjectResult bro)
                        {

                            string mes = bro.ToString();
                            var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == mes);
                            if (l.FirstOrDefault() != null)
                                mes = l.FirstOrDefault().ToText;
                            content.Value = new
                            {
                                message = mes
                            };
                        }
                        else if (content.Value is IList collection)
                        {


                            IList ret = new List<object>();
                            var m = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(NotDataMapAttribute), false).FirstOrDefault();
                            if (m != null)
                            {
                                ret = collection;

                            }
                            else
                            {
                                BaseReturnApi baseReturnApi = new BaseReturnApi(dataContext);
                                var setting = baseReturnApi.GetBaseReturnApis(context.ActionDescriptor.AttributeRouteInfo.Template).ToList();
                                if (setting.Count(n => n.DeleteFlag == false) > 0)
                                {
                                    foreach (var d in collection)
                                    {


                                        ret.Add(SetModel(setting, d));

                                    }
                                }
                            }
                            content.Value = new
                            {
                                success =true,
                                count = ret.Count,
                                recordsTotal = ret.Count,
                                recordsFiltered = ret.Count,
                                data = ret
                            };
                        }
                        else if(content.Value is string)
                        {

                        }
                        else
                        {
                            List<Object> ret = new List<object>();
                            var m = ((ControllerActionDescriptor)context.ActionDescriptor).MethodInfo.GetCustomAttributes(typeof(NotDataMapAttribute), false).FirstOrDefault();
                            if (m != null)
                            {
                                ret.Add(content.Value);

                            }
                            else
                            {

                                BaseReturnApi baseReturnApi = new BaseReturnApi(dataContext);
                                if (content.Value != null)
                                {
                                    var setting = baseReturnApi.GetBaseReturnApis(context.ActionDescriptor.AttributeRouteInfo.Template).ToList();
                                    if (setting.Count(n => n.DeleteFlag == false) > 0)
                                    {


                                        ret.Add(SetModel(setting, content.Value));

                                    }
                                }


                            }
                            content.Value = new
                            {
                                success = true,
                                count = ret.Count,
                                recordsTotal = ret.Count,
                                recordsFiltered = ret.Count,
                                data = ret.FirstOrDefault()
                            };
                        }


                        // content = new ObjectResult(d);
                    }
                }
            }
        
        
        }
        public ExpandoObject SetModel(List<BaseApiReturn> setting, object d)
        {
            ExpandoObject v = new ExpandoObject();
          
            foreach (var s in setting)
            {
                if (s.DeleteFlag == false && d!=null) {
                    object vv=null;
                    if (s.BaseLanguage != null)
                    {
                        try
                        {
                            var t = (d is ExpandoObject eo) ? eo.FirstOrDefault(x => x.Key == s.PropertyName).Value : d.GetType().GetProperty(s.PropertyName).GetValue(d);
                            var l = s.BaseLanguage.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == t.ToString());
                            if (l.FirstOrDefault() != null)
                            {
                                vv = l.FirstOrDefault().ToText;
                            }
                            else
                            {
                                vv = t;
                            }
                        }catch(Exception ex)
                        {

                        }
                    }
                    else
                    {
                        vv = d.GetType().GetProperty(s.PropertyName).GetValue(d);
                    }
                    if (vv is ICollection lvv)
                    {
                        List<ExpandoObject> le = new List<ExpandoObject>();
                        foreach (var v1 in lvv)
                        {
                            le.Add(SetModel(s.SubClass, v1));
                        }
                        v.TryAdd(s.PropertyNameNew, le);
                    }
                    else
                    {

                        if (s.SubClass.Count(n=>n.DeleteFlag==false) > 0)
                        {
                            v.TryAdd(s.PropertyNameNew, SetModel(s.SubClass, vv));
                        }
                        else
                        {
                            if (d == null)
                            {
                                v.TryAdd(s.PropertyNameNew, null);
                            }
                            else
                            {
                                v.TryAdd(s.PropertyNameNew, vv);
                            }
                        }
                    }
                }
            }
            return v;
        }
        public Filtter(BaseDbContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async void OnActionExecuting(ActionExecutingContext context)
        {
            var hl = context.HttpContext.Request.Headers["lang"].ToString();
            if (hl != null && hl != "")
                lang = hl;
            if (!(context.Controller is Controller))
            {

                if (!context.ModelState.IsValid)
                {
                    var langError = dataContext.BaseLanguage.Where(n => n.DeleteFlag == false && n.Error == true);


                    context.HttpContext.Response.Clear();
                    context.HttpContext.Response.StatusCode = 400;
                    context.HttpContext.Response.ContentType = "application/json";
                    string message = null;
                    foreach (var m in context.ModelState.Values)
                    {
                        foreach (var e in m.Errors)
                        {
                            var l = langError.Where(n => n.DeleteFlag == false && n.Lang == lang && n.OrigianlText == e.ErrorMessage);
                            string mes = e.ErrorMessage;
                            if (l.FirstOrDefault() != null)
                                mes = l.FirstOrDefault().ToText;

                            if (message != null)
                                message = message + "\n";
                            message = message + mes;
                        }
                    }

                    context.Result = new ObjectResult(new
                    {
                        message = message
                    });
                    //context.Exception = null;

                }
            }

        }
    }
}
