﻿using System;
using System.Collections.Generic;
using Framework.BaseContext;
using Framework.BaseEntity;
using Framework.BaseModel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;
using AutoMapper;
using System.Dynamic;
using Framework.BaseAttrib;

namespace Framework.Startup
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BaseCRUD<Entity, BaseEntityModel, baseDbContext>:BaseWebController<baseDbContext>  where baseDbContext : BaseDbContext, new()  where Entity : Framework.BaseEntity.BaseEntityCusCode, new()
        where BaseEntityModel : Framework.BaseEntity.BaseEntityModel,new()
    {

        public Dictionary<String, String> FieldMap = new Dictionary<string, string>();
        public virtual void BeforeAdd(Entity entity)
        {

        }
        public virtual void BeforeUpdate(Entity entity)
        {

        }
        public virtual void AfterAdd(Entity entity)
        {

        }
        public virtual void AfterUpdate(Entity entity)
        {

        }
        public virtual void BeforeDelete(Entity entity)
        {

        }
        public virtual void AfterDelete(Entity entity)
        {

        }

        public ExpandoObject GetUI(object UiClass, string theme = "default")
        {
            var c = UiClass.GetType();

            ExpandoObject extJs = null;
            foreach (var a in c.GetCustomAttributes(true).Where(n => n is ExtAttribute).ToList())
            {

                if (a is ExtAttribute extAttr)
                {
              
                        if (extAttr.theme == theme || (extAttr.theme == "default" && extJs == null))
                            extJs = extAttr.SetObject(theme, extJs, c, UiClass);
                    
                }
            }

            return extJs;
        }

        [HttpGet]
        [Framework.BaseAttrib.NotDataMap]
        public IActionResult GetMainUI(string theme = "default")
        {
            BaseEntityModel user = new BaseEntityModel();

            var a = GetUI(user, theme);

            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }
        [HttpGet]
        [Framework.BaseAttrib.NotDataMap]
        public IActionResult GetAddUI(string theme = "default-add")
        {
            BaseEntityModel user = new BaseEntityModel();

            var a = GetUI(user, theme);

            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }
        [HttpGet]
        [Framework.BaseAttrib.NotDataMap]
        public IActionResult GetUpdateUI(string theme = "default-update")
        {
            BaseEntityModel user = new BaseEntityModel();

            var a = GetUI(user, theme);

            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }
        [HttpGet]
        [Framework.BaseAttrib.NotDataMap]
        public IActionResult GetViewUI(string theme = "default-view")
        {
            BaseEntityModel user = new BaseEntityModel();

            var a = GetUI(user, theme);

            string outputJson = JsonConvert.SerializeObject(a);

            return new JavaScriptResult(outputJson);
        }
        [HttpGet]
        public async System.Threading.Tasks.Task<IActionResult> GetAsync(string ApiKey, int page = 0, int start = 0, int limit = 0, string sort = null, string filter = null, string theme = "default")
        {
            if (ApiKey == null)
                return Problem("Please Login");

            var a = await GetRepo().GetUserFromApiKey(ApiKey, "th");
            if (a == null)
                return Problem("Please Login");

            BaseDataTable baseTable = new BaseDataTable();
            baseTable.Start = start;
            baseTable.Length = limit;
            if (sort != null)
            {
                var res = JsonConvert.DeserializeObject<List<BaseSortDatatable>>(sort);
                foreach(var f in FieldMap)
                {
                    var r = res.Where(n => n.property == f.Key).FirstOrDefault();
                    if (r != null)
                    {
                        r.property = f.Value;
                    }
                }
                baseTable.Sort = res;
            }
            if (filter != null)
            {
                var res = JsonConvert.DeserializeObject<List<BaseFilterDatatable>>(filter);
                foreach (var f in FieldMap)
                {
                    var r = res.Where(n => n.property == f.Key).FirstOrDefault();
                    if (r != null)
                    {
                        r.property = f.Value;
                    }
                }
                baseTable.Filter = res;
            }

            var v1 = GetRepo().GetDataAll<Entity>(n => n.Customer == a.BaseCustomer && n.DeleteFlag == false).ToDataTable(baseTable);

            return new ObjectResult(v1);
        }
        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> UpdateAsync([FromQuery]string ApiKey, BaseEntityModel entity)
        {
          
            if (ApiKey == null)
                return Problem("Please Login");

            var a = await GetRepo().GetUserFromApiKey(ApiKey, "th");
            if (a == null)
                return Problem("Please Login");
            var dbEntity = GetRepo().GetData<Entity>(n => n.Id == entity.GetId());
            if (dbEntity == null)
                return Problem("Data Not Found");
            var config = new MapperConfiguration(cfg => cfg.CreateMap<BaseEntityModel,Entity >().ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null)));
            var mapper = config.CreateMapper();
            mapper.Map(entity, dbEntity);
            dbEntity.UpdateDate = DateTime.Now;
            dbEntity.UpdateUser = a.BaseUser;
            BeforeUpdate(dbEntity);
            GetRepo().SaveChange();
            AfterUpdate(dbEntity);
            return Ok();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> DeleteAsync([FromQuery] string ApiKey, BaseEntityModel entity)
        {
            
            if (ApiKey == null)
                return Problem("Please Login");

            var a = await GetRepo().GetUserFromApiKey(ApiKey, "th");
            if (a == null)
                return Problem("Please Login");
            var dbEntity = GetRepo().GetData<Entity>(n => n.Id == entity.GetId());
            if (dbEntity == null)
                return Problem("Data Not Found");
            var config = new MapperConfiguration(cfg => cfg.CreateMap<BaseEntityModel, Entity>().ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null)));
            dbEntity.DeleteFlag = true;
            dbEntity.UpdateDate = DateTime.Now;
            dbEntity.UpdateUser = a.BaseUser;
            BeforeDelete(dbEntity);
            GetRepo().SaveChange();
            AfterDelete(dbEntity);
            return Ok();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> AddAsync([FromQuery] string ApiKey, BaseEntityModel entity)
        {
           
            if (ApiKey == null)
                return Problem("Please Login");

            var a = await GetRepo().GetUserFromApiKey(ApiKey, "th");
            if (a == null)
                return Problem("Please Login");
            var config = new MapperConfiguration(cfg => cfg.CreateMap<BaseEntityModel, Entity>().ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null)));
            var mapper = config.CreateMapper();
            var dbEntity = new Entity();
            mapper.Map(entity, dbEntity);
            dbEntity.CreateDate = DateTime.Now;
            dbEntity.CreateUser = a.BaseUser;

            BeforeAdd(dbEntity);
            GetRepo().Add(dbEntity);
            GetRepo().SaveChange();
            AfterAdd(dbEntity);
            return Ok();
        }
    }
}
