using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Framework.Startup
{
    public abstract class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public abstract IAuthorizationRequirement GetAuthorization();
        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {


            services.AddHttpContextAccessor();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IAuthorizationHandler, LoginAuthorization>();

            if (GetAuthorization() != null)
            {
                services.AddAuthorization(authConfig =>
                {
                    authConfig.AddPolicy("Login",
                      policyBuilder => policyBuilder
                          .Requirements.Add(GetAuthorization()));
                });
            }

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Authentication";
                options.AddScheme<MyScheme>("Authentication", "Authentication");
            });


            services.AddMvc(config =>
            {



            }).SetCompatibilityVersion(CompatibilityVersion.Latest).ConfigureApiBehaviorOptions(options =>
            {
                //options.InvalidModelStateResponseFactory = context =>
                //{

                //    //   var data = HttpServices.GetWealthHttpResponse<string>(false, "Bad Request - Model Is Invalid", 400);
                //    // return new BadRequestObjectResult(data);
                //    //ErrorController errorController = new ErrorController();
                //    //return errorController.Error("../Error/Error400");
                //   // return new RedirectResult("/Error/400");
                //    // return null;
                //};
                // options.SuppressConsumesConstraintForFormFileParameters = true;
                // options.SuppressInferBindingSourcesForParameters = true;
                options.SuppressModelStateInvalidFilter = true;
                //options.SuppressMapClientErrors = true;
            })
        .AddNewtonsoftJson(options =>
        {
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

        }
        );


            // services.AddControllers();
            services.AddControllersWithViews().AddControllersAsServices().AddRazorRuntimeCompilation();

            //services.AddResponseCaching();

            services.AddCors(options => options.AddPolicy("ApiCorsPolicy", build =>
            {
                build.SetIsOriginAllowed((host) => true)
                     // .WithOrigins(new string[] { "gitlab.o-cas.com" })
                     .AllowAnyHeader()
                     .AllowCredentials()
                     .SetPreflightMaxAge(TimeSpan.FromSeconds(3600))

                     .AllowAnyMethod().Build();
        }));
            // ... other code is omitted for the brevity

            services.AddDistributedMemoryCache();

            //services.AddSession(options =>
            //{
            //    options.IdleTimeout = TimeSpan.FromSeconds(10);
            //    options.Cookie.HttpOnly = true;
            //    options.Cookie.IsEssential = true;
            //});

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false; // Default is true, make it false
                options.MinimumSameSitePolicy = SameSiteMode.Lax;


            });

            services.ConfigureApplicationCookie(options => {
                options.Cookie.Name = "SCom";
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.ExpireTimeSpan = TimeSpan.FromHours(1);
                options.SlidingExpiration = false;

            });
          
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(1);//You can set Time  
                //options.Cookie.HttpOnly = true;
                options.Cookie.Name = "SCom";
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.Lax;
                options.Cookie.IsEssential = true;
                options.IOTimeout = TimeSpan.FromHours(1);
                

            });
            services.AddDataProtection().PersistKeysToFileSystem(new DirectoryInfo(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + "DataProtection"));



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
               // app.UseHsts();
            }
            // app.UseResponseCaching();

            //app.Use(async (context, next) =>
            //{
            //    context.Response.GetTypedHeaders().CacheControl =
            //        new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
            //        {
            //            Public = true,
            //            MaxAge = TimeSpan.FromSeconds(10)
            //        };
            //    context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
            //        new string[] { "Accept-Encoding" };

            //    await next();
            //});
       
            
        //   app.UseHttpsRedirection();
        app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();
            app.UseAuthorization();

            app.UseCors("ApiCorsPolicy");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
