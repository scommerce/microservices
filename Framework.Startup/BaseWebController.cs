﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;
using Framework.BaseContext;
using Framework.BaseFramework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;

namespace Framework.Startup
{
    public class BaseWebController<baseDbContext> : Controller where baseDbContext : BaseDbContext, new()
    {
        BaseDbContext dataContext;
        BaseRepository baseRepository;
        public IWebHostEnvironment _env;
        public BaseDbContext GetContext()
        {
            if (dataContext == null)
                dataContext = new baseDbContext();


            return dataContext;
        }
        public string GetCusCode() { 
            
                var cus = Environment.GetEnvironmentVariable("CusCode");
                return cus;
            
        }

        public BaseRepository GetRepo()
        {
            if (baseRepository == null)
                baseRepository = new BaseRepository(GetContext(), HttpContext);

            return baseRepository;
        }



        public IQueryable<TEntity> GetAll<TEntity>(Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {


                return GetRepo().GetDataAll<TEntity>(where);
            }
        }
        public string GetHeaderLang()
        {
            return HttpContext.Request.Headers["lang"].ToString();
        }
         
        public IActionResult GetHtml(string Name)
        {
            //return _env + "/html/"+Name+".html"; 
           
           // Response("Content-Disposition", new System.Net.Mime.ContentDisposition { Inline = true, FileName = "index.htm" }.ToString());
            return File(_env + "/html/" + Name + ".html", "text/plain");
        }

        public void LoginSession(String value)
        {
             HttpContext.Session.SetString("ApiKey", value);
        }
        public string GetApiKey()
        {
            return HttpContext.Session.GetString("ApiKey");
        }
        public void LogoutSession()
        {
            HttpContext.Session.Remove("ApiKey");
        }
    }
}
