﻿using Castle.DynamicProxy;
using Framework.BaseContext;
using Framework.BaseFramework;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Startup
{

    public class LoginRequirement : IAuthorizationRequirement
    {
        public LoginRequirement(string RedirectPage, BaseDbContext DataContext)
        {
            this.RedirectPage = RedirectPage;
            this.DataContext = DataContext;
        }
        public string RedirectPage { get; set; }
        public BaseDbContext DataContext { get; set; }
       
    }

    public class LoginAuthorization : AuthorizationHandler<LoginRequirement>
    {
     
        protected readonly IHttpContextAccessor _httpContextAccessor;


        public LoginAuthorization(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        protected override  Task HandleRequirementAsync(AuthorizationHandlerContext context, LoginRequirement requirement)
        {
            bool success = Login(context, requirement).Result;

            if (!success)
            {
                if (requirement.RedirectPage != null)
                {
                    _httpContextAccessor.HttpContext.Response.Redirect(requirement.RedirectPage);
                    //context.Succeed(requirement);
                    //return Task.CompletedTask;
                }
                else
                {
                    context.Fail();
                }
            }
            else
            {
                context.Succeed(requirement);
            }


            return Task.CompletedTask;

        }
        public async Task<bool> Login(AuthorizationHandlerContext context, LoginRequirement requirement)
        {
            String lang = "th";
            var hl = _httpContextAccessor.HttpContext.Request.Headers["lang"].FirstOrDefault();
            if (hl != null && hl != "")
                lang = hl;
            var api = _httpContextAccessor.HttpContext.Session.GetString("ApiKey");
            var baseRepository = new BaseRepository(requirement.DataContext);
            try
            {
                var data = await baseRepository.GetUserFromApiKey(api, lang);
                addUser(context,data.UserId.ToString(),data.Cuscode,data.UserName,data.CuscodeId.ToString());
                return true;
            }
            catch (Exception exe)
            {
                var langError = requirement.DataContext.BaseLanguage.Where(n => n.DeleteFlag == false && n.Error == true);


                //   context.HttpContext.Response.Clear();
                return false;
            }
        }

        //public abstract bool Login(AuthorizationHandlerContext context, IAuthorizationRequirement requirement);

        //protected virtual bool ApiKey(string apiKey, AuthorizationHandlerContext context, IAuthorizationRequirement requirement)
        //{


        //    if (apiKey != null && apiKey != "")
        //    {
        //        using (var conn = GetConnection())
        //        {
        //            ApiKeyServices apiServices = new ApiKeyServices(conn);

        //            ApiKey user = (ApiKey)apiServices.GetAll(n => n.Key == apiKey && n.DeleteFlag == false).FirstOrDefault(true);


        //            if (user != null)
        //            {
        //                addUser(context, user.User);
        //                //  CacheServices.SaveCache(CacheServices.GetCache(apiKey), apiKey);
        //                context.Succeed(requirement);
        //                return true;
        //            }

        //        }
        //    }
        //    return false;
        //}
        public void addUser(AuthorizationHandlerContext context, string UserId,string Customer, string UserName, string CustomerId)
        {
            var c = new ClaimsIdentity();
            var cl = new Claim(ClaimTypes.Name, "User");


            cl.Properties.Add("userId", UserId);
            cl.Properties.Add("cusCode", Customer);
            cl.Properties.Add("userName", UserName);
            cl.Properties.Add("cusCodeId", CustomerId);

            c.AddClaim(cl);

            context.User.AddIdentity(c);
        }


    }
}

