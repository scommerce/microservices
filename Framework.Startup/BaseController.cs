﻿using Framework.BaseAttrib;
using Framework.BaseContext;
using Framework.BaseFramework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Transactions;

namespace Framework.Startup
{
    public class BaseController<baseDbContext> : ControllerBase where baseDbContext: BaseDbContext, new()
    {
        BaseDbContext dataContext;
        BaseRepository baseRepository;
        public BaseDbContext GetContext()
        {
            if (dataContext == null)
                dataContext = new baseDbContext();

           
            return dataContext;
        }

        public ExpandoObject GetUI(object UiClass,string theme= "default")
        {
            var c = UiClass.GetType();

            ExpandoObject extJs = null;
            foreach (var a in c.GetCustomAttributes(true).Where(n => n is ExtAttribute ).ToList())
            {
              
                if (a is ExtAttribute extAttr)
                {
                    if(extAttr.theme == theme || (extAttr.theme== "default" && extJs==null))
                      extJs = extAttr.SetObject(theme,extJs, c, UiClass);
                }
            }
            
            return extJs;
        }
        public JavaScriptResult CreateExtjs(string name,string ui)
        {
            var script = " Ext.onReady(function () {Ext.define(\"" + name + "\", "+ui+");";
            script+= " Ext.create('Ext.container.Viewport', {";
            script += "layout: 'center',";

            script += " items:[Ext.create(\""+name+"\")]";
            script += "});});";
            return new JavaScriptResult(script);
        }
        public BaseRepository GetRepo()
        {
            if (baseRepository == null)
                baseRepository = new BaseRepository(GetContext());

            return baseRepository;
        }

       

        public IQueryable<TEntity> GetAll<TEntity>(Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            using (var scope = new TransactionScope( TransactionScopeOption.Required,
                new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {
              

                return GetRepo().GetDataAll<TEntity>(where);
            }
        }

        public TEntity GetData<TEntity>(Expression<Func<TEntity, bool>> where = null) where TEntity : class, new()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions()
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
            {


                return GetRepo().GetData<TEntity>(where);
            }
        }
        public string GetHeaderLang()
        {
            return HttpContext.Request.Headers["lang"].ToString();
        }
        public void LoginSession(String value)
        {
            HttpContext.Session.SetString("ApiKey", value);
        }
        public string GetApiKey()
        {
            return HttpContext.Session.GetString("ApiKey");
        }
        public void LogoutSession()
        {
            HttpContext.Session.Remove("ApiKey");
        }
    }
}
