﻿
using Kpi.Entity;
using Kpi.Models;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace KpiImport1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private async void button1_Click(object sender, EventArgs e)
        {
            int error = 0;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@"D:\KPI\load1.xlsx");
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;
            List<Evaluation> liste = new List<Evaluation>();
            List<EvaluationImportModel> listee = new List<EvaluationImportModel>();
            for (int i = 1; i <= rowCount; i++)
            {
                try
                {

                    Console.WriteLine(i + "/" + rowCount);

                    //Console.WriteLine(xlRange.Cells[i, 2].Value2.ToString());
                    EmployeeImport ei = new EmployeeImport();
                    ei.Name = xlRange.Cells[i, 2].Value2.ToString();
                    ei.EmpCode = xlRange.Cells[i, 4].Value2.ToString();
                    ei.Name = ei.Name.Replace("/n", "").Replace("/r", "").Replace(" ", "").Trim();
                    ei.EmpCode = ei.EmpCode.Replace("/n", "").Replace("/r", "").Replace(" ", "").Trim();
                    await SaveEmpImport(ei, true);

                    //EmployeeFlowImport ei = new EmployeeFlowImport();
                    //ei.Name = xlRange.Cells[i, 5].Value2.ToString();
                    //ei.Owner = xlRange.Cells[i, 9].Value2.ToString();
                    //if (xlRange.Cells[i, 14] != null && xlRange.Cells[i, 14].Value2 != null)
                    //{
                    //    ei.F1 = xlRange.Cells[i, 14].Value2.ToString() ;
                    //    ei.W1 = xlRange.Cells[i, 15].Value2.ToString();
                    //}
                    //if (xlRange.Cells[i, 16] != null && xlRange.Cells[i, 16].Value2 != null)
                    //{
                    //    ei.F2 = xlRange.Cells[i, 16].Value2.ToString();
                    //    ei.W2 = xlRange.Cells[i, 17].Value2.ToString();
                    //}
                    //if (xlRange.Cells[i, 18] != null && xlRange.Cells[i, 18].Value2 != null)
                    //{
                    //    ei.F3 = xlRange.Cells[i, 18].Value2.ToString();
                    //    ei.W3 = xlRange.Cells[i, 19].Value2.ToString();
                    //}
                    //if (xlRange.Cells[i, 20] != null && xlRange.Cells[i, 20].Value2 != null)
                    //{
                    //    ei.F4 = xlRange.Cells[i, 20].Value2.ToString();
                    //    ei.W4 = xlRange.Cells[i, 21].Value2.ToString();
                    //}
                    //if (xlRange.Cells[i, 22] != null && xlRange.Cells[i, 22].Value2 != null)
                    //{
                    //    ei.F5 = xlRange.Cells[i, 22].Value2.ToString();
                    //    ei.W5 = xlRange.Cells[i, 23].Value2.ToString();
                    //}
                    //if (xlRange.Cells[i, 24] != null && xlRange.Cells[i, 24].Value2 != null)
                    //{
                    //    ei.F6 = xlRange.Cells[i, 24].Value2.ToString();
                    //    ei.W6 = xlRange.Cells[i, 25].Value2.ToString();
                    //}
                    //await SaveFlowImport(ei,true);


                    //    if (listee.Where(n => n.Name == xlRange.Cells[i, 5].Value2.ToString()).FirstOrDefault() == null) { 
                    //    EvaluationImportModel em = new EvaluationImportModel();
                    //    em.Name = xlRange.Cells[i, 5].Value2.ToString();
                    //    em.T1 = false;
                    //    em.T2 = false;
                    //    em.T3 = false;
                    //    em.T4 = false;
                    //    em.T5 = false;
                    //    em.T6 = false;
                    //    if (xlRange.Cells[i, 26] != null && xlRange.Cells[i, 26].Value2 != null)
                    //        em.T1 = true;
                    //    if (xlRange.Cells[i, 27] != null && xlRange.Cells[i, 27].Value2 != null)
                    //        em.T2 = true;
                    //    if (xlRange.Cells[i, 28] != null && xlRange.Cells[i, 28].Value2 != null)
                    //        em.T3 = true;
                    //    if (xlRange.Cells[i, 29] != null && xlRange.Cells[i, 29].Value2 != null)
                    //        em.T4 = true;
                    //    if (xlRange.Cells[i, 30] != null && xlRange.Cells[i, 30].Value2 != null)
                    //        em.T5 = true;

                    //    if (em.Name.Contains("พัสดุ"))
                    //    {
                    //        if (em.T5)
                    //        {
                    //            em.T5 = false;
                    //            em.T6 = true;
                    //        }
                    //    }
                    //    listee.Add(em);
                    //}

                    //   SaveEvaluationTemplateImport(em, true);
                    //if (liste.Where(n => n.EvaluationName == xlRange.Cells[i, 5].Value2.ToString()).FirstOrDefault() == null)
                    //{
                    //    Evaluation evaluation = new Evaluation();
                    //    evaluation.Year = "2020";
                    //    evaluation.No = "02";
                    //    evaluation.StartDate = new DateTime(2020, 09, 02);
                    //    evaluation.EndDate = new DateTime(2020, 09, 30);
                    //    evaluation.EvaluationName = xlRange.Cells[i, 5].Value2.ToString();
                    //    evaluation.RequestTimeSheet = true;
                    //    evaluation.TimeSheetStart = new DateTime(2019, 09, 1);
                    //    evaluation.TimeSheetEnd = new DateTime(2020, 08, 31);
                    //    liste.Add(evaluation);
                    //}

                    // Thread.Sleep(1000);
                    Console.WriteLine("Success");
                }
                catch(Exception ex) {
                    Console.WriteLine(error++);
                    Console.WriteLine(ex.Message);
                }

            }
           


            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //rule of thumb for releasing com objects:
            //  never use two dots, all COM objects must be referenced and released individually
            //  ex: [somthing].[something].[something] is bad

            //release com objects to fully kill excel process from running in the background
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);
    


            foreach (var l in liste)
            {
                AddEval(l, true);
           
                Thread.Sleep(1000);
                Console.WriteLine("Save Success");
            }
            foreach (var l in listee)
            {
                SaveEvaluationTemplateImport(l, true);

                Thread.Sleep(1000);
                Console.WriteLine("Save Success");
            }
           
            MessageBox.Show("Success");
        }
        public async Task AddEval( object obj, bool json)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://203.150.107.134:30008/api/EvaluationApi/SaveEvaluationImport");


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);

                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }

          



            await client.SendAsync(request);
            
        }

        public async Task SaveEvaluationTemplateImport(object obj, bool json)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5000/api/EvaluationApi/SaveEvaluationTemplateImport");


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);

                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }





            await client.SendAsync(request);
            Console.WriteLine("SaveFinish");

        }

        public async Task SaveEmpImport(object obj, bool json)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://203.150.107.134:30008/api/EvaluationApi/SaveEmpImport");


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);

                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }





            await client.SendAsync(request);
            Console.WriteLine("SaveFinish");

        }

        public async Task SaveFlowImport(object obj, bool json)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5000/api/EvaluationApi/SaveFlowImport");


            HttpClient client = new HttpClient();
            ByteArrayContent content;
            if (obj != null)
            {
                if (json)
                {

                    var newUserJson = JsonConvert.SerializeObject(obj);

                    content = new StringContent(newUserJson, Encoding.UTF8, "application/json");
                }
                else
                {
                    content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)obj);

                }

                request.Content = content;
            }





            await client.SendAsync(request);
            Console.WriteLine("SaveFinish");

        }
    }
}
