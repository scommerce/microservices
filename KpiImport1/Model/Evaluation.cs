﻿
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace Kpi.Entity
{

    public class Evaluation
    {
        public string Year { get; set; }
        public string No { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string EvaluationName { get; set; }
        public string EvaluationDetail { get; set; }
        public bool RequestTimeSheet { get; set; }
        public DateTime? TimeSheetStart { get; set; }
        public DateTime? TimeSheetEnd { get; set; }

        public int BeforeNoti { get; set; }
        public int ExpireNoti { get; set; }
        public bool AllowNull { get; set; }
        public bool SkipFirst { get; set; }
        public string RecommandWording { get; set; }


    }
}
